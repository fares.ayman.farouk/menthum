import 'package:patientapp/application/constants.dart';

///TODO:: Change it when deploy to store to production...
Environment environment = Environment.development;

///TODO:: Change App version When deploy to store...
const String appVersion = "2.0.0+48";

String getBaseURL() {
  switch (environment) {
    case Environment.development:
      return APIsConstants.baseUrlDebug;
    case Environment.testDev:
      return APIsConstants.baseUrlDebug;
    case Environment.production:
      return APIsConstants.baseUrlRelease;
    case Environment.testProd:
      return APIsConstants.baseUrlRelease;
  }
}

String getBaseURLNotVersion() {
  switch (environment) {
    case Environment.development:
      return APIsConstants.baseUrlNotVDebug;
    case Environment.testDev:
      return APIsConstants.baseUrlNotVDebug;
    case Environment.production:
      return APIsConstants.baseUrlNotVRelease;
    case Environment.testProd:
      return APIsConstants.baseUrlNotVRelease;
  }
}

String getPusherKey() {
  switch (environment) {
    case Environment.development:
      return PusherConstants.pusherKeyDebug;
    case Environment.testDev:
      return PusherConstants.pusherKeyDebug;
    case Environment.production:
      return PusherConstants.pusherKeyRelease;
    case Environment.testProd:
      return PusherConstants.pusherKeyRelease;
  }
}

bool useAnalyticsInReleaseMode = environment == Environment.production;

enum Environment {
  development,
  production,
  testProd,
  testDev,
}
