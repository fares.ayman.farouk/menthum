import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:patientapp/data/caching/cached_item.dart';
import 'package:patientapp/domain/models/digital_prescriptions_mapper.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';

extension MediaQuiryValues on BuildContext {
  double get height => MediaQuery.of(this).size.height;

  double get width => MediaQuery.of(this).size.width;
}

extension CachedItemExtension on CachedItem {
  bool isValid(int expirationTimeInMillis) {
    int currentTimeInMillis = DateTime.now().millisecondsSinceEpoch;
    bool isValid = currentTimeInMillis - cacheTime <= expirationTimeInMillis;
    return isValid;
  }

  ///TODO:::
}

extension RouteMedicineName on RouteMedicineEnum {
  String routeName() {
    if (name == RouteMedicineEnum.CREAM.name) {
      return AppStrings.cream.tr();
    }

    if (name == RouteMedicineEnum.DROPS_ORAL.name) {
      return AppStrings.dropsOral.tr();
    }

    if (name == RouteMedicineEnum.EAR_DROP.name) {
      return AppStrings.earDrops.tr();
    }

    if (name == RouteMedicineEnum.EYE_DROPS.name) {
      return AppStrings.eyeDrops.tr();
    }

    if (name == RouteMedicineEnum.INTRADERMAL.name) {
      return AppStrings.intradermal.tr();
    }
    if (name == RouteMedicineEnum.INTRAVENOUS.name) {
      return AppStrings.intravenous.tr();
    }

    if (name == RouteMedicineEnum.INTRAMUSCULAR.name) {
      return AppStrings.intramuscular.tr();
    }
    if (name == RouteMedicineEnum.METERED_DOSE_INHALER.name) {
      return AppStrings.meteredDoseInhaler.tr();
    }
    if (name == RouteMedicineEnum.INTRANASAL.name) {
      return AppStrings.intranasal.tr();
    }

    if (name == RouteMedicineEnum.ORAL.name) {
      return AppStrings.oral.tr();
    }
    if (name == RouteMedicineEnum.RECTAL.name) {
      return AppStrings.rectal.tr();
    }
    if (name == RouteMedicineEnum.SUBCUTANEAS.name) {
      return AppStrings.subcutaneous.tr();
    }
    if (name == RouteMedicineEnum.SUBLINGUAL.name) {
      return AppStrings.sublingual.tr();
    }

    if (name == RouteMedicineEnum.TOPICAL.name) {
      return AppStrings.topical.tr();
    }
    if (name == RouteMedicineEnum.VAGINEL.name) {
      return AppStrings.vaginel.tr();
    }
    if (name == RouteMedicineEnum.SPRAY.name) {
      return AppStrings.spray.tr();
    }
    if (name == RouteMedicineEnum.MOUTH_WASH.name) {
      return AppStrings.mouthWash.tr();
    } else {
      return " ";
    }
  }
}

extension ForIntervalName on ForIntervalEnum {
  String forIntervalName() {
    if (name == ForIntervalEnum.monthly.name) {
      return AppStrings.monthly.tr();
    }

    if (name == ForIntervalEnum.weekly.name) {
      return AppStrings.weekly.tr();
    }

    if (name == ForIntervalEnum.daily.name) {
      return AppStrings.daily.tr();
    } else {
      return " ";
    }
  }
}

extension FrequencyName on FrequencyEnum {
  String freqName() {
    if (name == FrequencyEnum.month.name) {
      return AppStrings.month.tr();
    }

    if (name == FrequencyEnum.week.name) {
      return AppStrings.week.tr();
    }

    if (name == FrequencyEnum.day.name) {
      return AppStrings.day.tr();
    } else {
      return " ";
    }
  }
}

/// string calculation

enum CalculationTypeEnum { bmi, basic_colorie_intake, pregnancy , menstrual_list}
extension CalculationName on String {
  String calcTypeName() {
    if (this == CalculationTypeEnum.bmi.name) {
      return AppStrings.bmi.tr();
    } else if (this == CalculationTypeEnum.basic_colorie_intake.name) {
      return AppStrings.basicColorieIntake.tr();
    } else if (this == CalculationTypeEnum.pregnancy.name) {
      return AppStrings.pregnancy.tr();
    }
    else if (this == CalculationTypeEnum.menstrual_list.name) {
      return AppStrings.menstrualCycle.tr();
    } else {
      return "";
    }
  }
}
