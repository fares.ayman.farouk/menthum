import 'dart:convert';
import 'dart:developer';

import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/material.dart';
import 'package:patientapp/data/models/apiResponse/home_response/gp_working_shifts_response.dart';
import 'package:patientapp/data/models/apiResponse/location_filter/city_response.dart';
import 'package:patientapp/data/models/apiResponse/location_filter/country_response.dart';
import 'package:patientapp/data/models/apiResponse/location_filter/district_response.dart';
import 'package:patientapp/data/models/apiResponse/location_filter/governorate_response.dart';
import 'package:patientapp/data/models/apiResponse/profile/medicalProfileApiResponse.dart';
import 'package:patientapp/data/models/apiResponse/tests_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../data/caching/local_data_source.dart';
import '../data/models/apiResponse/auth_response/login_api_response.dart';
import '../presentation/resources/language_manager.dart';
import 'di.dart';

const String PREFS_KEY_LANG = "PREFS_KEY_LANG";
const String PREFS_KEY_IS_First_Time = "PREFS_KEY_IS_First_Time";
const String PREFS_KEY_HAS_PARENT = "PREFS_KEY_HAS_PARENT";
const String PREFS_KEY_IS_LOGED = "PREFS_KEY_IS_LOGED";
const String PREFS_KEY_USER_TOKEN = "PREFS_KEY_USER_TOKEN";
const String PREFS_KEY_DEPENDENT_TOKEN = "PREFS_KEY_DEPENDENT_TOKEN";
const String USER_Data = "USER_Data";
const String Medical_Profile = "Medical_Profile";
const String Finish_Payment = "Finish_Payment";

///Locations
const String COUNTRIES_LIST = "COUNTRIES_LIST";
const String CITIES_LIST = "CITIES_LIST";
const String GOVERNORATES_LIST = "GOVERNORATES_LIST";
const String DISTRICTS_LIST = "DISTRICTS_LIST";
const String LIST_OF_RADS_TESTS = "LIST_OF_RADS_TESTS";
const String LIST_OF_LABS_TESTS = "LIST_OF_LABS_TESTS";
const String GP_WORKING_SHIFTS = "GP_WORKING_SHIFTS";
const String ADD_CALL_COST = "ADD_CALL_COST";

class AppPreferences {
  final SharedPreferences _sharedPreferences;

  AppPreferences(this._sharedPreferences);

  String getAppLanguage() {
    String? language = _sharedPreferences.getString(PREFS_KEY_LANG);
    if (language != null && language.isNotEmpty) {
      return language;
    } else {
      // return default lang
      return LanguageType.ENGLISH.getValue();
    }
  }

  Future<String> getLocalDeviceLange() async {
    String? language = _sharedPreferences.getString(PREFS_KEY_LANG);
    if (language != null && language.isNotEmpty) {
      return language;
    } else {
      String? locale = await Devicelocale.currentLocale;
      String lang = locale == null ? 'ar' : locale.split('-')[0];

      if (lang != 'ar' && lang != 'en') {
        lang = 'ar';
      }

      await _sharedPreferences.setString(PREFS_KEY_LANG, lang);
      return lang;
    }
  }

  Future<void> changeAppLanguage() async {
    String currentLang = await getAppLanguage();
    LocalDataSource _localDataSource = instance<LocalDataSource>();
    _localDataSource.clearCache();
    removeSavedData();
    if (currentLang == LanguageType.ARABIC.getValue()) {
      // set english
      _sharedPreferences.setString(
          PREFS_KEY_LANG, LanguageType.ENGLISH.getValue());
    } else {
      // set arabic
      _sharedPreferences.setString(
          PREFS_KEY_LANG, LanguageType.ARABIC.getValue());
    }
  }

  Future<Locale> getLocal() async {
    String currentLang = await getLocalDeviceLange();
    if (currentLang == LanguageType.ENGLISH.getValue()) {
      return ENGLISH_LOCAL;
    } else {
      return ARABIC_LOCAL;
    }
  }

  Future<bool> isFirstTimeOnBoarding() async {
    bool? isFirstTime = _sharedPreferences.getBool(PREFS_KEY_IS_First_Time);
    if (isFirstTime != null) {
      return isFirstTime;
    } else {
      return true;
    }
  }

  Future<void> addFirstTimeOnBoarding(bool firstTime) async {
    await _sharedPreferences.setBool(PREFS_KEY_IS_First_Time, firstTime);
  }

  Future<bool> hasParent() async {
    bool? has = _sharedPreferences.getBool(PREFS_KEY_HAS_PARENT);
    if (has != null) {
      return has;
    } else {
      return false;
    }
  }

  Future<void> addParent(bool has) async {
    await _sharedPreferences.setBool(PREFS_KEY_HAS_PARENT, has);
  }

  Future<bool> hasToken() async {
    String? token = await getParentUserToken();
    if (token != null && token.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> addUser(dynamic userData) async {
    String user = json.encode(userData);
    await _sharedPreferences.setString(USER_Data, user);
  }

  Future<void> addMedicalProfile(dynamic medicalProfile) async {
    String user = json.encode(medicalProfile);
    await _sharedPreferences.setString(Medical_Profile, user);
    log("Medical Profile added successfully");
  }

  UserData? getUser() {
    String? user = _sharedPreferences.getString(USER_Data);

    if (user == null) {
      return null;
    } else {
      UserData userData = UserData.fromJson(jsonDecode(user.toString()));

      return userData;
    }
  }

  Data? getMedicalProfile() {
    String? medical = _sharedPreferences.getString(Medical_Profile);
    log("Medical Profile saved => ${medical != null}");

    if (medical == null) {
      return null;
    } else {
      Data medicalProfile = Data.fromJson(jsonDecode(medical.toString()));

      return medicalProfile;
    }
  }

  Future<void> addTriggerForFinishPayment(bool isFinished) async {
    await _sharedPreferences.setBool(Finish_Payment, isFinished);
  }

  bool? getTriggerForFinishPayment() {
    return _sharedPreferences.getBool(Finish_Payment);
  }

  String getUserId() {
    AppPreferences app = instance();
    UserData? user = app.getUser();

    return user?.id ?? "";
  }

  Future<void> addTemporaryToken(String token) async {
    removeTemporary();
    await _sharedPreferences.setString(PREFS_KEY_DEPENDENT_TOKEN, token);
  }

  Future<String?> getTemporaryToken() async {
    String? token = _sharedPreferences.getString(PREFS_KEY_DEPENDENT_TOKEN);

    if (token != null) {
      return token;
    } else {
      return null;
    }
  }

  Future<void> removeTemporary() async {
    await _sharedPreferences.setString(PREFS_KEY_DEPENDENT_TOKEN, "");
  }

  Future<void> addParentUserToken(String token) async {
    await _sharedPreferences.setString(PREFS_KEY_USER_TOKEN, token);
  }

  Future<String?> getParentUserToken() async {
    String? token = _sharedPreferences.getString(PREFS_KEY_USER_TOKEN);

    if (token != null) {
      return token;
    } else {
      return null;
    }
  }

  Future<void> addCallCost(int callCost) async {
    await _sharedPreferences.setInt(ADD_CALL_COST, callCost);
    log("add call cost to shared preference $callCost");
  }

  Future<int?> getCallCost() async {
    int? callCost = _sharedPreferences.getInt(ADD_CALL_COST);
    log("get call cost to shared preference $callCost");

    if (callCost != null) {
      return callCost;
    } else {
      return null;
    }
  }

  Future<void> removeUser() async {
    await _sharedPreferences.remove(USER_Data);
    await _sharedPreferences.remove(Medical_Profile);
    await _sharedPreferences.remove(PREFS_KEY_USER_TOKEN);
    await _sharedPreferences.remove(PREFS_KEY_DEPENDENT_TOKEN);
    await _sharedPreferences.remove(PREFS_KEY_HAS_PARENT);
  }

  Future<void> removeSavedData() async {
    log("@@@@Remove");
    await _sharedPreferences.remove(COUNTRIES_LIST);
    await _sharedPreferences.remove(GOVERNORATES_LIST);
    await _sharedPreferences.remove(CITIES_LIST);
    await _sharedPreferences.remove(DISTRICTS_LIST);
    await _sharedPreferences.remove(LIST_OF_RADS_TESTS);
    await _sharedPreferences.remove(LIST_OF_LABS_TESTS);
  }

  /// Location of search....

  Future<void> addCountries(List<Country> countries) async {
    String countriesList = json.encode(countries);
    await _sharedPreferences.setString(COUNTRIES_LIST, countriesList);
  }

  Future<List<Country>?> getCountries() async {
    String? countriesList = _sharedPreferences.getString(COUNTRIES_LIST);
    return countriesList == null
        ? null
        : CountryResponse.fromJsonForSharedPref(
                jsonDecode(countriesList.toString()))
            .countries;
  }

  Future<void> addGPWorkingShifts(List<GPWorkingShifts> gpWorkingShifts) async {
    String gpWorkingShiftsList = json.encode(gpWorkingShifts);
    await _sharedPreferences.setString(GP_WORKING_SHIFTS, gpWorkingShiftsList);
  }

  Future<List<GPWorkingShifts>?> getGPWorkingShifts() async {
    String? gpWorkingShiftsList =
        _sharedPreferences.getString(GP_WORKING_SHIFTS);
    return gpWorkingShiftsList == null
        ? null
        : GPWorkingShiftResponse.fromJsonForSharedPref(
                jsonDecode(gpWorkingShiftsList.toString()))
            .gpWorkingShifts;
  }

  Future<void> addGovernorates(List<Governorate> governorates) async {
    String gList = json.encode(governorates);
    await _sharedPreferences.setString(GOVERNORATES_LIST, gList);
  }

  Future<List<Governorate>?> getGovernorates() async {
    print("////////////////////////////////");
    String? gList = _sharedPreferences.getString(GOVERNORATES_LIST);
    return gList == null
        ? null
        : GovernorateResponse.fromJsonForSharedPref(
                jsonDecode(gList.toString()))
            .data;
  }

  Future<void> addCities(List<City> cities) async {
    String citiesList = json.encode(cities);
    await _sharedPreferences.setString(CITIES_LIST, citiesList);
  }

  Future<List<City>?> getCities() async {
    String? citiesList = _sharedPreferences.getString(CITIES_LIST);
    return citiesList == null
        ? null
        : CityResponse.fromJsonForSharedPref(jsonDecode(citiesList)).data;
  }

  Future<void> addTests(
      {required List<Test> tests, required String key}) async {
    String testsList = json.encode(tests);
    await _sharedPreferences.setString(key, testsList);
  }

  Future<List<Test>?> getTests({required String key}) async {
    String? testsList = _sharedPreferences.getString(key);
    return testsList == null
        ? null
        : TestsResponse.fromJsonForSharedPref(jsonDecode(testsList)).data;
  }

  Future<void> addDistrict(List<District> districts) async {
    String dList = json.encode(districts);
    await _sharedPreferences.setString(DISTRICTS_LIST, dList);
  }

  Future<List<District>?> getDistrict() async {
    String? dList = _sharedPreferences.getString(DISTRICTS_LIST);
    return dList == null
        ? null
        : DistrictResponse.fromJsonForSharedPref(jsonDecode(dList.toString()))
            .districts;
  }

  List<Subscription> getUserSubscription() {
    UserData? userData = getUser();

    if (userData == null) {
      return [];
    } else {
      return userData.subscriptions;
    }
  }
}
