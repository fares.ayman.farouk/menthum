import 'package:dio/dio.dart';
import 'package:facebook_app_events/facebook_app_events.dart';
import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/data/caching/local_data_source.dart';
import 'package:patientapp/data/caching/local_data_source_imp.dart';
import 'package:patientapp/data/network/network_info.dart';
import 'package:patientapp/data/network/web_services/calculation_web_services.dart';
import 'package:patientapp/data/network/web_services/consultations_web_services.dart';
import 'package:patientapp/data/network/web_services/home_web_services.dart';
import 'package:patientapp/data/network/web_services/menstrual_web_services.dart';
import 'package:patientapp/data/network/web_services/my_records_web_services.dart';
import 'package:patientapp/data/network/web_services/notifications_web_services.dart';
import 'package:patientapp/data/network/web_services/programs/pediatric_web_service.dart';
import 'package:patientapp/data/network/web_services/questions_web_services.dart';
import 'package:patientapp/data/network/web_services/rads_web_services.dart';
import 'package:patientapp/data/network/web_services/vital_web_services.dart';
import 'package:patientapp/data/repository/calculator_repostory_imp.dart';
import 'package:patientapp/data/repository/consultations_repositories_imp.dart';
import 'package:patientapp/data/repository/home_repository_impl.dart';
import 'package:patientapp/data/repository/menstrual_repository_imp.dart';
import 'package:patientapp/data/repository/my_records_repositories_imp.dart';
import 'package:patientapp/data/repository/programs/pediatric_repository_imp.dart';
import 'package:patientapp/data/repository/question_repositories_imp.dart';
import 'package:patientapp/data/repository/questionnaire_repository_imp.dart';
import 'package:patientapp/data/repository/rads_repository_imp.dart';
import 'package:patientapp/data/repository/vital_repository_imp.dart';
import 'package:patientapp/domain/business_logic/application_life_cycle/application_life_cycle_cubit.dart';
import 'package:patientapp/domain/business_logic/calculation/calories/calories_bloc.dart';
import 'package:patientapp/domain/business_logic/gp_working_shifts/gp_working_shifts_bloc.dart';
import 'package:patientapp/domain/business_logic/permission/permission_cubit.dart';
import 'package:patientapp/domain/business_logic/questionnaire/questionnaires/questionnaires_bloc.dart';
import 'package:patientapp/domain/business_logic/questionnaire/questions/questionnaire_questions_bloc.dart';
import 'package:patientapp/domain/business_logic/test_video/test_video_cubit.dart';
import 'package:patientapp/domain/business_logic/waiting_in_queue/waiting_in_queue_cubit.dart';
import 'package:patientapp/domain/repositories/calculation_repository.dart';
import 'package:patientapp/domain/repositories/programs/pediatric_repository.dart';
import 'package:patientapp/domain/repositories/questionnaire_repository.dart';
import '../data/network/web_services/questionnaire_web_services.dart';
import '../domain/business_logic/calculation/bmi/bmi_bloc.dart';
import '../domain/business_logic/calculation/calculation_types/calculation_types_bloc.dart';
import '../domain/business_logic/calculation/pregnancy/pregnancy_bloc.dart';
import '../domain/business_logic/dependent_account_bloc/dependent_account_bloc.dart';
import '../domain/business_logic/programs_blocs/diabetes/diabetes_bloc.dart';
import '../domain/business_logic/programs_blocs/pediatric/pediatric_bloc.dart';
import '../domain/business_logic/programs_blocs/programs/programs_bloc.dart';
import '../domain/business_logic/vitals/add_vital/add_vital_bloc.dart';
import '../domain/business_logic/vitals/vital_list_bloc/vital_bloc.dart';
import 'package:patientapp/domain/repositories/vital_repository.dart';
import 'package:patientapp/domain/services/dynamic_links/dynamic_links.dart';
import 'package:patientapp/domain/services/pusher_services/pusher_services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../data/local_data_source/database_helper.dart';
import '../data/network/dio_factory.dart';
import '../data/network/web_services/blog_web_services.dart';
import '../data/network/web_services/programs/diabetes_web_services.dart';
import '../data/network/web_services/laboratory_web_services.dart';
import '../data/network/web_services/login_web_services.dart';
import '../data/network/web_services/programs_web_service.dart';
import '../data/repository/auth_repository_imp.dart';
import '../data/repository/blog_repositories_imp.dart';
import '../data/repository/laboratory_repository_imp.dart';
import '../data/repository/notification_repository_imp.dart';
import '../data/repository/programs/diabetes_repository_imp.dart';
import '../data/repository/programs/programs_repository_imp.dart';
import '../data/servicecs/permission/permission_service_imp.dart';
import '../domain/business_logic/menstrual_cycle/menstrual_cycle_bloc.dart';
import '../domain/business_logic/menstrual_list/menstrual_list_bloc.dart';
import '../domain/business_logic/update_patient_profile/update_patient_profile_bloc.dart';
import '../domain/business_logic/vitals/vital_details/vital_details_bloc.dart';
import '../domain/repositories/auth_repositories.dart';
import '../domain/repositories/programs/diabetes_repository.dart';
import '../domain/repositories/programs/programs_repository.dart';
import '../domain/services/analytics_services/analytics_services.dart';
import '../domain/services/permission/i_permission.dart';

final instance = GetIt.instance;

Future<void> initAppModule() async {
  //app module , its module where we but all generic dependencies

  //shared prefs instance
  final sharedPrefs = await SharedPreferences.getInstance();
  instance.registerLazySingleton<SharedPreferences>(() => sharedPrefs);

  //app prefs instance
  instance
      .registerLazySingleton<AppPreferences>(() => AppPreferences(instance()));

  //network info
  instance.registerLazySingleton<NetWorkInfo>(
      () => NetWorkInfoImpl(InternetConnectionChecker()));

  //dio factory
  instance.registerLazySingleton<DioFactory>(() => DioFactory(instance()));

  Dio dio = await instance<DioFactory>().getDio();

  ///Local Data Source.
  //app services client
  instance.registerLazySingleton<LocalDataSource>(() => LocalDataSourceImp());

  //app services client
  instance.registerLazySingleton<AuthWebServices>(
      () => AuthWebServices(dio, instance()));

  ///Consultations
  //app services client
  instance.registerLazySingleton<ConsultationsWebServices>(
      () => ConsultationsWebServices(dio, instance()));

  // remote data source
  instance.registerLazySingleton<AuthRepositoryImp>(
      () => AuthRepositoryImp(instance(), instance(), instance()));

  // remote data source
  instance.registerLazySingleton<AuthRepository>(
      () => AuthRepositoryImp(instance(), instance(), instance()));

// remote data source
  instance.registerLazySingleton<ConsultationsRepositoriesImp>(() =>
      ConsultationsRepositoriesImp(
          instance(), instance(), instance(), instance()));

  /// Dependent Account
  // Dependent Account Bloc
  instance.registerFactory<DependentAccountBloc>(
      () => DependentAccountBloc(instance()));

  ///questions
  //app services client
  instance.registerLazySingleton<QuestionWebServices>(
      () => QuestionWebServices(dio, instance()));

  ///Local Data Source
  instance.registerLazySingleton<DatabaseDataSource>(
      () => DatabaseDataSource.instance);

  /// remote data source
  instance.registerLazySingleton<QuestionRepositoriesImp>(
      () => QuestionRepositoriesImp(
            instance(),
            instance(),
            instance(),
          ));

  ///Laboratory
  //app services client
  instance.registerLazySingleton<LaboratoryWebServices>(
      () => LaboratoryWebServices(dio, instance()));

// remote data source
  instance.registerLazySingleton<LaboratoryRepositoryImp>(
      () => LaboratoryRepositoryImp(instance(), instance(), instance()));

  // remote data source for rads
  instance.registerLazySingleton<RadsRepositoryImp>(
      () => RadsRepositoryImp(instance(), instance(), instance()));

  ///rads web services
  //app services client
  instance.registerLazySingleton<RadsWebServices>(
      () => RadsWebServices(dio, instance()));

  ///  MyRecords
  instance.registerLazySingleton<MyRecorderRepositoryImp>(
      () => MyRecorderRepositoryImp(instance(), instance(), instance()));

  instance.registerLazySingleton<MyRecordsWebServices>(
      () => MyRecordsWebServices(dio, instance()));

  ///notifications
  //app services client
  instance.registerLazySingleton<NotificationWebServices>(
      () => NotificationWebServices(dio, instance()));
// remote data source
  instance.registerLazySingleton<NotificationRepositoriesImp>(() =>
      NotificationRepositoriesImp(
          instance(), instance(), instance(), instance()));

  instance.registerLazySingleton<AnalyticsServices>(() => AnalyticsServices());

  /// facebook in app events
  instance.registerLazySingleton<FacebookAppEvents>(() => FacebookAppEvents());

  ///Home Web Services
  instance.registerLazySingleton<HomeWebServices>(
      () => HomeWebServices(dio, instance()));

  ///Home Repository
  instance.registerLazySingleton<HomeRepositoryImpl>(() => HomeRepositoryImpl(
        instance(),
        instance(),
        instance(),
      ));

  ///Menstrual  Services
  instance.registerLazySingleton<MenstrualWebServices>(
      () => MenstrualWebServices(dio, instance()));

  ///Menstrual Repository
  instance.registerLazySingleton<MenstrualRepositoryImp>(
      () => MenstrualRepositoryImp(
            instance(),
            instance(),
            instance(),
          ));

  ///Menstrual Bloc
  instance.registerFactory<MenstrualListBloc>(() => MenstrualListBloc());

  ///GP Working Shifts Bloc
  instance.registerFactory<GpWorkingShiftsBloc>(
      () => GpWorkingShiftsBloc(instance<HomeRepositoryImpl>()));

  instance.registerLazySingleton<ApplicationLifeCycleCubit>(
      () => ApplicationLifeCycleCubit());

  instance
      .registerLazySingleton<IPermissionService>(() => PermissionServiceImp());

  instance.registerLazySingleton<PermissionCubit>(
    () => PermissionCubit(
      instance<NetWorkInfo>(),
      instance<ApplicationLifeCycleCubit>(),
      instance<IPermissionService>(),
    ),
  );

  instance.registerLazySingleton<TestVideoCubit>(() => TestVideoCubit(
      applicationLifeCycle: instance<ApplicationLifeCycleCubit>()));

  instance.registerFactory<WaitingInQueueCubit>(() => WaitingInQueueCubit(
      questionRepositories: instance<QuestionRepositoriesImp>()));

  /// pusher
  instance.registerLazySingleton<PusherServices>(() => PusherServices());

  ///blogs
  //app services client
  instance.registerLazySingleton<BlogsWebServices>(
      () => BlogsWebServices(dio, instance()));

// remote data source
  instance.registerLazySingleton<BlogsRepositoryImp>(
      () => BlogsRepositoryImp(instance(), instance()));

  ///Update Patient Profile Bloc
  instance.registerFactory<UpdatePatientProfileBloc>(
      () => UpdatePatientProfileBloc(instance()));

  ///Update Patient Profile Bloc
  instance.registerFactory<MenstrualCycleBloc>(() => MenstrualCycleBloc());

  /// Vital

  //VitalWebServices
  instance.registerLazySingleton<VitalWebServices>(
      () => VitalWebServices(dio, instance()));
  // VitalRepository
  instance.registerLazySingleton<VitalRepository>(
      () => VitalRepositoryImp(instance(), instance(), instance()));
  //Add Vital Bloc
  instance.registerLazySingleton<AddVitalBloc>(() => AddVitalBloc(instance()));
  //VitalBloc
  instance.registerFactory<VitalBloc>(() => VitalBloc(instance(), instance()));
  //VitalDetailsBloc
  instance
      .registerFactory<VitalDetailsBloc>(() => VitalDetailsBloc(instance()));

  /// calculation
  //Calculation Web Services
  instance.registerLazySingleton<CalculationWebServices>(
      () => CalculationWebServices(dio, instance()));
  // Calculation Repository
  instance.registerLazySingleton<CalculationRepository>(
      () => CalculationRepositoryImp(instance(), instance(), instance()));
  //Calculation Types Bloc
  instance.registerFactory<CalculationTypesBloc>(
      () => CalculationTypesBloc(instance()));
  //Bmi Bloc
  instance.registerFactory<BmiBloc>(() => BmiBloc(instance()));

  //Calories Bloc
  instance.registerFactory<CaloriesBloc>(() => CaloriesBloc(instance()));

  //Pregnancy  Bloc
  instance.registerFactory<PregnancyBloc>(() => PregnancyBloc(instance()));

  /// Programs
  //programs Webservice
  instance.registerLazySingleton<ProgramsWebService>(
      () => ProgramsWebService(dio, instance()));
  // Programs Repository
  instance.registerLazySingleton<ProgramsRepository>(
      () => ProgramsRepositoryImp(instance(), instance()));
  //Programs Bloc
  instance.registerFactory<ProgramsBloc>(() => ProgramsBloc(instance()));

  /// diabetes
  // diabetes Webservice
  instance.registerLazySingleton<DiabetesWebService>(
      () => DiabetesWebService(dio, instance()));
  // diabetes Repository
  instance.registerLazySingleton<DiabetesRepository>(
      () => DiabetesRepositoryImp(instance(), instance()));
  // diabetes Bloc
  instance.registerFactory<DiabetesBloc>(() => DiabetesBloc(instance()));

  /// Pediatric
  // Pediatric Webservice
  instance.registerLazySingleton<PediatricWebService>(
      () => PediatricWebService(dio, instance()));
  // Pediatric Repository
  instance.registerLazySingleton<PediatricRepository>(
      () => PediatricRepositoryImp(instance(), instance()));
  // Pediatric Bloc
  instance.registerFactory<PediatricBloc>(() => PediatricBloc(instance()));

  /// questionnaire
  //questionnaire Web Services
  instance.registerLazySingleton<QuestionnaireWebServices>(
      () => QuestionnaireWebServices(dio, instance()));
  // questionnaire Repository
  instance.registerLazySingleton<QuestionnaireRepository>(
      () => QuestionnaireRepositoryImp(instance(), instance(), instance()));
  //questionnaires  Bloc
  instance.registerFactory<QuestionnairesBloc>(
      () => QuestionnairesBloc(instance()));
  //Questionnaire Questions Bloc
  instance.registerFactory<QuestionnaireQuestionsBloc>(
      () => QuestionnaireQuestionsBloc(instance()));

  ///Dynamic Link Service
  instance
      .registerLazySingleton<DynamicLinkService>(() => DynamicLinkService());
}
