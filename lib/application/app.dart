import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/domain/business_logic/permission/permission_cubit.dart';

import '../domain/business_logic/main_screen_navigator/main_screen_navigator_cubit.dart';
import '../domain/services/analytics_services/analytics_services.dart';
import '../main.dart';
import '../presentation/resources/routes_manger.dart';
import '../presentation/resources/theme_manger.dart';
import 'app_preference.dart';

class MyApp extends StatefulWidget {
  const MyApp._internal();

  static final MyApp _instance = MyApp._internal(); // single instance

  factory MyApp() => _instance;

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final AppPreferences _appPreferences = instance<AppPreferences>();
  final AnalyticsServices _analyticsServices = instance<AnalyticsServices>();

  @override
  void didChangeDependencies() {
    _appPreferences.getLocal().then((local) => {context.setLocale(local)});
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white,
        statusBarIconBrightness: Brightness.dark));

    return BlocProvider(
      create: (context) => instance<PermissionCubit>(),
      child: MultiBlocProvider(
          providers: [
            BlocProvider<MainScreenNavigatorCubit>(
              create: (context) => MainScreenNavigatorCubit(),
            )
          ],
          child: MaterialApp(
            navigatorKey: navigatorKey,
            navigatorObservers: [
              _analyticsServices.getAnalyticsObserver(),
            ],
            localizationsDelegates: context.localizationDelegates,
            supportedLocales: context.supportedLocales,
            locale: context.locale,
            debugShowCheckedModeBanner: false,
            onGenerateRoute: RouteGenerator.getRoute,
            initialRoute: Routes.splashRoute,
            theme: getApplicationTheme(),
            title: 'tikshif',
          )),
    );
  }
}
