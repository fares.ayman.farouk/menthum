import 'dart:developer';
import 'dart:io';
import 'dart:math' as math;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:patientapp/application/constants.dart';
import 'package:patientapp/data/caching/local_data_source.dart';
import 'package:patientapp/data/models/apiResponse/profile/medicalProfileApiResponse.dart';
import 'package:patientapp/data/network/error_handler.dart';
import 'package:patientapp/data/repository/auth_repository_imp.dart';
import 'package:patientapp/domain/services/analytics_services/analytics_services.dart';
import 'package:patientapp/presentation/resources/assets_manager.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';
import 'package:patientapp/presentation/resources/routes_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import 'package:url_launcher/url_launcher_string.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import '../data/models/apiRequest/auth_requests/verify_request.dart';
import '../data/models/apiResponse/auth_response/login_api_response.dart';
import '../data/network/failure.dart';
import '../domain/services/push_notification/firebase_notification.dart';
import '../main.dart';
import '../presentation/resources/strings_manger.dart';
import 'app_preference.dart';
import 'di.dart';

final AppPreferences appPreferences = instance<AppPreferences>();

String concatenateListOfStrings(List<String> listOfString) {
  var concatenate = StringBuffer();

  if (listOfString == null || listOfString.isEmpty) return "";

  listOfString.forEach((item) {
    concatenate.write(item);
    concatenate.write('\n');
  });

  return concatenate.toString();
}

String timeStampToDateEDMY(int timeStamp) {
  String lang = appPreferences.getAppLanguage();

  DateTime date = DateTime.fromMillisecondsSinceEpoch(timeStamp * 1000);
  return DateFormat('EEE, d MMM y', lang).format(date);
}

String dateTimeToDateEDMY(DateTime date) {
  String lang = appPreferences.getAppLanguage();

  return DateFormat('EEE, d MMM', lang).format(date);
}

String dateTimeToDateMMM(DateTime date) {
  String lang = appPreferences.getAppLanguage();

  return DateFormat('MMM', lang).format(date);
}

String dateTimeToDateD(DateTime date) {
  String lang = appPreferences.getAppLanguage();

  return DateFormat('d', lang).format(date);
}

String dateTimeToDateEEE(DateTime date) {
  String lang = appPreferences.getAppLanguage();

  return DateFormat('EEE', lang).format(date);
}

String dateTimeToDateEDMYY(DateTime date) {
  String lang = appPreferences.getAppLanguage();

  return DateFormat('yyyy-MM-dd', lang).format(date);
}

String dateTimeToDateEDMYYWithoutLang(DateTime date) {
  return DateFormat('yyyy-MM-dd','en').format(date);
}

DateTime stringToDate(String date) {
  return DateTime.parse(date);
}

DateTime getNowTimeWithZone() {
  tz.initializeTimeZones();

  final DateTime now = DateTime.now();
  final pacificTimeZone = tz.getLocation('Africa/Cairo');

  return tz.TZDateTime.from(now, pacificTimeZone);
}


String getDateYMDHM(DateTime dateTime) {
  /*String date = "",
      year,
      month,
      day,
      hour,
      minute;

  year = dateTime.year.toString();
  month = dateTime.month < 10
      ? "0${dateTime.month.toString()}"
      : dateTime.month.toString();
  day = dateTime.day < 10
      ? "0${dateTime.day.toString()}"
      : dateTime.day.toString();
  hour = dateTime.hour < 10
      ? "0${dateTime.hour.toString()}"
      : dateTime.hour.toString();
  minute = dateTime.minute < 10
      ? "0${dateTime.minute.toString()}"
      : dateTime.minute.toString();


  date = "$year-$month-$day $hour:$minute";*/
  String lang = appPreferences.getAppLanguage();
  return DateFormat("yyyy-MM-dd hh:mm a", lang).format(dateTime);
}

DateTime getDateTimeWithZone(DateTime dt) {
  tz.initializeTimeZones();

  final pacificTimeZone = tz.getLocation('Africa/Cairo');

  return tz.TZDateTime.from(dt, pacificTimeZone);
}

int convertDateTimeToTimestamp(DateTime dt) {
  int myTimeStamp =
      (Timestamp.fromDate(dt).microsecondsSinceEpoch / (1000 * 1000)).toInt();
  return myTimeStamp;
}

String timeStampToDateFormatYMD(int timeStamp) {
  DateTime date = DateTime.fromMillisecondsSinceEpoch(timeStamp * 1000);

  return DateFormat("yyyy-MM-dd").format(date);
}

DateTime timeStampToDateTime(int timeStamp) {
  return DateTime.fromMillisecondsSinceEpoch(timeStamp * 1000);
}

DateTime getCurrentDayWithoutHMS() =>
    DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);

int getTimeFromTimeStamp(int timeStamp) {
  DateTime date = DateTime.fromMillisecondsSinceEpoch(timeStamp * 1000);
  int hour = date.hour;

  return hour == 0 ? 24 : hour;
}

String DateFormat_YMD_HMS(DateTime dt) {
  return DateFormat("yyyy-MM-dd hh:mm:ss").format(getDateTimeWithZone(dt));
}

String timeStampToTimeWorkingShifts(int timeStamp) {
  DateTime date = DateTime.fromMillisecondsSinceEpoch(timeStamp * 1000);
  String lang = appPreferences.getAppLanguage();

  return DateFormat("hh:mm a", lang).format(date);
}

String timeStampToTime(int timeFrom, int timeTo) {
  DateTime from = DateTime.fromMillisecondsSinceEpoch(timeFrom * 1000);
  DateTime to = DateTime.fromMillisecondsSinceEpoch(timeTo * 1000);
  String lang = appPreferences.getAppLanguage();

  return "${DateFormat('h:mm a', lang).format(from)} - ${DateFormat('h:mm a', lang).format(to)}";
}

getThumbnailFile() async {
  final byteData = await rootBundle.load(VideoAssets.tikshifIntroVideo);
  Directory tempDir = await getTemporaryDirectory();

  File tempVideo = File("${tempDir.path}/${VideoAssets.tikshifIntroVideo}")
    ..createSync(recursive: true)
    ..writeAsBytesSync(byteData.buffer
        .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

  final file = await VideoThumbnail.thumbnailFile(
    video: tempVideo.path,
    thumbnailPath: (await getTemporaryDirectory()).path,
    imageFormat: ImageFormat.PNG,
    quality: 100,
  );
  print("///////////////// thampanal $file");

  return file;
}

String getBloodType(int type) {
  String value = '';
  switch (type) {
    case 1:
      value = 'A+';
      break;
    case 2:
      value = 'A-';
      break;
    case 3:
      value = 'B+';
      break;
    case 4:
      value = 'B-';
      break;
    case 5:
      value = 'O+';
      break;
    case 6:
      value = 'O-';
      break;
    case 7:
      value = 'AB+';
      break;
    case 8:
      value = 'AB-';
      break;
  }
  return value;
}

enum RechargeValues { Re50, Re100, Re200, Re500, Re1000 }

int getSelectedAmount(String type) {
  int value = 0;
  switch (type) {
    case "Re50":
      value = 50;
      break;
    case "Re100":
      value = 100;
      break;
    case "Re200":
      value = 200;
      break;
    case "Re500":
      value = 500;
      break;
    case "Re1000":
      value = 1000;
      break;
  }
  return value;
}

Future<void> openGoogleMap({required double lat, required double lng}) async {
  String googleUrl =
      "https://www.google.com/maps/search/?api=1&query=$lat,$lng";
  // if (await canLaunchUrlString(googleUrl)) {
  await launchUrlString(googleUrl);
  // } else {
  //   throw "could not open the Map";
  // }
}

Future<File?> downloadFile(String url, String name) async {
  final appStorage = await getApplicationDocumentsDirectory();
  final AppPreferences _appPreferences = instance<AppPreferences>();

  String? token = await _appPreferences.getParentUserToken();

  print(" Start");
  print("path:: ${appStorage.path}");
  try {
    final response = await Dio().get(
      url,
      options: Options(
          responseType: ResponseType.bytes,
          followRedirects: false,
          receiveTimeout: 0,
          headers: {"Authorization": "Bearer $token"}),
    );

    print("Type : ${response.headers["Content-Type"]}");

    String? extention = response.headers["Content-Type"]
        .toString()
        .split("/")
        .last
        .replaceAll("]", "");

    print("Exten: $extention");

    final file = File('${appStorage.path}/$name.$extention');
    final raf = file.openSync(mode: FileMode.write);
    raf.writeFromSync(response.data);
    await raf.close();

    return file;
  } catch (eror) {
    print("Erro $eror");

    return null;
  }
}

Future<void> getUserProfileData({String? token}) async {
  ///get trigger
  bool finishButtonPressed =
      instance<AppPreferences>().getTriggerForFinishPayment() ?? false;
  int wallet = 0;
  log("Payment Event: trigger : $finishButtonPressed - wallet: $wallet");

  ///get previous wallet if is finish button pressed for payment
  if (finishButtonPressed) {
    wallet = instance<AppPreferences>().getUser()?.wallet ?? 0;
    log("Payment Event: get wallet value $wallet");
  }

  AuthRepositoryImp repository = instance<AuthRepositoryImp>();
  Either<Failure, LoginResponse> response =
      await repository.getUserProfileData(token: token);
  response.fold((failure) {}, (loginResponse) async {
    if (finishButtonPressed && (loginResponse.data!.wallet! > wallet)) {
      ///send event and change the finish button pressed to false.
      log("Payment Event: send event and change the finish button pressed to false");
      instance<AnalyticsServices>().addEventAnalytics(
        eventName: EventsAnalytics.finishFawrySuccessfully,
        parameterName: EventsAnalytics.patientFinishFawrySuccessfully,
      );
      instance<AppPreferences>().addTriggerForFinishPayment(false);
      log("Payment Event: patient didn't pay, change the finish button pressed to false");
    } else {
      instance<AppPreferences>().addTriggerForFinishPayment(false);
      log("Payment Event: patient didn't pay, change the finish button pressed to false");
    }
  });
}

Future<void> getMedicalProfileData({String? token}) async {
  AuthRepositoryImp repository = instance<AuthRepositoryImp>();
  Either<Failure, MedicalProfileApiResponse> response =
      await repository.getMedicalProfile();
  response.fold((failure) {}, (medicalProfile) async {});
}

Future<FilePickerResult?> getLocalFile(List<String> extentions) async {
  FilePickerResult? result = await FilePicker.platform.pickFiles(
    type: FileType.custom,
    allowedExtensions: extentions,
  );
  return result;
}

Future<void> clearUserData() async {
  try {
    AppPreferences _appPreferences = instance<AppPreferences>();
    LocalDataSource _localDataSource = instance<LocalDataSource>();

    ///Remove the app Preference and token.
    await _appPreferences.removeUser();

    ///Remove the run Time cache
    _localDataSource.clearCache();

    /// delete device token
    deleteDeviceToken();
  } catch (e) {
    print("********${e.toString()}");
  }
}

String checkMobileNumber(String? text) {
  String mobile = "";
  if (text != null && text.isNotEmpty) {
    if (text.substring(0, 1) == "0") {
      mobile = text.substring(1, text.length);
    } else {
      mobile = text;
    }
  }

  return mobile;
}

///For handle The API errors like the internet connection error and the user unauthorized.
Future<void> handleApiError(
    {required BuildContext context, required Failure failure}) async {
  try {
    /// handling the toast.
    if (failure.code == ResponseCode.NO_INTERNET_CONNECTION) {
      showToast(message: AppStrings.noInternetConnection.tr(), success: false);
    } else {
      showToast(
          message: failure.message ?? AppStrings.noInternetConnection.tr(),
          success: false);

      log("NOTTTTTTTTTTTTTTTTT ${failure.code}");

      if (failure.code == ResponseCode.UNAUTORISED) {
        log("NOTTTTTTTTTTTTTTTTT");

        clearUserData();
        Navigator.pushReplacementNamed(context, Routes.authScreen);
      } else if (failure.code == ResponseCode.ACCOUNT_NOT_VERIFIED) {
        UserData? userData = await appPreferences.getUser();
        if (userData?.phone != null && userData?.phone?.isEmpty == true) {
          Navigator.pushReplacementNamed(context, Routes.changePhone,
              arguments: {
                "userPhone": userData?.phone ?? "",
                "countryCode": userData?.countryCode ?? "+20",
                "popScreen": false
              });
        } else {
          Navigator.pushReplacementNamed(context, Routes.otpScreen,
              arguments: VerifyRequest(
                  mobile: userData?.phone ?? "",
                  countryCode: userData?.countryCode ?? "+20",
                  code: ""));
        }
      }
    }
  } catch (e) {
    debugPrint("*****Fail in the handleApiError Fun in Utils${e.toString()}");
  }
}

String cutLongString(String text) {
  return text.substring(0, 20);
}

showToast({
  required String message,
  required bool success,
}) {
  final SnackBar snackBar;
  snackBar = SnackBar(
    content: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Icon(success ? Icons.check_circle : Icons.error,
            color: ColorManager.primary),
        const SizedBox(
          width: AppPadding.p8,
        ),
        Expanded(child: Text(message)),
      ],
    ),
  );

  ScaffoldMessenger.of(navigatorKey.currentContext!).showSnackBar(snackBar);
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

double degreeToRadians(double degree) {
  return (math.pi / 180) * degree;
}

List<int> getListOfNumbers(int startNumber, int endNumber) {
  List<int> numbers = [];
  for (int i = startNumber; i <= endNumber; i++) {
    numbers.add(i);
  }
  return numbers;
}
