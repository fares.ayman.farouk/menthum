import 'package:patientapp/application/environment.dart';

class APIsConstants {
  static const String versionUrl = "v1";

  static const String baseUrlDebug =
      "https://staging.tikshif.com/api/$versionUrl/";
  static const String baseUrlRelease = "https://tikshif.com/api/$versionUrl/";

  static const String baseUrlNotVDebug = "https://staging.tikshif.com/api/";
  static const String baseUrlNotVRelease = "https://tikshif.com/api/";

  // static const String baseUrl = "https://staging.tikshif.com/api/$versionUrl/";
  // static const String baseUrlWithoutV = "https://staging.tikshif.com/api/";

  static String baseUrl = getBaseURL();
  static String baseUrlWithoutV = getBaseURLNotVersion();

  static const String loginRoute = "login";
  static const String changeMobileNumber = "profile/change_mobile_number";
  static const String registerRoute = "register";
  static const String resendOtpRoute = "resend";
  static const String verifyURL = "verify";
  static const String resetPassword = "password/reset";
  static const String forgetPassword = "password/mobile";
  static const String logout = "logout";
  static const String getResult = "getResult?page=";
  static const String loginWithProvider = "social/login";
  static const String registerWithProvider = "social/register";

  static const String termsPage = "contents/content/";
  static const String servicesConditions = "services-conditions";
  static const String termsConditions = "terms-conditions";
  static const String about = "about";
  static const String contact_us = "contact-us";

  static const String upcomingAppointments = "getAppointments";
  static const String homePage = "home_page";
  static const String getQuestions = "questions";
  static const String sendQuestionAnswers = "questions-answers";
  static const String getSpecialties = "getSpecialties?q=";
  static const String checkCoupon = "validate_coupon?code=";
  static const String getPendPrescriptions =
      "results/get_pending_prescriptions";
  static const String getDigitalizedPrescriptions =
      "files/get_digital_prescriptions";
  static const String getPrescription = 'files/get_prescriptions';
  static const String orderOnline = 'files/order_prescription';
  static const String clinicProfile = "clinic/";
  static const String clinic = "clinic/search?page=";
  static const String medicalProfile = "medical-profile";
  static const String saveMedicalProfile = "medical-profile/save";
  static const String profile = "profile";

  static const String editProfile = "profile/edit";
  static const String updatePassword = "profile/password/update";
  static const String payment = "payment/init";
  static const String subscribe = "subscription/subscribe";
  static const String renew = "subscription/renew";

  static const String callGp = "call-gp?call_id=";
  static const String triedCall = "missed_call";
  static const String getTransactions = "get_transactions?page=";

  static const String getGovernorates = "/governorates";
  static const String getCities = "/cities";
  static const String getDistricts = "/district";

  static const String makeAppointment = "appointment";

  static const String laboratorySerch = "laboratory/search";
  static const String laboratory = "laboratory/";
  static const String radiologyBranches = "radiology/branches/";

  /// rads apis
  static const String getRadiologies = "radiology/branches/search";
  static const String getRadiologyProfile = "radiology/branches/search";
  static const String labResults = "results/lab";
  static const String radsResults = "results/radiology";
  static const String getTests = "getTests";
  static const String getOneResult = "getOneResult?call_id=";
  static const String uploadRecordReport = "files/";
  static const String uploadRadPrescription = "files/prescription";
  static const String updatePrescriptionFile = "files/update_prescription";
  static const String deletePrescription = "files/delete_prescription";

  static const String setPassword = "profile/password/reset";
  static const String getReports = "get_reports?type=";
  static const String createCallLog = "create_call_log";
  static const String getPlans = "subscription/plans";
  static const String removeCallFromQueue = "remove_call_queue";
  static const String notifyMe = "notify_me";

  /// tikshif locations
  static const String nanoClinicsInfo = "nano-clinics/info";
  static const String listNotifications = "list-notifications";
  static const String setNotificationRead = "notifications/update-read/";

  /// Gp Working Shifts
  static const String getGpWorkingShifts = "get_working_shifts_gp";

  /// menstrual
  static const String menstrual = "menstrual";
  static const String addMenstrual = "menstrual/store";
  static const String empty = "";
  static const int zero = 0;
  static const int apiTimeOut =
      30000; // half minute to API sendTimeout and receiveTimeout

  /// dependant account

  static String showDependantAccount = "dependent/accounts/show/";
  static String getDependantAccounts = "dependent/accounts";
  static String addDependantAccount = "child/store";
  static String getBlogsList = "contents/articles";
  static String getBlog = "contents/articles/show/";

  /// vitals
  static String allReadingType = "readings/allReadingType";
  static String readingsStore = "readings/store";
  static String readings = "readings";

  ///Calculation
  static String calculationTypes = "calculations/get_types";
  static String calculationBmi = "calculations/bmi";
  static String calculationCalories = "calculations/basic_colorie_intake";
  static String calculationPregnancy = "calculations/pregnancy_date";

  ///programs
  static const programs = 'programs';

  ///diabetes
  static String diabetesToday = 'programs/diabetes/patient-events/today';
  static String diabetesEnroll = 'programs/diabetes/enroll';
  static String diabetesIndex = 'programs/diabetes/patient-events/index?page=';
  static String diabetesDone = 'programs/diabetes/patient-events/done';

  /// pediatric
  static String pediatricHome = 'programs/pediatric/patient-events/home?patient_id=';
  static String pediatricToday = 'programs/pediatric/patient-events/getVaccins?patient_id=';
  static String pediatricEnroll = 'programs/pediatric/patient-events/enroll';
  static String pediatricDone = 'programs/pediatric/patient-events/done';
  static String pediatricCalculate = 'programs/pediatric/patient-events/HeadCircumferenceOrweightBabyDone';
  static String pediatricHistory = 'programs/pediatric/patient-events/index';
  static String vaccinesReport = 'programs/pediatric/reports/vaccins';
  static String weightReport = 'programs/pediatric/reports/last-weight';
  static String headReport = 'programs/pediatric/reports/last-head-circumferences';

  /// questionnaires
  static String questionnaire = "questionnaires";
}

class AppConstants {
  static const String tikshifWebsite = "https://tikshif.com/";

  static const String CurrencyType = "EGP";
  static const String weightType = "k";
  static const String heightType = "cm";

  static const String token = "SEND TOKEN HERE";

  static const String landscapeOrientation = "LANDSCAPE";
  static const String portraitOrientations = "PORTRAIT";

  /// Run Time Cash Keys
  static const String UPCOMING_APPOINTMENT_KEY = "UPCOMING_APPOINTMENT_KEY";
  static const String TIKSHIF_RESULTS_KEY = "TIKSHIF_RESULTS_KEY";
  static const String NANO_CLINICS_LOCATIONS_KEY = "NANO_CLINICS_LOCATIONS_KEY";
  static const String NOTIFICATION_LOCAL_KEY = "NOTIFICATION_LOCAL_KEY";
  static const String GP_WORKING_SHIFTS = "GP_WORKING_SHIFTS";

  static const int CACHE_INTERVAL = 60000; // 1 minute cache.
  static const int CACHE_INTERVAL_NOTIFICATION =
      30000; //////// half minute cache.

  static const int APPOITNMENT_UNCONFIRMED = 1;
  static const int APPOINTMENT_CONFIRMED = 2;
  static const int APPOINTMENT_CANCELED = 3;
  static const int APPOINTMENT_CHECKDIN = 4;
  static const int APPOINTMENT_INCONSULTATION = 5;
  static const int APPOINTMENT_COMPLETED = 6;

  static const int CLINIC = 1;
  static const int LAB = 2;
  static const int RAD = 3;

  static const int IN_HOME = 1;
  static const int NOT_HOME = 0;

  static const int QuestionTextField = 1;
  static const int QuestionMultiChoice = 4;
  static const int QuestionSingleChoice = 3;

  static const int maxStringLength = 20;

  static const String tikshifAppOnGooglePlay =
      "https://play.google.com/store/apps/details?id=com.tikshif.patient";

  static const String whatsapp = "whatsapp";
  static const String sms = "sms";
}

class AppKeyConstants {
  static const String RESULTS_ID = "resultId";
  static const String NOTIFICATION_TYPE = "notificationType";
  static const String heightType = "cm";
  static const int lengthOfToken = 40;
}

class ConstantsStrings {
  static const String radiology = "radiology";
  static const String laboratory = "laboratory";
}

class EventsAnalytics {
  static const String itemName = "item_name";
  static const String firebaseScreen = "firebase_screen";
  static const String firebaseScreenClass = "firebase_screen_class";

  /// events names
  static const String videoCall = "video_call";
  static const String freeVideoCall = "free_video_call";

  static const String bookingDoctor = "booking_doctor";
  static const String bookingLab = "booking_lab";
  static const String bookingRad = "booking_rad";
  static const String screenView = "screen_view";
  static const String loginScreenViewEvent = "login_screen_view";
  static const String loginButtonPressedEvent = "login_button_pressed";
  static const String facebookLoginButtonPressedEvent =
      "facebook_login_button_pressed";
  static const String googleButtonPressedEvent = "login_google_button_pressed";
  static const String appleButtonPressedEvent = "apple_login_button_pressed";

  static const String signUpScreenViewEvent = "signup_screen_view";
  static const String signUpButtonPressedEvent = "signup_button_pressed";

  /// parameters names
  static const String callGp = "call_gp";

  ///parameters login
  static const String loginScreenParameter = "View Login Screen";
  static const String loginTapParameter = "Tap Login Button";

  static const String facebookLoginTapParameter = "Tap Facebook Login Button";

  static const String googleLoginTapParameter = "Tap Google Login Button";

  static const String appleLoginTapParameter = "Tap Apple Login Button";

  ///parameters sign up
  static const String signUpScreenParameter = "Sign Up Screen";
  static const String signUpTapParameter = "Tap Sign Up Button";

  /// screen names and class names.
  static const String loginScreenName = "login_screen";
  static const String loginClassName = "LoginScreen";

  static const String signupScreenName = "signup_screen";
  static const String signupClassName = "RegisterScreen";

  static const String callGPScreenName = "call_gp_screen";
  static const String callGPClassName = "ConferencePage";

  static const String mainScreenName = "main_screen";
  static const String mainClassName = "MainScreen";

  static const String myVisitsScreenName = "my_visits_screen";
  static const String myVisitsClassName = "MyVisitsScreen";

  static const String visitDetailsScreenName = "visit_details_screen";
  static const String visitDetailsClassName = "VisitDetails";

  static const String bookDoctorScreenName = "book_doctor_screen";
  static const String bookDoctorClassName = "DoctorProfileScreen";

  static const String bookLabScreenName = "book_lab_screen";
  static const String bookLabClassName = "LabProfileScreen";

  static const String bookRadScreenName = "book_rad_screen";
  static const String bookRadClassName = "RadProfileScreen";

  static const String verifyAccountSuccessfully = "verify_account_successfully";
  static const String getVerifiedSuccessfully =
      "Account Get Verified Successfully";

  static const String missedVideoCall = "missed_video_call";
  static const String getMissedVideoCall =
      "Patient make a call out the working shifts";

  static const String finishFawrySuccessfully = "finish_fawry_successfully";
  static const String patientFinishFawrySuccessfully =
      "patient_finish_fawry_successfully";
}

class SliderItemsConstants {
  static const String callGP = "call_gp";
  static const String bookAppointment = "book_appointment";
  static const String callPhone = "call_phone";
  static const String subscriptionPayment = "subscription_payment";
  static const String subscriptionPlansList = "subscription_plans_list";
  static const String subscriptionRenew = "subscription_renew";
  static const String visit = "visit";
}

/// pusher constants
class PusherConstants {
  static const String queueConnect = "queue-connect";
  static const String queueChange = "queue-change";
  static const String queueFailed = "queue-failed";

  static const String pusherKeyRelease = "7c8256e87c251ca33b43";
  static const String pusherKeyDebug = "b6a60ae5011417d10f39";
  static const String cluster = "eu";
}

///kochava constants
// class KochavaConstants {
//   static const androidGUIDDebug = 'kotikshif-gtqft9';
//   static const iosGUIDDebug = 'kotikshif-ios-ie01jax';
//
//   ///TODO:: GET THE KOCHAVA RELEASE GUID
//   static const androidGUIDRelease = '';
//   static const iosGUIDRelease = '';
// }
