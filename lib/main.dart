import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:patientapp/presentation/resources/language_manager.dart';

import 'application/app.dart';
import 'application/di.dart';
import 'domain/services/dynamic_links/dynamic_links.dart';
import 'domain/services/push_notification/firebase_notification.dart';
import 'domain/services/push_notification/local_notification.dart';

GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await initAppModule();

  /// firebase initialization
  await Firebase.initializeApp();
  await setupFlutterNotifications();
  await instance<DynamicLinkService>().handleDynamicLinks();

  /// local initialize
  flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: onSelectedNotification);

  /// back ground
  onBackgroundNotification();

  /// when app is working in front
  onMessageNotification();

  /// in for ground
  frontBackgroundNotification();

  /// run app
  runApp(EasyLocalization(
    supportedLocales: const [ENGLISH_LOCAL, ARABIC_LOCAL],
    path: ASSET_PATH_LOCALISATIONS,
    child: Phoenix(child: MyApp()),
  ));
}
