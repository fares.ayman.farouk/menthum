import 'package:flutter/material.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';

import '../resources/values_manger.dart';

class TransactionDataPage extends StatelessWidget {
  const TransactionDataPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        padding: EdgeInsets.symmetric(
          vertical: AppPadding.p16,
          horizontal: AppPadding.p16,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Transaction Data",
                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      fontSize: AppSize.s20,
                      fontWeight: FontWeight.bold,
                    ),
              ),
              const SizedBox(
                height: AppSize.s24,
              ),
              Padding(
                padding: const EdgeInsets.only(top: AppPadding.p16),
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p16),
                  height: AppSize.s170,
                  decoration: BoxDecoration(
                      color: ColorManager.white,
                      border: Border.all(
                        color: ColorManager.primary,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "INT230922-24441",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            "NA",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            "Success",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            "Credit",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            "Admin",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                        ],
                      ),
                      const SizedBox(
                        width: AppSize.s50,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "رقم معامله منثم",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                          Text(
                            "رقم المعامله الخارجي",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                          Text(
                            "الحاله",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                          Text(
                            "النوع",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                          Text(
                            "بدات من ",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: AppPadding.p16),
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p16),
                  height: AppSize.s170,
                  decoration: BoxDecoration(
                      color: ColorManager.white,
                      border: Border.all(
                        color: ColorManager.primary,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "sep-2023, 03:02 AM-22",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            "sep-2023, 03:02 AM-22",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            "sep-2023, 03:02 AM-22",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            "sep-2023, 03:02 AM-22",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                        ],
                      ),
                      const SizedBox(
                        width: AppSize.s50,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "وقت البدء",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                          Text(
                            "وقت التنفيد",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                          Text(
                            "تم ارسال المبلغ في ",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                          Text(
                            "تم استلام المبلغ في ",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: AppPadding.p16),
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p16),
                  height: AppSize.s250,
                  decoration: BoxDecoration(
                      color: ColorManager.white,
                      border: Border.all(
                        color: ColorManager.primary,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Gain",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            "احمد ابو السعود صلاح ",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            "ابو السعود الشيخ حساب منثم",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            "System",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            "Internal",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            "NA",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            "بنفسه",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                        ],
                      ),
                      const SizedBox(
                        width: AppSize.s50,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "المصدر",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                          Text(
                            "الوجهه",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                          Text(
                            "الوجهه",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                          Text(
                            "الطريقه",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                          Text(
                            "المسار",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                          Text(
                            "ملاحظات",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                          Text(
                            "قناه القيد الدائن",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: AppPadding.p16),
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p16),
                  height: AppSize.s120,
                  decoration: BoxDecoration(
                      color: ColorManager.white,
                      border: Border.all(
                        color: ColorManager.primary,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "EGP 8.66",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                    fontSize: AppSize.s14,
                                    color: ColorManager.primary),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            "EGP 8.66",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                    fontSize: AppSize.s14,
                                    color: ColorManager.primary),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            "EGP 0.00",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        width: AppSize.s80,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "المبلغ الاجاملي",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                          Text(
                            "المبلغ الصافي",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                          Text(
                            "رسوم المعامله",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: AppPadding.p16),
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p16),
                  height: AppSize.s120,
                  decoration: BoxDecoration(
                      color: ColorManager.white,
                      border: Border.all(
                        color: ColorManager.primary,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "EGP 20,059.95",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                    fontSize: AppSize.s14,
                                    color: ColorManager.primary),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            "EGP 20,059.95",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                    fontSize: AppSize.s14,
                                    color: ColorManager.primary),
                          ),
                        ],
                      ),
                      const SizedBox(
                        width: AppSize.s80,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "الرصيد الاجمالي",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                          const SizedBox(
                            height: AppSize.s4,
                          ),
                          Text(
                            "المبلغ المتوفر",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
