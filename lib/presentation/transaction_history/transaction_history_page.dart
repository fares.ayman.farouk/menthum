import 'package:flutter/material.dart';
import 'package:patientapp/presentation/resources/routes_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';

import '../resources/color_manger.dart';

class TransactionHistoryPage extends StatelessWidget {
  const TransactionHistoryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(
          vertical: AppPadding.p16,
          horizontal: AppPadding.p16,
        ),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Transaction History",
                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      fontSize: AppSize.s20,
                      fontWeight: FontWeight.bold,
                    ),
              ),
              const SizedBox(
                height: AppSize.s24,
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: 10,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(
                            context, Routes.transactionDataScreen);
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(top: AppPadding.p16),
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: AppPadding.p4),
                          height: AppSize.s170,
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 2,
                                  blurRadius: 3,
                                  offset: Offset(
                                      1, 3), // changes position of shadow
                                ),
                              ],
                              color: ColorManager.white,
                              border: Border.all(
                                color: ColorManager.white,
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Row(
                                children: [
                                  Icon(
                                    Icons.monetization_on,
                                    color: ColorManager.blackColor,
                                    size: AppSize.s30,
                                  ),
                                  const SizedBox(
                                    width: AppSize.s10,
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "تحويل وارد من جين",
                                        textAlign: TextAlign.left,
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleMedium
                                            ?.copyWith(
                                              fontSize: AppSize.s16,
                                            ),
                                      ),
                                      Text(
                                        "22 sep 2023  30:55:03",
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleMedium
                                            ?.copyWith(
                                                fontSize: AppSize.s12,
                                                color: ColorManager.fontColor),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: AppPadding.p16),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "3000 EGP+",
                                      textAlign: TextAlign.right,
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleMedium
                                          ?.copyWith(
                                            fontSize: AppSize.s16,
                                            color: ColorManager.primary,
                                          ),
                                    ),
                                    Text(
                                      "نجحت",
                                      textAlign: TextAlign.right,
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleMedium
                                          ?.copyWith(
                                            fontSize: AppSize.s12,
                                            color: ColorManager.primary,
                                          ),
                                    ),
                                    Text(
                                      """
            الرصيد الاجمالي
            20,042
            """,
                                      textAlign: TextAlign.right,
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleMedium
                                          ?.copyWith(
                                            fontSize: AppSize.s12,
                                            color: ColorManager.blackColor,
                                          ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
