import 'package:flutter/material.dart';

import '../resources/values_manger.dart';


class AppRoundedButton extends StatelessWidget {
  final String text;

final  void Function()? onPressed;

  const AppRoundedButton({required this.text,required this.onPressed, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: AppPadding.p24,
        right: AppPadding.p24,
      ),
      child: ElevatedButton(
        onPressed: onPressed,
        child: Text(text),
      ),
    );
  }
}
