import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../resources/assets_manager.dart';

class ErrorNetworkImage extends StatefulWidget {
  const ErrorNetworkImage({Key? key}) : super(key: key);

  @override
  _ErrorNetworkImageState createState() => _ErrorNetworkImageState();
}

class _ErrorNetworkImageState extends State<ErrorNetworkImage> {
  @override
  Widget build(BuildContext context) {
    return Image.asset(
      ImageAssets.appLogo,
    );
  }
}
