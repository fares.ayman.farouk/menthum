import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:lottie/lottie.dart';
import 'package:patientapp/data/repository/auth_repository_imp.dart';
import 'package:patientapp/domain/business_logic/terms_page/terms_page_bloc.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';
import '../../application/di.dart';
import '../../data/models/apiResponse/TermsPageApiResponse.dart';
import '../resources/assets_manager.dart';
import '../resources/color_manger.dart';
import '../resources/font_manger.dart';

class TermsConditionsPopup extends StatefulWidget {
  final pageUrl;
  final String title;

  //Function? function;

  TermsConditionsPopup(this.pageUrl, this.title, /*this.function*/);

  @override
  State<TermsConditionsPopup> createState() => _TermsConditionsPopupState();
}

class _TermsConditionsPopupState extends State<TermsConditionsPopup> {
  AuthRepositoryImp repository = instance<AuthRepositoryImp>();

  //bool _checkedTerms = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorManager.transparentSheet,
      child: Container(
        padding: const EdgeInsets.all(AppPadding.p24),
        margin: const EdgeInsets.only(
            top: AppPadding.p40,
            left: AppPadding.p24,
            right: AppPadding.p24,
            bottom: AppPadding.p24),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(AppRadius.m25)),
        ),
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          widget.title,
                          // pageUrl == APIsConstants.termsConditions
                          //     ? AppStrings.termsAndConditions.tr()
                          //     : pageUrl == APIsConstants.servicesConditions
                          //         ? AppStrings.serviceAndConditions.tr()
                          //         : AppStrings.aboutTikshif.tr(),
                          style:
                              Theme.of(context).textTheme.titleMedium?.copyWith(
                                  //fontSize: FontSize.s18,
                                  fontWeight: FontWeightManager.bold),
                        ),
                        IconButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            icon: const Icon(Icons.close))
                      ],
                    ),
                    const SizedBox(
                      height: AppSize.s8,
                    ),
                    BlocProvider(
                      create: (_) => TermsPageBloc(repository)
                        ..add(GetTermsPageEvent(widget.pageUrl)),
                      child: BlocConsumer<TermsPageBloc, TermsPageState>(
                          builder: (_, state) {
                            if (state is LoadingState) {
                              return Center(
                                child: Lottie.asset(
                                  JsonAssets.loadingAnimations,
                                ),
                              );
                            } else if (state is SuccessState) {
                              Data termsData = state.terms;
                              //var document = parse(termsData.html);
                              return Html(data: termsData.html);
                            } else if (state is FailState) {
                              return Center(
                                child: Lottie.asset(
                                  JsonAssets.noInternet,
                                ),
                              );
                            } else {
                              return Container();
                            }
                          },
                          listener: (_, state) {}),
                    ),

                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
