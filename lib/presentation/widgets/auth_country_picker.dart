import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';

class CountryPickerWidget extends StatelessWidget {
  final Function getCountryCode;
  String? initialValue;

  CountryPickerWidget(
      {Key? key, required this.getCountryCode, this.initialValue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CountryCodePicker(
      onChanged: (value) => getCountryCode(value.dialCode),

      // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
      initialSelection: initialValue ?? '+20',
      favorite: ['+20', 'EGY'],
      // optional. Shows only country name and flag
      showCountryOnly: false,
      // optional. Shows only country name and flag when popup is closed.
      showOnlyCountryWhenClosed: false,
      hideMainText: false,
      // optional. aligns the flag and the Text left
      alignLeft: false,
    );
  }
}
