import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../resources/values_manger.dart';

class IconOpacity extends StatelessWidget {
  final Widget icon;

  IconOpacity({required this.icon, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Container(
      margin: const EdgeInsets.all(AppPadding.p8),
      decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.5),
          borderRadius: BorderRadius.circular(AppRadius.m10)
      ),
      child: IconButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        icon: icon,
      ),
    );
  }
}
