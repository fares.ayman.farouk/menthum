import 'package:flutter/material.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';
import 'package:patientapp/presentation/resources/styles_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';

class ProfileIconDesgin extends StatelessWidget {
  final Widget child;
  final bool editProfile;
  final VoidCallback? onPressed;
  final EdgeInsets? edgeInsets;

  const ProfileIconDesgin({
    Key? key,
    required this.child,
    this.editProfile = false,
    this.onPressed,
    this.edgeInsets,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topLeft,
      children: [
        Container(
          margin: edgeInsets,
          height: AppSize.s80,
          width: AppSize.s80,
          padding: const EdgeInsets.all(AppSize.s10),
          decoration: profileBoxDecoration,
          child: child,
        ),
        /*editProfile
            ? GestureDetector(
                onTap: onPressed,
                child: Align(
                  child: Container(
                    margin: EdgeInsets.only(left: 36, top: 40),
                    padding: EdgeInsets.all(AppSize.s8),
                    child: Icon(
                      Icons.edit,
                      color: ColorManager.primaryLight,
                      size: AppSize.s25,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(AppSize.s25),
                      color: ColorManager.iconLight,
                    ),
                  ),
                ),
              )
            : Container(),*/
      ],
    );
  }
}
