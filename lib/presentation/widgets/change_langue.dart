import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';

import '../../application/app_preference.dart';
import '../../application/di.dart';
import '../resources/color_manger.dart';
import '../resources/strings_manger.dart';
import '../resources/values_manger.dart';

class ChangeLange extends StatelessWidget {
  ChangeLange({Key? key}) : super(key: key);
  final AppPreferences _appPreferences = instance<AppPreferences>();

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        showDio(context);
      },
      child: Text(
        AppStrings.langeDisplay.tr(),
        style: Theme.of(context).textTheme.titleMedium?.copyWith(
              fontSize: AppSize.s15,
              color: ColorManager.primary,
              decoration: TextDecoration.underline,
            ),
      ),
    );
  }


  showDio(context){

    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(AppStrings.changeLang.tr()),
          content: Text(AppStrings.changeLangMessage.tr()),
          actions: [
            CupertinoDialogAction(

                child: Text(AppStrings.yes.tr()),
                onPressed: () {

                  _appPreferences.changeAppLanguage();
                  Phoenix.rebirth(context);
                }),
            CupertinoDialogAction(
              isDestructiveAction: true,
              isDefaultAction: true,
              child: Text(AppStrings.no.tr()),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

}
