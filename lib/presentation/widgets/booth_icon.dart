import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:patientapp/presentation/resources/assets_manager.dart';

import 'error_network_image.dart';

class BooothIcon extends StatelessWidget {
  String icon;
  double? size;

  BooothIcon({required this.icon, this.size = 50});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size,
      width: size,
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                color: Colors.grey.shade300,
                blurRadius: 5.0,
                offset: const Offset(0, 5)),
          ]),
      child: Center(
        child: CachedNetworkImage(
          // width: size! - (10),
          // height: size! - (10),
          imageUrl: icon,
          placeholder: (context, url) => Lottie.asset(
            JsonAssets.loadingAnimations,
          ),
          errorWidget: (context, url, error) =>  const ErrorNetworkImage(),
        ),
      ),
    );
  }
}
