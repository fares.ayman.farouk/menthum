import 'package:flutter/material.dart';

import '../resources/color_manger.dart';
import '../resources/values_manger.dart';

class StrokeButton extends StatelessWidget {
  final String text;
  final Function() onTap;

  const StrokeButton({
    Key? key, required this.text, required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        margin: const EdgeInsets.symmetric(
            horizontal: AppSize.s20,
            vertical: AppSize.s8),
        padding: const EdgeInsets.symmetric(
            vertical: AppPadding.p16),
        decoration: BoxDecoration(
          border: Border.all(
            color: ColorManager.borderColor,
            width: AppSize.s1,
          ),
          color: ColorManager.white,
          borderRadius:
          BorderRadius.circular(AppSize.s14),
        ),
        child: Center(
            child: Text(
              text,
              style: Theme.of(context)
                  .textTheme
                  .headlineLarge!
                  .copyWith(
                  color: ColorManager.borderColor,
                  fontSize: AppSize.s14),
            )),
      ),
    );
  }
}