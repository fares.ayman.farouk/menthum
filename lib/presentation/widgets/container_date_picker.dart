import 'package:flutter/material.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';

import '../resources/styles_manger.dart';
import '../resources/values_manger.dart';

class ContainerDatePicker extends StatelessWidget {
  final Function() onTap;
  final String title, date, error;

  ContainerDatePicker(
      {required this.onTap, required this.date, required this.title, this.error = "", Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: Theme.of(context).textTheme.titleSmall,
        ),
        const SizedBox(
          height: AppSize.s5,
        ),
        GestureDetector(
          onTap: onTap,
          child: Container(
            decoration: profileSpinnerBoxDecoration,
            padding: const EdgeInsets.all(AppPadding.p12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  date,
                  style: Theme.of(context).textTheme.titleSmall?.copyWith(
                    color: ColorManager.fourthColor
                  ),
                ),
                Icon(Icons.calendar_today, color: ColorManager.fourthColor)
              ],
            ),
          ),
        ),
        Visibility(
          visible: error.isNotEmpty,
          child: Padding(
            padding: const EdgeInsetsDirectional.only(
                start: AppPadding.p8, top: AppPadding.p4),
            child: Text(
              error,
              style: Theme.of(context)
                  .textTheme
                  .titleSmall
                  ?.copyWith(
                  color: Colors.red.shade600, fontSize: 13),
            ),
          ),
        ),
      ],
    );
  }
}
