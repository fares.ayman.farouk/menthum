import 'package:flutter/material.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';

class AppBarDesign extends StatelessWidget with PreferredSizeWidget {
  String title;
  bool needFilter;
  Widget? icon;
  bool isBackButton;
  VoidCallback? onTap;
  VoidCallback? onBackPressed;
  bool hasActionButton;
  Widget? menuIcon;

  AppBarDesign(
      {Key? key,
      required this.title,
      required this.hasActionButton,
      this.icon,
      this.onTap,
      this.onBackPressed,
      this.isBackButton = true,
        this.needFilter=true,
      this.menuIcon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leadingWidth: 60,
      title: Text(
        title,
        style: Theme.of(context).textTheme.titleMedium?.copyWith(
              color: ColorManager.blackColor,
              fontSize: AppSize.s16,
            ),
      ),
      leading: isBackButton
          ? IconButton(
              onPressed: onBackPressed == null
                  ? () => Navigator.pop(context)
                  : onBackPressed,
              icon: const Icon(
                Icons.arrow_back_ios,
              ),
            )
          : Container(child: menuIcon),
      elevation: AppSize.s0_25,
      actions: needFilter?[
        hasActionButton ? InkWell(onTap: onTap, child: icon) : Container(),
        const SizedBox(
          width: AppPadding.p15,
        )
      ]:[],
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
