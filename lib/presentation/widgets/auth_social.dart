import 'package:flutter/material.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';

import '../resources/values_manger.dart';

class AuthSocial extends StatelessWidget {
  final String text;
  final Widget socialIcon;
  final Function() onTap;

  const AuthSocial(
      {required this.text,
      required this.socialIcon,
      required this.onTap,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: AppPadding.p24,
        right: AppPadding.p24,
      ),
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          height: AppSize.s56,
          decoration: BoxDecoration(
              color: ColorManager.bgTextFieldColor,
              borderRadius: BorderRadius.circular(32.0)),
          child: ListTile(
            leading: socialIcon,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  text,
                  style: Theme.of(context).textTheme.titleMedium?.copyWith(
                        fontSize: 14,
                        color: ColorManager.textSocialColor,
                      ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
