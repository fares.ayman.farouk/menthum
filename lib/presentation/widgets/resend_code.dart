import 'package:flutter/material.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';
import 'package:patientapp/presentation/resources/font_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';

class ResendCode extends StatelessWidget {
  final VoidCallback onPress;
  String text;

   ResendCode({super.key, required this.onPress, required this.text});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: AppPadding.p20),
      child: Align(
        alignment: Alignment.centerRight,
        child: TextButton(
          onPressed: onPress,
          child: Text(
            text,
            style: Theme.of(context).textTheme.titleMedium?.copyWith(
                color: ColorManager.textColor, fontSize: FontSize.s14),
          ),
        ),
      ),
    );
  }
}
