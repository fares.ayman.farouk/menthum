import 'package:flutter/cupertino.dart';

class CupertinoDialogWidget extends StatefulWidget {
  final String title;
  final String body;
  final String trueText;
  final String falseText;

  final VoidCallback trueFunc;
  final VoidCallback falseFunc;

  const CupertinoDialogWidget({
    Key? key,
    required this.title,
    required this.body,
    required this.trueText,
    required this.falseText,
    required this.trueFunc,
    required this.falseFunc,
  }) : super(key: key);

  @override
  State<CupertinoDialogWidget> createState() => _CupertinoDialogWidgetState();
}

class _CupertinoDialogWidgetState extends State<CupertinoDialogWidget> {
  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(widget.title),
      content: Text(widget.body),
      actions: [
        CupertinoDialogAction(
            child: Text(widget.trueText), onPressed: widget.trueFunc),
        CupertinoDialogAction(
            isDestructiveAction: true,
            isDefaultAction: true,
            child: Text(widget.falseText),
            onPressed: widget.falseFunc)
      ],
    );
  }
}
