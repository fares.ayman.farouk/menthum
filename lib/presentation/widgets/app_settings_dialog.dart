import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';

class AppSettingsDialog extends StatelessWidget {
  final Function openAppSettings;
  final Function cancelDialog;
  final String message;

  const AppSettingsDialog(
      {Key? key,
      required this.openAppSettings,
      required this.cancelDialog,
      required this.message})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(
            height: 15,
          ),
          Text(message),
          const SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextButton(
                  onPressed: () => openAppSettings(),
                  child: Text(AppStrings.openAppSettings.tr())),
              TextButton(
                  onPressed: () => cancelDialog(),
                  child: Text(
                    AppStrings.cancel.tr(),
                    style: const TextStyle(color: Colors.red),
                  )),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
