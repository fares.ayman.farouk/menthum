import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:patientapp/presentation/resources/font_manger.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';

import '../resources/assets_manager.dart';

class EmptyContentWidget extends StatelessWidget {
  const EmptyContentWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SvgPicture.asset(ImageAssets.splashLogo),
          const SizedBox(
            height: AppSize.s50,
          ),
           Text(AppStrings.noResultFound.tr(),
             style: Theme.of(context).textTheme.headlineLarge?.copyWith(fontSize: FontSize.s20),
           )
        ],
      ),
    );
  }
}
