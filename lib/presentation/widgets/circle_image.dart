import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../resources/assets_manager.dart';
import '../resources/color_manger.dart';
import '../resources/values_manger.dart';
import 'error_network_image.dart';

class CircleImage extends StatelessWidget {
  final String imageUrl;
  final double width;

  const CircleImage({Key? key, required this.imageUrl, required this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: width,
        height: width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(AppPadding.p70),
          color: ColorManager.roundedContainerColor,
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: imageUrl.isNotEmpty ? CachedNetworkImage(
          imageUrl: imageUrl,
          fit: BoxFit.fitWidth,
          placeholder: (context, url) => Lottie.asset(
            JsonAssets.loadingAnimations,
          ),
          errorWidget: (context, url, error) => const ErrorNetworkImage(),
        ) : Image.asset(ImageAssets.unknownUser));
  }
}
