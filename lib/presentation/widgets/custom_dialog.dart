import 'package:flutter/material.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';

import '../resources/color_manger.dart';


class CustomDialog extends StatelessWidget {
  EdgeInsets? padding;
  final double? borderRadius;
  final Widget contentBox;
  Color? color;

  CustomDialog({
    this.padding,
    this.color,
    this.borderRadius = AppSize.s20,
    required this.contentBox,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: padding ?? null,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(borderRadius!),
      ),
      elevation: 0,
      backgroundColor: color ?? ColorManager.white,
      child: contentBox,
    );
  }
}
