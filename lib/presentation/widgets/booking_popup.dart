import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:patientapp/application/constants.dart';
import 'package:patientapp/application/utils.dart';
import 'package:patientapp/data/models/apiRequest/clinic/book_clinic_appointment_req.dart';
import 'package:patientapp/data/models/apiResponse/clinic/clinic_response.dart';
import 'package:patientapp/domain/business_logic/clinic_profile/clinic_profile_bloc.dart';
import 'package:patientapp/domain/business_logic/rads_profile/rads_profile_bloc.dart';
import 'package:patientapp/presentation/resources/language_manager.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';
import 'package:patientapp/presentation/resources/styles_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';
import 'package:patientapp/presentation/widgets/app_rounded_btn.dart';

import '../../application/di.dart';
import '../../data/models/apiResponse/consultations_responses/clinic_profile_response.dart';
import '../../domain/services/analytics_services/analytics_services.dart';
import '../resources/color_manger.dart';
import '../resources/font_manger.dart';
import 'container_date_picker.dart';

class BookingPopup extends StatefulWidget {
  final List<WorkingShift>? workingShifts;
  final List<Hospital>? hospitals;
  final bool hasWorkingShifts;
  final String type;
  final int? callId;
  final String? labId;
  final int? inHome;
  final List<String>? tests;

  BookingPopup({
    required this.type,
    this.workingShifts,
    this.hospitals,
    this.hasWorkingShifts = true,
    this.callId,
    this.labId,
    this.inHome,
    this.tests,
  });

  @override
  State<BookingPopup> createState() => _BookingPopupState();
}

class _BookingPopupState extends State<BookingPopup> {
  int indexOfWorkingShiftSelected = 0;
  int indexOfFavoriteTimeSelected = 0;
  int indexOfChosenHospital = -1;

  String favoriteTime = "";

  late DateTime fromInHome;
  late DateTime toInHome;

  bool testsFromHomeChecked = false;
  AnalyticsServices analyticsServices = instance<AnalyticsServices>();

  @override
  void initState() {
    super.initState();
    fromInHome = getNowTimeWithZone();
    toInHome = fromInHome;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorManager.transparentSheet,
      child: Container(
        padding: const EdgeInsets.all(AppPadding.p24),
        margin: const EdgeInsets.only(
            left: AppPadding.p24,
            right: AppPadding.p24,
            bottom: AppPadding.p24),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(AppRadius.m25)),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  AppStrings.newAppointment.tr(),
                  style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      fontSize: FontSize.s18,
                      fontWeight: FontWeightManager.bold),
                ),
                IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: const Icon(Icons.close))
              ],
            ),
            const SizedBox(
              height: AppSize.s8,
            ),
            Text(AppStrings.chooseAvailableAppointment.tr(),
                style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                      fontSize: FontSize.s14,
                      color: ColorManager.fontLight,
                    )),
            const SizedBox(
              height: AppSize.s30,
            ),
            if (widget.hasWorkingShifts &&
                widget.workingShifts?.isNotEmpty == true) ...[
              Text(AppStrings.chooseDay.tr(),
                  style: Theme.of(context).textTheme.titleMedium?.copyWith(
                        fontSize: FontSize.s14,
                        fontWeight: FontWeight.bold,
                      )),
              const SizedBox(
                height: AppSize.s5,
              ),
              SizedBox(
                height: 150,
                child: ListView.builder(
                  itemCount: widget.workingShifts?.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (cxt, i) {
                    return InkWell(
                      onTap: () {
                        setState(() {
                          indexOfWorkingShiftSelected = i;
                          indexOfFavoriteTimeSelected = 0;
                        });
                      },
                      child: itemAvailableTimes(
                          context: context,
                          selected: i == indexOfWorkingShiftSelected,
                          day: int.parse(
                              widget.workingShifts?[i].weekDay.toString() ??
                                  ""),
                          from: widget.workingShifts?[i].timeFrom ?? 0,
                          to: widget.workingShifts?[i].timeTo ?? 0),
                    );
                  },
                ),
              ),
            ],

            ///chose clinic
            if (widget.type == AppStrings.clinic) ...[
              const SizedBox(
                height: AppSize.s20,
              ),
              Text(
                AppStrings.chooseFavoriteTime.tr(),
                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      fontSize: FontSize.s14,
                      fontWeight: FontWeight.bold,
                    ),
              ),
              const SizedBox(
                height: AppSize.s5,
              ),
              getFavoriteTime(
                fromDate: widget.workingShifts != null &&
                        widget.workingShifts?.isNotEmpty == true
                    ? widget.workingShifts![indexOfWorkingShiftSelected]
                        .timeFrom as int
                    : 1659607200,
                toDate: widget.workingShifts != null &&
                        widget.workingShifts?.isNotEmpty == true
                    ? widget.workingShifts![indexOfWorkingShiftSelected].timeTo
                        as int
                    : 1659650399,
              ),
            ],

            ///chose hospital
            if (widget.hospitals != null && widget.hospitals!.isNotEmpty) ...[
              const SizedBox(
                height: AppSize.s10,
              ),
              Text(
                AppStrings.chooseHospital.tr(),
                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      fontSize: FontSize.s14,
                      fontWeight: FontWeight.bold,
                    ),
              ),
              const SizedBox(
                height: AppSize.s5,
              ),
              getHospitalsList(),
            ],
            Visibility(
              visible: widget.inHome == 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    AppStrings.takeTestHome.tr(),
                    style: getMediumStyle(
                      color: ColorManager.secondColor,
                      fontSize: AppSize.s14,
                    ),
                  ),
                  /*  SwitchWidget(
                      onChanged: (value) {
                        setState(() {
                          testsFromHomeChecked = (value as bool?)!;
                        });
                      },
                      value: testsFromHomeChecked), */
                ],
              ),
            ),
            if (widget.type == AppStrings.laboratory &&
                widget.inHome == 1 &&
                testsFromHomeChecked) ...[
              ContainerDatePicker(
                  onTap: () {
                    DatePicker.showDateTimePicker(context,
                        showTitleActions: true,
                        minTime: getNowTimeWithZone(), onChanged: (date) {
                      print('change $date');
                    }, onConfirm: (date) {
                      setState(() {
                        fromInHome = getDateTimeWithZone(date);
                        toInHome = getDateTimeWithZone(date);
                      });
                      print('confirm $getDateTimeWithZone($date)');
                    },
                        currentTime: getNowTimeWithZone(),
                        locale: isRtl(context) ? LocaleType.ar : LocaleType.en);
                  },
                  date: getDateYMDHM(fromInHome),
                  title: AppStrings.timeFrom.tr()),
              const SizedBox(
                height: AppSize.s20,
              ),
              ContainerDatePicker(
                onTap: () {
                  DatePicker.showTimePicker(context,
                      showTitleActions: true,
                      //minTime: getNowTimeWithZone(),
                      showSecondsColumn: false, onChanged: (date) {
                    print('change $date');
                  }, onConfirm: (date) {
                    setState(() {
                      toInHome = getDateTimeWithZone(date);
                    });
                    print('confirm $getDateTimeWithZone($date)');
                  },
                      currentTime: toInHome,
                      locale: isRtl(context) ? LocaleType.ar : LocaleType.en);
                },
                title: AppStrings.timeTo.tr(),
                date: getDateYMDHM(toInHome),
              ),
            ],
            const SizedBox(
              height: AppSize.s20,
            ),
            AppRoundedButton(
                text: AppStrings.bookNow.tr(),
                onPressed: () {
                  if (widget.type == AppStrings.clinic) {
                    /// clinic analytics
                    analyticsServices.addEventAnalytics(
                        eventName: EventsAnalytics.bookingDoctor,
                        parameterName: EventsAnalytics.bookingDoctor);

                    context.read<ClinicProfileBloc>().add(MakeAppointmentEvent(
                        bookClinicAppointmentReq: BookAppointmentReq(
                            type: widget.type,
                            callId: widget.callId,
                            hospitalId:
                                widget.hospitals?[indexOfChosenHospital].id,
                            workingshiftId: widget
                                .workingShifts?[indexOfWorkingShiftSelected].id,
                            favoriteTime: favoriteTime)));
                  } else if (widget.type == AppStrings.laboratory) {
                    /// lab analytics
                    analyticsServices.addEventAnalytics(
                        eventName: EventsAnalytics.bookingLab,
                        parameterName: EventsAnalytics.bookingLab);
                    context.read<RadsProfileBloc>().add(
                          MakeRadAppointmentEvent(
                            BookAppointmentReq(
                              type: widget.type,
                              callId: widget.callId,
                              laboratory_id: widget.labId,
                              hospitalId:
                                  widget.hospitals?[indexOfChosenHospital].id,
                              inHome: testsFromHomeChecked
                                  ? AppConstants.IN_HOME
                                  : AppConstants.NOT_HOME,
                              tests: widget.tests,
                              timeFrom: testsFromHomeChecked
                                  ? convertDateTimeToTimestamp(fromInHome)
                                      .toString()
                                  : null,
                              timeTo: testsFromHomeChecked
                                  ? convertDateTimeToTimestamp(toInHome)
                                      .toString()
                                  : null,
                            ),
                          ),
                        );
                  }
                }),
          ],
        ),
      ),
    );
  }

  Widget getFavoriteTime({required int fromDate, required int toDate}) {
    List<DateTime> listOFFavoriteTimes = [];
    const timePeriod = Duration(minutes: 30);

    DateTime from = DateTime.fromMillisecondsSinceEpoch(
        fromDate * 1000); // DateFormat("hh:mm").parse(fromDate);
    DateTime to = DateTime.fromMillisecondsSinceEpoch(
        toDate * 1000); // DateFormat("hh:mm").parse(toDate);

    listOFFavoriteTimes.add(from);

    for (int i = 0; i < 30 && from.compareTo(to) == -1; i++) {
      from = from.add(timePeriod);
      listOFFavoriteTimes.add(from);
    }
    setState(() {
      favoriteTime = DateFormat("hh:mm a")
          .format(listOFFavoriteTimes[indexOfFavoriteTimeSelected]);
    });

    return Container(
      height: AppSize.s44,
      child: ListView.builder(
        itemCount: listOFFavoriteTimes.length,
        scrollDirection: Axis.horizontal,
        physics: const BouncingScrollPhysics(),
        itemBuilder: (context, int index) {
          return InkWell(
            onTap: () {
              setState(() {
                indexOfFavoriteTimeSelected = index;
                favoriteTime =
                    DateFormat("hh:mm a").format(listOFFavoriteTimes[index]);
              });
            },
            child: favTimeItem(
                DateFormat("hh:mm a").format(listOFFavoriteTimes[index]),
                indexOfFavoriteTimeSelected == index),
          );
        },
      ),
    );
  }

  Widget getHospitalsList() {
    return SizedBox(
      height: AppSize.s44,
      child: ListView.builder(
        itemCount: widget.hospitals?.length ?? 0,
        scrollDirection: Axis.horizontal,
        physics: const BouncingScrollPhysics(),
        itemBuilder: (context, int index) {
          return InkWell(
            onTap: () {
              setState(() {
                indexOfChosenHospital = index;
              });
            },
            child: favHospitalItem(
                widget.hospitals![index].name!, indexOfChosenHospital == index),
          );
        },
      ),
    );
  }

  Widget favTimeItem(String time, bool isSelected) {
    return Container(
      padding: const EdgeInsets.all(AppPadding.p8),
      margin: const EdgeInsets.all(AppPadding.p4),
      decoration: BoxDecoration(
        border: isSelected
            ? Border.all(color: ColorManager.primary, width: 2)
            : Border.all(color: ColorManager.blackColor, width: 1),
        color: isSelected ? ColorManager.primary : null,
        borderRadius: const BorderRadius.all(
          Radius.circular(AppRadius.m12),
        ),
      ),
      child: Text(
        time,
        style: Theme.of(context).textTheme.bodyText1?.copyWith(
            fontSize: AppSize.s14,
            color: isSelected ? ColorManager.white : ColorManager.blackColor),
      ),
    );
  }

  Widget favHospitalItem(String name, bool isSelected) {
    return Container(
      padding: const EdgeInsets.all(AppPadding.p8),
      margin: const EdgeInsets.all(AppPadding.p4),
      decoration: BoxDecoration(
        border: isSelected
            ? Border.all(color: ColorManager.primary, width: 2)
            : Border.all(color: ColorManager.blackColor, width: 1),
        color: isSelected ? ColorManager.primary : null,
        borderRadius: const BorderRadius.all(
          Radius.circular(AppRadius.m12),
        ),
      ),
      child: Text(
        name,
        style: Theme.of(context).textTheme.bodyText1?.copyWith(
            fontSize: AppSize.s14,
            color: isSelected ? ColorManager.white : ColorManager.blackColor),
      ),
    );
  }

  Widget itemAvailableTimes(
      {required context,
      required bool selected,
      required int day,
      required int from,
      required int to}) {
    return Container(
        width: 80,
        margin: const EdgeInsets.symmetric(
            horizontal: AppPadding.p8, vertical: AppPadding.p8),
        decoration: BoxDecoration(
          border: selected
              ? Border.all(color: ColorManager.primary, width: 1)
              : null,
          boxShadow: selected ? shadowList : null,
          color: ColorManager.primary,
          borderRadius: const BorderRadius.all(
            Radius.circular(AppRadius.m12),
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(AppSize.s5),
              child: Text(AppStrings.weekDays[day % 7].tr(),
                  style: Theme.of(context).textTheme.headlineLarge?.copyWith(
                      color: Colors.white,
                      fontSize: AppSize.s16,
                      fontWeight: FontWeight.bold)),
            ),
            Expanded(
              child: Container(
                width: 80,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(AppRadius.m12),
                    bottomLeft: Radius.circular(AppRadius.m12),
                  ),
                  border: Border.all(
                    width: AppSize.s1,
                    color: ColorManager.roundedContainerStrokeColor,
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                          //'08:00 AM'
                          timeStampToTimeWorkingShifts(from),
                          style: Theme.of(context)
                              .textTheme
                              .bodySmall
                              ?.copyWith(fontSize: AppSize.s14)),
                      const Icon(Icons.keyboard_arrow_down),
                      Text(
                          //'05:00 PM'
                          timeStampToTimeWorkingShifts(to),
                          style: Theme.of(context)
                              .textTheme
                              .bodySmall
                              ?.copyWith(fontSize: AppSize.s14)),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ));
  }
}
