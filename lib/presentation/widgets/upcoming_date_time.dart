import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:patientapp/application/constants.dart';
import 'package:patientapp/presentation/resources/assets_manager.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';

import '../resources/font_manger.dart';

class UpcomingDateTime extends StatelessWidget {
  final String date, time, status, statusId;
  final bool showStatus;

  const UpcomingDateTime(
      {Key? key,
      required this.date,
      required this.time,
      required this.status,
      this.showStatus = true,
      required this.statusId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Visibility(
              visible: date.isNotEmpty,
              child: Text(
                date,
                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      fontSize: AppSize.s12,
                    ),
              ),
            ),
            const SizedBox(
              width: AppSize.s5,
            ),
            Visibility(
              visible: status.isNotEmpty && statusId.isNotEmpty && showStatus,
              child: Container(
                padding: const EdgeInsets.symmetric(
                    horizontal: AppSize.s10, vertical: AppSize.s5),
                decoration: BoxDecoration(
                  color: statusId.isNotEmpty ? changeStatusColor() : null,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(AppRadius.m5),
                  ),
                ),
                child: Text(
                  textAlign: TextAlign.start,
                  status,
                  style: Theme.of(context).textTheme.bodySmall?.copyWith(
                      fontSize: FontSize.s9, color: ColorManager.white),
                ),
              ),
            ),
          ],
        ),
        Visibility(
          visible: time.isNotEmpty,
          child: Padding(
            padding: const EdgeInsets.only(top: AppSize.s5),
            child: Row(
              children: [
                SvgPicture.asset(ImageAssets.homeClock),
                const SizedBox(
                  width: AppSize.s10,
                ),
                Text(
                  time,
                  style: Theme.of(context).textTheme.bodySmall?.copyWith(
                      color: ColorManager.primary, fontSize: AppSize.s13),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Color changeStatusColor() {
    if (int.parse(statusId) == AppConstants.APPOINTMENT_CANCELED ||
        int.parse(statusId) == AppConstants.APPOITNMENT_UNCONFIRMED) {
      return Colors.red;
    } else if (int.parse(statusId) == AppConstants.APPOINTMENT_CONFIRMED ||
        int.parse(statusId) == AppConstants.APPOINTMENT_COMPLETED) {
      return Colors.green;
    } else {
      return Colors.blue;
    }
  }
}
