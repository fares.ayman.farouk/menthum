import 'package:flutter/material.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';
import 'package:patientapp/presentation/resources/styles_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';

class ExpandableWidget extends StatefulWidget {
  final Widget child;
  final String title;
  bool visible;

  ExpandableWidget({
    super.key,
    required this.child,
    required this.title,
    this.visible = false,
  });

  @override
  State<ExpandableWidget> createState() => _ExpandableWidgetState();
}

class _ExpandableWidgetState extends State<ExpandableWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(AppPadding.p8),
      padding: const EdgeInsets.symmetric(
          horizontal: AppPadding.p8, vertical: AppPadding.p16),
      decoration: BoxDecoration(
          boxShadow: shadowList,
          color: Colors.white,
          borderRadius: BorderRadius.circular(AppSize.s20)),
      child: Column(children: [
        InkWell(
          onTap: () {
            setState(() {
              widget.visible = !widget.visible;
            });
          },
          child: Row(
            children: [
              const SizedBox(
                width: AppPadding.p8,
              ),
              Text(
                widget.title,
                style: Theme.of(context)
                    .textTheme
                    .bodyMedium
                    ?.copyWith(fontWeight: FontWeight.bold),
              ),
              const Expanded(child: SizedBox()),
              CircleAvatar(
                  backgroundColor: ColorManager.primary,
                  radius: AppSize.s18,
                  child: Icon(
                    widget.visible
                        ? Icons.arrow_circle_up_sharp
                        : Icons.arrow_circle_down_sharp,
                    color: Colors.white,
                  )),
              const SizedBox(
                width: AppPadding.p8,
              )
            ],
          ),
        ),
        Visibility(
          visible: widget.visible,
          child: widget.child,
        ),
      ]),
    );
  }
}
