import 'package:flutter/material.dart';

import '../resources/values_manger.dart';

class AuthTextField extends StatelessWidget {
  final String hintText;
  final String? errorText;
  final TextInputType keyboardType;
  final bool obscureText;
  final TextEditingController controller;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final Function validator;
  final int? maxLength;
  final bool noSpace;
  final Function(String)? onChanged;

  const AuthTextField(
      {required this.hintText,
      required this.keyboardType,
      required this.obscureText,
      this.onChanged,
      this.errorText,
      required this.controller,
      this.prefixIcon,
      this.suffixIcon,
      this.noSpace = false,
      this.maxLength,
      required this.validator,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: noSpace
          ? const EdgeInsets.all(0.0)
          : const EdgeInsets.only(
              left: AppPadding.p24,
              right: AppPadding.p24,
            ),
      child: TextFormField(
        cursorColor: Colors.black,
        controller: controller,
        obscureText: obscureText,
        validator: (val) => validator(val),
        keyboardType: keyboardType,
        maxLength: maxLength,
        onChanged: onChanged,
        style: Theme.of(context)
            .textTheme
            .bodyMedium
            ?.copyWith(color: Colors.black),
        decoration: InputDecoration(
            labelText: hintText,
            //hintText: hintText,
            prefixIcon: prefixIcon,
            suffixIcon: suffixIcon,
            errorText: errorText),
      ),
    );
  }
}
