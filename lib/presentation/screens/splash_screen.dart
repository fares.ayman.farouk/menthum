import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/login_api_response.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';
import '../../application/app_preference.dart';
import '../../application/di.dart';
import '../../data/models/apiRequest/auth_requests/verify_request.dart';
import '../../domain/models/main_screen_tabs.dart';
import '../../domain/services/push_notification/local_notification.dart';
import '../resources/assets_manager.dart';
import '../resources/routes_manger.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Timer? _timer;
  final AppPreferences _appPreferences = instance<AppPreferences>();

  _startDelay() {
    _timer = Timer(const Duration(seconds: 3), _goNext);
  }

  _goNext() async {
    // if (await _appPreferences.isFirstTimeOnBoarding() == true) {
    //   Navigator.pushReplacementNamed(context, Routes.onBoardingRoute);
    // }else
    if (/* await _appPreferences.isFirstTimeOnBoarding() == false &&*/
        await _appPreferences.hasToken() == false) {
      Navigator.pushReplacementNamed(context, Routes.authScreen);
    } else if (/*await _appPreferences.isFirstTimeOnBoarding() == false &&*/
        await _appPreferences.hasToken() == true &&
            (await _appPreferences.getUser())?.isMobileVerified == true) {
      Navigator.pushReplacementNamed(context, Routes.mainScreen,
          arguments: MainTabs.home.index);
      init();
    } else if ((await _appPreferences.getUser())?.isMobileVerified != true) {
      UserData? userData = await _appPreferences.getUser();
      if (userData?.phone != null && userData?.phone?.isEmpty == true) {
        Navigator.pushReplacementNamed(context, Routes.changePhone, arguments: {
          "userPhone": userData?.phone ?? "",
          "countryCode": userData?.countryCode ?? "",
          "popScreen": false,
        });
      } else {
        Navigator.pushReplacementNamed(context, Routes.otpScreen,
            arguments: VerifyRequest(
                mobile: userData?.phone ?? "",
                countryCode: userData?.countryCode ?? "",
                code: ""));
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _startDelay();
  }

  init() async {
    /// work notification when app closed
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();
    if (initialMessage != null) {
      onSelectedNotification(initialMessage.data.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Center(
          child: Icon(
        Icons.monetization_on,
        size: AppSize.s80,
        color: ColorManager.primary,
      )),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _timer?.cancel();
  }
}
