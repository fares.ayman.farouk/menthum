import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';
import 'package:patientapp/presentation/resources/font_manger.dart';

import '../../../resources/assets_manager.dart';
import '../../../resources/language_manager.dart';
import '../../../resources/strings_manger.dart';
import '../../../resources/values_manger.dart';
import '../../../widgets/change_langue.dart';
import '../login/login_screen.dart';
import '../register/register_screen.dart';

class AuthScreen extends StatefulWidget {
  const AuthScreen({Key? key}) : super(key: key);

  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    TabController _tabController = TabController(
      length: 2,
      vsync: this,
    );

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const SizedBox(
            height: AppSize.s25,
          ),

          /// change lange
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: Align(
                alignment: isRtl(context)
                    ? Alignment.centerLeft
                    : Alignment.centerRight,
                child: ChangeLange()),
          ),
          Icon(
            Icons.monetization_on,
            size: AppSize.s80,
            color: ColorManager.primary,
          ),
          /*  SvgPicture.asset(ImageAssets.authLogo), */
          const SizedBox(
            height: AppSize.s15,
          ),

          /// tab bar

          Center(
            child: TabBar(
              controller: _tabController,
              isScrollable: true,
              labelColor: ColorManager.primary,
              labelStyle: Theme.of(context)
                  .textTheme
                  .headlineMedium
                  ?.copyWith(fontSize: FontSize.s20),
              indicatorColor: ColorManager.primary,
              tabs: [
                Tab(text: AppStrings.labelSignIn.tr()),
                Tab(text: AppStrings.labelSignUp.tr()),
              ],
            ),
          ),

          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: [LoginScreen(), RegisterScreen()],
            ),
          )
        ],
      ),
    );
  }
}
