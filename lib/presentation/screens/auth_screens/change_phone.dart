import 'package:easy_localization/easy_localization.dart' as E;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';

import '../../../application/app_preference.dart';
import '../../../application/di.dart';

import '../../../application/utils.dart';
import '../../../data/models/apiError/login_api_error.dart';
import '../../../data/models/apiRequest/auth_requests/verify_request.dart';
import '../../../data/models/apiResponse/auth_response/login_api_response.dart';
import '../../../data/repository/auth_repository_imp.dart';
import '../../../domain/business_logic/auth/login/login_bloc.dart';
import '../../resources/assets_manager.dart';
import '../../resources/color_manger.dart';
import '../../resources/font_manger.dart';
import '../../resources/routes_manger.dart';
import '../../resources/strings_manger.dart';
import '../../resources/values_manger.dart';
import '../../widgets/app_rounded_btn.dart';
import '../../widgets/auth_country_picker.dart';
import '../../widgets/auth_text_field.dart';
import '../../widgets/custom_dialog.dart';

class ChangePhoneNumber extends StatefulWidget {
  String? userPhone;
  String? countryCode;
  bool? popScreen;

  ChangePhoneNumber(
      {Key? key, this.userPhone, this.countryCode = "+20", this.popScreen})
      : super(key: key);

  @override
  State<ChangePhoneNumber> createState() => _ChangePhoneNumberState();
}

class _ChangePhoneNumberState extends State<ChangePhoneNumber> {
  TextEditingController phoneController = TextEditingController();

  AuthRepositoryImp loginRepository = instance<AuthRepositoryImp>();

  String errorPhone = "";

  final _formKey = GlobalKey<FormState>();

  AppPreferences preferences = instance<AppPreferences>();

  clearData() {
    appPreferences.removeUser();
    Navigator.pushReplacementNamed(context, Routes.authScreen);
  }


  @override
  void initState() {
    super.initState();
    String countryCode = widget.countryCode??"";
    if(countryCode.isEmpty)
      widget.countryCode="+20";

  }

  @override
  Widget build(BuildContext context) {
    phoneController.text = widget.userPhone ?? "";
    return WillPopScope(
      onWillPop: () async {
        if (widget.popScreen == true) {
          return true;
        } else {
          clearData();
          return false;
        }
      },
      child: SafeArea(
        child: BlocProvider(
          create: (context) => LoginBloc(loginRepository),
          child: BlocConsumer<LoginBloc, LoginState>(
            listener: (context, state) {
              /// loading
              if (state is LoadingLoginState) {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return CustomDialog(
                          color: Colors.transparent,
                          borderRadius: AppSize.s20,
                          contentBox: Center(
                            child: Lottie.asset(
                              JsonAssets.loadingAnimations,
                            ),
                          ));
                    });
              }

              /// sucess
              else if (state is SuccessChangePhoneState) {

                VerifyRequest verifyRequest = VerifyRequest(
                    mobile: checkMobileNumber(phoneController.value.text),
                    countryCode: widget.countryCode ?? "+20",
                    code: "");
                Navigator.pop(context);
                Navigator.pushReplacementNamed(context, Routes.otpScreen,
                    arguments: verifyRequest);
              } else if (state is FailedChangePhoneState) {
                Navigator.pop(context);

                LoginApiErrorResponse r = loginApiErrorResponseFromJson(
                    state.failure.body.toString());

                if (r.errors != null) {
                  errorPhone = concatenateListOfStrings(r.errors!.mobile!);
                }

                final snackBar = SnackBar(
                  content: Text(r.message.toString()),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            },

            ////////////////////////////
            builder: (context, state) {
              return Scaffold(
                backgroundColor: Theme.of(context).backgroundColor,
                body: SizedBox(
                  width: double.infinity,
                  height: double.infinity,

                  ////
                  child: SingleChildScrollView(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          const SizedBox(
                            height: AppSize.s20,
                          ),
                          SvgPicture.asset(ImageAssets.authLogo),
                          const SizedBox(
                            height: AppSize.s14,
                          ),
                          const SizedBox(
                            height: AppSize.s40,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              left: AppPadding.p24,
                              right: AppPadding.p24,
                            ),
                            child: Text(
                              AppStrings.labelWritePhone.tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(
                                      color: ColorManager.textColor,
                                      fontSize: FontSize.s14),
                            ),
                          ),
                          const SizedBox(
                            height: AppSize.s16,
                          ),
                          Directionality(
                            textDirection: TextDirection.ltr,
                            child: AuthTextField(
                              hintText: AppStrings.phoneNumber.tr(),
                              keyboardType: TextInputType.phone,
                              maxLength: 11,
                              obscureText: false,
                              controller: phoneController,
                              errorText: errorPhone,
                              prefixIcon: CountryPickerWidget(
                                getCountryCode: (value) {
                                  setState(() {
                                    widget.countryCode = value;
                                  });
                                },
                              ),
                              validator: (value) {},
                            ),
                          ),
                          const SizedBox(
                            height: AppSize.s20,
                          ),
                          AppRoundedButton(
                            text: AppStrings.changeNum.tr(),
                            onPressed: () async {
                              errorPhone = "";

                              if (phoneController.text == widget.userPhone &&
                                  phoneController.text.isNotEmpty) {
                                UserData? useData = await preferences.getUser();

                                if (useData?.isMobileVerified == true) {
                                  Navigator.pushReplacementNamed(
                                      context, Routes.mainScreen);
                                } else {
                                  VerifyRequest verifyRequest = VerifyRequest(
                                      mobile: checkMobileNumber(
                                          phoneController.value.text),
                                      countryCode: widget.countryCode??"+20",
                                      code: "");
                                  Navigator.pushReplacementNamed(
                                      context, Routes.otpScreen,
                                      arguments: verifyRequest);
                                }
                              } else {
                                context.read<LoginBloc>().add(ChangePhoneNum(
                                    checkMobileNumber(
                                        phoneController.value.text),
                                    widget.countryCode??"+20"));
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
