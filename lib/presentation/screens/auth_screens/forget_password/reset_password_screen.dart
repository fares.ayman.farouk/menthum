import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:patientapp/application/constants.dart';

import 'package:patientapp/application/di.dart';
import 'package:patientapp/application/extensions.dart';
import 'package:patientapp/application/utils.dart';

import 'package:patientapp/data/models/apiError/reset_password_api_error.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/reset_password_request.dart';
import 'package:patientapp/data/repository/auth_repository_imp.dart';
import 'package:patientapp/domain/business_logic/auth/forget_password/forget_password_bloc.dart';
import 'package:patientapp/presentation/resources/routes_manger.dart';
import 'package:patientapp/presentation/widgets/custom_dialog.dart';
import '../../../../data/models/apiError/login_api_error.dart';
import '../../../../data/models/apiRequest/auth_requests/forget_password_request.dart';
import '../../../../data/models/apiRequest/auth_requests/resend_request.dart';
import '../../../../data/network/error_handler.dart';
import '../../../../domain/business_logic/auth/otp/otp_bloc.dart';
import '../../../resources/assets_manager.dart';
import '../../../resources/font_manger.dart';
import '../../../resources/language_manager.dart';
import '../../../resources/strings_manger.dart';
import '../../../resources/values_manger.dart';
import '../../../widgets/app_rounded_btn.dart';
import '../../../widgets/auth_text_field.dart';
import '../../../widgets/resend_code.dart';

class ResetPasswordScreen extends StatefulWidget {
  final String countryCode;
  final String mobile;

  const ResetPasswordScreen({
    required this.countryCode,
    required this.mobile,
    super.key,
  });

  @override
  State<ResetPasswordScreen> createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {
  String errorCode = "", errorPassword = "";

  TextEditingController codeController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  TextEditingController confirmController = TextEditingController();

  AuthRepositoryImp authRepositoryImp = instance<AuthRepositoryImp>();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PasswordBloc>(
      create: (context) => PasswordBloc(authRepositoryImp),
      child: BlocConsumer<PasswordBloc, PasswordStates>(
        listener: (context, state) {
          if (state is ResetPasswordLoading || state is ForgetPasswordLoading) {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return CustomDialog(
                      color: Colors.transparent,
                      borderRadius: AppSize.s20,
                      contentBox: Center(
                        child: Lottie.asset(
                          JsonAssets.loadingAnimations,
                        ),
                      ));
                });
          } else if (state is ResetPasswordSuccess) {
            Navigator.pop(context);
            Navigator.pushNamed(context, Routes.authScreen);
          } else if (state is ResetPasswordFailed) {
            Navigator.pop(context);
            handleApiError(context: context, failure: state.failure);

            if (state.failure.code == ResponseCode.VALIDATION_ERROR) {
              ResetPasswordApiError r =
                  resetPasswordApiErrorFromJson(state.failure.body);
              if (r.errors != null) {
                errorCode = concatenateListOfStrings(r.errors!.code!);
                errorPassword = concatenateListOfStrings(r.errors!.password!);
              }
            }
          } else if (state is ForgetPasswordSuccess) {
            Navigator.pop(context);
          } else if (state is ForgetPasswordFailed) {
            Navigator.pop(context);

            handleApiError(context: context, failure: state.failure);
            if (state.failure.code == ResponseCode.VALIDATION_ERROR) {
              LoginApiErrorResponse r =
                  loginApiErrorResponseFromJson(state.failure.body.toString());
              final snackBar = SnackBar(
                content: Text(r.message.toString()),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            }
          }
        },
        builder: (context, state) {
          return SafeArea(
            child: Scaffold(
              backgroundColor: Theme.of(context).backgroundColor,
              body: SizedBox.expand(
                child: SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        const SizedBox(
                          height: AppSize.s20,
                        ),
                        Align(
                            alignment: isRtl(context)
                                ? Alignment.centerRight
                                : Alignment.centerLeft,
                            child: IconButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              icon: const Padding(
                                padding: EdgeInsets.only(
                                    right: AppPadding.p20,
                                    left: AppPadding.p20),
                                child: Icon(
                                  Icons.arrow_back_ios,
                                ),
                              ),
                            )),
                        const SizedBox(
                          height: AppSize.s20,
                        ),
                        SvgPicture.asset(ImageAssets.authLogo),
                        const SizedBox(
                          height: AppSize.s14,
                        ),
                        Text(
                          AppStrings.resetPassword.tr(),
                          textAlign: TextAlign.center,
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium
                              ?.copyWith(fontSize: FontSize.s26),
                        ),
                        const SizedBox(
                          height: AppSize.s40,
                        ),
                        AuthTextField(
                          hintText: AppStrings.code.tr(),
                          maxLength: 6,
                          keyboardType: TextInputType.number,
                          obscureText: false,
                          errorText: errorCode.isEmpty ? null : errorCode,
                          controller: codeController,
                          validator: (value) {},
                        ),
                        ResendCode(
                          onPress: () {
                            context.read<PasswordBloc>().add(
                                  ForgetPasswordEvent(
                                    ForgetPasswordRequest(
                                      mobile: checkMobileNumber(widget.mobile),
                                      countryCode: widget.countryCode,
                                    ),
                                  ),
                                );
                          },
                          text: AppStrings.reSendCodeSms.tr(),
                        ),
                        const SizedBox(
                          height: AppSize.s20,
                        ),
                        AuthTextField(
                          hintText: AppStrings.newPassword.tr(),
                          keyboardType: TextInputType.text,
                          maxLength: 16,
                          obscureText: true,
                          errorText:
                              errorPassword.isEmpty ? null : errorPassword,
                          controller: passwordController,
                          validator: (value) {},
                        ),
                        const SizedBox(
                          height: AppSize.s20,
                        ),
                        AuthTextField(
                          hintText: AppStrings.confirmPassword.tr(),
                          keyboardType: TextInputType.text,
                          obscureText: true,
                          maxLength: 16,
                          controller: confirmController,
                          validator: (value) {},
                        ),
                        const SizedBox(
                          height: AppSize.s20,
                        ),
                        AppRoundedButton(
                          text: AppStrings.changePassword.tr(),
                          onPressed: () {
                            errorCode = "";
                            errorPassword = "";
                            context.read<PasswordBloc>().add(
                                  ResetPasswordEvent(
                                    ResetPasswordRequest(
                                        code: codeController.value.text,
                                        countryCode: widget.countryCode,
                                        mobile: widget.mobile,
                                        password: passwordController.value.text,
                                        passwordConfirmation:
                                            confirmController.value.text),
                                  ),
                                );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget resendCode(context) {
    return BlocConsumer<OTPBloc, OTPState>(listener: (context, state) {
      if (state is LoadingOTPState) {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return CustomDialog(
                  color: Colors.transparent,
                  borderRadius: AppSize.s20,
                  contentBox: Center(
                    child: Lottie.asset(
                      height: context.height * .2,
                      width: context.width * .2,
                      JsonAssets.loadingAnimations,
                    ),
                  ));
            });
      } else if (state is FailResendOTPState) {
        Navigator.pop(context);
        final snackBar =
            SnackBar(content: Text(state.failure.message.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      } else if (state is SuccessResendOTPState) {
        Navigator.pop(context);
      }
    }, builder: (context, state) {
      return Container(
        child: Column(
          children: [
            ResendCode(
              onPress: () {
                context.read<OTPBloc>().add(ResendOTPEvent(
                      ResendOtpRequest(
                        sendVia: AppConstants.sms,
                        countryCode: widget.countryCode,
                        mobile: checkMobileNumber(
                          widget.mobile,
                        ),
                      ),
                    ));
              },
              text: AppStrings.reSendCodeSms.tr(),
            ),
            ResendCode(
              onPress: () {
                context.read<OTPBloc>().add(ResendOTPEvent(
                      ResendOtpRequest(
                        sendVia: AppConstants.whatsapp,
                        countryCode: widget.countryCode,
                        mobile: checkMobileNumber(
                          widget.mobile,
                        ),
                      ),
                    ));
              },
              text: AppStrings.reSendCodeWhatsapp.tr(),
            ),
          ],
        ),
      );
    });
  }
}
