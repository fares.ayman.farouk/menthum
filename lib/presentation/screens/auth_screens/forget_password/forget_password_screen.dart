import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart' as E;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/forget_password_request.dart';
import 'package:patientapp/data/network/error_handler.dart';
import 'package:patientapp/presentation/resources/language_manager.dart';
import 'package:patientapp/presentation/resources/routes_manger.dart';
import '../../../../application/di.dart';
import '../../../../application/utils.dart';
import '../../../../data/models/apiError/login_api_error.dart';
import '../../../../data/repository/auth_repository_imp.dart';
import '../../../../domain/business_logic/auth/forget_password/forget_password_bloc.dart';
import '../../../resources/assets_manager.dart';
import '../../../resources/color_manger.dart';
import '../../../resources/font_manger.dart';
import '../../../resources/strings_manger.dart';
import '../../../resources/values_manger.dart';
import '../../../widgets/auth_country_picker.dart';
import '../../../widgets/app_rounded_btn.dart';
import '../../../widgets/auth_text_field.dart';
import '../../../widgets/custom_dialog.dart';

class ForgetPasswordScreen extends StatelessWidget {
  ForgetPasswordScreen({Key? key}) : super(key: key);

  TextEditingController phoneController = TextEditingController();
  AuthRepositoryImp authRepositoryImp = instance<AuthRepositoryImp>();
  String countryCode = "+20";
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => PasswordBloc(authRepositoryImp),
      child: BlocConsumer<PasswordBloc, PasswordStates>(
        listener: (context, state) {
          if (state is ForgetPasswordLoading) {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return CustomDialog(
                      color: Colors.transparent,
                      borderRadius: AppSize.s20,
                      contentBox: Center(
                        child: Lottie.asset(
                          JsonAssets.loadingAnimations,
                        ),
                      ));
                });
          } else if (state is ForgetPasswordSuccess) {
            Navigator.pop(context);
            Navigator.pushNamed(context, Routes.resetPasswordScreen,
                arguments: {
                  AppStrings.code: countryCode,
                  AppStrings.phoneNumber:
                      checkMobileNumber(phoneController.value.text),
                });
          } else if (state is ForgetPasswordFailed) {
            Navigator.pop(context);
            if (state.failure.code == ResponseCode.NO_INTERNET_CONNECTION) {
              showToast(
                  message: AppStrings.noInternetConnection.tr(),
                  success: false);
              return;
            } else {
              LoginApiErrorResponse r =
                  loginApiErrorResponseFromJson(state.failure.body.toString());
              final snackBar = SnackBar(
                content: Text(r.message.toString()),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            }
          }
        },
        builder: (context, state) {
          return SafeArea(
            child: Scaffold(
              backgroundColor: Theme.of(context).backgroundColor,
              body: Container(
                width: double.infinity,
                height: double.infinity,
                child: SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        const SizedBox(
                          height: AppSize.s20,
                        ),
                        Align(
                            alignment: isRtl(context)
                                ? Alignment.centerRight
                                : Alignment.centerLeft,
                            child: IconButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              icon: const Padding(
                                padding: EdgeInsets.only(
                                    right: AppPadding.p20,
                                    left: AppPadding.p20),
                                child: Icon(
                                  Icons.arrow_back_ios,
                                ),
                              ),
                            )),
                        const SizedBox(
                          height: AppSize.s20,
                        ),
                        SvgPicture.asset(ImageAssets.authLogo),
                        const SizedBox(
                          height: AppSize.s14,
                        ),
                        Text(
                          AppStrings.labelForgetPassword.tr(),
                          textAlign: TextAlign.center,
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium
                              ?.copyWith(fontSize: FontSize.s26),
                        ),
                        const SizedBox(
                          height: AppSize.s40,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: AppPadding.p24,
                            right: AppPadding.p24,
                          ),
                          child: Text(
                            AppStrings.labelWritePhone.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                    color: ColorManager.textColor,
                                    fontSize: FontSize.s14),
                          ),
                        ),
                        const SizedBox(
                          height: AppSize.s16,
                        ),
                        Directionality(
                          textDirection: TextDirection.ltr,
                          child: AuthTextField(
                            hintText: AppStrings.phoneNumber.tr(),
                            keyboardType: TextInputType.phone,
                            maxLength: 11,
                            obscureText: false,
                            controller: phoneController,
                            prefixIcon: CountryPickerWidget(
                              getCountryCode: (value) {
                                countryCode = value;
                              },
                            ),
                            validator: (value) {},
                          ),
                        ),
                        const SizedBox(
                          height: AppSize.s20,
                        ),
                        AppRoundedButton(
                          text: AppStrings.submit.tr(),
                          onPressed: () {
                            context.read<PasswordBloc>().add(
                                  ForgetPasswordEvent(
                                    ForgetPasswordRequest(
                                      mobile: checkMobileNumber(
                                          phoneController.value.text),
                                      countryCode: countryCode,
                                    ),
                                  ),
                                );
                            // Navigator.pushNamed(context, Routes.resetPasswordScreen);
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
