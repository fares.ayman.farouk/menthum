import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/application/utils.dart';
import 'package:patientapp/data/models/apiError/reset_password_api_error.dart';
import 'package:patientapp/data/repository/auth_repository_imp.dart';
import 'package:patientapp/domain/business_logic/auth/forget_password/forget_password_bloc.dart';
import 'package:patientapp/presentation/resources/assets_manager.dart';
import 'package:patientapp/presentation/resources/routes_manger.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';
import 'package:patientapp/presentation/widgets/appbar_design.dart';
import 'package:patientapp/presentation/widgets/app_rounded_btn.dart';
import 'package:patientapp/presentation/widgets/auth_text_field.dart';
import 'package:patientapp/presentation/widgets/custom_dialog.dart';

import '../../../../domain/models/main_screen_tabs.dart';

class SetPasswordScreen extends StatefulWidget {
  const SetPasswordScreen({Key? key}) : super(key: key);

  @override
  State<SetPasswordScreen> createState() => _SetPasswordScreenState();
}

class _SetPasswordScreenState extends State<SetPasswordScreen> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController passwordController = TextEditingController();

  TextEditingController confirmController = TextEditingController();
  String errorPassword = "";
  AuthRepositoryImp authRepositoryImp = instance<AuthRepositoryImp>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarDesign(
          title: AppStrings.setPassword.tr(),
          hasActionButton: false,
          isBackButton: false),
      backgroundColor: Theme.of(context).backgroundColor,
      body: WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: SizedBox.expand(
          child: SingleChildScrollView(
            child: BlocProvider(
              create: (context) => PasswordBloc(authRepositoryImp),
              child: BlocConsumer<PasswordBloc, PasswordStates>(
                listener: (context, state) {
                  if (state is SetPasswordLoading) {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return CustomDialog(
                              color: Colors.transparent,
                              borderRadius: AppSize.s20,
                              contentBox: Center(
                                child: Lottie.asset(
                                  JsonAssets.loadingAnimations,
                                ),
                              ));
                        });
                  } else if (state is SetPasswordSuccess) {
                    Navigator.pop(context);
                    Navigator.pushReplacementNamed(context, Routes.mainScreen,
                     arguments:  MainTabs.home.index);
                  } else if (state is SetPasswordFailed) {
                    Navigator.pop(context);
                    ResetPasswordApiError r =
                        resetPasswordApiErrorFromJson(state.failure.body);
                    if (r.errors != null) {
                      errorPassword =
                          concatenateListOfStrings(r.errors!.password!);
                    }

                    final snackBar = SnackBar(
                      content: Text(r.message.toString()),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                },
                builder: (context, state) {
                  return Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        const SizedBox(
                          height: AppSize.s20,
                        ),
                        SvgPicture.asset(ImageAssets.authLogo),
                        const SizedBox(
                          height: AppSize.s20,
                        ),
                        AuthTextField(
                          hintText: AppStrings.newPassword.tr(),
                          maxLength: 16,
                          keyboardType: TextInputType.text,
                          obscureText: true,
                          errorText:
                              errorPassword.isEmpty ? null : errorPassword,
                          controller: passwordController,
                          validator: (value) {},
                        ),
                        const SizedBox(
                          height: AppSize.s20,
                        ),
                        AuthTextField(
                          hintText: AppStrings.confirmPassword.tr(),
                          keyboardType: TextInputType.text,
                          maxLength: 16,
                          obscureText: true,
                          controller: confirmController,
                          validator: (value) {},
                        ),
                        const SizedBox(
                          height: AppSize.s20,
                        ),
                        AppRoundedButton(
                          text: AppStrings.setPassword.tr(),
                          onPressed: () {
                            errorPassword = "";
                            context.read<PasswordBloc>().add(
                                  SetPasswordEvent(
                                    password: passwordController.value.text,
                                    confirmPassword:
                                        confirmController.value.text,
                                  ),
                                );
                          },
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}
