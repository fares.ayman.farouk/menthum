import 'dart:developer';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:patientapp/application/extensions.dart';
import 'package:patientapp/application/utils.dart';
import 'package:patientapp/data/models/apiError/login_api_error.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/verify_request.dart';
import 'package:patientapp/data/models/apiResponse/account_not_verified_response.dart';
import 'package:patientapp/data/network/error_handler.dart';
import 'package:patientapp/domain/models/main_screen_tabs.dart';
import 'package:patientapp/presentation/resources/routes_manger.dart';
import 'package:patientapp/presentation/widgets/custom_dialog.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import '../../../../application/constants.dart';
import '../../../../application/di.dart';
import '../../../../data/repository/auth_repository_imp.dart';
import '../../../../domain/business_logic/auth/login/login_bloc.dart';
import '../../../../domain/services/analytics_services/analytics_services.dart';
import '../../../resources/assets_manager.dart';
import '../../../resources/color_manger.dart';
import '../../../resources/strings_manger.dart';
import '../../../resources/values_manger.dart';
import '../../../widgets/auth_social.dart';
import 'dart:io' show Platform;

class SocialAuthentications extends StatefulWidget {
  const SocialAuthentications({Key? key}) : super(key: key);

  @override
  State<SocialAuthentications> createState() => _SocialAuthenticationsState();
}

class _SocialAuthenticationsState extends State<SocialAuthentications> {
  AuthRepositoryImp loginRepository = instance<AuthRepositoryImp>();
  AnalyticsServices analyticsServices = instance<AnalyticsServices>();

  ///Qr code scanner
  int canningCounter = 0;

  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  Barcode? result;
  late QRViewController controller;
  late LoginBloc loginBloc;

  @override
  void initState() {
    loginBloc = LoginBloc(loginRepository);
  }

  @override
  void dispose() {
    super.dispose();
    try {
      if (controller != null) controller.dispose();
    } catch (e) {
      print("$e");
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => loginBloc,
      child: BlocConsumer<LoginBloc, LoginState>(
        listener: (context, state) async {
          log(state.toString());
          if (state is LoadingLoginState) {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return CustomDialog(
                      color: Colors.transparent,
                      borderRadius: AppSize.s20,
                      contentBox: Center(
                        child: Lottie.asset(
                          JsonAssets.loadingAnimations,
                        ),
                      ));
                });
          }

          /// sucess login
          else if (state is SuccessLoginState) {
            Navigator.pop(context);
            if (state.loginResponse.data?.hasPassword == false) {
              Navigator.pushReplacementNamed(context, Routes.setPassword);
            } else {
              Navigator.pushReplacementNamed(context, Routes.mainScreen,
                  arguments: MainTabs.home.index);
            }
          }

          /// failure
          else if (state is FailLoginState) {
            print("################${state.failure.code}");
            Navigator.pop(context);
            canningCounter = 0;

            ///  no connection
            if (state.failure.code == ResponseCode.NO_INTERNET_CONNECTION) {
              showToast(
                  message: AppStrings.noInternetConnection.tr(),
                  success: false);
              return;
            } else if (state.failure.code == ResponseCode.DEFAULT) {
              showToast(message: state.failure.message!, success: false);
            } else {
              LoginApiErrorResponse r =
                  loginApiErrorResponseFromJson(state.failure.body.toString());

              final snackBar = SnackBar(
                content: Text('${r.message.toString()}'),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);

              /// go to verify account
              if (state.failure.code == ResponseCode.ACCOUNT_NOT_VERIFIED) {
                ///get user data.
                AccountNotVerifiedResponse userData =
                    accountNotVerifiedResponseFromJson(state.failure.body);

                ///save user data and token.
                await appPreferences.addUser(userData.data);
                await appPreferences
                    .addParentUserToken(userData.data?.token ?? "");
                print("!@#!@#${userData.data?.token}");

                Navigator.pushNamed(context, Routes.otpScreen,
                    arguments: VerifyRequest(
                        mobile: userData.data?.mobile ?? "",
                        countryCode: userData.data?.countryCode ?? "+20",
                        code: ''));
              }
            }
          } else if (state is SuccessLoginProviderState) {
            Navigator.pop(context);
            if (state.response.data?.isMobileVerified != true) {
              ///save user data and token.
              await appPreferences.addUser(state.response.data);
              await appPreferences
                  .addParentUserToken(state.response.data?.token ?? "");
              print("!@#!@#${state.response.data?.token}");
              if (state.response.data?.phone != null &&
                  state.response.data?.phone?.isEmpty == true) {
                print("Routes.changePhone");

                Navigator.pushReplacementNamed(context, Routes.changePhone,
                    arguments: {
                      "userPhone": state.response.data?.phone ?? "",
                      "countryCode": state.response.data?.countryCode ?? "",
                      "popScreen": true
                    });
              } else {
                print("Routes.otpScreen");

                Navigator.pushNamed(context, Routes.otpScreen,
                    arguments: VerifyRequest(
                        mobile: state.response.data?.phone ?? "",
                        countryCode: state.response.data?.countryCode ?? "",
                        code: ""));
              }
            } else {
              print("Routes.mainScreen");
              Navigator.pushReplacementNamed(context, Routes.mainScreen);
            }
          }
        },
        builder: (context, state) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              AuthSocial(
                text: AppStrings.labelScanQrCode.tr(),
                socialIcon:
                    SvgPicture.asset(IconAssets.qrCode, height: AppSize.s24),
                onTap: () async {
                  showQrCodeScanner(context);
                },
              ),
              const SizedBox(
                height: AppSize.s20,
              ),
              AuthSocial(
                text: AppStrings.labelLoginGoogle.tr(),
                socialIcon: SvgPicture.asset(ImageAssets.google),
                onTap: () {
                  /// Send Event...
                  analyticsServices.addEventAnalytics(
                      eventName: EventsAnalytics.googleButtonPressedEvent,
                      parameterName: EventsAnalytics.googleLoginTapParameter);

                  context.read<LoginBloc>().add(LoginWithGoogleEvent());
                },
              ),
              const SizedBox(
                height: AppSize.s20,
              ),

              /// apple
              Visibility(
                visible: Platform.isIOS,
                child: Padding(
                  padding: const EdgeInsets.only(
                    bottom: AppSize.s30,
                  ),
                  child: AuthSocial(
                    text: AppStrings.labelLoginApple.tr(),
                    socialIcon: Icon(
                      Icons.apple,
                      size: 30,
                      color: ColorManager.blackColor,
                    ),
                    onTap: () {
                      /// Send Event...
                      analyticsServices.addEventAnalytics(
                          eventName: EventsAnalytics.appleButtonPressedEvent,
                          parameterName:
                              EventsAnalytics.appleLoginTapParameter);

                      context.read<LoginBloc>().add(LoginWithAppleEvent());
                    },
                  ),
                ),
              ),

              AuthSocial(
                text: AppStrings.labelLoginFacebook.tr(),
                socialIcon: SvgPicture.asset(ImageAssets.facebook),
                onTap: () {
                  /// Send Event...
                  analyticsServices.addEventAnalytics(
                      eventName:
                          EventsAnalytics.facebookLoginButtonPressedEvent,
                      parameterName: EventsAnalytics.facebookLoginTapParameter);
                  context.read<LoginBloc>().add(LoginWithFacebookEvent());
                },
              ),
              const SizedBox(
                height: AppSize.s30,
              ),
            ],
          );
        },
      ),
    );
  }

  int getLengthOfToken(String token) {
    List<String> data = token.split('|');
    if (data.length > 1) return data[1].length;
    return 0;
  }

  void showQrCodeScanner(context) async {
    showDialog(
        context: context,
        builder: (context) {
          return SimpleDialog(
            title: Row(
              children: [
                SvgPicture.asset(IconAssets.qrCode, height: AppSize.s24),
                const SizedBox(
                  width: AppSize.s16,
                ),
                Text(
                  AppStrings.labelScanQrCode.tr(),
                  style: TextStyle(fontSize: AppSize.s18),
                ),
              ],
            ),
            children: [
              SizedBox(
                height: context.height * .3,
                width: 300,
                child: StatefulBuilder(builder: (context, StateSetter setS) {
                  return QRView(
                      key: qrKey,
                      onQRViewCreated: (QRViewController controller) {
                        this.controller = controller;
                        controller.resumeCamera();
                        controller.scannedDataStream
                            .listen((scanData) {})
                            .onData((data) {
                          int length = getLengthOfToken(data.code ?? "");
                          if (data.code != null &&
                              length == AppKeyConstants.lengthOfToken &&
                              canningCounter == 0) {
                            canningCounter++;
                            if (canningCounter == 1) {
                              Navigator.pop(context);
                              loginBloc.add(LoginWithQrCodeWithFacebookEvent(
                                  token: "${data.code}"));
                            }
                          }
                        });
                      });
                }),
              ),
            ],
          );
        }).whenComplete(() {
      //controller.dispose();
    });
  }
}
