import 'package:easy_localization/easy_localization.dart' as E;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/application/constants.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/application/extensions.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/verify_request.dart';
import 'package:patientapp/data/repository/auth_repository_imp.dart';
import 'package:patientapp/domain/business_logic/auth/otp/otp_bloc.dart';
import 'package:patientapp/presentation/resources/routes_manger.dart';
import 'package:patientapp/presentation/widgets/auth_country_picker.dart';
import 'package:patientapp/presentation/widgets/custom_dialog.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import '../../../../application/utils.dart';
import '../../../../data/models/apiRequest/auth_requests/resend_request.dart';
import '../../../../domain/models/main_screen_tabs.dart';
import '../../../../domain/services/analytics_services/analytics_services.dart';
import '../../../resources/assets_manager.dart';
import '../../../resources/color_manger.dart';
import '../../../resources/font_manger.dart';
import '../../../resources/strings_manger.dart';
import '../../../resources/values_manger.dart';
import '../../../widgets/app_rounded_btn.dart';
import '../../../widgets/resend_code.dart';

class OtpScreen extends StatefulWidget {
  final VerifyRequest verifyRequest;

  const OtpScreen({required this.verifyRequest});

  @override
  State<OtpScreen> createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  final _formKey = GlobalKey<FormState>();
  AuthRepositoryImp authRepository = instance<AuthRepositoryImp>();
  AppPreferences appPreferences = instance<AppPreferences>();

  bool shouldPop = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return shouldPop;
      },
      child: SafeArea(
        child: BlocProvider(
          create: (context) => OTPBloc(authRepository)
            ..add(ResendOTPEvent(ResendOtpRequest(
                sendVia: AppConstants.sms,
                countryCode: widget.verifyRequest.countryCode,
                mobile: checkMobileNumber(widget.verifyRequest.mobile)))),
          child: BlocConsumer<OTPBloc, OTPState>(
            listener: (context, state) {
              if (state is LoadingOTPState) {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return CustomDialog(
                          color: Colors.transparent,
                          borderRadius: AppSize.s20,
                          contentBox: Center(
                            child: Lottie.asset(
                              height: context.height * .2,
                              width: context.width * .2,
                              JsonAssets.loadingAnimations,
                            ),
                          ));
                    });
              } else if (state is SuccessVerifyOTPState) {
                Navigator.pop(context);
                appPreferences.addUser(state.verifyResponse.data);

                /// Send Event...
                instance<AnalyticsServices>().addEventAnalytics(
                    eventName: EventsAnalytics.verifyAccountSuccessfully,
                    parameterName: EventsAnalytics.getVerifiedSuccessfully);

                Navigator.pushReplacementNamed(
                  context,
                  Routes.mainScreen,
                  arguments: MainTabs.home.index,
                );
              } else if (state is FailVerifyOTPState) {
                Navigator.pop(context);
                final snackBar =
                    SnackBar(content: Text(state.failure.message.toString()));
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }

              /// failed resend otp
              else if (state is FailResendOTPState) {
                Navigator.pop(context);
                final snackBar =
                    SnackBar(content: Text(state.failure.message.toString()));
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else if (state is SuccessResendOTPState) {
                Navigator.pop(context);
              }
            },
            builder: (context, state) {
              print("STAT build:::::: $state");

              return Scaffold(
                backgroundColor: Theme.of(context).backgroundColor,
                body: Container(
                  width: double.infinity,
                  height: double.infinity,
                  child: SingleChildScrollView(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          const SizedBox(
                            height: AppSize.s20,
                          ),
                          const SizedBox(
                            height: AppSize.s20,
                          ),
                          SvgPicture.asset(ImageAssets.authLogo),
                          const SizedBox(
                            height: AppSize.s14,
                          ),
                          Text(
                            AppStrings.labelEnterOtp.tr(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(fontSize: FontSize.s26),
                          ),
                          const SizedBox(
                            height: AppSize.s40,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: AppPadding.p20),
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: TextButton(
                                onPressed: () {
                                  print(
                                      "Phone::${widget.verifyRequest.mobile}");
                                  Navigator.pushNamed(
                                      context, Routes.changePhone,
                                      arguments: {
                                        "userPhone":
                                            widget.verifyRequest.mobile,
                                        "countryCode":
                                            widget.verifyRequest.countryCode,
                                        "popScreen": true,
                                      });
                                },
                                child: Text(
                                  AppStrings.changeMobileNumber.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium
                                      ?.copyWith(
                                          color: ColorManager.textColor,
                                          fontSize: FontSize.s14),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              left: AppPadding.p24,
                              right: AppPadding.p24,
                              top: AppPadding.p24,
                            ),
                            child: Text(
                              AppStrings.labelPleaseEnterOtp.tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(
                                      color: ColorManager.textColor,
                                      fontSize: FontSize.s14),
                            ),
                          ),
                          const SizedBox(
                            height: AppSize.s16,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: AppPadding.p20),
                            child: Directionality(
                              textDirection: TextDirection.ltr,
                              child: PinCodeTextField(
                                keyboardType: TextInputType.number,
                                pinTheme: PinTheme(
                                  shape: PinCodeFieldShape.box,
                                  borderRadius: BorderRadius.circular(5),
                                  fieldHeight: 50,
                                  fieldWidth: 40,
                                  activeFillColor: Colors.white,
                                ),
                                length: 6,
                                appContext: context,
                                onChanged: (value) {
                                  debugPrint(value);
                                  widget.verifyRequest.code = value;
                                  print(widget.verifyRequest.code);
                                },
                                onCompleted: (v) {
                                  print("%% Completed: " + v);
                                  print(widget.verifyRequest.code);
                                  setState(() {});
                                  debugPrint("Completed");
                                },
                              ),
                            ),
                          ),
                          ResendCode(
                            onPress: () {
                              context.read<OTPBloc>().add(
                                    ResendOTPEvent(
                                      ResendOtpRequest(
                                        sendVia: AppConstants.sms,
                                        countryCode:
                                            widget.verifyRequest.countryCode,
                                        mobile: checkMobileNumber(
                                            widget.verifyRequest.mobile),
                                      ),
                                    ),
                                  );
                            },
                            text: AppStrings.reSendCodeSms.tr(),
                          ),
                          ResendCode(
                            onPress: () {
                              context.read<OTPBloc>().add(
                                    ResendOTPEvent(
                                      ResendOtpRequest(
                                        sendVia: AppConstants.whatsapp,
                                        countryCode:
                                            widget.verifyRequest.countryCode,
                                        mobile: checkMobileNumber(
                                            widget.verifyRequest.mobile),
                                      ),
                                    ),
                                  );
                            },
                            text: AppStrings.reSendCodeWhatsapp.tr(),
                          ),
                          AppRoundedButton(
                            text: AppStrings.submit.tr(),
                            onPressed: widget.verifyRequest.code.length == 6
                                ? () {
                                    widget.verifyRequest.mobile =
                                        checkMobileNumber(
                                            widget.verifyRequest.mobile);
                                    context.read<OTPBloc>().add(VerifyOTPEvent(
                                        verifyRequest: widget.verifyRequest));
                                  }
                                : null,
                          ),
                          const SizedBox(
                            height: AppSize.s24,
                          ),
                          AppRoundedButton(
                            text: AppStrings.switchAccount.tr(),
                            onPressed: () {
                              ///remove data and go to login
                              appPreferences.removeUser();
                              Navigator.pushReplacementNamed(
                                  context, Routes.authScreen);
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
