import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart' as E;
import 'package:lottie/lottie.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:patientapp/application/constants.dart';

import 'package:patientapp/data/models/apiRequest/auth_requests/login_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/verify_request.dart';
import 'package:patientapp/data/network/error_handler.dart';
import 'package:patientapp/domain/business_logic/auth/login/login_bloc.dart';
import 'package:patientapp/domain/services/analytics_services/analytics_services.dart';
import '../../../../application/di.dart';
import '../../../../application/utils.dart';
import '../../../../data/models/apiError/login_api_error.dart';
import '../../../../data/models/apiResponse/account_not_verified_response.dart';
import '../../../../data/repository/auth_repository_imp.dart';
import '../../../../domain/models/main_screen_tabs.dart';
import '../../../../domain/services/push_notification/firebase_notification.dart';
import '../../../resources/assets_manager.dart';
import '../../../resources/color_manger.dart';
import '../../../resources/font_manger.dart';
import '../../../resources/routes_manger.dart';
import '../../../resources/strings_manger.dart';
import '../../../resources/values_manger.dart';
import '../../../widgets/app_rounded_btn.dart';
import '../../../widgets/auth_country_picker.dart';
import '../../../widgets/auth_text_field.dart';
import '../../../widgets/custom_dialog.dart';
import '../widgets/social_authentications.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  String countryCode = "+20", errorPhone = "", errorPassword = "";
  final _formKey = GlobalKey<FormState>();

  AuthRepositoryImp loginRepository = instance<AuthRepositoryImp>();
  AnalyticsServices analyticsServices = instance<AnalyticsServices>();

  late LoginBloc loginBloc;

  @override
  void initState() {
    loginBloc = LoginBloc(loginRepository);

    analyticsServices.addEventAnalytics(
        eventName: EventsAnalytics.loginScreenViewEvent,
        parameterName: EventsAnalytics.loginScreenParameter);
    analyticsServices.addScreenTrackingAnalytics(
        parameterScreenClass: EventsAnalytics.loginClassName,
        parameterScreenName: EventsAnalytics.loginScreenName);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: BlocProvider(
        create: (context) => loginBloc,
        child: BlocConsumer<LoginBloc, LoginState>(
          listener: (context, state) async {
            /// loading
            if (state is LoadingLoginState) {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return CustomDialog(
                        color: Colors.transparent,
                        borderRadius: AppSize.s20,
                        contentBox: Center(
                          child: Lottie.asset(
                            JsonAssets.loadingAnimations,
                          ),
                        ));
                  });
            }

            /// sucess login
            else if (state is SuccessLoginState) {
              Navigator.pop(context);
              if (state.loginResponse.data?.hasPassword == false) {
                Navigator.pushReplacementNamed(context, Routes.setPassword);
              } else {
                Navigator.pushReplacementNamed(context, Routes.mainScreen,
                    arguments: MainTabs.home.index);
              }
            }

            ///  Wrong Credentials Login
            else if (state is WrongCredentialsLoginState) {
              Navigator.pop(context);
              final snackBar = SnackBar(
                content: Text(state.msg ?? ""),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            }

            /// failure
            else if (state is FailLoginState) {
              print("################");
              Navigator.pop(context);

              ///  no connection
              if (state.failure.code == ResponseCode.NO_INTERNET_CONNECTION) {
                showToast(
                    message: AppStrings.noInternetConnection.tr(),
                    success: false);
                return;
              } else if (state.failure.code == ResponseCode.DEFAULT) {
                showToast(message: state.failure.message!, success: false);
              } else {
                LoginApiErrorResponse r = loginApiErrorResponseFromJson(
                    state.failure.body.toString());

                if (r.errors != null) {
                  errorPhone = concatenateListOfStrings(r.errors!.mobile!);
                  errorPassword = concatenateListOfStrings(r.errors!.password!);
                }

                final snackBar = SnackBar(
                  content: Text('${r.message.toString()}'),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);

                /// go to verify account
                if (state.failure.code == ResponseCode.ACCOUNT_NOT_VERIFIED) {
                  ///get user data.
                  AccountNotVerifiedResponse userData =
                      accountNotVerifiedResponseFromJson(state.failure.body);

                  ///save user data and token.
                  await appPreferences.addUser(userData.data);
                  await appPreferences
                      .addParentUserToken(userData.data?.token ?? "");
                  print("!@#!@#${userData.data?.token}");

                  Navigator.pushNamed(context, Routes.otpScreen,
                      arguments: VerifyRequest(
                          mobile: phoneController.text,
                          countryCode: countryCode,
                          code: ''));
                }
              }
            } else if (state is SuccessLoginProviderState) {
              Navigator.pop(context);
              if (state.response.data?.isMobileVerified != true) {
                ///save user data and token.
                await appPreferences.addUser(state.response.data);
                await appPreferences
                    .addParentUserToken(state.response.data?.token ?? "");
                print("!@#!@#${state.response.data?.token}");
                if (state.response.data?.phone != null &&
                    state.response.data?.phone?.isEmpty == true) {
                  print("Routes.changePhone");

                  Navigator.pushReplacementNamed(context, Routes.changePhone,
                      arguments: {
                        "userPhone": state.response.data?.phone ?? "",
                        "countryCode": state.response.data?.countryCode ?? "",
                        "popScreen": true
                      });
                } else {
                  print("Routes.otpScreen");

                  Navigator.pushNamed(context, Routes.otpScreen,
                      arguments: VerifyRequest(
                          mobile: state.response.data?.phone ?? "",
                          countryCode: state.response.data?.countryCode ?? "",
                          code: ""));
                }
              } else {
                print("Routes.mainScreen");
                Navigator.pushReplacementNamed(context, Routes.mainScreen);
              }
            }
          },
          builder: (context, state) {
            return Scaffold(
              resizeToAvoidBottomInset: false,
              backgroundColor: Theme.of(context).backgroundColor,
              body: Container(
                width: double.infinity,
                height: double.infinity,
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: AppPadding.p16),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            const SizedBox(
                              height: AppSize.s20,
                            ),
                            Text(
                              "تسجيل الدخول باستخدام رمز التعريف الشخصي المكون من 6 ارقام",
                              textAlign: TextAlign.center,
                              maxLines: 10,
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(
                                    fontSize: AppSize.s16,
                                    color: ColorManager.primary,
                                  ),
                            ),
                            const SizedBox(
                              height: AppSize.s20,
                            ),
                            OTPTextField(
                              length: 6,
                              width: MediaQuery.of(context).size.width,
                              fieldWidth: 50,
                              style: TextStyle(fontSize: 17),
                              textFieldAlignment: MainAxisAlignment.spaceAround,
                              fieldStyle: FieldStyle.box,
                              onCompleted: (pin) {
                                Navigator.pushReplacementNamed(
                                    context, Routes.mainScreen);
                              },
                            ),
                            const SizedBox(
                              height: AppSize.s20,
                            ),
                            Text(
                              "او",
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(
                                      fontSize: AppSize.s16,
                                      fontWeight: FontWeightManager.bold),
                            ),
                            const SizedBox(
                              height: AppSize.s20,
                            ),
                            /* Directionality(
                              textDirection: TextDirection.ltr,
                              child: AuthTextField(
                                hintText: AppStrings.phoneNumber.tr(),
                                keyboardType: TextInputType.phone,
                                obscureText: false,
                                controller: phoneController,
                                maxLength: 11,
                                prefixIcon: CountryPickerWidget(
                                  getCountryCode: (value) {
                                    countryCode = value;
                                  },
                                ),
                                errorText: errorPhone.isEmpty ? null : errorPhone,
                                validator: (value) {},
                              ),
                            ),
                            const SizedBox(
                              height: AppSize.s20,
                            ),
                            Directionality(
                              textDirection: TextDirection.ltr,
                              child: AuthTextField(
                                hintText: AppStrings.password.tr(),
                                keyboardType: TextInputType.text,
                                obscureText: true,
                                controller: passwordController,
                                maxLength: 16,
                                suffixIcon: const Text(''),
                                errorText:
                                    errorPassword.isEmpty ? null : errorPassword,
                                validator: (value) {},
                              ),
                            ),
                            const SizedBox(
                              height: AppSize.s14,
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: AppPadding.p20),
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: TextButton(
                                  onPressed: () {
                                    Navigator.pushNamed(
                                        context, Routes.forgetPasswordScreen);
                                  },
                                  child: Text(
                                    AppStrings.labelForgetPassword.tr(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleMedium
                                        ?.copyWith(
                                            color: ColorManager.textColor,
                                            fontSize: FontSize.s14),
                                  ),
                                ),
                              ),
                            ),
                          */
                            AppRoundedButton(
                              onPressed: () async {
                                Navigator.pushReplacementNamed(
                                    context, Routes.mainScreen);
                              },
                              text: "استخدم البصمه البيومتريه",
                            ),
                            /*  const SizedBox(height: AppSize.s30),
                            const SocialAuthentications(), */
                            const SizedBox(
                              height: AppSize.s40,
                            ),
                            Text(
                              "المال و الامان مرتبطان",
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(
                                      fontSize: AppSize.s16,
                                      fontWeight: FontWeightManager.bold),
                            ),
                            const SizedBox(
                              height: AppSize.s20,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: AppSize.s80,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: ColorManager.white,
                            border: Border.all(
                              color: ColorManager.primary,
                            ),
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20))),
                        child: TextButton(
                          onPressed: () {},
                          child: Text(
                            "هل نسيت رمو التعرف الشخصي؟",
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s16,
                                  color: ColorManager.primary,
                                ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
