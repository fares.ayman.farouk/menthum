import 'package:easy_localization/easy_localization.dart' as E;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:lottie/lottie.dart';
import 'package:patientapp/application/constants.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/application/extensions.dart';
import 'package:patientapp/application/utils.dart';
import 'package:patientapp/data/models/apiError/register_api_error.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/register_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/verify_request.dart';
import 'package:patientapp/data/network/error_handler.dart';
import 'package:patientapp/data/repository/auth_repository_imp.dart';
import 'package:patientapp/domain/business_logic/auth/register/register_bloc.dart';
import 'package:patientapp/domain/services/analytics_services/analytics_services.dart';
import 'package:patientapp/presentation/resources/assets_manager.dart';
import 'package:patientapp/presentation/resources/font_manger.dart';
import 'package:patientapp/presentation/resources/routes_manger.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';
import 'package:patientapp/presentation/widgets/terms_conditions_popup.dart';
import 'package:patientapp/presentation/widgets/app_rounded_btn.dart';
import 'package:patientapp/presentation/widgets/auth_text_field.dart';
import 'package:patientapp/presentation/widgets/custom_dialog.dart';

import '../../../../domain/services/push_notification/firebase_notification.dart';
import '../../../resources/color_manger.dart';
import '../../../widgets/auth_country_picker.dart';
import '../widgets/social_authentications.dart';

class RegisterScreen extends StatefulWidget {
  RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController phoneController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  TextEditingController nameController = TextEditingController();

  TextEditingController emailController = TextEditingController();

  String countryCode = "+20",
      errorPhone = "",
      errorPassword = "",
      errorEmail = "",
      errorFullName = "";

  bool _checkedTerms = false;

  final _formKey = GlobalKey<FormState>();

  AnalyticsServices analyticsServices = instance<AnalyticsServices>();
  AuthRepositoryImp authRepository = instance<AuthRepositoryImp>();

  @override
  void initState() {
    analyticsServices.addEventAnalytics(
        eventName: EventsAnalytics.signUpScreenViewEvent,
        parameterName: EventsAnalytics.signUpScreenParameter);

    analyticsServices.addScreenTrackingAnalytics(
        parameterScreenClass: EventsAnalytics.signupClassName,
        parameterScreenName: EventsAnalytics.signupScreenName);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: BlocProvider(
        create: (context) => RegisterBloc(authRepository),
        child: BlocConsumer<RegisterBloc, RegisterState>(
          listener: (context, state) {
            if (state is LoadingRegisterState) {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return CustomDialog(
                        color: Colors.transparent,
                        borderRadius: AppSize.s20,
                        contentBox: Center(
                          child: Lottie.asset(
                            height: context.height * .2,
                            width: context.width * .2,
                            JsonAssets.loadingAnimations,
                          ),
                        ));
                  });
            } else if (state is SuccessRegisterState) {
              Navigator.pop(context);
              //TODO go to the Verify Screen.
              /// Save the user data.

              Navigator.pushReplacementNamed(context, Routes.otpScreen,
                  arguments: VerifyRequest(
                      mobile: phoneController.value.text,
                      countryCode: countryCode,
                      code: ""));
            } else if (state is FailRegisterState) {
              Navigator.pop(context);
              if (state.failure.code == ResponseCode.NO_INTERNET_CONNECTION) {
                // final snackBar = SnackBar(
                //   behavior: SnackBarBehavior.floating,
                //   margin: EdgeInsets.only(bottom: context.height * .915),
                //   content: Text(state.failure.message.toString()),
                //   backgroundColor: ColorManager.borderColor,
                //   duration: const Duration(seconds: 4),
                // );
                // ScaffoldMessenger.of(context).showSnackBar(snackBar);
                showToast(
                    message: AppStrings.noInternetConnection.tr(),
                    success: false);
                return;
              } else {
                RegisterApiErrorResponse r = registerApiErrorResponseFromJson(
                    state.failure.body.toString());
                if (r.errors != null) {
                  errorPhone = concatenateListOfStrings(r.errors!.mobile!);
                  errorFullName = concatenateListOfStrings(r.errors!.fullname!);
                  errorEmail = concatenateListOfStrings(r.errors!.email!);
                  errorPassword = concatenateListOfStrings(r.errors!.password!);
                }
                print("---------------------------------");
                print(state.failure.message);
                print(state.failure.code);
                print("---------------------------------");
                final snackBar = SnackBar(
                  content: Text(state.failure.code == 429
                      ? state.failure.message.toString()
                      : r.message.toString()),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            }
          },
          builder: (context, state) {
            return Scaffold(
              backgroundColor: Theme.of(context).backgroundColor,
              body: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      const SizedBox(
                        height: AppSize.s20,
                      ),
                      Directionality(
                        textDirection: TextDirection.ltr,
                        child: AuthTextField(
                          hintText: AppStrings.phoneNumber.tr(),
                          keyboardType: TextInputType.phone,
                          obscureText: false,
                          maxLength: 11,
                          errorText: errorPhone.isEmpty ? null : errorPhone,
                          controller: phoneController,
                          prefixIcon: CountryPickerWidget(
                            getCountryCode: (value) {
                              countryCode = value;
                            },
                          ),
                          validator: (value) {},
                        ),
                      ),
                      const SizedBox(
                        height: AppSize.s20,
                      ),
                      AuthTextField(
                        hintText: AppStrings.email.tr(),
                        keyboardType: TextInputType.emailAddress,
                        maxLength: 50,
                        obscureText: false,
                        errorText: errorEmail.isEmpty ? null : errorEmail,
                        controller: emailController,
                        validator: (value) {},
                      ),
                      const SizedBox(
                        height: AppSize.s20,
                      ),
                      AuthTextField(
                        hintText: AppStrings.fullName.tr(),
                        keyboardType: TextInputType.text,
                        obscureText: false,
                        maxLength: 25,
                        controller: nameController,
                        errorText: errorFullName.isEmpty ? null : errorFullName,
                        validator: (value) {},
                      ),
                      const SizedBox(
                        height: AppSize.s20,
                      ),
                      AuthTextField(
                        hintText: AppStrings.password.tr(),
                        keyboardType: TextInputType.text,
                        obscureText: true,
                        maxLength: 16,
                        errorText: errorPassword.isEmpty ? null : errorPassword,
                        controller: passwordController,
                        suffixIcon: Text(''),
                        validator: (value) {},
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Checkbox(
                            value: _checkedTerms,
                            onChanged: (value) {
                              setState(() {
                                _checkedTerms = value as bool;
                              });
                            },
                          ),
                          Text(
                            AppStrings.labelAccept.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                    color: ColorManager.textColor,
                                    fontSize: FontSize.s14),
                          ),
                          TextButton(
                            onPressed: () {
                              showModalBottomSheet(
                                context: context,
                                isScrollControlled: true,
                                builder: (context) => TermsConditionsPopup(
                                    APIsConstants.termsConditions,
                                    AppStrings.termsAndConditions.tr()),
                              );
                            },
                            child: Text(
                              AppStrings.termsAndConditions.tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineSmall
                                  ?.copyWith(
                                      color: ColorManager.primary,
                                      fontSize: FontSize.s14),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: AppSize.s20,
                      ),
                      AppRoundedButton(
                        text: AppStrings.labelSignUp.tr(),
                        onPressed: () async {
                          errorPassword = "";
                          errorPhone = "";
                          errorEmail = "";
                          errorFullName = "";

                          if (_checkedTerms) {
                            /// Send Event...
                            analyticsServices.addEventAnalytics(
                                eventName:
                                    EventsAnalytics.signUpButtonPressedEvent,
                                parameterName:
                                    EventsAnalytics.signUpTapParameter);

                            context.read<RegisterBloc>().add(
                                  RegisterWithPhoneEvent(
                                    RegisterRequest(
                                        mobile: checkMobileNumber(
                                            phoneController.value.text),
                                        countryCode: countryCode,
                                        password: passwordController.value.text,
                                        passwordConfirmation:
                                            passwordController.value.text,
                                        email: emailController.value.text,
                                        fullName: nameController.value.text,
                                        language: context.locale.languageCode,
                                        deviceToken: await createDeviceToken()),
                                  ),
                                );
                          } else {
                            Fluttertoast.showToast(
                                msg:
                                    '${AppStrings.labelAccept.tr()} ${AppStrings.termsAndConditions.tr()}');
                          }
                        },
                      ),
                      const SizedBox(height: AppSize.s30),
                      const SocialAuthentications(),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
