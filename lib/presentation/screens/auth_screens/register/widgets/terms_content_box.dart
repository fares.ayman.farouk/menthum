import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../resources/color_manger.dart';
import '../../../../resources/font_manger.dart';
import '../../../../resources/strings_manger.dart';
import '../../../../resources/values_manger.dart';


class TermsContentBox extends StatefulWidget {
  final String data;

  const TermsContentBox({Key? key, required this.data}) : super(key: key);

  @override
  State<TermsContentBox> createState() => _TermsCustomDialogState();
}

class _TermsCustomDialogState extends State<TermsContentBox> {
  @override
  Widget build(BuildContext context) {
    return contentBox(context);
  }

  contentBox(context) {
    return Container(
      padding: EdgeInsets.all(AppPadding.p15),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  AppStrings.termsAndConditions.tr(),
                  style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      color: ColorManager.blackColor, fontSize: FontSize.s20),
                ),
                IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.close))
              ],
            ),
            const SizedBox(
              height: AppSize.s16,
            ),
            Text(
              "Where does it come from? Contrary to popular belief, Lorem Ipsum s not simply random text. It has roots in a piece of classical Latin literaturefrom 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of deFinibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "
              "Lorem ipsum dolor sit amet.., comes from a line in section 1.10.32."
              "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham"
              "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham"
              "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham",
            ),
          ],
        ),
      ),
    );
  }
}
