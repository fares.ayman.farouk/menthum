import 'package:flutter/material.dart';

import '../../resources/color_manger.dart';
import '../../resources/values_manger.dart';

class NotificationPage extends StatelessWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Notifications",
          style: Theme.of(context).textTheme.titleMedium?.copyWith(
                fontSize: AppSize.s16,
                color: ColorManager.primary,
              ),
        ),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(
            horizontal: AppPadding.p16, vertical: AppPadding.p16),
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                itemCount: 10,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(top: AppPadding.p16),
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: AppPadding.p10,
                        vertical: AppPadding.p10,
                      ),
                      height: AppSize.s200,
                      decoration: BoxDecoration(
                          color: ColorManager.white,
                          border: Border.all(
                            color: ColorManager.primary,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            "العائد",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                    fontSize: AppSize.s16,
                                    color: ColorManager.blackColor,
                                    fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(
                            height: AppSize.s8,
                          ),
                          Text(
                            """ 
                           لقد اكتسبت 8.66 جم في حساب منثم 16.77 نسبه العائد سنوياا
                           ادخر اكتر تزد اموالك
                           قم بدهوه اصدقائك الي منثم و احصل علي فرصه الفوز ب 100000 جنيه لك و لهم 
                            """,
                            maxLines: 10,
                            textAlign: TextAlign.right,
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  fontSize: AppSize.s14,
                                  color: ColorManager.fourthColor,
                                ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
