import 'dart:math' as math;
import 'dart:math';

import 'package:badges/badges.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:patientapp/application/utils.dart';
import 'package:patientapp/domain/business_logic/gp_working_shifts/gp_working_shifts_bloc.dart';
import 'package:patientapp/domain/business_logic/main_screen_navigator/main_screen_navigator_cubit.dart';
import 'package:patientapp/domain/business_logic/notification_list/notification_list_bloc.dart';
import 'package:patientapp/domain/models/main_screen_tabs.dart';
import 'package:patientapp/presentation/resources/assets_manager.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';
import 'package:patientapp/presentation/resources/language_manager.dart';
import 'package:patientapp/presentation/resources/routes_manger.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';
import 'package:patientapp/presentation/resources/styles_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';
import 'package:patientapp/presentation/screens/main_screen/widget/box_doctor_popup.dart';
import 'package:patientapp/presentation/screens/main_screen/widget/section_button.dart';

import '../../../application/di.dart';
import '../../../data/repository/notification_repository_imp.dart';

class MainScreenContent extends StatefulWidget {
  //int tabIndex = 0;

  MainScreenContent(/*int tab,*/ {Key? key}) : super(key: key) {
    // tabIndex = tab;
  }

  @override
  State<MainScreenContent> createState() => _MainScreenContentState();
}

class _MainScreenContentState extends State<MainScreenContent>
    with SingleTickerProviderStateMixin {
  ///  animation declaration
  late Animation<double> rotateAnimation;

  late Animation callDoctorBoxAnimation;

  late AnimationController animationController;

  NotificationRepositoriesImp notificationRepositoriesImp =
      instance<NotificationRepositoriesImp>();

  late NotificationListBloc notificationListBloc;

  @override
  void initState() {
    notificationListBloc = NotificationListBloc(notificationRepositoriesImp);
    animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 400));
    setRotation(90);

    callDoctorBoxAnimation = Tween(begin: -100.0, end: -1.0).animate(
        CurvedAnimation(
            parent: animationController, curve: Curves.fastOutSlowIn));

    Future.delayed(const Duration(seconds: 3)).then((value) {
      if (context.read<MainScreenNavigatorCubit>().currentTab ==
          MainTabs.home) {
        context.read<MainScreenNavigatorCubit>().callDoctor();
      }
    });

    // WidgetsBinding.instance.addPostFrameCallback((_) =>
    //     context.read<MainScreenNavigatorCubit>().changeTab(widget.tabIndex));

    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  /// rotate animation
  void setRotation(int degree) {
    double angle = degree * pi / 180;
    rotateAnimation =
        Tween<double>(begin: 0, end: angle).animate(animationController);
  }

  bool _isArabicLang() {
    return context.locale == ARABIC_LOCAL;
  }

  bool _isFirstScreenVisible = true;

  @override
  Widget build(BuildContext context) {
    if (notificationListBloc.isClosed) {
      notificationListBloc = NotificationListBloc(notificationRepositoriesImp);
    }

    return BlocConsumer<MainScreenNavigatorCubit, MainScreenNavigatorState>(
      listener: (context, state) {
        if (state is ShowCallDoctorState) {
          /// start animation
          setRotation(45);
          animationController.forward(from: 0);
        }

        if (state is! ShowCallDoctorState) {
          animationController.reset();
        }
      },
      builder: (context, state) {
        return GestureDetector(
          onTap: state is ShowSideMenuHomeState
              ? () {
                  context
                      .read<MainScreenNavigatorCubit>()
                      .hideSideMenu(AppStrings.home);
                }
              : null,
          child: Container(
            height: double.infinity,
            width: double.infinity,
            padding: state is ShowSideMenuHomeState
                ? const EdgeInsets.all(
                    AppPadding.p20,
                  )
                : null,
            decoration: BoxDecoration(
              boxShadow: state is ShowSideMenuHomeState ? shadowList : null,
              color: Theme.of(context).backgroundColor,
              borderRadius: state is ShowSideMenuHomeState
                  ? BorderRadius.circular(AppSize.s50)
                  : null,
            ),
            child: Scaffold(
              appBar:
                  context.read<MainScreenNavigatorCubit>().currentTab.index == 0
                      ? AppBar(
                          elevation: AppSize.s0,
                          leadingWidth: 30,
                          leading: InkWell(
                            onTap: () {
                              state is ShowSideMenuHomeState
                                  ? context
                                      .read<MainScreenNavigatorCubit>()
                                      .hideSideMenu(AppStrings.home)
                                  : context
                                      .read<MainScreenNavigatorCubit>()
                                      .showSideMenu(AppStrings.home);
                            },
                            child: Container(
                              padding: _isArabicLang()
                                  ? const EdgeInsets.only(right: AppPadding.p8)
                                  : const EdgeInsets.only(left: AppPadding.p8),
                              child: state is ShowSideMenuHomeState
                                  ? const Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: Icon(Icons.arrow_back_ios,
                                          size: AppSize.s25),
                                    )
                                  : Transform(
                                      transform: Matrix4.rotationY(
                                          _isArabicLang() ? math.pi : 0),
                                      alignment: FractionalOffset.center,
                                      child: SvgPicture.asset(
                                        ImageAssets.appDrawerIcon,
                                      ),
                                    ),
                            ),
                          ),
                          actions: [
                            InkWell(
                              onTap: () {
                                Navigator.pushNamed(
                                  context,
                                  Routes.notificationsScreen,
                                );
                              },
                              child: Padding(
                                  padding: const EdgeInsets.only(
                                      right: 20.0, top: 5, left: 20),
                                  child: SvgPicture.asset(
                                      ImageAssets.notificationIcon)),
                            ),
                          ],
                        )
                      : AppBar(
                          toolbarHeight: 0,
                          elevation: 0,
                        ),
              body: Stack(
                children: [
                  /// screen body
                  PageView(
                    controller:
                        context.read<MainScreenNavigatorCubit>().pageController,
                    children: context.read<MainScreenNavigatorCubit>().tabs,
                    onPageChanged: (int index) {
                      print(index);
                      context.read<MainScreenNavigatorCubit>().changeTab(index);
                    },
                  ),

                  /// build call doctor
                  /* AnimatedBuilder(
                    builder: (context, widget) {
                      return Positioned(
                        left: AppSize.s20,
                        right: AppSize.s20,
                        bottom: callDoctorBoxAnimation.value * 3,
                        child: BoxDoctorPopup(),
                      );
                    },
                    animation: callDoctorBoxAnimation,
                  ) */
                ],
              ),

              /* /// float action button
              floatingActionButton: InkWell(
                onTap: () {
                  context.read<MainScreenNavigatorCubit>().callDoctor();
                },
                child: BlocProvider(
                  create: (context) => instance<GpWorkingShiftsBloc>()
                    ..add(GetGPWorkingShiftsEvent()),
                  child:
                      BlocConsumer<GpWorkingShiftsBloc, GPWorkingShiftsState>(
                          listener: (context, state) {
                    if (state is FailedGPWorkingShiftsState) {
                      /// Something went wrong.
                      debugPrint("failure:${state.failure.message}");
                      handleApiError(context: context, failure: state.failure);
                    }
                  }, builder: (context, state) {
                    return FocusDetector(
                      onVisibilityGained: () {
                        /// check if the user has new upcoming ( upcoming is removed).
                        if (!_isFirstScreenVisible) {
                          instance<GpWorkingShiftsBloc>()
                            ..add(GetGPWorkingShiftsEvent());
                        }
                        _isFirstScreenVisible = false;
                      },
                      child: AnimatedBuilder(
                          animation: rotateAnimation,
                          builder: (context, widget) {
                            return floatActionButton();
                          }),
                    );
                  }),
                ),
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked, */

              /// bottom navigation bar
              bottomNavigationBar: bottomNavigationBar(),
            ),
          ),
        );
      },
    );
  }

  /*  Widget floatActionButton() {
    return Container(
        width: AppSize.s60,
        height: AppSize.s60,
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.shade300,
                  offset: Offset(0, 10),
                  blurRadius: 10)
            ]),
        child: Transform.rotate(
          angle: rotateAnimation.value,
          child: Center(
            child: SvgPicture.asset(ImageAssets.addIcon),
          ),
        ));
  }
 */
  Widget bottomNavigationBar() {
    return SizedBox(
      height: AppSize.s70,
      child: BottomAppBar(
        color: Colors.white,
        elevation: AppSize.s3,
        shape: const CircularNotchedRectangle(),
        child: BottomAppBar(
          elevation: AppSize.s3,
          color: Colors.white,
          shape: const CircularNotchedRectangle(),
          child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                SectionButton(
                    ImageAssets.homeIcon, 0, ImageAssets.homeSelectedIcon),
                const SizedBox(
                  width: 35,
                ),
                SectionButton(ImageAssets.clock, 1, ImageAssets.clock),
                /* IconButton(
                    onPressed: () {
                      Navigator.pushNamed(
                          context, Routes.transactionhistoryScreen);
                    },
                    icon: Icon(
                      Icons.history,
                      color: ColorManager.primary,
                    )), */
                /* SectionButton(ImageAssets.clock, 0, ImageAssets.clock), */
                const SizedBox(
                  width: 35,
                ),
                SectionButton(ImageAssets.profileIcon, 2,
                    ImageAssets.profileSelectedIcon),
              ]),
        ),
      ),
    );
  }

  /* Widget buildNotificationIcon(NotificationListState state) {
    if (state is NotificationListSucess) {
      return InkWell(
        onTap: () {
          Navigator.pushNamed(context, Routes.notificationsScreen,
              arguments: notificationListBloc);
        },
        child: Padding(
          padding: const EdgeInsets.only(right: 20.0, top: 5, left: 20),
          child: state.unReadCount! > 0
              ? Padding(
                  padding: EdgeInsets.only(top: 5),
                  child: Badge(
                      badgeContent: Text(
                        "${state.unReadCount}",
                        style:
                            TextStyle(color: ColorManager.white, fontSize: 13),
                      ),
                      child: SvgPicture.asset(ImageAssets.notificationIcon)),
                )
              : SvgPicture.asset(ImageAssets.notificationEmptyIcon),
        ),
      );
    }

    /// no notification
    else {
      return InkWell(
        onTap: () {
          Navigator.pushNamed(context, Routes.notificationsScreen,
              arguments: notificationListBloc);
        },
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: SvgPicture.asset(ImageAssets.notificationEmptyIcon)),
      );
    }
  } */
}
