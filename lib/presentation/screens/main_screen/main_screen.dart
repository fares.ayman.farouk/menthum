import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/application/extensions.dart';
import 'package:patientapp/domain/business_logic/main_screen_navigator/main_screen_navigator_cubit.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';
import 'package:patientapp/presentation/resources/language_manager.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';
import 'package:patientapp/presentation/resources/styles_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';
import 'package:patientapp/presentation/screens/main_screen/widget/animated_drawer_design.dart';
import 'package:patientapp/presentation/screens/main_screen/widget/drawer_screen.dart';
import 'package:patientapp/presentation/screens/main_screen/main_screen_content.dart';
import 'package:fluttertoast/fluttertoast.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  bool fristBack = true;

  Future<bool> _onWillPop() async {
    if (fristBack) {
      Fluttertoast.showToast(
          msg: AppStrings.backMsg.tr(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          fontSize: 16.0);
      fristBack = false;
      return false;
    } else {
      return true;
    }
  }

  double x1Offset = 0;
  double y1Offset = 0;
  double x2Offset = 0;
  double y2Offset = 0;
  double scaleFactor = 1;
  double scaleFactor2 = 1;
  bool isDrawerOpen = false;

  closeDrawer() {
    // setState(() {
    x1Offset = 0;
    y1Offset = 0;
    x2Offset = 0;
    y2Offset = 0;
    scaleFactor = 1;
    scaleFactor2 = 1;
    isDrawerOpen = false;
    // });
  }

  openDrawer() {
    if (context.locale == ARABIC_LOCAL) {
      x1Offset = -context.width * 0.02;
      x2Offset = -context.width * 0.1;
    } else {
      x1Offset = context.width * 0.5;
      x2Offset = context.width * 0.54;
    }
    y1Offset = 180;
    y2Offset = 150;

    y1Offset = context.height * 0.18;
    y2Offset = context.height * 0.15;

    scaleFactor = 0.6;
    scaleFactor2 = 0.55;
    isDrawerOpen = true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: BlocConsumer<MainScreenNavigatorCubit, MainScreenNavigatorState>(
        listener: (context, state) {
          if (context.read<MainScreenNavigatorCubit>().isSideMenuOpen) {
            openDrawer();
            print("Listen from Show Side Menue State");
          }
          if (!context.read<MainScreenNavigatorCubit>().isSideMenuOpen) {
            closeDrawer();
            print("Listen from Hide Side Menue State");
          }
        },
        builder: (context, state) {
          return SafeArea(
            child: Scaffold(
              body: Stack(
                children: [
                  DrawerScreen(),
                  AnimatedDrawerDesign(
                    duration: 200,
                    isDrawerOpen: isDrawerOpen,
                    scaleF: scaleFactor2,
                    x: x1Offset,
                    y: y1Offset,
                    child: Container(
                      height: double.infinity,
                      width: double.infinity,
                      padding: const EdgeInsets.all(AppPadding.p20),
                      decoration: BoxDecoration(
                        boxShadow: isDrawerOpen ? darkShadowList : null,
                        color: ColorManager.primaryLight,
                        borderRadius: isDrawerOpen
                            ? BorderRadius.circular(AppSize.s50)
                            : null,
                      ),
                    ),
                  ),
                  AnimatedDrawerDesign(
                    x: x2Offset,
                    y: y2Offset,
                    isDrawerOpen: isDrawerOpen,
                    scaleF: scaleFactor,
                    duration: 200,
                    child: Container(
                      height: double.infinity,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        boxShadow: isDrawerOpen ? darkShadowList : null,
                        color: ColorManager.primaryLight,
                        borderRadius: isDrawerOpen
                            ? BorderRadius.circular(AppSize.s50)
                            : null,
                      ),
                      child: BlocBuilder<MainScreenNavigatorCubit,
                          MainScreenNavigatorState>(
                        builder: (context, state) {
                          switch (context
                              .read<MainScreenNavigatorCubit>()
                              .screenName) {
                            case AppStrings.home:
                              return MainScreenContent(/*MainTabs.home.index*/);
                            /* case AppStrings.myVitals:
                              return const MyVitalsScreen();
                            case AppStrings.myProfile:
                              //return MainScreenContent(MainTabs.profile.index);
                              return ProfileScreen();
                            case AppStrings.Prescriptions:
                              return const PrescriptionsScreen();
                            case AppStrings.myRecords:
                              return MyRecordsScreen();
                            case AppStrings.myResult:
                              return const MyVisitsScreen();
                            case AppStrings.myPrograms:
                              return MyProgramsScreen(); */
                            default:
                              return const Center(
                                child: Text("Screen Not Found"),
                              );
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
