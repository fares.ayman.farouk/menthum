import 'package:flutter/cupertino.dart';

class AnimatedDrawerDesign extends StatelessWidget {
  final double x;
  final double y;
  final double scaleF;
  final Widget child;
  final int duration;
  final bool isDrawerOpen;

  AnimatedDrawerDesign({
    required this.x,
    required this.y,
    required this.scaleF,
    required this.child,
    required this.duration,
    required this.isDrawerOpen,
  });

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      transform: Matrix4.translationValues(x, y, 0)
        ..scale(scaleF)
        ..rotateY(isDrawerOpen ? -0.5 : 0),
      duration: Duration(milliseconds: duration),
      child: child,
    );
  }
}
