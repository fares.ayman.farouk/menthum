import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/application/extensions.dart';
import 'package:patientapp/application/utils.dart';
import 'package:patientapp/data/repository/auth_repository_imp.dart';
import 'package:patientapp/domain/business_logic/auth/login/login_bloc.dart';
import 'package:patientapp/domain/business_logic/main_screen_navigator/main_screen_navigator_cubit.dart';
import 'package:patientapp/presentation/resources/assets_manager.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';
import 'package:patientapp/presentation/resources/language_manager.dart';
import 'package:patientapp/presentation/resources/routes_manger.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';
import 'package:patientapp/presentation/widgets/custom_dialog.dart';
import 'package:patientapp/presentation/widgets/profile_icon_design.dart';
import 'package:toggle_switch/toggle_switch.dart';

import '../../../../application/constants.dart';
import '../../../resources/font_manger.dart';
import '../../../widgets/terms_conditions_popup.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  AuthRepositoryImp authRepository = instance<AuthRepositoryImp>();
  AppPreferences appPreferences = instance<AppPreferences>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => LoginBloc(authRepository),
      child: BlocConsumer<LoginBloc, LoginState>(
        listener: (context, state) {
          if (state is LoadingLogoutState) {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return CustomDialog(
                      color: Colors.transparent,
                      borderRadius: AppSize.s20,
                      contentBox: Center(
                        child: Lottie.asset(
                          JsonAssets.loadingAnimations,
                        ),
                      ));
                });
          } else if (state is SuccessLogoutState) {
            Navigator.pop(context);
            Navigator.pushReplacementNamed(context, Routes.authScreen);
          } else if (state is FailLogoutState) {
            Navigator.pop(context);
            handleApiError(context: context, failure: state.failure);
          }
        },
        builder: (context, state) {
          return Stack(
            children: [
              Container(
                color: Theme.of(context).backgroundColor,
                width: double.infinity,
                height: double.infinity,
                padding: const EdgeInsets.only(
                  left: AppPadding.p20,
                  top: AppPadding.p20,
                  bottom: AppPadding.p20,
                  right: AppPadding.p15,
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      /// user data
                      Padding(
                        padding: const EdgeInsets.only(left: AppSize.s8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ProfileIconDesgin(
                              edgeInsets: const EdgeInsets.only(
                                  bottom: AppSize.s8, right: AppSize.s8),
                              child: Center(
                                child: ClipRRect(
                                    borderRadius:
                                        BorderRadius.circular(AppSize.s20),
                                    child: Icon(
                                      Icons.monetization_on,
                                      color: ColorManager.primary,
                                      size: AppSize.s50,
                                    )
                                    /* CachedNetworkImage(
                                    imageUrl: context
                                            .read<MainScreenNavigatorCubit>()
                                            .userData
                                            .avatar ??
                                        "",
                                    errorWidget: (context, url, error) =>
                                        Lottie.asset(JsonAssets.noInternet,
                                            height: 200),
                                    placeholder: (context, url) => Lottie.asset(
                                      JsonAssets.loadingAnimations,
                                    ),
                                  ), */
                                    ),
                              ),
                            ),
                            SizedBox(
                              height: context.height * .01,
                            ),
                            Text(
                              "احمد الشيخ",
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineSmall
                                  ?.copyWith(
                                      color: ColorManager.secondColor,
                                      fontSize: FontSize.s20),
                            ),
                            SizedBox(
                              height: context.height * .01,
                            ),
                            Text(
                              "01554568899",
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle2
                                  ?.copyWith(
                                      color: ColorManager.secondColor,
                                      fontSize: FontSize.s14),
                            )
                          ],
                        ),
                      ),

                      SizedBox(
                        height: context.height * .08,
                      ),

                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                              bottom: AppSize.s20,
                            ),
                            child: Row(
                              children: [
                                SizedBox(
                                  height: 34,
                                  width: 28,
                                  child: SvgPicture.asset(
                                    ImageAssets.homeIcon,
                                  ),
                                ),
                                const SizedBox(
                                  width: AppPadding.p8,
                                ),
                                Text(
                                  "Add Money",
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineSmall
                                      ?.copyWith(
                                          color: ColorManager.secondColor,
                                          fontSize: FontSize.s16),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              bottom: AppSize.s20,
                            ),
                            child: Row(
                              children: [
                                SizedBox(
                                  height: 34,
                                  width: 28,
                                  child: SvgPicture.asset(
                                    ImageAssets.bookDoctorIcon,
                                  ),
                                ),
                                const SizedBox(
                                  width: AppPadding.p8,
                                ),
                                Text(
                                  "withdraw",
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineSmall
                                      ?.copyWith(
                                          color: ColorManager.secondColor,
                                          fontSize: FontSize.s16),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              bottom: AppSize.s20,
                            ),
                            child: Row(
                              children: [
                                SizedBox(
                                  height: 34,
                                  width: 28,
                                  child: SvgPicture.asset(
                                    ImageAssets.clock,
                                  ),
                                ),
                                const SizedBox(
                                  width: AppPadding.p8,
                                ),
                                Text(
                                  "Money transfer",
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineSmall
                                      ?.copyWith(
                                          color: ColorManager.secondColor,
                                          fontSize: FontSize.s16),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),

                      /// drawer
                      /* Column(
                        children: drawerItems
                            .map(
                              (element) => InkWell(
                                onTap: () async {
                                  if (element['title'] == AppStrings.logOut) {
                                    context
                                        .read<LoginBloc>()
                                        .add(LogoutEvent());
                                  } else {
                                    log(element['title']);
                                    if (element['title'] == AppStrings.home ||
                                        element['title'] ==
                                            AppStrings.myProfile) {
                                      context
                                          .read<MainScreenNavigatorCubit>()
                                          .hideSideMenu(element['title']);
                                      // if (element['title'] == AppStrings.myProfile) {
                                      //   context
                                      //       .read<MainScreenNavigatorCubit>()
                                      //       .changeTab(MainTabs.profile.index);
                                      // }
                                    } else {
                                      log(element['route']);
                                      Navigator.pushNamed(
                                          context, element['route']);
                                    }
                                  }
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    bottom: AppSize.s20,
                                  ),
                                  child: Row(
                                    children: [
                                      Container(
                                        height: 34,
                                        width: 28,
                                        child: SvgPicture.asset(
                                          element['icon'],
                                        ),
                                      ),
                                      const SizedBox(
                                        width: AppPadding.p8,
                                      ),
                                      Text(
                                        element['title'].toString().tr(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineSmall
                                            ?.copyWith(
                                                color: ColorManager.secondColor,
                                                fontSize: FontSize.s16),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                            .toList(),
                      ), */
                      SizedBox(
                        height: context.height * .02,
                      ),
                      const Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: AppPadding.p10),
                        child: Divider(
                          height: AppSize.s2,
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: AppPadding.p8,
                          ),
                          Text(
                            AppStrings.language.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium
                                ?.copyWith(
                                    color: ColorManager.secondColor,
                                    fontSize: FontSize.s16),
                          ),
                          const SizedBox(
                            height: AppPadding.p8,
                          ),
                          ToggleSwitch(
                            customWidths: const [AppSize.s60, AppSize.s60],
                            cornerRadius: 20.0,
                            activeBgColors: [
                              [ColorManager.primary],
                              [ColorManager.primary]
                            ],
                            activeFgColor: Colors.white,
                            inactiveBgColor: Colors.grey,
                            inactiveFgColor: Colors.white,
                            totalSwitches: 2,
                            initialLabelIndex:
                                appPreferences.getAppLanguage() == ARABIC
                                    ? 0
                                    : 1,
                            labels: const ['عربي', 'ENG'],
                            icons: const [null, null],
                            onToggle: (index) {
                              if ((isRtl(context) && index == 1) ||
                                  (!isRtl(context) && index == 0)) {
                                showDio(context);
                              }

                              log('switched to: $index');
                            },
                          ),
                          /* TextButton(
                            onPressed: () {
                              showModalBottomSheet(
                                context: context,
                                isScrollControlled: true,
                                builder: (context) => TermsConditionsPopup(
                                    APIsConstants.about,
                                    AppStrings.aboutTikshif.tr()),
                              );
                            },
                            child: Text(
                              AppStrings.aboutTikshif.tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineMedium
                                  ?.copyWith(
                                      color: ColorManager.secondColor,
                                      fontSize: FontSize.s16),
                            ),
                          ), */
                          /* TextButton(
                            onPressed: () {
                              showModalBottomSheet(
                                context: context,
                                isScrollControlled: true,
                                builder: (context) => TermsConditionsPopup(
                                  APIsConstants.contact_us,
                                  AppStrings.contactUs.tr(),
                                ),
                              );
                            },
                            child: Text(
                              AppStrings.contactUs.tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineMedium
                                  ?.copyWith(
                                      color: ColorManager.secondColor,
                                      fontSize: FontSize.s16),
                            ),
                          ), */
                        ],
                      )
                    ],
                  ),
                ),
              ),

              /// setting button
              Align(
                alignment:
                    isRtl(context) ? Alignment.topLeft : Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.all(AppPadding.p16),
                  child: IconButton(
                    onPressed: () {
                      Navigator.pushNamed(context, Routes.settingsScreen);
                    },
                    icon: const Icon(Icons.settings),
                    color: ColorManager.blackColor,
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }

  showDio(context) {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(AppStrings.changeLang.tr()),
          content: Text(AppStrings.changeLangMessage.tr()),
          actions: [
            CupertinoDialogAction(
                child: Text(AppStrings.yes.tr()),
                onPressed: () {
                  appPreferences.changeAppLanguage();
                  Phoenix.rebirth(context);
                }),
            CupertinoDialogAction(
              isDestructiveAction: true,
              isDefaultAction: true,
              child: Text(AppStrings.no.tr()),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }
/*Widget DrowerItemUI(Map element){
    return InkWell(
      onTap: () async {
        if (element['title'] == AppStrings.logOut) {
          String? token = await appPreferences.getToken();
          log("@@@@@@@@@$token");
          context
              .read<LoginBloc>()
              .add(LogoutEvent(token: token ?? ""));
        } else {
          log(element['title']);
          context
              .read<MainScreenNavigatorCubit>()
              .hideSideMenu(element['title']);
        }
      },
      child: Padding(
        padding: const EdgeInsets.only(
          bottom: AppSize.s20,
        ),
        child: Row(
          children: [
            SvgPicture.asset(
              element['icon'],
            ),
            SizedBox(
              height: context.height * .02,
            ),
            Text(
              element['title'].toString().tr(),
              style: Theme.of(context)
                  .textTheme
                  .headlineSmall
                  ?.copyWith(
                  color: ColorManager.secondColor,
                  fontSize: FontSize.s16),
            ),
          ],
        ),
      ),
    );
  }*/

}

List<Map> drawerItems = [
  {
    'icon': IconAssets.homeIcon,
    'title': AppStrings.home,
  },
  {
    'icon': IconAssets.resultsIcon,
    'title': AppStrings.myResult,
    'route': Routes.myVisits
  },
  {
    'icon': IconAssets.prescriptionsIcon,
    'title': AppStrings.prescriptions,
    'route': Routes.prescriptions
  },
  {
    'icon': IconAssets.programsIcon,
    'title': AppStrings.myPrograms,
    'route': Routes.myProgramsScreen
  },
  {
    'icon': IconAssets.resultsIcon,
    'title': AppStrings.myRecords,
    'route': Routes.myRecords
  },
  {
    'icon': ImageAssets.dashboardVitals,
    'title': AppStrings.myVitals,
    'route': Routes.myVitals
  },
  {
    'icon': IconAssets.blogIcon,
    'title': AppStrings.blogs,
    'route': Routes.blogsScreen
  },
  {
    'icon': IconAssets.calculatorIcon,
    'title': AppStrings.calculation,
    'route': Routes.calculation,
  },
  {
    'icon': IconAssets.questionnaireIcon,
    'title': AppStrings.questionnaires,
    'route': Routes.questionnairesListScreen,
  },
  {
    'icon': IconAssets.userIcon,
    'title': AppStrings.myProfile,
    'route': Routes.profilesScreen,
  },
  {
    'icon': IconAssets.signOutIcon,
    'title': AppStrings.logOut,
  },
];
