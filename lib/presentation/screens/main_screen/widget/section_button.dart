import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';

import '../../../../domain/business_logic/main_screen_navigator/main_screen_navigator_cubit.dart';
import '../../../resources/color_manger.dart';

class SectionButton extends StatelessWidget {
  String image;
  int index;
  String selectedImage;

  SectionButton(this.image, this.index, this.selectedImage, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MainScreenNavigatorCubit, MainScreenNavigatorState>(
      builder: (context, state) {
        return Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              ///top selected line
              Container(
                height: AppSize.s3,
                width: AppSize.s25,

                decoration: BoxDecoration(
                  color: index ==
                      context
                          .read<MainScreenNavigatorCubit>()
                          .currentTab
                          .index
                      ? ColorManager.primary
                      : ColorManager.white,

                  borderRadius: BorderRadius.circular(AppSize.s3),
                ),
              ),

              const SizedBox(height: AppSize.s10,),
              /// icon container
              SizedBox(
                width: double.infinity,
                child: IconButton(
                  icon: SvgPicture.asset(
                    index ==
                            context
                                .read<MainScreenNavigatorCubit>()
                                .currentTab
                                .index
                        ? selectedImage
                        : image,
                  ),
                  onPressed: () {
                    context.read<MainScreenNavigatorCubit>().changeTab(index);
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
