import 'dart:developer';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/data/models/apiResponse/home_response/gp_working_shifts_response.dart';
import 'package:patientapp/domain/business_logic/gp_working_shifts/gp_working_shifts_bloc.dart';
import 'package:patientapp/presentation/resources/language_manager.dart';
import 'package:patientapp/presentation/resources/routes_manger.dart';

import '../../../resources/assets_manager.dart';
import '../../../resources/color_manger.dart';
import '../../../resources/strings_manger.dart';
import '../../../resources/values_manger.dart';
import 'box_doctor_shape_container.dart';

class BoxDoctorPopup extends StatelessWidget {
  BoxDoctorPopup({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: AppSize.s200,
      padding: const EdgeInsets.only(top: AppPadding.p4),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(AppRadius.m20),
        boxShadow: [
          BoxShadow(
              color: Colors.grey.shade300.withOpacity(.2),
              blurRadius: 5.0,
              offset: const Offset(0, -10)),
        ],
      ),
      child: ClipPath(
        clipper: BoxDoctorShape(),
        child: Container(
          height: AppSize.s200,
          decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(AppRadius.m20),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              /// book doctor
              itemColumn(ImageAssets.bookDoctorIcon, AppStrings.bookDoctor.tr(),
                  context, () {
                Navigator.pushNamed(context, Routes.specialities);
              }),

              /// call gp
              itemColumn(
                  isRtl(context) ? ImageAssets.callGpAr : ImageAssets.callGpEn,
                  AppStrings.callGP.tr(),
                  context, () async {
                final AppPreferences appPreferences =
                    instance<AppPreferences>();
                List<GPWorkingShifts>? gpWorkingShifts =
                    await appPreferences.getGPWorkingShifts();
                if (gpWorkingShifts != null) {
                  //CallScenario.callGpHandlingFunc(context, gpWorkingShifts);
                } else {
                  log("GP working shifts api is down");
                  context
                      .read<GpWorkingShiftsBloc>()
                      .add(GetGPWorkingShiftsEvent());
                }
              }),
            ],
          ),
        ),
      ),
    );
  }

  Widget itemColumn(String icon, String title, context, onTap) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InkWell(
          onTap: onTap,
          child: Container(
            height: AppSize.s80,
            width: AppSize.s80,
            decoration: BoxDecoration(
              color: ColorManager.fontLight.withAlpha(20),
              borderRadius: BorderRadius.circular(AppSize.s20),
            ),
            child: Center(
              child: SvgPicture.asset(
                icon,
                width: AppSize.s25,
                height: AppSize.s25,
              ),
            ),
          ),
        ),
        const SizedBox(
          height: AppSize.s8,
        ),
        Text(
          title,
          style: Theme.of(context).textTheme.titleMedium,
        )
      ],
    );
  }
}
