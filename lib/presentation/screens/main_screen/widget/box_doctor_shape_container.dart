import 'package:flutter/material.dart';

class BoxDoctorShape extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    double w = size.width;
    double h = size.height;

    final path = Path();

    path.lineTo(20, 0);
    path.quadraticBezierTo(0, 0, 0, 20);

    path.lineTo(0, h - 20);
    path.quadraticBezierTo(0, h, 20, h);

    path.lineTo(w - 20, h);
    path.quadraticBezierTo(w, h, w, h - 20); // 3

    path.lineTo(w, 20); //4 point
    path.quadraticBezierTo(w, 0, w - 20, 0); //4 point

    path.lineTo(w * .5 + 40, 0); //5

    path.quadraticBezierTo(w * .5 + 30, 0, w * .5 + 20, 10); //5
    path.quadraticBezierTo(w * .5, 30, w * .5 - 20, 10);
    path.quadraticBezierTo(w * .5 - 30, 0, w * .5 - 40, 0);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
