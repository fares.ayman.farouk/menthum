import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:patientapp/data/models/apiResponse/upcoming_response.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';
import 'package:patientapp/presentation/screens/home/widgets/upcomingItem.dart';
import 'package:patientapp/presentation/widgets/appbar_design.dart';

class AllAppointmentsScreen extends StatelessWidget {
  final List<AppointmentItem> upcomingAppointmentsList;

  const AllAppointmentsScreen({required this.upcomingAppointmentsList});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarDesign(
        title: AppStrings.upcomingBookings.tr(),
        hasActionButton: false,
      ),
      body: Padding(
        padding: const EdgeInsets.all(AppPadding.p15),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              for (int i = 0; i < upcomingAppointmentsList.length; i++)
                UpcomingItem(
                  appointmentItem: upcomingAppointmentsList[i],
                  inViewAll: true,
                  headerVisible: true,
                  showStatus: false,
                  //width: double.infinity,
                )
            ],
          ),
        ),
      ),
    );
  }
}
