import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/application/constants.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/presentation/resources/assets_manager.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';
import 'package:patientapp/presentation/resources/routes_manger.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';
import 'package:patientapp/presentation/screens/home/widgets/dashboard_item.dart';
import 'package:patientapp/presentation/screens/home/widgets/smooth_home_page_indicator.dart';
import 'package:patientapp/presentation/screens/home/widgets/home_upcoming_bookings.dart';
import 'package:patientapp/presentation/screens/home/widgets/tikshif_locations.dart';

import '../../../data/models/apiResponse/auth_response/login_api_response.dart';
import '../../../data/models/apiResponse/specialties_response.dart';
import 'widgets/home_dashboard.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  AppPreferences appPreferences = instance<AppPreferences>();

  //static id for book second opinion

  //final String bookSecondOpinionId="95d32b21-6c1e-4942-970b-324dc21beb0f";

  UserData? userData;

  @override
  void initState() {
    getUser();
  }

  getUser() async {
    userData = await appPreferences.getUser();
    setState(() {});
    if (userData != null &&
        userData?.hasPassword != null &&
        userData?.hasPassword != true) {
      print("@@@@@@@@@@@@@@@@@@@@@!!!false");
      Fluttertoast.showToast(
          msg:
              "your account not have password! \n please, enter a new password..!");
      Navigator.pushReplacementNamed(context, Routes.setPassword);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
              vertical: AppPadding.p24, horizontal: AppPadding.p16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      Text(
                        "مرحبا AhmedElshiekh",
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium
                            ?.copyWith(fontSize: AppSize.s16),
                      ),
                      const SizedBox(
                        height: AppSize.s1,
                      ),
                      Text(
                        "رقم عميل منثم : 56987",
                        style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                            fontSize: AppSize.s12,
                            color: ColorManager.fourthColor),
                      ),
                    ],
                  ),
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      "مفعل",
                      style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                            fontSize: AppSize.s18,
                            color: ColorManager.primary,
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: AppSize.s20,
              ),
              /* HomeUpcomingBookings(),
              const SizedBox(
                height: AppSize.s20,
              ), */
              const SmoothHomePageIndicator(),
              const SizedBox(
                height: AppSize.s20,
              ),
              Container(
                height: AppSize.s50,
                decoration: BoxDecoration(
                    color: ColorManager.blackColor,
                    border: Border.all(
                      color: ColorManager.blackColor,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.monetization_on,
                      color: ColorManager.white,
                    ),
                    const SizedBox(
                      width: AppSize.s4,
                    ),
                    Text(
                      "أضافه اموال",
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(
                          fontSize: AppSize.s16, color: ColorManager.white),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: AppPadding.p20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      height: AppSize.s50,
                      decoration: BoxDecoration(
                          color: ColorManager.white,
                          border: Border.all(
                            color: ColorManager.primary,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: AppPadding.p24),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            const Icon(
                              Icons.monetization_on,
                              color: ColorManager.primary,
                            ),
                            const SizedBox(
                              width: AppSize.s4,
                            ),
                            Text(
                              "ارسال اموال",
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(
                                      fontSize: AppSize.s16,
                                      color: ColorManager.primary),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: AppSize.s50,
                      decoration: BoxDecoration(
                          color: ColorManager.white,
                          border: Border.all(
                            color: ColorManager.primary,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: AppPadding.p24),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            const Icon(
                              Icons.monetization_on,
                              color: ColorManager.primary,
                            ),
                            const SizedBox(
                              width: AppSize.s4,
                            ),
                            Text(
                              "سحب",
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(
                                      fontSize: AppSize.s16,
                                      color: ColorManager.primary),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: AppPadding.p16),
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p16),
                  height: AppSize.s100,
                  decoration: BoxDecoration(
                      color: ColorManager.blackColor,
                      border: Border.all(
                        color: ColorManager.blackColor,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(
                        Icons.calculate,
                        color: ColorManager.white,
                        size: AppSize.s50,
                      ),
                      const SizedBox(
                        width: AppSize.s20,
                      ),
                      Expanded(
                        child: Text(
                          "اكتشف المبلغ الذي يمكنك ادخاره مع منثم",
                          maxLines: 2,
                          textAlign: TextAlign.right,
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium
                              ?.copyWith(
                                  fontSize: AppSize.s16,
                                  color: ColorManager.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: AppPadding.p16),
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p16),
                  height: AppSize.s70,
                  decoration: BoxDecoration(
                      color: ColorManager.primary,
                      border: Border.all(
                        color: ColorManager.primary,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(
                        Icons.people,
                        color: ColorManager.white,
                        size: AppSize.s50,
                      ),
                      const SizedBox(
                        width: AppSize.s20,
                      ),
                      Expanded(
                        child: Text(
                          "تفقد من بين اصدقائك يستخدم منثم",
                          maxLines: 2,
                          textAlign: TextAlign.right,
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium
                              ?.copyWith(
                                  fontSize: AppSize.s16,
                                  color: ColorManager.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: AppPadding.p16),
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p16),
                  height: AppSize.s250,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                      color: ColorManager.white,
                      border: Border.all(
                        color: ColorManager.white,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        """ 
                        لقد حصلت علي 0.0 من المكافات
                        قم بدعوه اصدقائق لكي تستمتع بالتجربه متفرده علي منثم كل شخص سيتم انضمامه بفضل دعوتك سوف تحصل علي نقاط مكافاه 
                        تقدر ب 500 جنيه مصري
                        """,
                        maxLines: 10,
                        textAlign: TextAlign.right,
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium
                            ?.copyWith(
                                fontSize: AppSize.s16,
                                color: ColorManager.blackColor),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Icon(
                            Icons.people,
                            color: ColorManager.blackColor,
                            size: AppSize.s50,
                          ),
                          const SizedBox(
                            width: AppSize.s20,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: ColorManager.white,
                                border: Border.all(
                                  color: ColorManager.primary,
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: AppPadding.p24,
                                  vertical: AppPadding.p8),
                              child: Text(
                                "ارسل دعوه الان",
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium
                                    ?.copyWith(
                                        fontSize: AppSize.s16,
                                        color: ColorManager.primary),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: AppSize.s20,
              ),
              Padding(
                padding: const EdgeInsets.only(top: AppPadding.p16),
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p16),
                  height: AppSize.s170,
                  decoration: BoxDecoration(
                      color: ColorManager.white,
                      border: Border.all(
                        color: ColorManager.primary,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: Row(
                    children: [
                      Icon(
                        Icons.people,
                        color: ColorManager.blackColor,
                        size: AppSize.s50,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            """ 
                            نظم شئونك الماليه مع
                            منثم موني وايز
                            الخطط الماليه
                            """,
                            maxLines: 10,
                            textAlign: TextAlign.right,
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                    fontSize: AppSize.s16,
                                    color: ColorManager.blackColor),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: ColorManager.white,
                                border: Border.all(
                                  color: ColorManager.primary,
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: AppPadding.p24,
                                  vertical: AppPadding.p8),
                              child: Text(
                                "اعرف المزيد من المعلومات عنهم",
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium
                                    ?.copyWith(
                                        fontSize: AppSize.s16,
                                        color: ColorManager.primary),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),

              /* DashBoardItem(
                isImage: false,
                onTap: () {
                  Navigator.pushNamed(context, Routes.doctorsScreenList,
                      arguments: {
                        "specialitiesList": [Speciality()],
                        "specialityId": AppStrings.bookSecondOpinionId
                      });
                },
                icon: ImageAssets.bookDoctorIcon,
                text: AppStrings.bookSecondOpinion.tr(),
              ), */
              /* const SizedBox(
                height: AppSize.s20,
              ),
              const HomeDashboard(),
              const SizedBox(
                height: AppSize.s20,
              ),
              const TikshifLocations(), */
            ],
          ),
        ),
      ),
    );
  }
}
