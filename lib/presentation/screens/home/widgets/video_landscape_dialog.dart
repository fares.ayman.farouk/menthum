import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:patientapp/application/constants.dart';
import 'package:patientapp/presentation/widgets/app_rounded_btn.dart';
import 'package:patientapp/presentation/widgets/icon_widget.dart';
import 'package:video_player/video_player.dart';
import 'package:wakelock/wakelock.dart';
import '../../../resources/assets_manager.dart';

class VideoLandscapeDialog extends StatefulWidget {
  final String url;

  const VideoLandscapeDialog({Key? key, required this.url}) : super(key: key);

  @override
  _VideoLandscapeDialogState createState() => _VideoLandscapeDialogState();
}

class _VideoLandscapeDialogState extends State<VideoLandscapeDialog> {
  late VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    // if network video
    _controller = VideoPlayerController.network(widget.url);

    //if local video
    /*.asset(VideoAssets.tikshifIntroVideo);*/

    _controller.addListener(() {
      setState(() {});
    });
    _controller.setLooping(true);
    _controller.initialize().then((_) => setState(() {}));

    setLandscape();

    _controller.play();
  }

  Future setLandscape() async {
    await SystemChrome.setEnabledSystemUIOverlays([]);
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);

    await Wakelock.enable();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
        child: Scaffold(
      body: _controller.value.isInitialized
          ? SizedBox.expand(
              child: AspectRatio(
                aspectRatio: _controller.value.aspectRatio,
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: <Widget>[
                    VideoPlayer(_controller),
                    ControlsOverlay(
                      controller: _controller,
                      orientation: AppConstants.landscapeOrientation,
                    ),
                    VideoProgressIndicator(_controller, allowScrubbing: true),
                  ],
                ),
              ),
            )
          : Container(),
    ));
  }

  @override
  void dispose() {
    _controller.dispose();
    setAllOrientations();
    super.dispose();
  }

  Future setAllOrientations() async {
    await SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    await SystemChrome.setPreferredOrientations(DeviceOrientation.values);

    if(MediaQuery.of(context).orientation == Orientation.landscape){
      //if Orientation is landscape then set to portrait
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitDown,
        DeviceOrientation.portraitUp,
      ]);
    }

    await Wakelock.disable();
  }
}

class ControlsOverlay extends StatelessWidget {
  final VideoPlayerController controller;
  final orientation;

  ControlsOverlay(
      {Key? key, required this.controller, required this.orientation})
      : super(key: key);

  static const List<Duration> _exampleCaptionOffsets = <Duration>[
    Duration(seconds: -10),
    Duration(seconds: -3),
    Duration(seconds: -1, milliseconds: -500),
    Duration(milliseconds: -250),
    Duration(),
    Duration(milliseconds: 250),
    Duration(seconds: 1, milliseconds: 500),
    Duration(seconds: 3),
    Duration(seconds: 10),
  ];
  static const List<double> _examplePlaybackRates = <double>[
    0.25,
    0.5,
    1.0,
    1.5,
    2.0,
    3.0,
    5.0,
    10.0,
  ];

  bool isLandscape = true;

  final String TAG = 'ControlsOverlay ';

  @override
  Widget build(BuildContext context) {
    // switch(widget.orientation){
    //   case AppConstants.landscapeOrientation:
    //     _isLandscape = true;
    //     break;
    //   case AppConstants.portraitOrientations:
    //     _isLandscape = false;
    //     break;
    // }
    // print('_isLandscape $_isLandscape');

    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: const Duration(milliseconds: 50),
          reverseDuration: const Duration(milliseconds: 200),
          child: controller.value.isPlaying
              ? const SizedBox.shrink()
              : Container(
                  color: Colors.black26,
                  child: const Center(
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: 100.0,
                      semanticLabel: 'Play',
                    ),
                  ),
                ),
        ),
        GestureDetector(
          onTap: () {
            controller.value.isPlaying ? controller.pause() : controller.play();
          },
        ),
        Align(
          alignment: Alignment.topRight,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: IconOpacity(
              icon: const Icon(Icons.close),
            ),
          ),
        ),
        /*Align(
          alignment: Alignment.bottomRight,
          // child: IconButton(
          //   onPressed: (){
          //     print('$TAG is Landscape to Portrait');
          //     isLandscape ?  showVideoLandscapeDialog(context, controller, this)
          //         : null ;//Navigator.pop(context);
          //
          //     setState(() {
          //       isLandscape = !isLandscape;
          //     });
          //   },
          //   icon: isLandscape ? const Icon(Icons.filter_center_focus) :
          //   const Icon(Icons.zoom_out_map_sharp)
          //   ,
          // )
          child: orientation == AppConstants.portraitOrientations
              ? IconButton(
            onPressed: () {
              print('$TAG is Portrait to Landscape');
              showVideoLandscapeDialog(context);
            },
            icon: const Icon(Icons.filter_center_focus),
          )
              : IconButton(
            onPressed: () {
              print('$TAG is Landscape to Portrait');
              Navigator.pop(context);
            },
            icon: const Icon(Icons.zoom_out_map_sharp),
          ),
        ),*/
        /*Align(
          alignment: Alignment.bottomLeft,
          child: PopupMenuButton<Duration>(
            initialValue: controller.value.captionOffset,
            tooltip: 'Caption Offset',
            onSelected: (Duration delay) {
              controller.setCaptionOffset(delay);
            },
            itemBuilder: (BuildContext context) {
              return <PopupMenuItem<Duration>>[
                for (final Duration offsetDuration in _exampleCaptionOffsets)
                  PopupMenuItem<Duration>(
                    value: offsetDuration,
                    child: Text('${offsetDuration.inMilliseconds}ms'),
                  )
              ];
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(
                // Using less vertical padding as the text is also longer
                // horizontally, so it feels like it would need more spacing
                // horizontally (matching the aspect ratio of the video).
                vertical: 12,
                horizontal: 16,
              ),
              child: Text('${controller.value.captionOffset.inMilliseconds}ms'),
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: PopupMenuButton<double>(
            initialValue: controller.value.playbackSpeed,
            tooltip: 'Playback speed',
            onSelected: (double speed) {
              controller.setPlaybackSpeed(speed);
            },
            itemBuilder: (BuildContext context) {
              return <PopupMenuItem<double>>[
                for (final double speed in _examplePlaybackRates)
                  PopupMenuItem<double>(
                    value: speed,
                    child: Text('${speed}x'),
                  )
              ];
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(
                // Using less vertical padding as the text is also longer
                // horizontally, so it feels like it would need more spacing
                // horizontally (matching the aspect ratio of the video).
                vertical: 12,
                horizontal: 16,
              ),
              child: Text('${controller.value.playbackSpeed}x'),
            ),
          ),
        ),*/
      ],
    );
  }
}
