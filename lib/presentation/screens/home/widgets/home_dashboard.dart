import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../main_screen/widget/drawer_screen.dart';
import '../../../resources/values_manger.dart';
import 'dashboard_item.dart';

class HomeDashboard extends StatelessWidget {
  const HomeDashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        for (int i = 1; i < 8; i += 2)
          Padding(
            padding: const EdgeInsets.only(bottom: AppPadding.p15),
            child: Row(
              children: [
                Expanded(
                    child: DashBoardItem(
                  onTap: () {
                    Navigator.pushNamed(context, drawerItems[i]["route"]);
                  },
                  icon: drawerItems[i]["icon"],
                  text: drawerItems[i]["title"].toString().tr(),
                )),
                const SizedBox(
                  width: AppSize.s15,
                ),
                Expanded(
                    child: DashBoardItem(
                  onTap: () {
                    Navigator.pushNamed(context, drawerItems[i + 1]["route"]);
                  },
                  icon: drawerItems[i + 1]["icon"],
                  text: drawerItems[i + 1]["title"].toString().tr(),
                )),
              ],
            ),
          ),
      ],
    );
  }
}
