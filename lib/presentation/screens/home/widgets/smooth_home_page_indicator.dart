import 'dart:developer';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:path_provider/path_provider.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/application/constants.dart';
import 'package:patientapp/application/extensions.dart';
import 'package:patientapp/application/utils.dart';
import 'package:patientapp/data/models/apiResponse/homePageApiResponse.dart';
import 'package:patientapp/data/models/apiResponse/home_response/gp_working_shifts_response.dart';
import 'package:patientapp/domain/business_logic/home_page/home_bloc.dart';
import 'package:patientapp/domain/business_logic/main_screen_navigator/main_screen_navigator_cubit.dart';
import 'package:patientapp/presentation/resources/routes_manger.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:patientapp/presentation/resources/assets_manager.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';
import 'package:patientapp/presentation/screens/home/widgets/video_landscape_dialog.dart';

import '../../../../application/di.dart';
import '../../../../data/repository/consultations_repositories_imp.dart';
import '../../../../domain/business_logic/subscription_plans/subscription_plans_cubit.dart';
import '../../../../domain/models/digital_prescriptions_mapper.dart';
import '../../../resources/color_manger.dart';
import '../../../widgets/custom_dialog.dart';
import '../../../widgets/error_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';

class SmoothHomePageIndicator extends StatefulWidget {
  const SmoothHomePageIndicator({Key? key}) : super(key: key);

  @override
  _SmoothHomePageIndicatorState createState() =>
      _SmoothHomePageIndicatorState();
}

class _SmoothHomePageIndicatorState extends State<SmoothHomePageIndicator> {
  CarouselController buttonCarouselController = CarouselController();

  int currentIndex = 0;
  ConsultationsRepositoriesImp repository =
      instance<ConsultationsRepositoriesImp>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeBloc(repository)..add(HomePageEvent()),
      child: BlocBuilder<HomeBloc, HomeStates>(
        builder: (_, state) {
          if (state is SuccessHome) {
            if (state.homeItem != null && state.homeItem.isNotEmpty) {
              context.read<MainScreenNavigatorCubit>().callCost =
                  state.homeItem[0].callCost!;
            }
            return Column(
              children: [
                /// slider
                CarouselSlider(
                    items: state.homeItem
                        .map((item) => SmoothHomePage(item))
                        .toList(),
                    carouselController: buttonCarouselController,
                    options: CarouselOptions(
                      height: context.height * .25,
                      viewportFraction: 0.9,
                      initialPage: 0,
                      enableInfiniteScroll: true,
                      reverse: false,
                      autoPlay: true,
                      autoPlayInterval: const Duration(seconds: 5),
                      autoPlayAnimationDuration:
                          const Duration(milliseconds: 1000),
                      autoPlayCurve: Curves.fastOutSlowIn,
                      enlargeCenterPage: true,
                      scrollDirection: Axis.horizontal,
                      onPageChanged: (index, reason) {
                        setState(() {
                          currentIndex = index;
                        });
                      },
                    )),
                const SizedBox(
                  height: AppSize.s10,
                ),

                /// dots slider
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      for (int i = 0; i < state.homeItem.length; i++)
                        Container(
                          height: AppSize.s12,
                          width: AppSize.s12,
                          margin: const EdgeInsets.only(left: AppPadding.p4),
                          decoration: BoxDecoration(
                              color: currentIndex == i
                                  ? ColorManager.primary
                                  : Colors.transparent,
                              borderRadius:
                                  BorderRadius.circular(AppPadding.p20),
                              border: currentIndex == i
                                  ? null
                                  : Border.all(color: ColorManager.primary)),
                        ),
                    ],
                  ),
                )
              ],
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}

class SmoothHomePage extends StatelessWidget {
  final DataListItem homeItem;

  SmoothHomePage(this.homeItem, {Key? key}) : super(key: key);

  String? _thumbnailFile;

  Future<File> copyAssetFile() async {
    final byteData = await rootBundle.load(VideoAssets.tikshifIntroVideo);
    Directory tempDir = await getTemporaryDirectory();

    File tempVideo = File("${tempDir.path}/${VideoAssets.tikshifIntroVideo}")
      ..createSync(recursive: true)
      ..writeAsBytesSync(byteData.buffer
          .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

    return tempVideo;
  }

  @override
  Widget build(BuildContext context) {
    switch (homeItem.extra?.type) {
      case AppStrings.video:
        return Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(AppSize.s20),
            ),
            clipBehavior: Clip.antiAliasWithSaveLayer,
            child: GestureDetector(
              onTap: () {
                showVideoLandscapeDialog(context, homeItem.value ?? "");
              },
              child: Stack(
                alignment: Alignment.center,
                children: [
                  // Image.file(File(_thumbnailFile!)),
                  Image.asset(ImageAssets.videoThumbnail),
                  Container(),
                  const CircleAvatar(
                    radius: 30,
                    backgroundColor: Colors.black45,
                    child: Icon(
                      Icons.play_arrow,
                      size: 40,
                      color: Colors.white,
                    ),
                  )
                ],
              ),
            ));
      case AppStrings.image:
        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(AppSize.s20),
          ),
          clipBehavior: Clip.antiAliasWithSaveLayer,
          child: BlocProvider(
            create: (context) => SubscriptionPlansCubit(
                instance<ConsultationsRepositoriesImp>()),
            child: BlocConsumer<SubscriptionPlansCubit, SubscriptionPlansState>(
              listener: (context, state) {
                log("State::: ${state.getPlansState}  ${state.subscribeState}");
                if (state.subscribeState == RequestState.loaded) {
                  Navigator.pushNamed(context, Routes.paymentScreen,
                      arguments: state.paymentUrl);
                } else if (state.subscribeState == RequestState.error) {
                  handleApiError(context: context, failure: state.failure!);
                }
              },
              builder: (context, state) => GestureDetector(
                onTap: () async {
                  String item = homeItem.extra?.callToAction ?? "";
                  switch (item) {

                    /// call GP
                    case SliderItemsConstants.callGP:
                      if (homeItem.extra?.coupon != null &&
                          homeItem.extra!.coupon!.isNotEmpty) {
                        //CallScenario.couponInit = homeItem.extra?.coupon;
                      }
                      final AppPreferences _appPreferences =
                          instance<AppPreferences>();
                      List<GPWorkingShifts>? gpWorkingShifts =
                          await _appPreferences.getGPWorkingShifts();
                      /* CallScenario.callGpHandlingFunc(
                        context,
                        gpWorkingShifts ?? [],
                      ); */
                      return;

                    /// book appointment
                    case SliderItemsConstants.bookAppointment:
                      Navigator.pushNamed(context, Routes.specialities);
                      return;

                    /// call phone
                    case SliderItemsConstants.callPhone:
                      String url = "tel:${homeItem.extra?.phone}";
                      await launchUrlString(url);
                      return;

                    ///subscription plans
                    case SliderItemsConstants.subscriptionPlansList:
                      Navigator.pushNamed(
                          context, Routes.subscriptionPlansScreen);
                      return;

                    /// subscription Payment
                    case SliderItemsConstants.subscriptionPayment:
                      context
                          .read<SubscriptionPlansCubit>()
                          .subscribe(homeItem.extra!.planId!);

                      return;
                  }
                },
                child: SizedBox(
                  width: context.width * .8,
                  child: CachedNetworkImage(
                    imageUrl: homeItem.value ?? "",
                    fit: BoxFit.fill,
                    placeholder: (context, url) => Lottie.asset(
                      JsonAssets.loadingAnimations,
                    ),
                    errorWidget: (context, url, error) =>
                        const ErrorNetworkImage(),
                  ),
                ),
              ),
            ),
          ),
        );
    }
    return Container();
  }

  void showVideoLandscapeDialog(context, String url) {
    showDialog(
        context: context,
        builder: (BuildContext cxt) {
          return CustomDialog(
              color: Colors.transparent,
              borderRadius: AppSize.s0,
              contentBox: VideoLandscapeDialog(
                url: url,
              ));
        });
  }
}
