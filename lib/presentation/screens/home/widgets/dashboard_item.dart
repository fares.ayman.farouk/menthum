import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../resources/color_manger.dart';
import '../../../resources/values_manger.dart';

class DashBoardItem extends StatelessWidget {
  final Function() onTap;
  final String text;
  final String icon;
  final bool isImage;

  const DashBoardItem(
      {required this.onTap,
      required this.text,
      required this.icon,
      Key? key,
      this.isImage = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 75,
            padding: const EdgeInsets.all(AppPadding.p20),
            decoration: BoxDecoration(
              borderRadius:
                  const BorderRadius.all(Radius.circular(AppRadius.m15)),
              border: Border.all(
                width: AppSize.s1,
                color: ColorManager.roundedContainerStrokeColor,
              ),
              color: ColorManager.roundedContainerColor,
            ),
            child: isImage
                ? Image.asset(
                    icon,
                    height: 80,
                    width: 80,

                  )
                : SvgPicture.asset(icon),
          ),
          const SizedBox(
            height: AppSize.s8,
          ),
          Row(
            children: [
              Expanded(
                child: Text(
                    text,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(
                        color: ColorManager.ThirdColor, fontSize: AppSize.s12),
                  ),
              ),
            ],
          ),

        ],
      ),
    );
  }
}
