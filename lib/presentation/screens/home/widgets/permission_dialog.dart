import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/domain/business_logic/permission/permission_cubit.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';

class PermissionDialog extends StatelessWidget {
  final bool isLocationServicesEnabled;
  final bool isLocationPermissionGranted;

  const PermissionDialog(
      {Key? key,
      required this.isLocationServicesEnabled,
      required this.isLocationPermissionGranted})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(
            height: 15,
          ),
          Text(AppStrings.openLocationPermissionSettings.tr()),
          const SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(AppStrings.locationPermission.tr()),
              TextButton(
                  onPressed: isLocationPermissionGranted
                      ? null
                      : () {
                          context
                              .read<PermissionCubit>()
                              .requestLocationPermission();
                        },
                  child: Text(isLocationPermissionGranted
                      ? AppStrings.allowed.tr()
                      : AppStrings.allow.tr())),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(AppStrings.locationServices.tr()),
              TextButton(
                  onPressed: isLocationServicesEnabled
                      ? null
                      : () {
                          context
                              .read<PermissionCubit>()
                              .openLocationSettings();
                        },
                  child: Text(isLocationServicesEnabled
                      ? AppStrings.allowed.tr()
                      : AppStrings.allow.tr())),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
