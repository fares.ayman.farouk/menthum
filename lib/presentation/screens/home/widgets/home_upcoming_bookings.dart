import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/application/utils.dart';
import 'package:patientapp/data/models/apiResponse/upcoming_response.dart';
import 'package:patientapp/data/repository/consultations_repositories_imp.dart';
import 'package:patientapp/domain/business_logic/upcoming_appointments/upcoming_appointments_bloc.dart';
import 'package:patientapp/presentation/resources/routes_manger.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';
import 'package:patientapp/presentation/resources/styles_manger.dart';
import 'package:patientapp/presentation/screens/home/widgets/upcomingItem.dart';
import 'package:shimmer/shimmer.dart';

import '../../../resources/color_manger.dart';
import '../../../resources/values_manger.dart';

class HomeUpcomingBookings extends StatefulWidget {
  @override
  State<HomeUpcomingBookings> createState() => _HomeUpcomingBookingsState();
}

class _HomeUpcomingBookingsState extends State<HomeUpcomingBookings> {
  ConsultationsRepositoriesImp repository =
  instance<ConsultationsRepositoriesImp>();

  late List<AppointmentItem> upcomingAppointmentsList;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
      UpcomingAppointmentsBloc(repository)
        ..add(UpcomingAppointmentsEvent()),
      child: BlocConsumer<UpcomingAppointmentsBloc, UpcomingAppointmentsStates>(
        listener: (context, state) {
          if (state is LoadingAppointmentsState) {} else
          if (state is SuccessUpcomingAppointmentsState) {} else
          if (state is FailedAppointmentsState) {
            handleApiError(context: context, failure: state.failure);
          }
        },
        builder: (context, state) {
          return FocusDetector(
            onVisibilityGained: () {
              /// get the new upcoming data.
              context
                  .read<UpcomingAppointmentsBloc>()
                  .add(UpcomingAppointmentsEvent());
            },
            child: buildUI(state),
          );
        },
      ),
    );
  }

  Widget buildUI(state) {
    if (state is SuccessUpcomingAppointmentsState) {
      upcomingAppointmentsList = state.upcomingAppoitmentsList;
      return Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppStrings.upcomingBookings.tr(),
                style: Theme
                    .of(context)
                    .textTheme
                    .titleMedium
                    ?.copyWith(fontSize: AppSize.s20),
              ),
              TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, Routes.viewAllAppointmentsScreen,
                      arguments: upcomingAppointmentsList);
                },
                child: Text(
                  AppStrings.viewAll.tr(),
                  style: Theme
                      .of(context)
                      .textTheme
                      .titleMedium
                      ?.copyWith(
                      color: ColorManager.primary,
                      decoration: TextDecoration.underline),
                ),
              ),
            ],
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            physics: BouncingScrollPhysics(),
            child: Row(
              children: [
                for (int i = 0; i < upcomingAppointmentsList.length; i++)
                  UpcomingItem(
                    appointmentItem: upcomingAppointmentsList[i],
                    showStatus: false,
                    //inViewAll: false,
                  )
              ],
            ),
          ),
        ],
      );
    } else if (state is LoadingAppointmentsState) {
      return _loadingShimmer();
    } else if (state is EmptyAppointmentsState) {
      return Container();
    } else {
      return Container();
    }
  }

  _loadingShimmer() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Shimmer.fromColors(
              baseColor: Colors.grey.shade400,
              highlightColor: Colors.grey.shade100,
              child: Container(
                width: AppSize.s100,
                height: AppSize.s30,
                color: Colors.white,
              ),
            ),
            Shimmer.fromColors(
              baseColor: Colors.grey.shade400,
              highlightColor: Colors.grey.shade100,
              child: Container(
                width: AppSize.s100,
                height: AppSize.s30,
                color: Colors.white,
              ),
            ),
          ],
        ),
        SizedBox(
          height: AppSize.s8,
        ),
        Container(
          padding: const EdgeInsets.all(AppPadding.p20),
          decoration: BoxDecoration(
              boxShadow: shadowList,
              borderRadius: const BorderRadius.all(
                Radius.circular(AppRadius.m20),
              ),
              color: ColorManager.roundedContainerColor),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Shimmer.fromColors(
                        baseColor: Colors.grey.shade400,
                        highlightColor: Colors.grey.shade100,
                        child: Container(
                          width: AppSize.s100,
                          height: AppSize.s30,
                          color: Colors.white,
                        ),
                      ),
                      const SizedBox(
                        height: AppSize.s5,
                      ),
                    ],
                  ),
                  Shimmer.fromColors(
                    baseColor: Colors.grey.shade400,
                    highlightColor: Colors.grey.shade100,
                    child: CircleAvatar(),
                  ),
                ],
              ),
              const SizedBox(
                height: AppSize.s20,
              ),
              Container(
                padding: const EdgeInsets.all(AppPadding.p15),
                decoration: BoxDecoration(
                  borderRadius:
                  const BorderRadius.all(Radius.circular(AppRadius.m15)),
                  color: ColorManager.background,
                ),
                child: Row(
                  children: [
                    Shimmer.fromColors(
                      baseColor: Colors.grey.shade400,
                      highlightColor: Colors.grey.shade100,
                      child: Container(
                        width: 40.0,
                        height: 40.0,
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(
                              Radius.circular(AppRadius.m15)),
                          color: ColorManager.roundedContainerColor,
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: AppSize.s10,
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey.shade400,
                      highlightColor: Colors.grey.shade100,
                      child: Container(
                        width: AppSize.s100,
                        height: AppSize.s30,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
