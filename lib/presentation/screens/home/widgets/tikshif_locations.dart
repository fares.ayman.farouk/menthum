import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/application/extensions.dart';
import 'package:patientapp/application/utils.dart';
import 'package:patientapp/data/repository/consultations_repositories_imp.dart';
import 'package:patientapp/domain/business_logic/nano_clinics/nano_clinics_cubit.dart';
import 'package:patientapp/domain/business_logic/permission/permission_cubit.dart';
import 'package:patientapp/domain/services/google_maps/google_maps_service.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';
import 'package:patientapp/presentation/resources/routes_manger.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';
import 'package:patientapp/presentation/screens/home/widgets/permission_dialog.dart';
import 'package:patientapp/presentation/widgets/app_settings_dialog.dart';
import 'package:shimmer/shimmer.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class TikshifLocations extends StatefulWidget {
  const TikshifLocations({Key? key}) : super(key: key);

  @override
  State<TikshifLocations> createState() => _TikshifLocationsState();
}

class _TikshifLocationsState extends State<TikshifLocations> {
  NanoClinicsCubit nanoClinicsCubit =
      NanoClinicsCubit(instance<ConsultationsRepositoriesImp>());
  bool finishInitialization = false;
  final Completer<GoogleMapController> _mapController = Completer();

  @override
  void initState() {
    initializeMapsService();
  }

  initializeMapsService() async {
    await nanoClinicsCubit.getNanoClinicsInfo();
    await GoogleMapsServices.initializeIcons();
    // await GoogleMapsServices.listenForPermissionStatus();

    setState(() {
      finishInitialization = true;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<NanoClinicsCubit>(
      create: (context) => nanoClinicsCubit,
      child: BlocConsumer<NanoClinicsCubit, NanoClinicsState>(
        listener: (context, state) {
          if (state is FailedNanoClinicsState) {
            handleApiError(context: context, failure: state.failure);
          }
        },
        builder: (context, state) {
          if (state is LoadedNanoClinicsState) {
            return Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      AppStrings.labelTikshifLocations.tr(),
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium
                          ?.copyWith(fontSize: AppSize.s20),
                    ),
                    MultiBlocListener(
                      listeners: [
                        BlocListener<PermissionCubit, PermissionState>(
                          listenWhen: (p, c) =>
                              p.isLocationPermissionGrantedAndServiceEnabled !=
                                  c.isLocationPermissionGrantedAndServiceEnabled &&
                              c.isLocationPermissionGrantedAndServiceEnabled &&
                              c.displayPermissionDialog,
                          listener: (context, permissionState) {
                            permissionState.displayPermissionDialog = false;

                            Navigator.pop(context);

                            Navigator.pushNamed(
                                context, Routes.tikshifLocationsScreen,
                                arguments: state.nanoClinics);
                          },
                        ),
                        BlocListener<PermissionCubit, PermissionState>(
                          listenWhen: (p, c) =>
                              p.displayOpenAppSettingsDialog !=
                                  c.displayOpenAppSettingsDialog &&
                              c.displayOpenAppSettingsDialog,
                          listener: (context, state) {
                            showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  content: AppSettingsDialog(
                                    cancelDialog: () {
                                      context
                                          .read<PermissionCubit>()
                                          .hidOpenAppSettingsDialog();
                                    },
                                    openAppSettings: () {
                                      context
                                          .read<PermissionCubit>()
                                          .openAppSettings();
                                    },
                                    message: AppStrings.cantContinue.tr(),
                                  ),
                                );
                              },
                            ).then((value) => context
                                .read<PermissionCubit>()
                                .hidOpenAppSettingsDialog());
                          },
                        ),
                        BlocListener<PermissionCubit, PermissionState>(
                          listenWhen: (p, c) =>
                              p.displayOpenAppSettingsDialog !=
                                  c.displayOpenAppSettingsDialog &&
                              !c.displayOpenAppSettingsDialog,
                          listener: (context, state) {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                      child: TextButton(
                        onPressed: () {
                          /// request location permission.
                          bool isPermissionsAvailable = context
                              .read<PermissionCubit>()
                              .state
                              .isLocationPermissionGrantedAndServiceEnabled;

                          if (isPermissionsAvailable) {
                            Navigator.pushNamed(
                                context, Routes.tikshifLocationsScreen,
                                arguments: state.nanoClinics);
                          } else {
                            context
                                .read<PermissionCubit>()
                                .state
                                .displayPermissionDialog = true;
                            showDialog(
                              context: context,
                              builder: (context) {
                                final bool isLocationPermissionGranted = context
                                    .select((PermissionCubit element) => element
                                        .state.isLocationPermissionGranted);
                                // to get the new data right away.
                                final bool isLocationServicesEnabled = context
                                    .select((PermissionCubit element) => element
                                        .state.isLocationServicesEnabled);
                                return AlertDialog(
                                  content: PermissionDialog(
                                      isLocationPermissionGranted:
                                          isLocationPermissionGranted,
                                      isLocationServicesEnabled:
                                          isLocationServicesEnabled),
                                );
                              },
                            ).then((value) => context
                                .read<PermissionCubit>()
                                .state
                                .displayPermissionDialog = false);
                          }
                        },
                        child: Text(
                          AppStrings.viewAll.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium
                              ?.copyWith(
                                  color: ColorManager.primary,
                                  decoration: TextDecoration.underline),
                        ),
                      ),
                    ),
                  ],
                ),
                /* finishInitialization &&
                        GoogleMapsServices.tikshifSelectedMarker != null
                    ? Container(
                        height: context.height * .3,
                        width: context.width,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(AppSize.s24)),
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        child: MapWidget(
                            nanoClinics: state.nanoClinics,
                            mapController: _mapController,
                            indexSelected: state.nanoClinics[0].id,
                            tikshifNotSelectedMarker:
                                GoogleMapsServices.tikshifNotSelectedMarker!,
                            tikshifSelectedMarker:
                                GoogleMapsServices.tikshifSelectedMarker!,
                            initialCameraPosition: LatLng(
                                GoogleMapsServices.currentLocation?.latitude ??
                                    state.nanoClinics[0].lat,
                                GoogleMapsServices.currentLocation?.longitude ??
                                    state.nanoClinics[0].lng)),
                      )
                    : Container(), */
              ],
            );
          } else if (state is LoadingNanoClinicsState) {
            return _loadingShimmer();
          } else {
            return Container();
          }
        },
      ),
    );
  }

  _loadingShimmer() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Shimmer.fromColors(
              baseColor: Colors.grey.shade400,
              highlightColor: Colors.grey.shade100,
              child: Container(
                width: AppSize.s100,
                height: AppSize.s30,
                color: Colors.white,
              ),
            ),
            Shimmer.fromColors(
              baseColor: Colors.grey.shade400,
              highlightColor: Colors.grey.shade100,
              child: Container(
                width: AppSize.s100,
                height: AppSize.s30,
                color: Colors.white,
              ),
            ),
          ],
        ),
        SizedBox(
          height: AppSize.s8,
        ),
        Container(
          width: double.infinity,
          height: context.height * .2,
          child: Shimmer.fromColors(
            baseColor: Colors.grey.shade400,
            highlightColor: Colors.grey.shade100,
            child: Container(
              width: AppSize.s100,
              height: AppSize.s30,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }
}
