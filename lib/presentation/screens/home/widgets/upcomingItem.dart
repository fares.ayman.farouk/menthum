import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:patientapp/application/constants.dart';
import 'package:patientapp/application/extensions.dart';
import 'package:patientapp/application/utils.dart';
import 'package:patientapp/data/models/apiResponse/upcoming_response.dart';
import 'package:patientapp/presentation/resources/assets_manager.dart';
import 'package:patientapp/presentation/resources/font_manger.dart';
import 'package:patientapp/presentation/resources/language_manager.dart';
import 'package:patientapp/presentation/resources/routes_manger.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';
import 'package:patientapp/presentation/widgets/upcoming_date_time.dart';

import '../../../resources/color_manger.dart';
import '../../../resources/styles_manger.dart';
import '../../../resources/values_manger.dart';
import '../../../widgets/error_network_image.dart';

class UpcomingItem extends StatefulWidget {
  final AppointmentItem appointmentItem;
  bool inViewAll;
  bool headerVisible;
  double? width;
  bool showStatus;

  UpcomingItem(
      {Key? key,
      required this.appointmentItem,
      this.inViewAll = false,
      this.headerVisible = false,
      this.showStatus = true,
      this.width})
      : super(key: key);

  @override
  State<UpcomingItem> createState() => _UpcomingItemState();
}

class _UpcomingItemState extends State<UpcomingItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.inViewAll ? double.infinity : context.width * .85,
      margin: const EdgeInsets.all(AppSize.s5),
      padding: const EdgeInsets.all(AppPadding.p20),
      decoration: appBoxDecoration.copyWith(color: ColorManager.roundedContainerColor),
      child: Stack(children: [
        Align(
          alignment: isRtl(context) ? Alignment.topLeft : Alignment.topRight,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Visibility(
                visible: widget.appointmentItem.status.toString().isNotEmpty &&
                    widget.appointmentItem.statusId.toString().isNotEmpty,
                child: Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: AppSize.s10, vertical: AppSize.s5),
                  decoration: BoxDecoration(
                    color: changeStatusColor(),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(AppRadius.m5),
                    ),
                  ),
                  child: Text(
                    textAlign: TextAlign.start,
                    widget.appointmentItem.status ?? "",
                    style: Theme.of(context).textTheme.bodySmall?.copyWith(
                        fontSize: FontSize.s9, color: ColorManager.white),
                  ),
                ),
              ),
              Container(
                height: AppSize.s30,
                child: InkWell(
                    onTap: () => openGoogleMap(
                        lat: widget.appointmentItem.lat ?? 0,
                        lng: widget.appointmentItem.lng ?? 0),
                    child: SvgPicture.asset(ImageAssets.homeRoundedArrow)),
              ),
            ],
          ),
        ),
        Column(
          children: [
            if (widget.appointmentItem.inHome == AppConstants.IN_HOME ||
                widget.appointmentItem.type == AppConstants.CLINIC ||
                widget.headerVisible == true)
              Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      UpcomingDateTime(
                        date: "${widget.appointmentItem.date}",
                        time: "${widget.appointmentItem.time}",
                        status: widget.appointmentItem.status ?? "",
                        statusId: widget.appointmentItem.statusId ?? "0",
                        showStatus: widget.showStatus,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: AppSize.s10,
                  ),
                ],
              ),
            if (widget.appointmentItem.tests != null &&
                (widget.headerVisible == true ||
                    widget.appointmentItem.inHome == AppConstants.NOT_HOME))
              _tests(),
            GestureDetector(
              onTap: () {
                appointmentOnTap();
              },
              child: Container(
                padding: const EdgeInsets.all(AppPadding.p10),
                decoration: BoxDecoration(
                  borderRadius:
                      const BorderRadius.all(Radius.circular(AppRadius.m15)),
                  color: ColorManager.background,
                ),
                child: Row(
                  children: [
                    Container(
                        width: 50.0,
                        height: 50.0,
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(
                              Radius.circular(AppRadius.m32)),
                          color: ColorManager.roundedContainerColor,
                        ),
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        child: CachedNetworkImage(
                          imageUrl: widget.appointmentItem.logo ?? "",
                          fit: BoxFit.fitWidth,
                          placeholder: (context, url) => Lottie.asset(
                            JsonAssets.loadingAnimations,
                          ),
                          errorWidget: (context, url, error) =>
                          const ErrorNetworkImage(),
                        )),
                    const SizedBox(
                      width: AppSize.s10,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.appointmentItem.name ?? "",
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium
                                ?.copyWith(
                                    fontSize: AppSize.s14,
                                    ),
                          ),
                          SizedBox(
                            width: context.width * .4,
                            child: Text(
                              widget.appointmentItem.type == AppConstants.CLINIC
                                  ? widget.appointmentItem.specialty!.name ?? ""
                                  : widget.appointmentItem.address ?? "",
                              maxLines: 2,
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(
                                      color: ColorManager.fontLight,
                                      fontSize: AppSize.s12),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ]),
    );
  }

  _tests() {
    String tests = widget.appointmentItem.tests!
        .map((e) => e.name)
        .toString()
        .replaceAll('(', "")
        .replaceAll(')', "")
        .replaceAll(",", " - ");
    //print(tests);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppStrings.tests.tr(),
          style: Theme.of(context).textTheme.titleMedium?.copyWith(
                fontSize: AppSize.s14,
              ),
        ),
        const SizedBox(
          height: AppSize.s5,
        ),
        Row(
          children: [
            SvgPicture.asset(ImageAssets.dashboardResults, width: AppSize.s25),
            const SizedBox(
              width: AppSize.s10,
            ),
            SizedBox(
              width: context.width * .6,
              child: Text(
                tests,
                style: Theme.of(context).textTheme.bodySmall?.copyWith(
                    color: ColorManager.primary, fontSize: AppSize.s13),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: AppSize.s10,
        ),
      ],
    );
  }

  void appointmentOnTap() {
    /// 1 clinic - 2 lab - 3 rads.
    if (widget.appointmentItem.type == AppConstants.CLINIC) {
      Navigator.pushNamed(context, Routes.doctorProfileScreen, arguments: {
        "isUpcoming": true,
        "profileId": widget.appointmentItem.id,
        "date": widget.appointmentItem.date,
        "time": widget.appointmentItem.time,
        "status": widget.appointmentItem.status,
        "statusId": widget.appointmentItem.statusId,
      });
    } else if (widget.appointmentItem.type == AppConstants.LAB) {
      Navigator.pushNamed(context, Routes.labProfileScreen, arguments: {
        "isUpcoming": true,
        "labId": widget.appointmentItem.id,
        "availableTests": widget.appointmentItem.tests,
        "date": widget.appointmentItem.date,
        "time": widget.appointmentItem.time,
        "totalCost": widget.appointmentItem.totalCost.toString(),
        "totalTime": widget.appointmentItem.totalTime.toString(),
        "status": widget.appointmentItem.status,
        "statusId": widget.appointmentItem.statusId,
      }); //96421c69-fa47-473c-8027-27d1e44321d3
    } else if (widget.appointmentItem.type == AppConstants.RAD) {
      Navigator.pushNamed(context, Routes.radProfileScreen, arguments: {
        "isUpcoming": true,
        "radId": widget.appointmentItem.id,
        "availableTests": widget.appointmentItem.tests,
        "date": widget.appointmentItem.date,
        "time": widget.appointmentItem.time,
        "totalCost": widget.appointmentItem.totalCost.toString(),
        "totalTime": widget.appointmentItem.totalTime.toString(),
        "status": widget.appointmentItem.status,
        "statusId": widget.appointmentItem.statusId,
      }); //96421c69-fa47-473c-8027-27d1e44321d3
    }
  }

  Color changeStatusColor() {
    if (int.parse(widget.appointmentItem.statusId ?? "0") ==
            AppConstants.APPOINTMENT_CANCELED ||
        int.parse(widget.appointmentItem.statusId ?? "0") ==
            AppConstants.APPOITNMENT_UNCONFIRMED) {
      return Colors.red;
    } else if (int.parse(widget.appointmentItem.statusId ?? "0") ==
            AppConstants.APPOINTMENT_CONFIRMED ||
        int.parse(widget.appointmentItem.statusId ?? "0") ==
            AppConstants.APPOINTMENT_COMPLETED) {
      return Colors.green;
    } else {
      return Colors.blue;
    }
  }
}
