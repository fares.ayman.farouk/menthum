class AppMargin {
  static const double m8 = 8.0;
}

class AppRadius {
  static const double m40 = 40.0;
  static const double m32 = 32.0;
  static const double m25 = 25.0;
  static const double m20 = 20.0;
  static const double m15 = 15.0;
  static const double m17 = 17.0;
  static const double m12 = 12.0;
  static const double m10 = 10.0;
  static const double m8 = 8.0;
  static const double m5 = 5.0;
}

class AppPadding {
  static const double p1 = 1.0;
  static const double p4 = 4.0;
  static const double p8 = 8.0;
  static const double p10 = 10.0;
  static const double p12 = 12.0;
  static const double p13 = 13.0;
  static const double p14 = 14.0;
  static const double p15 = 15.0;
  static const double p18 = 18.0;
  static const double p16 = 16.0;
  static const double p20 = 20.0;
  static const double p24 = 24.0;
  static const double p30 = 30.0;
  static const double p40 = 40.0;
  static const double p50 = 50.0;
  static const double p70 = 70.0;
  static const double p100 = 100.0;
}

class AppSize {
  static const double s_1 = -1;
  static const double s0 = 0;
  static const double s0_25 = 0.25;
  static const double s1 = 1;
  static const double s1_5 = 1.5;
  static const double s2 = 2;
  static const double s3 = 3;
  static const double s4 = 4;
  static const double s5 = 5;
  static const double s6 = 6;
  static const double s7 = 7;
  static const double s8 = 8;
  static const double s10 = 10;
  static const double s12 = 12;
  static const double s13 = 13;
  static const double s14 = 14;
  static const double s15 = 15;
  static const double s16 = 16;
  static const double s17 = 17;
  static const double s18 = 18;
  static const double s20 = 20;
  static const double s24 = 24;
  static const double s25 = 25;
  static const double s30 = 30;
  static const double s32 = 32;
  static const double s40 = 40;
  static const double s44 = 44;
  static const double s50 = 50;
  static const double s56 = 56;
  static const double s70 = 70;
  static const double s80 = 80;
  static const double s60 = 60;
  static const double s90 = 90;
  static const double s120 = 120;
  static const double s125 = 125;
  static const double s100 = 100;
  static const double s130 = 130;
  static const double s170 = 170;
  static const double s200 = 200;
  static const double s250 = 250;
  static const double s300 = 300;
  static const double s350 = 350;

  static const double heightSpecialityAppointment = 210;
  static const double heightLaboratoryAppointment = 260;
  static const double heightRadiologyAppointment = 270;
}
