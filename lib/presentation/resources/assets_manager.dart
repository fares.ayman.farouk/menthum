const String imagePath = "assets/images";
const String jsonPath = "assets/json";
const String iconPath = "assets/icons";
const String videoPath = "assets/video";

class VideoAssets {
  static const String tikshifIntroVideo = "$videoPath/tikshif_intro_video.mp4";
}

class ImageAssets {
  static const String splashLogo = "$imagePath/splash_icon.svg";
  static const String unknownUser = "$imagePath/unknown_user.png";
  static const String appLogo = "$imagePath/logo_ver.png";
  static const String onBoarding1 = "$imagePath/onboarding_1.png";
  static const String onBoarding2 = "$imagePath/onboarding_2.png";
  static const String onBoarding3 = "$imagePath/onboarding_3.png";
  static const String authLogo = "$imagePath/auth_logo.svg";
  static const String google = "$imagePath/google.svg";
  static const String facebook = "$imagePath/facebook.svg";
  static const String appDrawerIcon = "$imagePath/app_drawer_icon.svg";
  static const String notificationIcon = "$imagePath/notification_icon.svg";
  static const String notificationEmptyIcon = "$imagePath/notification_icon_empty.svg";
  static const String addIcon = "$imagePath/add_icon.svg";
  static const String extendedMap = "$imagePath/extended_map.png";
  static const String videoThumbnail = "$imagePath/video_thumbnail2.JPG";

  static const String tikshifNotSelectedMarker = "$imagePath/tikshif_not_selected_marker.png";
  static const String tikshifSelectedMarker = "$imagePath/tikshif_selected_marker.png";

  /// main Screen Images
  static const String homeIcon = "$imagePath/home_icon.svg";
  static const String profileIcon = "$imagePath/profile_icon.svg";
  static const String homeSelectedIcon = "$imagePath/home_selected_icon.svg";
  static const String profileSelectedIcon = "$imagePath/profile_selected_icon.svg";
  static const String bookDoctorIcon = "$imagePath/book_doctor_icon.svg";
  static const String callGpAr = "$imagePath/call_gp_ar.svg";
  static const String callGpEn = "$imagePath/call_gb_en.svg";

 /// visit details
  static const String clock = "$imagePath/clock.svg";
  static const String mapLocationIcon = "$imagePath/map_loction_icon.svg";
  static const String doctorAvatar = "$imagePath/doctor_avatar.svg";
  static const String pdfIcon = "$imagePath/pdf.png";
  static const String downloadIcon = "$imagePath/download_Icon.png";
  static const String nutrition = "$imagePath/nutrition.png";


  static const String homeClock = "$imagePath/clock.svg";
  static const String homeRoundedArrow = "$imagePath/rounded_arrow.svg";
  static const String homeRoundedPerson = "$imagePath/rectangle_person.png";
  static const String dashboardPrograms = "$imagePath/dashboard_programs.svg";
  static const String dashboardResults = "$imagePath/dashboard_results.svg";
  static const String dashboardVitals = "$imagePath/dashboard_vitals.svg";
  static const String dashboardPrescriptions = "$imagePath/dashboard_prescriptions.svg";
  static const String map = "$imagePath/map.png";
  static const String location = "$imagePath/location.svg";
  static const String call = "$imagePath/call.svg";

  static const String notification = "$imagePath/notification.svg";
  static const String prescriptionsItemSVG = "$imagePath/prescriptions_item.svg";
  static const String prescriptionsItemPNG = "$imagePath/prescriptions_item.png";

  /// Specialities
  static const String searchIcon = "$imagePath/search_icon.svg";
  static const String cardilogy = "$imagePath/cardilogy.png";

  static const String spinner = "$imagePath/spinner.png";


}

class JsonAssets {
  static const String loadingAnimations = "$jsonPath/loading_indecator.json";
  static const String loadingCallGb = "$jsonPath/loading_call_gb.json";
  static const String noInternet = "$jsonPath/no_internet.json";
}

class IconAssets {
  static const String homeIcon = "$iconPath/home.svg";
  static const String blogIcon = "$iconPath/blog.svg";
  static const String prescriptionsIcon = "$iconPath/prescriptions.svg";
  static const String resultsIcon = "$iconPath/results.svg";
  static const String programsIcon = "$iconPath/programs.svg";
  static const String signOutIcon = "$iconPath/sign_out.svg";
  static const String vitalsIcon = "$iconPath/results.svg";
  static const String userIcon = "$iconPath/user.svg";
  static const String backButton = "$iconPath/back_button.svg";
  static const String filterButton = "$iconPath/filter.svg";
  static const String editIcon = "$iconPath/edit.svg";
  static const String chatIcon = "$iconPath/chat.svg";
  static const String qrCode = "$iconPath/qr_code.svg";

  /// programs
  static const String healthProfileIcon = "$iconPath/health_profile_icon.png";
  static const String obgynIcon = "$iconPath/obgyn_icon.png";
  static const String diabetesIcon = "$iconPath/diabetes_icon.png";
  static const String psychologyIcon = "$iconPath/psychology_icon.png";
  static const String pediatricIcon = "$iconPath/pediatric_icon.png";


  /// calculations
  static const String calculatorIcon = "$iconPath/calculator.svg";
  static const String questionnaireIcon = "$iconPath/questionnaire.svg";

}
