import 'package:flutter/material.dart';

class ColorManager {
  static Color blackColor = const Color(0xff070C18);
  static Color fontLight = const Color(0xff727A83);
  static Color secondFontLight = const Color(0x88070C18);
  static Color pageIndicator = const Color(0x0ffbf1f4);
  static const Color primary = Color(0xff08A79E);
  static const Color primaryLight = Color(0xff4DD9D1);
  static Color background = const Color(0xfFFFFFFF);
  static Color secondColor = const Color(0xff343333);
  static Color fontColor = const Color(0xff070C18);
  static Color white = const Color(0xfFFFFFFF);
  static Color offWhite = const Color(0xffFCFCFC);
  static Color bgButton = const Color(0xff27b2aa);
  static Color callButton = const Color(0xff16d39a);
  static Color hintColor = const Color(0xff8189b0);
  static Color textFiledColor = const Color(0xff8189b0);
  static Color textSocialColor = const Color(0xff2E3E5C);
  static Color textColor = const Color(0xff708399);
  static Color bgTextFieldColor = const Color(0xffFAFAFA);
  static Color error = const Color(0xff070C18);
  static Color transparentSheet = const Color(0xff757575);

  static Color profileBackground = const Color(0xfff8f9fe);

  static Color specialistLight = const Color(0xff8A94A4);
  static Color fontDark = const Color(0xff7581A1);
  static Color iconLight = const Color(0xffF0F0F0);
  static Color iconDark = const Color(0xff182D64);

  static Color ThirdColor = const Color(0xff0D253C);
  static Color inactiveSwitchTrackColor = const Color(0xff99A1AE);
  static Color fourthColor = const Color(0xff6B7E7C);
  static Color roundedContainerColor = const Color(0xffFBFBFB);
  static Color roundedContainerStrokeColor = const Color(0xffEDEDED);
  static Color dividerColor = const Color(0xffE9EDEC);
  static Color notificationBGColor = const Color(0xffFCFCFC);
  static Color notificationBgNotReadColor = const Color(0xffdefaff);

  static const Color borderColor = Color(0xffE85454);

  static Color green = const Color(0xff21bf1b);
  static Color fertileColor =
      const Color(0xFF333A47); // const Color(0xff6DCFFF);
  static Color trackColor = const Color(0xff98DBFC);
}
