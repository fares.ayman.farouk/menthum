import 'dart:developer';

import 'package:camera/camera.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/verify_request.dart';
import 'package:patientapp/data/models/apiResponse/tests_response.dart';
import 'package:patientapp/data/models/apiResponse/tikshif_locations/nano_clinic_response.dart';
import 'package:patientapp/data/models/apiResponse/upcoming_response.dart';
import 'package:patientapp/data/repository/question_repositories_imp.dart';
import 'package:patientapp/domain/business_logic/dependent_account_bloc/dependent_account_bloc.dart';
import 'package:patientapp/domain/business_logic/notification_list/notification_list_bloc.dart';
import 'package:patientapp/domain/business_logic/question/question_bloc.dart';
import 'package:patientapp/domain/models/digital_prescriptions_mapper.dart';
import 'package:patientapp/domain/models/menstrual_cycle/menstrual_cycle.dart';
import 'package:patientapp/domain/services/analytics_services/analytics_services.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';
import 'package:patientapp/presentation/screens/auth_screens/auth_screen/auth_acreen.dart';
import 'package:patientapp/presentation/screens/auth_screens/otp_screen/otp_screen.dart';
import 'package:patientapp/presentation/screens/auth_screens/set_password/set_password_screen.dart';
import 'package:patientapp/presentation/screens/main_screen/notification_page.dart';
import 'package:patientapp/presentation/transaction_history/transaction_data_page.dart';
import 'package:patientapp/presentation/transaction_history/transaction_history_page.dart';

import 'package:showcaseview/showcaseview.dart';

import '../../application/constants.dart';
import '../../data/models/apiResponse/call_gp/call_grp_response.dart';
import '../../data/models/apiResponse/specialties_response.dart';
import '../../domain/business_logic/programs_blocs/diabetes/diabetes_bloc.dart';
import '../../domain/models/pediatric/pediatric_home.dart';
import '../screens/auth_screens/change_phone.dart';
import '../screens/auth_screens/forget_password/forget_password_screen.dart';
import '../screens/auth_screens/forget_password/reset_password_screen.dart';
import '../screens/main_screen/main_screen.dart';
import '../screens/splash_screen.dart';

class Routes {
  static const String splashRoute = "/";
  static const String introScreen = "/introScreen";

  static const String authScreen = "/AuthScreen";
  static const String resetPasswordScreen = "/ResetPasswordScreen";
  static const String forgetPasswordScreen = "/forgetPasswordScreen";
  static const String welcome = "/welcome";
  static const String onBoardingRoute = "/onBoarding";
  static const String mainScreen = "/mainScreen";
  static const String myVisits = "/myVisits";
  static const String visitDetails = "/visitDetails";
  static const String doctorsScreenList = "/doctorsScreenList";
  static const String profilesScreen = "/profileScreen";
  static const String doctorProfileScreen = "/doctorProfileScreen";
  static const String addDependentScreen = "/addDependentScreen";

  static const String notificationsScreen = "/notificationsScreen";
  static const String settingsScreen = "/settingsScreen";

  static const String assigningDoctor = "/assigningDoctorScreen";
  static const String specialities = "/SpecialitiesScreen";
  static const String myVitals = "/myVitalsScreen";
  static const String otpScreen = "/otpScreen";
  static const String questionScreen = "/questionScreen";
  static const String viewAllAppointmentsScreen = "/viewAllAppointmentsScreen";
  static const String prescriptions = "/prescriptions";

  static const String labsListScreen = "/labsListScreen";
  static const String labProfileScreen = "/labProfileScreen";
  static const String radsListScreen = "/radsListScreen";
  static const String radProfileScreen = "/radProfileScreen";
  static const String myProgramsScreen = "/myProgramsScreen";
  static const String rechargeBalanceScreen = "/rechargeBalanceScreen";
  static const String subscriptionPlansScreen = "/subscriptionPlansScreen";
  static const String paymentScreen = "/paymentScreen";
  static const String testVideoScreen = "/testVideoScreen";
  static const String medicalPrescriptions = "/medicalPrescriptions";
  static const String setPassword = "/setPassword";
  static const String myRecords = "/myRecords";
  static const String tikshifLocationsScreen = "/tikshifLocationsScreen";
  static const String changePhone = "/changePhone";
  static const String forceUpdate = "/forceUpdate";
  static const String waitingCallScreen = "/waitingCallScreen";
  static const String renewSubscription = "/renewSubscription";
  static const String blogsScreen = "/blogsScreen";
  static const String blogDetailsScreen = "/blogDetailsScreen";
  static const String menstrualCycleScreen = "/menstrualCycleScreen";
  static const String menstrual_list = "/menstrual_list";
  static const String vitalDetails = "/vitalDetails";
  static const String calculation = "/calculation";
  static const String bmi = "/bmi";
  static const String basic_colorie_intake = "/basic_colorie_intake";
  static const String pregnancy = "/pregnancy";
  static const String questionnairesListScreen = "/questionnairesListScreen";
  static const String questionnaireQuestionsScreen =
      "/questionnaireQuestionsScreen";
  static const String diabetesScreen = "/diabetes";
  static const String diabetesHistoryScreen = "/diabetesHistoryScreen";
  static const String pediatric = "/pediatric";
  static const String pediatricHome = "/pediatricHome";
  static const String pediatricVaccine = "/pediatricHomeType1";
  static const String pediatricHeadCircum = "/pediatricHomeType2";
  static const String pediatricBabyWeight = "/pediatricHomeType3";
  static const String pediatricHistoryScreen = "/pediatricHistoryScreen";
  static const String transactionhistoryScreen = "/transactionhistoryScreen";
  static const String transactionDataScreen = "/transactionDataScreen";
}

class RouteGenerator {
  static Route<dynamic> getRoute(RouteSettings settings) {
    AnalyticsServices analyticsServices = instance<AnalyticsServices>();

    switch (settings.name) {
      case Routes.splashRoute:
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.splashRoute),
            builder: (_) => const SplashScreen());

      case Routes.authScreen:
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.authScreen),
          builder: (_) => const AuthScreen(),
        );

      case Routes.transactionhistoryScreen:
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.transactionhistoryScreen),
          builder: (_) => const TransactionHistoryPage(),
        );
      case Routes.transactionDataScreen:
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.transactionDataScreen),
          builder: (_) => const TransactionDataPage(),
        );

      case Routes.notificationsScreen:
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.notificationsScreen),
          builder: (_) => const NotificationPage(),
        );

      case Routes.resetPasswordScreen:
        final resetPasswordRequest = settings.arguments as Map;
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.resetPasswordScreen),
            builder: (_) => ResetPasswordScreen(
                  countryCode: resetPasswordRequest[AppStrings.code],
                  mobile: resetPasswordRequest[AppStrings.phoneNumber],
                ));

      case Routes.forgetPasswordScreen:
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.forgetPasswordScreen),
            builder: (_) => ForgetPasswordScreen());

      /* case Routes.onBoardingRoute:
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.onBoardingRoute),
            builder: (_) => const OnBoardingScreen()); */

      case Routes.mainScreen:
        analyticsServices.addScreenTrackingAnalytics(
            parameterScreenClass: EventsAnalytics.mainClassName,
            parameterScreenName: EventsAnalytics.mainScreenName);
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.mainScreen),
          builder: (_) => MainScreen(),
        );

      /* case Routes.myVisits:
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.myVisits),
            builder: (_) => const MyVisitsScreen());

      /// visit details
      case Routes.visitDetails:
        final parametersMap = settings.arguments as Map;
        final resultId = parametersMap[AppKeyConstants.RESULTS_ID] as int;
        int? notificationType =
            parametersMap[AppKeyConstants.NOTIFICATION_TYPE] as int?;
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.visitDetails),
          builder: (_) => ShowCaseWidget(
            enableAutoScroll: true,
            builder: Builder(
              builder: (context) {
                return VisitDetails(
                  id: resultId,
                  notificationType: notificationType,
                );
              },
            ),
          ),
        );

      case Routes.myProgramsScreen:
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.myProgramsScreen),
            builder: (_) => MyProgramsScreen());

      case Routes.rechargeBalanceScreen:
        final balance = settings.arguments as String;
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.rechargeBalanceScreen),
            builder: (_) => RechargeBalanceScreen(
                  balance: balance,
                ));
      case Routes.subscriptionPlansScreen:
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.subscriptionPlansScreen),
            builder: (_) => SubscriptionPlansScreen());

      case Routes.renewSubscription:
        final parametersMap = settings.arguments as Map;
        int subscriptionId = parametersMap["subscriptionId"] as int;
        int? planId = parametersMap["planId"] as int?;

        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.renewSubscription),
            builder: (_) => RenewSubscription(
                  subscriptionId: subscriptionId,
                  planId: planId,
                ));

      case Routes.paymentScreen:
        final url = settings.arguments as String;
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.paymentScreen),
            builder: (_) => PaymentScreen(
                  url: url,
                ));

      case Routes.doctorsScreenList:
        final parametersMap = settings.arguments as Map;
        final specialitiesList =
            parametersMap["specialitiesList"] as List<Speciality>;
        final specialityId = parametersMap["specialityId"] as String;
        int? callId = parametersMap["callId"] as int?;
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.doctorsScreenList),
            builder: (_) => DoctorsScreen(
                  specialitiesList: specialitiesList,
                  specialityId: specialityId,
                  callId: callId,
                ));

      case Routes.profilesScreen:
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.profilesScreen),
            builder: (_) => ProfileScreen());

      case Routes.addDependentScreen:
        DependentAccountBloc bloc = settings.arguments as DependentAccountBloc;
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.addDependentScreen),
            builder: (_) => AddDependentScreen(
                  bloc: bloc,
                ));

      case Routes.doctorProfileScreen:
        analyticsServices.addScreenTrackingAnalytics(
            parameterScreenClass: EventsAnalytics.bookDoctorClassName,
            parameterScreenName: EventsAnalytics.bookDoctorScreenName);

        Map parametersMap = settings.arguments as Map;
        bool isUpcoming = parametersMap["isUpcoming"] as bool;
        String id = parametersMap["profileId"] as String;
        int? callId = parametersMap["callId"] as int?;
        String? date = parametersMap["date"] as String?;
        String? time = parametersMap["time"] as String?;
        String? status = parametersMap["status"] as String?;
        String? statusId = parametersMap["statusId"] as String?;
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.doctorProfileScreen),
            builder: (_) => DoctorProfileScreen(
                  isUpComing: isUpcoming,
                  profileId: id,
                  date: date,
                  time: time,
                  callId: callId,
                  status: status,
                  statusId: statusId,
                ));

      case Routes.notificationsScreen:
        NotificationListBloc bloc = settings.arguments as NotificationListBloc;
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.notificationsScreen),
            builder: (_) => NotificationsScreen(
                  notificationListBloc: bloc,
                ));

      case Routes.settingsScreen:
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.settingsScreen),
            builder: (_) => const SettingsScreen());

      /// call GP
      case Routes.assigningDoctor:
        Map parametersMap = settings.arguments as Map;

        int callId = parametersMap["callId"] as int;
        String? coupon = parametersMap["coupon"] as String?;
        bool isCameraClosed = parametersMap["isCameraClosed"] as bool;

        final int callType = parametersMap["callType"] as int;
        CallData? roomData = parametersMap["roomData"] as CallData?;

        QuestionRepositoriesImp repository =
            instance<QuestionRepositoriesImp>();

        log("Log This is Coupon : ${coupon != null && coupon.isNotEmpty} $coupon");
        analyticsServices.addEventAnalytics(
            eventName: coupon != null && coupon.isNotEmpty
                ? EventsAnalytics.freeVideoCall
                : EventsAnalytics.videoCall,
            parameterName: EventsAnalytics.callGp);

        analyticsServices.addScreenTrackingAnalytics(
            parameterScreenClass: EventsAnalytics.callGPClassName,
            parameterScreenName: EventsAnalytics.callGPScreenName);

        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.assigningDoctor),
          builder: (_) => BlocProvider(
            create: (context) => ConferenceCubit(
                repository: repository,
                callId: callId,
                coupon: coupon,
                callType: callType,
                roomData: roomData),
            child: ConferencePage(isCameraClosed: isCameraClosed),
          ),
        );

      case Routes.specialities:
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.specialities),
            builder: (_) => const SpecialitiesScreen());

      case Routes.myVitals:
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.myVitals),
            builder: (_) => const MyVitalsScreen());
      case Routes.otpScreen:
        final verifyRequest = settings.arguments as VerifyRequest;
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.otpScreen),
            builder: (_) => OtpScreen(verifyRequest: verifyRequest));

      case Routes.questionScreen:
        String? coupon = settings.arguments as String?;

        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.questionScreen),
            builder: (_) => QuestionScreen(
                  coupon: coupon,
                ));

      case Routes.viewAllAppointmentsScreen:
        final appointments = settings.arguments as List<AppointmentItem>;
        return MaterialPageRoute(
            settings:
                const RouteSettings(name: Routes.viewAllAppointmentsScreen),
            builder: (_) =>
                AllAppointmentsScreen(upcomingAppointmentsList: appointments));

      ///prescriptions Screen
      case Routes.prescriptions:
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.prescriptions),
            builder: (_) => const PrescriptionsScreen());

      ///Labs List Screen
      case Routes.labsListScreen:
        Map parametersMap = settings.arguments as Map;
        int? callId = parametersMap["callId"] as int?;
        final recommendedLaps = parametersMap["recomendedTests"] as List<Test>?;
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.labsListScreen),
            builder: (_) => LabsListScreen(
                  recommendedTests: recommendedLaps,
                  callId: callId,
                ));

      ///Labs profile Screen
      case Routes.labProfileScreen:
        analyticsServices.addScreenTrackingAnalytics(
            parameterScreenClass: EventsAnalytics.bookLabClassName,
            parameterScreenName: EventsAnalytics.bookLabScreenName);

        Map parametersMap = settings.arguments as Map;
        bool isUpcoming = parametersMap["isUpcoming"] as bool;
        String labId = parametersMap["labId"] as String;
        List<Test>? availableTests =
            parametersMap["availableTests"] as List<Test>?;
        List<Test>? notAvailableTests =
            parametersMap["notAvailableTests"] as List<Test>?;
        String? totalCost = parametersMap["totalCost"] as String?;
        String? totalTime = parametersMap["totalTime"] as String?;
        String? date = parametersMap["date"] as String?;
        String? time = parametersMap["time"] as String?;
        int? callId = parametersMap["callId"] as int?;
        String? status = parametersMap["status"] as String?;
        String? statusId = parametersMap["statusId"] as String?;
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.labProfileScreen),
            builder: (_) => LabProfileScreen(
                isUpComing: isUpcoming,
                totalCost: totalCost,
                totalTime: totalTime,
                callId: callId,
                labId: labId,
                date: date,
                time: time,
                availableTests: availableTests,
                notAvailableTests: notAvailableTests,
                status: status,
                statusId: statusId));

      ///rads List Screen
      case Routes.radsListScreen:
        Map parametersMap = settings.arguments as Map;
        int? callId = parametersMap["callId"] as int?;
        final recommendedTests =
            parametersMap["recomendedTests"] as List<Test>?;
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.radsListScreen),
            builder: (_) => RadsListScreen(
                recommendedTests: recommendedTests, callId: callId));

      ///rads profile Screen
      case Routes.radProfileScreen:
        analyticsServices.addScreenTrackingAnalytics(
            parameterScreenClass: EventsAnalytics.bookRadClassName,
            parameterScreenName: EventsAnalytics.bookRadScreenName);

        Map parametersMap = settings.arguments as Map;
        bool isUpcoming = parametersMap["isUpcoming"] as bool;
        int? callId = parametersMap["callId"] as int?;
        String radId = parametersMap["radId"] as String;
        List<Test>? availableTests =
            parametersMap["availableTests"] as List<Test>?;
        List<Test>? notAvailableTests =
            parametersMap["notAvailableTests"] as List<Test>?;
        String? totalCost = parametersMap["totalCost"] as String?;
        String? totalTime = parametersMap["totalTime"] as String?;
        String? date = parametersMap["date"] as String?;
        String? time = parametersMap["time"] as String?;
        String? status = parametersMap["status"] as String?;
        String? statusId = parametersMap["statusId"] as String?;
        return MaterialPageRoute(
            builder: (_) => RadProfileScreen(
                isUpComing: isUpcoming,
                callId: callId,
                totalCost: totalCost,
                totalTime: totalTime,
                radId: radId,
                date: date,
                time: time,
                availableTests: availableTests,
                notAvailableTests: notAvailableTests,
                status: status,
                statusId: statusId));

      case Routes.testVideoScreen:
        Map parametersMap = settings.arguments as Map;
        List<CameraDescription> availableCamera =
            parametersMap["availableCameras"] as List<CameraDescription>;
        QuestionBloc questionBloc =
            parametersMap["questionBloc"] as QuestionBloc;
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.testVideoScreen),
            builder: (_) => TestVideoScreen(
                  availableCamera: availableCamera,
                  questionBloc: questionBloc,
                ));

      /// medical prescriptions
      case Routes.medicalPrescriptions:
        final List<DrugMapper> digitalPrescriptions =
            settings.arguments as List<DrugMapper>;
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.medicalPrescriptions),
            builder: (_) => MedicalPrescriptions(
                  digitalPrescriptions: digitalPrescriptions,
                ));
 */
      /// set password
      case Routes.setPassword:
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.setPassword),
            builder: (_) => const SetPasswordScreen());

/*       /// my Records
      case Routes.myRecords:
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.myRecords),
            builder: (_) => MyRecordsScreen());

      /// my tikshif Locations Screen
      case Routes.tikshifLocationsScreen:
        List<NanoClinic> nanoClinics = settings.arguments as List<NanoClinic>;
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.tikshifLocationsScreen),
            builder: (_) => TikshifLocationScreen(
                  nanoClinics: nanoClinics,
                ));

      /// waiting screen
      case Routes.waitingCallScreen:
        Map parametersMap = settings.arguments as Map;

        bool isCameraClosed = parametersMap['isCameraClosed'] as bool;
        int callId = parametersMap['callId'] as int;
        int waitingNumber = parametersMap['waitingNumber'] as int;
        String waitingMessage = parametersMap['waitingMessage'] as String;
        String? coupon = parametersMap["coupon"] as String?;

        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.waitingCallScreen),
            builder: (_) => WaitingCallScreen(
                  isCameraClosed: isCameraClosed,
                  callId: callId,
                  waitingNumber: waitingNumber,
                  waitingMessage: waitingMessage,
                  coupon: coupon,
                ));

 */ /// my tikshif Locations Screen
      case Routes.changePhone:
        Map parametersMap = settings.arguments as Map;
        String? countryCode = parametersMap["countryCode"] as String?;
        String? phoneNumber = parametersMap["userPhone"] as String;
        bool? popScreen = parametersMap["popScreen"] as bool?;

        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.changePhone),
            builder: (_) => ChangePhoneNumber(
                  userPhone: phoneNumber,
                  countryCode: countryCode,
                  popScreen: popScreen,
                ));

/*       /// blogs Screen
      case Routes.blogsScreen:
        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.blogsScreen),
            builder: (_) => const BlogsListScreen());

      /// blog details Screen
      case Routes.blogDetailsScreen:
        Map parametersMap = settings.arguments as Map;

        final String slug = parametersMap['slug'] as String;

        return MaterialPageRoute(
            settings: const RouteSettings(name: Routes.blogDetailsScreen),
            builder: (_) => BlogDetailsScreen(slug: slug));

      /// force update
      case Routes.forceUpdate:
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.forceUpdate),
          builder: (_) => const ForceUpdate(),
        );

      /// Menstrual Cycle
      case Routes.menstrualCycleScreen:
        final MenstrualCycle periodData = settings.arguments as MenstrualCycle;

        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.menstrualCycleScreen),
          builder: (_) => MenstrualCycleScreen(
            periodData: periodData,
          ),
        );

      /// Menstrual List
      case Routes.menstrual_list:
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.menstrualCycleScreen),
          builder: (_) => const MenstrualList(),
        );

      /// force update
      case Routes.vitalDetails:
        Map parametersMap = settings.arguments as Map;
        String typeId = parametersMap["typeId"] as String;
        String name = parametersMap["name"] as String;

        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.vitalDetails),
          builder: (_) => VitalDetails(
            typeId: typeId,
            name: name,
          ),
        );

      /// calculation
      case Routes.calculation:
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.calculation),
          builder: (_) => Calculation(),
        );

      /// bmi
      case Routes.bmi:
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.calculation),
          builder: (_) => const BMIScreen(),
        );

      /// basic calorie intake
      case Routes.basic_colorie_intake:
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.calculation),
          builder: (_) => const BasicCalories(),
        );

      /// pregnancy
      case Routes.pregnancy:
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.calculation),
          builder: (_) => const PregnancyScreen(),
        );

      /// questionnaires List Screen
      case Routes.questionnairesListScreen:
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.calculation),
          builder: (_) => const QuestionnairesListScreen(),
        );

      /// questionnaires List Screen
      case Routes.questionnaireQuestionsScreen:
        Map parametersMap = settings.arguments as Map;
        String slug = parametersMap["slug"] as String;
        String title = parametersMap["title"] as String;

        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.calculation),
          builder: (_) =>
              QuestionnaireQuestionsScreen(slug: slug, title: title),
        );

      /// diabetes screen
      case Routes.diabetesScreen:
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.diabetesScreen),
          builder: (_) => DiabetesScreen(),
        );

      /// diabetes history screen
      case Routes.diabetesHistoryScreen:
        Map parametersMap = settings.arguments as Map;
        final diabetesBloc = parametersMap["DiabetesBloc"] as DiabetesBloc;

        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.diabetesHistoryScreen),
          builder: (_) => DiabetesHistoryScreen(diabetesBloc: diabetesBloc),
        );

      /// children screen
      case Routes.pediatric:
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.pediatric),
          builder: (_) => ChildrenScreen(),
        );

      /// pediatric Home screen
      case Routes.pediatricHome:
        Map parametersMap = settings.arguments as Map;
        final String id = parametersMap['id'];
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.pediatric),
          builder: (_) => PediatricHomeScreen(id: id),
        );

      /// pediatric History screen
      case Routes.pediatricHistoryScreen:
        Map parametersMap = settings.arguments as Map;
        final String id = parametersMap['patient_id'];
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.pediatricHistoryScreen),
          builder: (_) => PediatricHistoryScreen(patientId: id),
        );

      /// Vaccin screen
      case Routes.pediatricVaccine:
        Map parametersMap = settings.arguments as Map;
        final String id = parametersMap['id'];
        final PediatricHomeEntity pediatricHomeEntity = parametersMap['ped'];

        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.pediatric),
          builder: (_) => PediatricVaccineScreen(
              id: id, pediatricHomeEntity: pediatricHomeEntity),
        );

      /// pediatric BabyWeight screen
      case Routes.pediatricBabyWeight:
        Map parametersMap = settings.arguments as Map;
        final String id = parametersMap['id'];
        final PediatricHomeEntity pediatricHomeEntity = parametersMap['ped'];
        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.pediatricHistoryScreen),
          builder: (_) => BabeCalculateScreen(
              patientId: id, pediatricHomeEntity: pediatricHomeEntity),
        );

      /// pediatric BabyWeight screen
      case Routes.pediatricHeadCircum:
        Map parametersMap = settings.arguments as Map;
        final String id = parametersMap['id'];
        final PediatricHomeEntity pediatricHomeEntity = parametersMap['ped'];

        return MaterialPageRoute(
          settings: const RouteSettings(name: Routes.pediatricHistoryScreen),
          builder: (_) => BabeCalculateScreen(
              patientId: id, pediatricHomeEntity: pediatricHomeEntity),
        );
 */
      default:
        return unDefinedRoute();
    }
  }

  static Route<dynamic> unDefinedRoute() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: "unDefinedRoute"),
      builder: (_) => Scaffold(
        appBar: AppBar(
          title: Text(AppStrings.noRouteFound.tr()),
        ),
        body: Center(child: Text(AppStrings.noRouteFound.tr())),
      ),
    );
  }
}
