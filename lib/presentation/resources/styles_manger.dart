import 'package:flutter/material.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';
import 'font_manger.dart';

TextStyle _getTextStyle(double fontSize, FontWeight fontWeight, Color color) {
  return TextStyle(
      fontSize: fontSize,
      fontFamily: FontConstants.fontFamily,
      color: color,
      fontWeight: fontWeight);
}

// regular style

TextStyle getRegularStyle(
    {double fontSize = FontSize.s12, required Color color}) {
  return _getTextStyle(fontSize, FontWeightManager.regular, color);
}

// medium style

TextStyle getMediumStyle(
    {double fontSize = FontSize.s12, required Color color}) {
  return _getTextStyle(fontSize, FontWeightManager.medium, color);
}

// medium style

TextStyle getLightStyle(
    {double fontSize = FontSize.s12, required Color color}) {
  return _getTextStyle(fontSize, FontWeightManager.light, color);
}

// bold style

TextStyle getBoldStyle({double fontSize = FontSize.s12, required Color color}) {
  return _getTextStyle(fontSize, FontWeightManager.bold, color);
}

// semibold style
 TextStyle getSemiBoldStyle(
    {double fontSize = FontSize.s12, required Color color}) {
  return _getTextStyle(fontSize, FontWeightManager.semiBold, color);
}

List<BoxShadow> shadowList = [
  BoxShadow(
    color: ColorManager.fontLight.withOpacity(.4),
    blurRadius: 10,
    offset: const Offset(0, 2),
  ),
];

List<BoxShadow> selectedShadowList = [
  BoxShadow(
    color: ColorManager.fontLight.withOpacity(.8),
    //ColorManager.fontLight.withOpacity(.4),
    blurRadius: 10,
    offset: const Offset(0, 10),
  ),
];

List<BoxShadow> darkShadowList = [
  BoxShadow(
    color: ColorManager.blackColor, //ColorManager.fontLight.withOpacity(.4),
    blurRadius: 40,
    offset: const Offset(0, 10),
  ),
];

BoxDecoration appBoxDecoration = BoxDecoration(
    color: ColorManager.background,
    borderRadius: const BorderRadius.all(Radius.circular(AppRadius.m20)),
    boxShadow: shadowList);

BoxDecoration profileBoxDecoration = BoxDecoration(
    color: ColorManager.white,
    borderRadius: BorderRadius.circular(AppRadius.m15),
    boxShadow:profileShadowList,
);

BoxDecoration profileCircleBalanceBoxDecoration = BoxDecoration(
  color: ColorManager.white,
  shape: BoxShape.circle,
  //borderRadius: BorderRadius.circular(AppRadius.m15),
  boxShadow:profileShadowList,
);


List<BoxShadow> profileShadowList = [
  BoxShadow(
    color: ColorManager.fontLight.withOpacity(.2),
    blurRadius: 8,
    offset: const Offset(0, 2),
  ),
];

BoxDecoration profileEditBoxDecoration = BoxDecoration(
  color: ColorManager.white,
  borderRadius: BorderRadius.circular(AppRadius.m8),
  boxShadow: [
    BoxShadow(
      color: ColorManager.fontLight.withOpacity(.1),
      blurRadius: 2,
      offset: const Offset(0, 1),
    ),
  ],
);


BoxDecoration profileSpinnerBoxDecoration = BoxDecoration(
  color: ColorManager.profileBackground,
  borderRadius: BorderRadius.circular(AppRadius.m8),
);


InputDecoration profileTextFieldDecoration = InputDecoration(
  filled: true,
  fillColor: ColorManager.profileBackground,
// content padding
  contentPadding: const EdgeInsets.all(AppPadding.p10),
// hint style
  hintStyle:
  getRegularStyle(color: ColorManager.hintColor, fontSize: FontSize.s16),
  labelStyle:
  getMediumStyle(color: ColorManager.hintColor, fontSize: FontSize.s16),
  errorStyle: getRegularStyle(color: ColorManager.borderColor),

// enabled border style
  enabledBorder: const OutlineInputBorder(
      borderSide: BorderSide.none,
      borderRadius: BorderRadius.all(Radius.circular(AppRadius.m8))),

// focused border style
  focusedBorder: const OutlineInputBorder(
      borderSide: BorderSide.none,
      borderRadius: BorderRadius.all(Radius.circular(AppRadius.m8))),

// error border style
  errorBorder: const OutlineInputBorder(
      borderSide: BorderSide.none,
      borderRadius: BorderRadius.all(Radius.circular(AppRadius.m8))),
// focused border style
  focusedErrorBorder: const OutlineInputBorder(
      borderSide: BorderSide.none,
      borderRadius: BorderRadius.all(Radius.circular(AppRadius.m8))),
);
