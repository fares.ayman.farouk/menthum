class AppStrings {
  static const noRouteFound = "no_route_found";

// onBoarding
  static const langeDisplay = "lange_display";
  static const onBoardingTitle1 = "on_boarding_title1";
  static const onBoardingTitle2 = "on_boarding_title2";
  static const onBoardingTitle3 = "on_boarding_title3";
  static const onBoardingSubTitle1 = "on_boarding_sub_title1";
  static const onBoardingSubTitle2 = "on_boarding_sub_title2";
  static const onBoardingSubTitle3 = "on_boarding_sub_title3";
  static const skip = "skip";
  static const getStarted = "get_started";

  static const kilo = "kilo";
  static const cm = "cm";
  static const egp = "egp";

  static const married = "married";
  static const single = "single";
  static const divorce = "divorce";
  static const widowed = "widowed";
  static const male = "male";
  static const female = "female";

  static const gender = "gender";
  static const marital_status = "marital_status";
  static const birth_date = "birth_date";
  static const expectedBirthDate = "expected_birth_date";

  static const old_password = "old_password";
  static const new_password = "new_password ";
  static const confirm_new_password = "confirm_new_password";

  //login
  static const labelSignIn = "label_sign_in";
  static const backMainAccount = "back_main_account";
  static const labelSignUp = "label_sign_up";
  static const phoneNumber = "phone_number";
  static const password = "password";
  static const coupon = "coupon";
  static const haveCoupon = "have_coupon";
  static const haveCouponMessage = "have_coupon_message";
  static const labelForgetPassword = "label_forget_password";
  static const labelLoginGoogle = "label_login_google";
  static const labelScanQrCode = "label_scan_qr_code";
  static const labelLoginFacebook = "label_login_facebook";
  static const labelLoginApple = "label_login_apple";
  static const labelNotHaveAccount = "label_not_have_account";
  static const email = "email";
  static const fullName = "full_name";
  static const labelAlreadyHaveAccount = "label_already_account";
  static const submit = "submit";
  static const switchAccount = "switch_account";
  static const labelWritePhone = "label_write_phone";
  static const changePassword = "change_password";
  static const confirmPassword = "confirm_password";
  static const newPassword = "new_password";
  static const reSendCodeSms = "re_send_code_sms";
  static const reSendCodeWhatsapp = "re_send_code_whatsapp";
  static const changeMobileNumber = "change_mobile_number";
  static const code = "code";
  static const labelAgreeOur = "label_agree_our";
  static const termsAndConditions = "label_terms_conditions";
  static const serviceAndConditions = "label_service_conditions";
  static const resetPassword = "reset_password";
  static const labelSetPhone = "label_set_phone";
  static const labelEnterOtp = "label_enter_otp";
  static const labelPleaseEnterOtp = "label_please_enter_otp";
  static const changeNum = "change_number";

  /// main screen Strings
  static const bookDoctor = "book_doctor";
  static const callGP = "call_gp";
  static const backMsg = "back_msg";
  static const chargeYourWallet = "charge_your_wallet";
  static const hello = "hello";

  /// my visits screen strings
  static const myVisits = "my_visits";
  static const visitDetails = "visit_details";

  ///visit details
  static const branch = "branch";
  static const reports = "reports";
  static const recommendations = "recommendations";
  static const welcome = "label_welcome";
  static const uploadNew = "upload_new";
  static const upcomingBookings = "upcoming_bookings";
  static const viewAll = "view_all";
  static const myResult = "my_results";
  static const tests = "tests";
  static const appointments = "appointments";
  static const seeYourReport = "see_your_report";
  static const seeYourAppointment = "see_your_appointment";
  static const seeYourRecommendations = "see_your_recommendations";

  /// prescriptions
  static const Prescriptions = "prescriptions";
  static const medicalPrescriptions = "medical_prescriptions";
  static const timesPerDay = "times_day";
  static const timesPerWeek = "times_week";
  static const timesPerMonth = "times_month";
  static const yes = "yes";
  static const no = "no";
  static const deletePrescriptions = "delete_prescriptions";
  static const deletePrescriptionsMessage = "delete_prescriptions_message";

  static const myPrograms = "my_programs";
  static const myVitals = "my_vitals";
  static const labelTikshifLocations = "label_tikshif_locations";
  static const notification = "label_notification";
  static const tikshifReport = "tikshif_report";
  static const laboratories = "laboratories";
  static const radiologist = "radiologist";
  static const takeTestHome = "take_test_home";

  /// settings screen
  static const settings = "settings";
  static const language = "language";
  static const notificationOn = "notification_on";
  static const autoUpdate = "auto_update";
  static const changeLang = "change_lang";
  static const changeLangMessage = "change_lang_message";

  /// Assigning  a Doctor
  static const assigningDoctor = "assigning_doctor";
  static const youWillRedirectedToTalkGp = "you_will_redirected_to_talk_gp";
  static const willCallYouThrough = "will_call_you_through";
  static const seconds = "seconds";

  /// specialities
  static const specialities = "specialities";
  static const searchInput = "search_input";
  static const search = "search";

  //side menu
  static const home = 'home';
  static const prescriptions = 'prescriptions';
  static const myProfile = 'my_profile';
  static const logOut = 'log_out';
  static const aboutTikshif = 'about_tikshif';
  static const contactUs = 'contact_us';
  static const about = 'about';
  static const contactNumbers = 'contact_numbers';
  static const availableTimes = 'available_times';
  static const consultationFees = 'consultation_fees';
  static const consultationTime = 'consultation_time';
  static const minutes = 'minutes';
  static const gallery = 'gallery';

  //profile
  static const editProfile = 'edit_profile';
  static const addDependent = "add_dependent";
  static const saveUpdates = 'save_updates';
  static const wallet_balance = "wallet_balance";
  static const plans = "plans";
  static const youAreNotSubscribe = "you_are_not_subscribe";
  static const recharge = "recharge";
  static const subscribe = "subscribe";
  static const mySubscriptions = "my_subscription";

  static const myCurrentSubscription = "my_current_subscription";
  static const renewSubscription = "renew_subscription";
  static const availablePlans = "available_plans";
  static const renew = "renew";

  static const renewAt = "renew_at";
  static const medical_profile = "medical_profile";
  static const myFamily = "my_family";
  static const blood_group = "blood_group";
  static const weight = "weight";
  static const height = "height";
  static const medical_conditions = "medical_conditions";

  static const occupation = "occupation";

  static const immunization = "immunization";
  static const food_allergies = "food_allergies";
  static const medication_allergies = "medication_allergies";
  static const chronic_medical_conditions = "chronic_medical_conditions";
  static const chronic_medication = "chronic_medication";
  static const operations_hospitalization = "operations_hospitalization";
  static const family_history_diseases = "family_history_diseases";
  static const last_checkup = "last_checkup";
  static const social_habits = "social_habits";
  static const exercise_habits = "exercise_habits";
  static const diet = "diet";
  static const general_health = "general_health";
  static const emergency_contact = "emergency_contact";
  static const download = "download";

  static const couponValid = "coupon_valid";
  static const couponEnterValid = "coupon_enter_valid";
  static const msgCoupon = "msg_coupon";
  static const check = "check";
  static const nothing = "nothing";
  static const finish = "finish";

  static const cash = "cash";
  static const creditCard = "credit_card";
  static const paid = "paid";
  static const lastTransactions = "last_transactions";
  static const msgChooseAmount = "msg_choose_amount";
  static const titlePayment = "title_payment";
  static const labelAccept = "label_accept";

  static const cancel = "cancel";
  static const openAppSettings = "open_app_settings";
  static const allowCallPermissions = "allow_call_permissions";
  static const camera = 'camera';
  static const mic = 'mic';
  static const nearby = 'nearby';

  //vital
  static const addNewVital = 'add_new_vital';
  static const editVital = 'edit_vital';
  static const add = 'add';
  static const edit = 'edit';
  static const delete = 'delete';
  static const vitalName = 'vital_name';
  static const value = 'value';

  //book a doctor

  static const filter = 'filter';
  static const priceRange = 'price_range';
  static const location = 'location';
  static const apply = 'apply';
  static const city = 'city';
  static const governorate = 'governorate';

  //answer question
  static const answerQuestion = 'answer_question';
  static const answeredQuestion = 'answered_question';
  static const next = 'next';
  static const proceed = 'proceed';
  static const enterAnswer = 'enter_answer';

  //Prescriptions
  static const pendingPrescription = 'pending_prescription';
  static const tikshifPrescription = 'tikshif_prescription';

  // Rads..
  static const bookRad = 'book_rad';
  static const chooseLabTests = 'choose_lab_tests';
  static const chooseRadTests = 'choose_rad_tests';
  static const totalTime = 'total_time';
  static const totalPrice = 'total_price';

  static const bookLab = 'book_lab';

  static const newUpdateAvailable = 'new_update_available';
  static const update = 'update';

  static const List<String> weekDays = [
    "Sun", //0
    "Mon", //1
    "Tue", //2
    "Wed", //3
    "Thurs", //4
    "Fri", //5
    "Sat" //6
  ];

  // app name
  static const appName = 'app_name';

  // coming soon
  static const comingSoon = 'coming_soon';

  // Website
  static const website = 'website';

  ///Types of the Booking
  static const clinic = 'clinic';
  static const radiologyBranch = 'radiology_branch';
  static const laboratory = 'laboratory';
  static const lab_branches = 'lab_branches';

  /// booking popup
  static const newAppointment = "new_appointment";
  static const chooseAvailableAppointment = "choose_available_appointment";
  static const chooseFavoriteTime = "choose_favorite_time";
  static const chooseHospital = "choose_hospital";
  static const bookNow = "book_now";
  static const chooseDay = "choose_day";
  static const timeFrom = "time_from";
  static const timeTo = "time_to";

  ///test your camera screen
  static const testYourCamera = "test_your_camera";
  static const cameraPermissionError = "camera_permission_error";
  static const micPermissionError = "mic_permission_error";
  static const bluePermissionError = "blue_permission_error";
  static const allow = "allow";
  static const allowed = "allowed";

  /// Video Call
  static const messageOnRoomCrash = "message_on_room_crash";
  static const messageOnNoParticipant = "message_on_no_participant";
  static const messageOnHasParticipant = "message_on_has_participant";
  static const nowPrioritizingYourCall = "now_prioritizing_your_call";
  static const noAvailableDoctor = "no_available_doctor";
  static const notInTheWorkingHours = "not_in_the_working_hours";

  /// my Records
  static const myRecords = 'my_records';
  static const tikshif = "tikshif";
  static const lab = "lab";
  static const radiology = "radiology";
  static const specialist = "specialist";

  ///set Password.
  static const setPassword = "set_password";

  static const appointmentAt = "appointment_at";

  ///
  static const tikshifLocations = "tikshif_location";

  static const labsList = "labs_list";
  static const radsList = "rads_list";

  static const directions = "directions";
  static const km = "km";
  static const meter = "meter";

  /// digital prescriptions
  static const month = "month";
  static const week = "week";
  static const day = "day";

  static const forTime = "for_time";

  static const monthly = "monthly";
  static const weekly = "weekly";
  static const daily = "daily";
  static const forDuration = "for";

  static const cream = "cream";
  static const dropsOral = "drops_oral";
  static const earDrops = "ear_drops";
  static const eyeDrops = "eye_drops";
  static const nasalDrops = "nasal_drops";
  static const intradermal = "intradermal";
  static const intravenous = "intravenous";
  static const intramuscular = "intramuscular";
  static const meteredDoseInhaler = "metered_dose_inhaler";
  static const intranasal = "intranasal";
  static const oral = "oral";
  static const rectal = "rectal";
  static const subcutaneous = "subcutaneous";
  static const sublingual = "sublingual";
  static const topical = "topical";
  static const vaginel = "vaginel";
  static const spray = "spray";
  static const mouthWash = "mouth_wash";
  static const pressToChoose = "press_to_choose";

  /// Empty Screen
  static const noResultFound = "no_result_found";

  /// switch on slider...
  static const String video = "video";
  static const String image = "image";

  static const String noAvailableGp = "no_available_gp";
  static const String from = "from";
  static const String to = "to";
  static const String ok = "ok";

  static const yourCameraClosed = "your_camera_closed";
  static const noInternetConnection = "no_internet_connection";
  static const backOnline = "back_online";
  static const call = "call";
  static const proceedWithCameraResumed = "proceed_with_camera_resumed";

  static const noCameraOnDevice = "no_camera_on_device";
  static const openLocationPermissionSettings = "open_permission_settings";
  static const cantContinue = "cant_continue";
  static const locationPermission = "location_permission";
  static const locationServices = "location_services";

  static const costPerOneCall = "cost_per_one_call";
  static const costPerTwoCall = "cost_per_two_call";

  static const yourNumberInQueue = 'your_number_in_queue';
  static const yourEstimateTime = 'your_estimate_time';
  static const exitWaitingCall = 'exit_waiting_call';
  static const notifyMe = 'notify_me';
  static const youAreInNextQueue = 'you_are_in_next_queue';

  /// menstrual cycle
  static const menstrualCycle = 'menstrual_cycle';
  static const period = 'period';

  static const preOvulation = 'pre_ovulation';
  static const postOvulation = 'post_ovulation';
  static const ovulation = 'ovulation';

  static const fertileWindow = 'fertile_window';
  static const editCycle = 'edit_cycle';
  static const cycleLength = 'cycle_length';
  static const endPeriodDate = 'end_period_date';
  static const endAt = 'end_at';
  static const startPeriodDate = 'start_period_date';
  static const normalDay = 'normal_day';

  //menstrual list
  static const createNewCycle = 'create_new_cycle';
  static const addBleedingNote = 'add_bleeding_note';

  ///diabetes
  static const diagnosesDate = 'diagnoses_date';
  static const lastVisitDate = 'last_visit_date';
  static const enrollToFeature = 'enroll_to_feature';
  static const optional = 'optional';
  static const nextVisit = 'next_visit';
  static const addNote = 'add_note';
  static const showReadingHistory = 'show_reading_history';
  static const diabetesHistory = 'diabetes_history';
  static const todayReadings = 'today_readings';
  static const done = 'done';
  static const diabetesAdd = 'diabetes_add';

  /// Blogs
  static const blogs = 'blogs';
  static const blogDetails = 'blog_details';

  /// blood types
  static const a1 = 'A+';
  static const a2 = 'A-';
  static const b1 = 'B+';
  static const b2 = 'B-';
  static const o1 = 'O+';
  static const o2 = 'O-';
  static const ab1 = 'AB+';
  static const ab2 = 'AB-';

  /// programs
  static const healthProfile = 'health_profile';
  static const obgyn = 'obgyn';
  static const pediatric = 'pediatric';
  static const diabetes = 'diabetes';
  static const psychology = 'psychology';
  static const vaccinationsReport = 'vaccinations_report';
  static const headCircumferenceReport = 'head_circumference_report';
  static const babyWeightReport = 'baby_weight_report';

  /// vitals
  static const enterValidValue = 'enter_valid_value';
  static const freeCall = 'free_call';
  static const systole = 'systole';
  static const diastole = 'diastole';
  static const setPeriod = 'set_period';
  static const readAt = 'read_at';

  /// calculation
  static const calculation = 'calculation';
  static const bmi = 'bmi';
  static const basicColorieIntake = 'basic_colorie_intake';
  static const pregnancy = 'pregnancy';
  static const enterValues = 'enter_values';
  static const information = 'information';
  static const description = 'description';
  static const underWeight = 'under_weight';
  static const normal = 'normal';
  static const overWeight = 'over_weight';
  static const age = 'age';
  static const basicCaloriesNeeded = 'basic_calories_needed';
  static const firstDayLastPeriod = 'first_day_last_period';

  /// questionnaire
  static const questionnaireList = 'questionnaire_list';
  static const questionnaires = 'questionnaires';
  static const questionnaireResult = 'questionnaire_result';

  /// pediatric
  static const registerNewChild = "register_new_child";
  static const forMe = "for_me";
  static const registeredChildren = "registered_children";
  static const existsChildren = "exists_children";
  static const selectChild = "select_child";
  static const headCircumference = "head_circumference";
  static const minCircum = "min_circum";
  static const avgCircum = "avg_circum";
  static const maxCircum = "max_circum";
  static const weightBaby = "weight_baby";
  static const bookSecondOpinion = "book_second_opinion";

  // book second opinion id
  static const String bookSecondOpinionId =
      "999a8a07-ed0c-4aea-90ec-8d212942d62a";
}
