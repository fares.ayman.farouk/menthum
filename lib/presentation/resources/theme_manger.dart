import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:patientapp/presentation/resources/styles_manger.dart';
import 'package:patientapp/presentation/resources/values_manger.dart';
import 'color_manger.dart';
import 'font_manger.dart';

ThemeData getApplicationTheme() {
  return ThemeData(
    // main colors
    primaryColor: ColorManager.primary,
    splashColor: ColorManager.background,
    primaryColorLight: ColorManager.primaryLight,
    backgroundColor: ColorManager.background,
    hintColor: ColorManager.fontLight,

    // // app bar theme
    appBarTheme: AppBarTheme(
      centerTitle: true,
      color: ColorManager.background,
      elevation: AppSize.s1,
      titleTextStyle: getSemiBoldStyle(
        fontSize: FontSize.s16,
        color: ColorManager.blackColor,
      ),
      iconTheme: IconThemeData(color: ColorManager.blackColor),
      systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: ColorManager.background,
          statusBarBrightness: Brightness.dark),
    ),

    // button theme
    buttonTheme: ButtonThemeData(
      shape: const StadiumBorder(),
      disabledColor: ColorManager.primary,
      buttonColor: ColorManager.primary,
    ),

    // elevated button them
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        textStyle:
            getSemiBoldStyle(color: ColorManager.white, fontSize: FontSize.s17),
        primary: ColorManager.bgButton,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(AppSize.s16),
        ),
        minimumSize: const Size.fromHeight(AppSize.s50),
      ),
    ),

    textTheme: TextTheme(
      // bold
      titleLarge: getBoldStyle(
        fontSize: FontSize.s40,
        color: ColorManager.fontColor,
      ),
      headlineLarge: getBoldStyle(
        fontSize: FontSize.s32,
        color: ColorManager.fontColor,
      ),
      headlineMedium: getBoldStyle(
        fontSize: FontSize.s17,
        color: ColorManager.fontColor,
      ),

      //seme Bold
      headlineSmall: getSemiBoldStyle(
        fontSize: FontSize.s14,
        color: ColorManager.fontColor,
      ),

      // medium
      titleMedium:
          getMediumStyle(color: ColorManager.fontColor, fontSize: FontSize.s15),

      // light
      displaySmall:
          getLightStyle(color: ColorManager.fontColor, fontSize: FontSize.s14),

      // regular
      bodyLarge: getRegularStyle(
        fontSize: FontSize.s17,
        color: ColorManager.fontColor,
      ),
      bodyMedium: getRegularStyle(
        fontSize: FontSize.s16,
        color: ColorManager.fontColor,
      ),
      bodySmall: getRegularStyle(
        fontSize: FontSize.s10,
        color: ColorManager.fontColor,
      ),
    ),

    //input decoration theme (text form field)
    inputDecorationTheme: InputDecorationTheme(
      filled: true,
      fillColor: ColorManager.bgTextFieldColor,
      // content padding
      contentPadding: const EdgeInsets.all(AppPadding.p20),
      // hint style
      hintStyle: getRegularStyle(
          color: ColorManager.hintColor, fontSize: FontSize.s16),
      labelStyle:
          getMediumStyle(color: ColorManager.hintColor, fontSize: FontSize.s16),
      errorStyle: getRegularStyle(color: ColorManager.borderColor),

      // enabled border style
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: ColorManager.white, width: AppSize.s1),
          borderRadius: const BorderRadius.all(Radius.circular(AppSize.s16))),

      // focused border style
      focusedBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: ColorManager.primary, width: AppSize.s1),
          borderRadius: const BorderRadius.all(Radius.circular(AppSize.s16))),

      // error border style
      errorBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: ColorManager.borderColor, width: AppSize.s1),
          borderRadius: const BorderRadius.all(Radius.circular(AppSize.s16))),
      // focused border style
      focusedErrorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: ColorManager.white, width: AppSize.s1),
          borderRadius: const BorderRadius.all(Radius.circular(AppSize.s16))),
    ),

    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: ColorManager.primary,
    ),
    //label style
  );
}
