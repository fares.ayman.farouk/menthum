import 'package:patientapp/data/models/apiResponse/consultations_responses/consultations_response.dart';
import 'package:patientapp/data/models/apiResponse/home_response/gp_working_shifts_response.dart';
import 'package:patientapp/data/models/apiResponse/notification_list_response/notification_list_response.dart';
import 'package:patientapp/data/models/apiResponse/tikshif_locations/nano_clinic_response.dart';
import 'package:patientapp/data/models/apiResponse/upcoming_response.dart';

abstract class LocalDataSource {
  Future<UpcomingAppointmentsResponse> getUpcomingAppointments();

  Future<void> saveUpcomingAppointmentsToCash(
      UpcomingAppointmentsResponse response);

  ConsultationsResponse getTikshifResults();

  void saveTikshifResults(ConsultationsResponse response);

  NotificationResponse getNotificationList();

  void saveNotificationList(NotificationResponse response);

  NanoClinicResponse getTikshifBoothLocations();

  GPWorkingShiftResponse getGPWorkingShiftResponse();

  void saveGPWorkingShiftResponse(GPWorkingShiftResponse gpWorkingShiftResponse);

  void saveTikshifBoothLocations(NanoClinicResponse nanoClinicResponse);

  void clearCache();

  void removeFromCache(String key);

  bool isMapContainsKey(String key);
}
