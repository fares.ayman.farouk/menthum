import 'package:patientapp/application/constants.dart';
import 'package:patientapp/application/extensions.dart';
import 'package:patientapp/data/caching/cached_item.dart';
import 'package:patientapp/data/models/apiResponse/consultations_responses/consultations_response.dart';
import 'package:patientapp/data/models/apiResponse/home_response/gp_working_shifts_response.dart';
import 'package:patientapp/data/models/apiResponse/notification_list_response/notification_list_response.dart';
import 'package:patientapp/data/models/apiResponse/tikshif_locations/nano_clinic_response.dart';
import 'package:patientapp/data/models/apiResponse/upcoming_response.dart';
import 'package:patientapp/data/network/error_handler.dart';

import 'local_data_source.dart';

class LocalDataSourceImp implements LocalDataSource {
  Map<String, CachedItem> cacheMap = {};

  @override
  Future<UpcomingAppointmentsResponse> getUpcomingAppointments() async {
    CachedItem? cachedItem = cacheMap[AppConstants.UPCOMING_APPOINTMENT_KEY];
    if (cachedItem != null && cachedItem.isValid(AppConstants.CACHE_INTERVAL)) {
      return cachedItem.data;
    } else {
      throw ErrorHandler.handle(DataSource.CACHE_ERROR);
    }
  }

  @override
  Future<void> saveUpcomingAppointmentsToCash(
      UpcomingAppointmentsResponse response) async {
    cacheMap[AppConstants.UPCOMING_APPOINTMENT_KEY] =
        CachedItem(data: response);
  }

  @override
  void clearCache() {
    cacheMap.clear();
  }

  @override
  void removeFromCache(String key) {
    cacheMap.remove(key);
  }

  @override
  ConsultationsResponse getTikshifResults() {
    CachedItem? cachedItem = cacheMap[AppConstants.TIKSHIF_RESULTS_KEY];
    if (cachedItem != null && cachedItem.isValid(AppConstants.CACHE_INTERVAL)) {
      return cachedItem.data;
    } else {
      throw ErrorHandler.handle(DataSource.CACHE_ERROR);
    }
  }

  @override
  void saveTikshifResults(ConsultationsResponse response) {
    cacheMap[AppConstants.TIKSHIF_RESULTS_KEY] = CachedItem(data: response);
  }

  @override
  NanoClinicResponse getTikshifBoothLocations() {
    CachedItem? cachedItem = cacheMap[AppConstants.NANO_CLINICS_LOCATIONS_KEY];
    if (cachedItem != null && cachedItem.isValid(AppConstants.CACHE_INTERVAL)) {
      return cachedItem.data;
    } else {
      throw ErrorHandler.handle(DataSource.CACHE_ERROR);
    }
  }

  @override
  void saveTikshifBoothLocations(NanoClinicResponse nanoClinicResponse) {
    cacheMap[AppConstants.NANO_CLINICS_LOCATIONS_KEY] =
        CachedItem(data: nanoClinicResponse);
  }

  @override
  GPWorkingShiftResponse getGPWorkingShiftResponse() {
    CachedItem? cachedItem = cacheMap[AppConstants.GP_WORKING_SHIFTS];

    if (cachedItem != null && cachedItem.isValid(AppConstants.CACHE_INTERVAL)) {
      return cachedItem.data;
    } else {
      throw ErrorHandler.handle(DataSource.CACHE_ERROR);
    }
  }

  @override
  void saveGPWorkingShiftResponse(
      GPWorkingShiftResponse gpWorkingShiftResponse) {
    cacheMap[AppConstants.GP_WORKING_SHIFTS] =
        CachedItem(data: gpWorkingShiftResponse);
  }

  @override
  bool isMapContainsKey(String key) {
    return cacheMap.containsKey(key);
  }

  @override
  NotificationResponse getNotificationList() {
    CachedItem? cachedItem = cacheMap[AppConstants.NOTIFICATION_LOCAL_KEY];
    if (cachedItem != null &&
        cachedItem.isValid(AppConstants.CACHE_INTERVAL_NOTIFICATION)) {
      return cachedItem.data;
    } else {
      throw ErrorHandler.handle(DataSource.CACHE_ERROR);
    }
  }

  @override
  void saveNotificationList(NotificationResponse response) {
    cacheMap[AppConstants.NOTIFICATION_LOCAL_KEY] = CachedItem(data: response);
  }
}
