class CallLogsRequest {
  final int callId;
  final int event;
  final String comment;

  CallLogsRequest(
      {required this.callId, required this.event, required this.comment});


  Map<String, dynamic> toJson() => {
        "call_id": callId,
        "event": event,
        "comment": comment,
      };
}
