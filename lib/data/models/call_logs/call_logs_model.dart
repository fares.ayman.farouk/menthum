import 'package:patientapp/data/models/call_logs/call_logs_request.dart';

class CallLogsModel extends CallLogsRequest {
  String? id;
  final String date;
  int numOfTries;

  CallLogsModel({
    this.id,
    required super.callId,
    required super.event,
    required super.comment,
    required this.date,
    this.numOfTries = 0,
  });

  factory CallLogsModel.fromJson(Map<String, dynamic> json) {
    return CallLogsModel(
        id: json['id'],
        callId: json["call_id"],
        event: json["event"],
        comment: json["comment"],
        date: json["date"],
        numOfTries: json["num_of_tries"]);
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'call_id': callId,
      'event': event,
      'comment': comment,
      'date': date,
      'num_of_tries': numOfTries
    };
  }
}
