import 'dart:convert';

RegisterApiErrorResponse registerApiErrorResponseFromJson(String str) =>
    RegisterApiErrorResponse.fromJson(json.decode(str));

class RegisterApiErrorResponse {
  RegisterApiErrorResponse({
    required this.message,
    this.errors,
  });

  String? message;
  Errors? errors;

  factory RegisterApiErrorResponse.fromJson(Map<String, dynamic> json) =>
      RegisterApiErrorResponse(
        message: json.containsKey("message") ? json["message"] : null,
        errors:
            json.containsKey("errors") ? Errors.fromJson(json["errors"]) : null,
      );
}

class Errors {
  Errors(
      {this.email,
      this.fullname,
      this.password,
      this.passwordConfirmation,
      this.mobile,
      this.countryCode,
      this.language,
      this.gender,
      this.maritalStatus,
      this.birthOfDate,
      this.code,
      this.time_from,
      this.time_to,
      this.tests,
      this.childBirthdate,
      this.childName});

  List<String>? email;
  List<String>? fullname;
  List<String>? password;
  List<String>? passwordConfirmation;
  List<String>? mobile;
  List<String>? countryCode;
  List<String>? language;
  List<String>? gender;
  List<String>? birthOfDate;
  List<String>? maritalStatus;
  List<String>? code;
  List<String>? time_to;
  List<String>? time_from;
  List<String>? tests;
  List<String>? childName;
  List<String>? childBirthdate;

  factory Errors.fromJson(Map<String, dynamic> json) => Errors(
        email: json.containsKey("email")
            ? List<String>.from(json["email"].map((x) => x))
            : [],
        fullname: json.containsKey("fullname")
            ? List<String>.from(json["fullname"].map((x) => x))
            : [],
        password: json.containsKey("password")
            ? List<String>.from(json["password"].map((x) => x))
            : [],
        passwordConfirmation: json.containsKey("password_confirmation")
            ? List<String>.from(json["password_confirmation"].map((x) => x))
            : [],
        mobile: json.containsKey("mobile")
            ? List<String>.from(json["mobile"].map((x) => x))
            : [],
        countryCode: json.containsKey("country_code")
            ? List<String>.from(json["country_code"].map((x) => x))
            : [],
        language: json.containsKey("language")
            ? List<String>.from(json["language"].map((x) => x))
            : [],
        gender: json.containsKey("gender")
            ? List<String>.from(json["gender"].map((x) => x))
            : [],
        maritalStatus: json.containsKey("marital_status")
            ? List<String>.from(json["marital_status"].map((x) => x))
            : [],
        birthOfDate: json.containsKey("birthdate")
            ? List<String>.from(json["birthdate"].map((x) => x))
            : [],
        code: json.containsKey("code")
            ? List<String>.from(json["code"].map((x) => x))
            : [],
        time_from: json.containsKey("time_from")
            ? List<String>.from(json["time_from"].map((x) => x))
            : [],
        time_to: json.containsKey("time_to")
            ? List<String>.from(json["time_to"].map((x) => x))
            : [],
        tests: json.containsKey("tests")
            ? List<String>.from(json["tests"].map((x) => x))
            : [],
        childBirthdate: json.containsKey("childBirthdate")
            ? List<String>.from(json["childBirthdate"].map((x) => x))
            : [],
        childName: json.containsKey("childName")
            ? List<String>.from(json["childName"].map((x) => x))
            : [],
      );
}
