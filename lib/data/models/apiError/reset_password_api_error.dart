import 'dart:convert';

ResetPasswordApiError resetPasswordApiErrorFromJson(String str) =>
    ResetPasswordApiError.fromJson(json.decode(str));

class ResetPasswordApiError {
  ResetPasswordApiError({
    this.message,
    this.errors,
  });

  String? message;
  Errors? errors;

  factory ResetPasswordApiError.fromJson(Map<String, dynamic> json) =>
      ResetPasswordApiError(
        message: json.containsKey("message") ? json["message"] : null,
        errors:
            json.containsKey("errors") ? Errors.fromJson(json["errors"]) : null,
      );
}

class Errors {
  Errors({
    this.password,
    this.code,
    this.mobile,
    this.countryCode,
  });

  List<String>? password;
  List<String>? code;
  List<String>? mobile;
  List<String>? countryCode;

  factory Errors.fromJson(Map<String, dynamic> json) => Errors(
        password: json.containsKey("password")
            ? List<String>.from(json["password"].map((x) => x))
            : [],
        code: json.containsKey("code")
            ? List<String>.from(json["code"].map((x) => x))
            : [],
        mobile: json.containsKey("mobile")
            ? List<String>.from(json["mobile"].map((x) => x))
            : [],
        countryCode: json.containsKey("country_code")
            ? List<String>.from(json["country_code"].map((x) => x))
            : [],
      );
}
