import 'dart:convert';

LoginApiErrorResponse loginApiErrorResponseFromJson(String str) {
  return LoginApiErrorResponse.fromJson(json.decode(str));
}

String loginApiErrorResponseToJson(LoginApiErrorResponse data) =>
    json.encode(data.toJson());

class LoginApiErrorResponse {
  LoginApiErrorResponse({
    required this.message,
    this.errors,
  });

  String? message;
  Errors? errors;

  factory LoginApiErrorResponse.fromJson(Map<String, dynamic> json) =>
      LoginApiErrorResponse(
        message: json.containsKey("message") ? json["message"] : "",
        errors:
            json.containsKey("errors") ? Errors.fromJson(json["errors"]) : null,
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "errors": errors?.toJson(),
      };
}

class Errors {
  Errors({
    this.password,
    this.mobile,
    this.countryCode,
  });

  List<String>? password;
  List<String>? mobile;
  List<String>? countryCode;

  factory Errors.fromJson(Map<String, dynamic> json) => Errors(
        password: json.containsKey("password")
            ? List<String>.from(json["password"].map((x) => x))
            : [],
        mobile: json.containsKey("mobile")
            ? List<String>.from(json["mobile"].map((x) => x))
            : [],
        countryCode: json.containsKey("country_code")
            ? List<String>.from(json["country_code"].map((x) => x))
            : [],
      );

  Map<String, dynamic> toJson() => {
        "password": List<dynamic>.from(password!.map((x) => x)),
        "mobile": List<dynamic>.from(mobile!.map((x) => x)),
        "country_code": List<dynamic>.from(countryCode!.map((x) => x)),
      };
}
