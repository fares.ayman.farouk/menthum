import 'dart:convert';

UpdatePasswordApiErrorResponse UpdatePasswordApiErrorResponseFromJson(String str) {
  return UpdatePasswordApiErrorResponse.fromJson(json.decode(str));
}

String UpdatePasswordApiErrorResponseToJson(UpdatePasswordApiErrorResponse data) =>
    json.encode(data.toJson());

class UpdatePasswordApiErrorResponse {
  UpdatePasswordApiErrorResponse({
    required this.message,
    this.errors,
  });

  String? message;
  Errors? errors;

  factory UpdatePasswordApiErrorResponse.fromJson(Map<String, dynamic> json) =>
      UpdatePasswordApiErrorResponse(
        message: json.containsKey("message") ? json["message"] : "",
        errors:
            json.containsKey("errors") ? Errors.fromJson(json["errors"]) : null,
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "errors": errors?.toJson(),
      };
}

class Errors {
  Errors({
    this.old_password,
    this.new_password,
    this.new_password_confirmation
  });

  List<String>? old_password;
  List<String>? new_password;
  List<String>? new_password_confirmation;

  factory Errors.fromJson(Map<String, dynamic> json) => Errors(
        old_password: json.containsKey("old_password")
            ? List<String>.from(json["old_password"].map((x) => x))
            : [],
        new_password: json.containsKey("new_password")
            ? List<String>.from(json["new_password"].map((x) => x))
            : [],
    new_password_confirmation: json.containsKey("new_password_confirmation")
            ? List<String>.from(json["new_password_confirmation"].map((x) => x))
            : [],
      );

  Map<String, dynamic> toJson() => {
        "old_password": List<dynamic>.from(old_password!.map((x) => x)),
        "new_password": List<dynamic>.from(new_password!.map((x) => x)),
        "new_password_confirmation": List<dynamic>.from(new_password_confirmation!.map((x) => x)),
      };
}
