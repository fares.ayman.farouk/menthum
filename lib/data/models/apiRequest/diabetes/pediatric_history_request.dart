import '../../../../domain/models/pediatric/history_request.dart';

class PediatricHistoryRequestModel extends PediatricHistoryRequest {
  PediatricHistoryRequestModel({required super.patientId, required super.page});

  Map<String, dynamic> toJson() => {
        'patient_id': patientId,
        'page': page,
      };

  factory PediatricHistoryRequestModel.obj(PediatricHistoryRequest entity) =>
      PediatricHistoryRequestModel(
          patientId: entity.patientId, page: entity.page);
}
