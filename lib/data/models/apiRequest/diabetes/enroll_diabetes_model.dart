import '../../../../domain/models/diabetes/enroll_diabetes.dart';

class EnrollDiabetesModel extends EnrollDiabetes {
  EnrollDiabetesModel({required super.diagnosesDay, required super.lastVisit});

  Map<String, dynamic> toJson() => {
        'diagnoses_day': diagnosesDay,
        'last_visit': lastVisit,
      };

  factory EnrollDiabetesModel.obj(EnrollDiabetes enrollDiabetes) =>
      EnrollDiabetesModel(
        lastVisit: enrollDiabetes.lastVisit,
        diagnosesDay: enrollDiabetes.diagnosesDay,
      );
}
