import 'dart:convert';

RegisterRequest registerRequestFromJson(String str) =>
    RegisterRequest.fromJson(json.decode(str));

String registerRequestToJson(RegisterRequest data) =>
    json.encode(data.toJson());

class RegisterRequest {
  String fullName;
  String email;
  String password;
  String passwordConfirmation;
  String mobile;
  String countryCode;
  String language;
  String? gender;
  String? maritalStatus;
  String? birthdate;
  String deviceToken;

  RegisterRequest(
      {required this.fullName,
      required this.email,
      required this.password,
      required this.passwordConfirmation,
      required this.mobile,
      required this.countryCode,
      required this.language,
      this.gender,
      this.maritalStatus,
      this.birthdate,
      required this.deviceToken});

  factory RegisterRequest.fromJson(Map<String, dynamic> json) =>
      RegisterRequest(
          fullName: json["fullname"],
          email: json["email"],
          password: json["password"],
          passwordConfirmation: json["password_confirmation"],
          mobile: json["mobile"],
          countryCode: json["country_code"],
          language: json["language"],
          gender: json["gender"],
          maritalStatus: json["marital_status"],
          birthdate: json["birthdate"],
          deviceToken: json["device_token"]);

  Map<String, dynamic> toJson() => {
        "fullname": fullName,
        "email": email,
        "password": password,
        "password_confirmation": passwordConfirmation,
        "mobile": mobile,
        "country_code": countryCode,
        "language": language,
        "gender": gender,
        "marital_status": maritalStatus,
        "birthdate": birthdate,
        "device_token": deviceToken
      };
}
