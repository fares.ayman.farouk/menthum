class ResetPasswordRequest {
  String mobile;
  String countryCode;
  String code;
  String password;
  String passwordConfirmation;

  ResetPasswordRequest(
      {required this.mobile,
      required this.countryCode,
      required this.code,
      required this.password,
      required this.passwordConfirmation});

  Map<String, dynamic> toJson() => {
        "mobile": mobile,
        "country_code": countryCode,
        "code": code,
        "password": password,
        "password_confirmation": passwordConfirmation,
      };
}
