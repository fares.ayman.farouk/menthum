
import 'dart:convert';

EditProfileApiRequest editProfileApiRequestFromJson(Map<String, dynamic> str) =>
    EditProfileApiRequest.fromJson(str);

String editProfileApiRequestToJson(EditProfileApiRequest data) => json.encode(data.toJson());

class EditProfileApiRequest {
  EditProfileApiRequest({
    this.fullname,
    this.email,
    this.gender,
    this.birthOfDate,
    this.maritalStatus,
  });

  String? fullname;
  String? email;
  String? birthOfDate;
  int? gender;
  int? maritalStatus;

  factory EditProfileApiRequest.fromJson(Map<String, dynamic> json) => EditProfileApiRequest(
    fullname: json["fullname"],
    email:   json["email"]  ,
    gender: json["gender"]  ,
    birthOfDate: json["birthdate"]  ,
    maritalStatus: json["marital_status"]  ,
  );

  Map<String, dynamic> toJson() => {
    "fullname": fullname,
    "email": email,
    "gender": gender,
    "birthdate": birthOfDate,
    "marital_status": maritalStatus,
  };
}
