class VerifyRequest {
  String? mobile;
  String countryCode;
  String code;
  VerifyRequest(
      {required this.mobile,
        required this.countryCode,
        required this.code});

  Map<String, dynamic> toJson() => {
    "mobile": mobile,
    "country_code": countryCode,
    "code": code,
  };
}
