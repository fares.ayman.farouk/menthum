
import 'dart:convert';

UpdatePasswordApiRequest updatePasswordApiRequestFromJson(String str) => UpdatePasswordApiRequest.fromJson(json.decode(str));

String updatePasswordApiRequestToJson(UpdatePasswordApiRequest data) => json.encode(data.toJson());

class UpdatePasswordApiRequest {
  UpdatePasswordApiRequest({
    this.old_password,
    this.new_password,
    this.new_password_confirmation,
  });

  String? old_password;
  String? new_password;
  String? new_password_confirmation;

  factory UpdatePasswordApiRequest.fromJson(Map<String, dynamic> json) => UpdatePasswordApiRequest(
    old_password: json["old_password"],
    new_password:   json["new_password"]  ,
    new_password_confirmation: json["new_password_confirmation"]  ,
  );

  Map<String, dynamic> toJson() => {
    "old_password": old_password,
    "new_password": new_password,
    "new_password_confirmation": new_password_confirmation,
  };
}
