import 'dart:convert';

String forgetPasswordRequestToJson(ForgetPasswordRequest data) =>
    json.encode(data.toJson());

class ForgetPasswordRequest {
  ForgetPasswordRequest({
    this.mobile,
    this.countryCode,
  });

  String? mobile;
  String? countryCode;

  Map<String, dynamic> toJson() => {
        "mobile": mobile,
        "country_code": countryCode,
      };
}
