class LoginRequest {
  String mobile;
  String countryCode;
  String password;
  String deviceToken;

  LoginRequest({
    required this.mobile,
    required this.countryCode,
    required this.password,
    required this.deviceToken,
  });

  Map<String, dynamic> toJson() => {
        "mobile": mobile,
        "country_code": countryCode,
        "password": password,
        "device_token": deviceToken
      };
}
