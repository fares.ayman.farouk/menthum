// To parse this JSON data, do
//
//     final resendOtpRequest = resendOtpRequestFromJson(jsonString);

import 'dart:convert';

ResendOtpRequest resendOtpRequestFromJson(String str) =>
    ResendOtpRequest.fromJson(json.decode(str));

String resendOtpRequestToJson(ResendOtpRequest data) =>
    json.encode(data.toJson());

class ResendOtpRequest {
  ResendOtpRequest({this.mobile, this.countryCode, this.sendVia});

  String? mobile;
  String? countryCode;
  String? sendVia;

  factory ResendOtpRequest.fromJson(Map<String, dynamic> json) =>
      ResendOtpRequest(
          mobile: json["mobile"],
          countryCode: json["country_code"],
          sendVia: json["send_via"]);

  Map<String, dynamic> toJson() => {
        "mobile": mobile,
        "country_code": countryCode,
        "send_via": sendVia,
      };
}
