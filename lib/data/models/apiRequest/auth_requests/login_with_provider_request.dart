class LoginWithProviderRequest {
  String email;
  String fullname;
  String providerName;
  String providerToken;
  String deviceToken;

  LoginWithProviderRequest(
      {required this.email,
      required this.fullname,
      required this.providerName,
      required this.providerToken,
      required this.deviceToken});

  Map<String, dynamic> toJson() => {
        "email": email,
        "fullname": fullname,
        "provider_name": providerName,
        "provider_token": providerToken,
        "device_token" : deviceToken
      };
}
