import '../../../domain/models/diabetes/diabetes_done.dart';

class DiabetesDoneModel extends DiabetesDone {
  DiabetesDoneModel({
    required super.eventId,
    super.note,
    super.conditionId,
    super.unit,
    super.value,
  });

  Map<String, dynamic> toJson() => {
        'patient_medical_event_id': super.eventId,
        if (super.conditionId != null)
          'reading_condition_id': super.conditionId,
        if (super.unit != null) 'reading_condition_unit': super.unit,
        if (super.value != null) 'value': super.value,
        if (super.note != null) 'note': super.note,
      };

  factory DiabetesDoneModel.obj(DiabetesDone diabetesDone) => DiabetesDoneModel(
        eventId: diabetesDone.eventId,
        unit: diabetesDone.unit,
        conditionId: diabetesDone.conditionId,
        note: diabetesDone.note,
        value: diabetesDone.value,
      );
}
