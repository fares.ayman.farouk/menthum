class RadsRequest {
  int? country_id;
  int? governorate_id;
  List<int>? city_ids;
  int? district_id;
  List<String>? tests_branches_radiologies;
  String? q;
  int? min_cost;
  int? max_cost;

  RadsRequest(
      {this.country_id,
      this.governorate_id,
      this.city_ids,
      this.district_id,
      this.tests_branches_radiologies,
      this.q,
      this.min_cost,
      this.max_cost});

  Map<String, dynamic> toJson() {
    return {
      "country_id": country_id,
      "governorate_id": governorate_id,
      if (city_ids != null)
        for (int i = 0; i < city_ids!.length; i++) "city_id[$i]": city_ids![i],
      "district_id": district_id,
      if (tests_branches_radiologies != null)
        for (int i = 0; i < tests_branches_radiologies!.length; i++)
          "tests_branches_radiologies[$i]": tests_branches_radiologies![i],
      "q": q,
      "min_cost": min_cost,
      "max_cost": max_cost,
    };
  }
}
