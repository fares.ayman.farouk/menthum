import '../../../../../domain/models/calculation/bmi/bmi_request_entity.dart';

class BmiRequestModel extends BmiRequestEntity {
  BmiRequestModel({required super.height, required super.weight});

  Map<String, dynamic> toJson() => {
        "height": height,
        "weight": weight,
      };

  factory BmiRequestModel.obj(BmiRequestEntity entity) => BmiRequestModel(
        weight: entity.weight,
        height: entity.height,
      );
}
