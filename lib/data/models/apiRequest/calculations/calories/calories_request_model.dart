import 'package:patientapp/domain/models/calculation/calories/calories_request_entity.dart';

class CaloriesRequestModel extends CaloriesRequestEntity {
  CaloriesRequestModel(
      {required super.height, required super.weight, required super.age});

  Map<String, dynamic> toJson() => {
        "height": height,
        "weight": weight,
        "age": age,
      };

  factory CaloriesRequestModel.obj(CaloriesRequestEntity entity) =>
      CaloriesRequestModel(
        weight: entity.weight,
        height: entity.height,
        age: entity.age,
      );
}
