import 'package:patientapp/data/models/apiResponse/questionnaire/questionnaire_response.dart';

import '../../../../domain/models/questionnaire/answer_questionnaire_entity.dart';

class AnswerQuestionnaireRequest extends AnswerQuestionnaireEntity {
  AnswerQuestionnaireRequest({required super.questions});

  Map<String, dynamic> toJson() {
    List<QuestionnaireQuestionModel> questionList =
        List<QuestionnaireQuestionModel>.from(
      questions.map(
        (e) => QuestionnaireQuestionModel(
          id: e.id,
          type: e.type,
          question: e.question,
          options: e.options,
          answer: e.answer,
        ),
      ),
    );

    return {"answers": List<dynamic>.from(questionList.map((x) => x.toJson()))};
  }

  factory AnswerQuestionnaireRequest.obj(
          AnswerQuestionnaireEntity answerQuestions) =>
      AnswerQuestionnaireRequest(questions: answerQuestions.questions);
}
