import '../../../../domain/models/pediatric/enroll_entity.dart';

class EnrollRequestModel extends EnrollRequestEntity {
  EnrollRequestModel({
    required super.patientId,
    required super.birthDate,
    required super.gender,
  });

  Map<String, dynamic> toJson() => {
        'patient_id': patientId,
        'birthdate': birthDate,
        'gender': gender,
      };

  factory EnrollRequestModel.obj(EnrollRequestEntity enrollRequestEntity) =>
      EnrollRequestModel(
        patientId: enrollRequestEntity.patientId,
        birthDate: enrollRequestEntity.birthDate,
        gender: enrollRequestEntity.gender,
      );
}
