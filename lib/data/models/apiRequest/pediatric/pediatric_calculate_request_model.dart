import '../../../../domain/models/pediatric/pediatric_calculate_request_Entity.dart';

class PediatricCalculateRequestModel extends PediatricCalculateRequestEntity {
  PediatricCalculateRequestModel(
      {required super.id, required super.type, required super.value});

  Map<String, dynamic> toJson() => {
        'patient_id': id,
        'type': type,
        'value': value,
      };

  factory PediatricCalculateRequestModel.obj(
          PediatricCalculateRequestEntity calculateEntity) =>
      PediatricCalculateRequestModel(
        value: calculateEntity.value,
        type: calculateEntity.type,
        id: calculateEntity.id,
      );
}
