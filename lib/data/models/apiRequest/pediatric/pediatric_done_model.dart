import 'package:patientapp/domain/models/pediatric/pediatric_done.dart';

class PediatricDoneModel extends PediatricDoneEntity {
  PediatricDoneModel(
      {required super.patientId,
      required super.patientMedicalEventId,
      super.value,
      super.note});

  Map<String, dynamic> toJson() => {
        'patient_id': patientId,
        'patient_medical_event_id': patientMedicalEventId,
        'value': value,
        'note': note
      };

  factory PediatricDoneModel.obj(PediatricDoneEntity entity) =>
      PediatricDoneModel(
          patientMedicalEventId: entity.patientMedicalEventId,
          patientId: entity.patientId,
          value: entity.value,
          note: entity.note);
}
