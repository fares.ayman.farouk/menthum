import 'package:patientapp/domain/models/menstrual_cycle/edit_cycle.dart';

class EditCycleModel extends EditCycle {
  EditCycleModel(
      {required super.bleedingEndAt, required super.note, required super.id});

  Map<String, dynamic> toJson() =>
      {'bleeding_end_at': bleedingEndAt, 'bleeding_notes': note};

  factory EditCycleModel.obj(EditCycle editCycle) => EditCycleModel(
      id: editCycle.id,
      note: editCycle.note,
      bleedingEndAt: editCycle.bleedingEndAt);
}
