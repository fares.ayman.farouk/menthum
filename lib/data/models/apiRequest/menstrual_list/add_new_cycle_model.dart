import 'package:patientapp/domain/models/menstrual_cycle/add_new_cycle.dart';

class AddNewCycleModel extends AddNewCycle {
  AddNewCycleModel({
    required super.startPeriodDate,
    required super.cycleLength,
  });

  Map<String, dynamic> toJson() =>
      {'length': cycleLength, 'start_at': startPeriodDate};

  factory AddNewCycleModel.obj(AddNewCycle addNewCycle) => AddNewCycleModel(
      cycleLength: addNewCycle.cycleLength,
      startPeriodDate: addNewCycle.startPeriodDate);
}
