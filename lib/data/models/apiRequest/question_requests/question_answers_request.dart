import 'dart:convert';

String questionAnswersRequestToJson(QuestionAnswersRequest data) =>
    json.encode(data.toJson());

class QuestionAnswersRequest {
  QuestionAnswersRequest({required this.answers, this.coupon});

  List<QuestionAnswers> answers;
  String? coupon;

  Map<String, dynamic> toJson() => {
        "answers": List<dynamic>.from(answers.map((x) => x.toJson())),
        "code": coupon
      };
}

class QuestionAnswers {
  QuestionAnswers({
    this.id,
    this.type,
    this.answer,
  });

  String? id;
  int? type;
  dynamic answer;

  Map<String, dynamic> toJson() => {
        "id": id,
        "type": type,
        "answer": answer,
      };
}
