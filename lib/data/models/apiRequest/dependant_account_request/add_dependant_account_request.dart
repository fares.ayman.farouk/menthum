import 'dart:convert';

String addDependantAccountRequestToJson(AddDependantAccountRequest data) =>
    json.encode(data.toJson());

class AddDependantAccountRequest {
  AddDependantAccountRequest({
    this.childName,
    this.childBirthdate,
    this.email,
    this.password,
  });

  String? childName;
  String? childBirthdate;
  String? email;
  String? password;

  Map<String, dynamic> toJson() => {
        "childName": childName,
        "childBirthdate": childBirthdate,
        "email": email,
        "password": password,
      };
}
