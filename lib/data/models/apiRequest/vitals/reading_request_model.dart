import 'package:patientapp/domain/models/vitals/request/reading_request.dart';

class ReadingRequestModel extends ReadingRequestEntity {
  ReadingRequestModel(
      {required super.typeId, required super.from, required super.to});

  Map<String, dynamic> toJson() {
    return {"type_id": typeId, "from": from, "to": to};
  }

  factory ReadingRequestModel.obj(ReadingRequestEntity readingRequestEntity) =>
      ReadingRequestModel(
          typeId: readingRequestEntity.typeId,
          from: readingRequestEntity.from,
          to: readingRequestEntity.to);
}
