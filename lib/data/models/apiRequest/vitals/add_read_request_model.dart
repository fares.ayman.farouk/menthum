import '../../../../domain/models/vitals/request/add_read_request.dart';

class AddReadRequestModel extends AddReadRequestEntity {
  AddReadRequestModel(
      {required super.id,
      required super.value,
      required super.readingAt,
      required super.readingConditionId});

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "value": value,
      "reading_at": readingAt,
      "reading_condition_id": readingConditionId
    };
  }

  factory AddReadRequestModel.obj(AddReadRequestEntity addReadRequestEntity) =>
      AddReadRequestModel(
        id: addReadRequestEntity.id,
        readingAt: addReadRequestEntity.readingAt,
        value: addReadRequestEntity.value,
        readingConditionId: addReadRequestEntity.readingConditionId,
      );
}
