class LaboratoryRequest {
  int? country_id;
  int? governorate_id;
  List<int>? city_ids;
  int? district_id;
  List<String>? tests_laboratories;
  String? q;
  int? min_cost;
  int? max_cost;

  LaboratoryRequest(
      {this.country_id,
      this.governorate_id,
      this.city_ids,
      this.district_id,
      this.tests_laboratories,
      this.q,
      this.min_cost,
      this.max_cost});

  Map<String, dynamic> toJson() => {
        "country_id": country_id,
        "governorate_id": governorate_id,
    if(city_ids !=null)
      for(int i= 0;i<city_ids!.length;i++)
        "city_id[$i]": city_ids![i],
        "district_id": district_id,
        if (tests_laboratories != null)
          for (int i = 0; i < tests_laboratories!.length; i++)
            "tests_laboratories[$i]": tests_laboratories![i].toString(),
        "q": q,
        "min_cost": min_cost,
        "max_cost": max_cost,
      };
}
