import 'dart:convert';

import 'package:patientapp/presentation/resources/strings_manger.dart';

BookAppointmentReq bookClinicAppointmentReqFromJson(String str) =>
    BookAppointmentReq.fromJson(json.decode(str));

String bookClinicAppointmentReqToJson(BookAppointmentReq data) =>
    json.encode(data.toJson());

class BookAppointmentReq {
  BookAppointmentReq({
    this.type,
    this.workingshiftId,
    this.hospitalId,
    this.favoriteTime,
    this.callId,
    this.radiology_branch_id,
    this.laboratory_id,
    this.inHome,
    this.timeFrom,
    this.timeTo,
    this.tests,
  });

  String? type;
  int? workingshiftId;
  String? hospitalId;
  String? favoriteTime;
  int? callId;
  String? radiology_branch_id;
  String? laboratory_id;
  int? inHome;
  String? timeFrom;
  String? timeTo;
  List<String>? tests;

  factory BookAppointmentReq.fromJson(Map<String, dynamic> json) =>
      BookAppointmentReq(
        type: json["type"],
        workingshiftId: json["workingshift_id"],
        hospitalId: json["hospital_id"],
        favoriteTime: json["favorite_time"],
        callId: json["call_id"],
        inHome: json["in_home"],
        timeFrom: json["time_from"],
        timeTo: json["time_to"],
      );

  Map<String, dynamic> toJson() {
    return {
      "type": type,
      if (workingshiftId != null) "workingshift_id": workingshiftId,
      if (hospitalId != null) "hospital_id": hospitalId,
      if (favoriteTime != null) "favorite_time": favoriteTime,
      if (type == AppStrings.radiologyBranch)
        "radiology_branch_id": radiology_branch_id,
      if (type == AppStrings.laboratory) "laboratory_id": laboratory_id,
      if (callId != null) "call_id": callId,
      if (inHome != null) "in_home": inHome,
      if (timeTo != null) "time_to": timeTo,
      if (timeFrom != null) "time_from": timeFrom,
      if (type != AppStrings.clinic) "tests": tests,
    };
  }
}
