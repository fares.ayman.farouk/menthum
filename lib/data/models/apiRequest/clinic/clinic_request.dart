class ClinicRequest {
  int? country_id;
  int? governorate_id;
  List<int>? city_ids;
  int? district_id;
  List<String>? specialtiesId;
  String? q;
  int? min_cost;
  int? max_cost;

  ClinicRequest(
      {this.country_id,
      this.governorate_id,
      this.city_ids,
      this.district_id,
      this.specialtiesId,
      this.q,
      this.min_cost,
      this.max_cost});

  Map<String, dynamic> toJson() => {
        "country_id": country_id,
        "governorate_id": governorate_id,
        if (city_ids != null && city_ids!.isNotEmpty)
          for (int i = 0; i < city_ids!.length; i++) "city_id[$i]": city_ids![i],
        "district_id": district_id,
        "specialties[0]": specialtiesId,
        "q": q,
        "min_cost": min_cost,
        "max_cost": max_cost,
      };
}
