import '../../../../../domain/models/calculation/pregnancy/pregnancy_response.dart';

class PregnancyResponse {
  PregnancyResponse({
    this.status,
    this.message,
    this.code,
    this.pregnancy,
  });

  bool? status;
  String? message;
  int? code;
  PregnancyModel? pregnancy;

  factory PregnancyResponse.fromJson(Map<String, dynamic> json) =>
      PregnancyResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        pregnancy: PregnancyModel.fromJson(json['data']),
      );
}

class PregnancyModel extends PregnancyResponseEntity {
  PregnancyModel(
      {required super.numWeek,
      required super.name,
      required super.description,
      required super.pregnancyDate});

  factory PregnancyModel.fromJson(Map<String, dynamic> json) => PregnancyModel(
        name: json['name'],
        numWeek: json['num_week'],
        pregnancyDate: json['pregnancy_date'],
        description: json['description'],
      );
}
