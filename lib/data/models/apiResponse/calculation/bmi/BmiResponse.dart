import 'package:patientapp/domain/models/calculation/bmi/bmi_response_entity.dart';

class BmiResponse {
  BmiResponse({
    this.status,
    this.message,
    this.code,
    this.bmiModel,
  });

  bool? status;
  String? message;
  int? code;
  BmiResponseModel? bmiModel;

  factory BmiResponse.fromJson(Map<String, dynamic> json) => BmiResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        bmiModel: BmiResponseModel.fromJson(json['data']),
      );
}

class BmiResponseModel extends BmiResponseEntity {
  BmiResponseModel(
      {required super.bmi,
      required super.idealWeight,
      required super.name,
      required super.description});

  factory BmiResponseModel.fromJson(Map<String, dynamic> json) =>
      BmiResponseModel(
        bmi: json["bmi"].toString(),
        idealWeight: json["ideal_weight"],
        name: json["name"],
        description: json['description'],
      );
}
