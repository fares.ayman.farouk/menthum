

class CalculationTypesResponse {
  CalculationTypesResponse({
    this.status,
    this.message,
    this.code,
    this.types,
  });

  bool? status;
  String? message;
  int? code;
  List<String>? types;

  factory CalculationTypesResponse.fromJson(Map<String, dynamic> json) =>
      CalculationTypesResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        types: List<String>.from(json["data"].map((x) => x)),
      );
}
