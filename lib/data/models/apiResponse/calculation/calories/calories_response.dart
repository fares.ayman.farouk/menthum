class CalorieResponse {
  CalorieResponse({
    this.status,
    this.message,
    this.code,
    this.calories,
  });

  bool? status;
  String? message;
  int? code;
  CalorieResModel? calories;

  factory CalorieResponse.fromJson(Map<String, dynamic> json) =>
      CalorieResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        calories: CalorieResModel.fromJson(json['data']),
      );
}

class CalorieResModel {
  String? basCalorie;
  CalorieResModel({this.basCalorie});
  factory CalorieResModel.fromJson(Map<String, dynamic> json) =>
      CalorieResModel(basCalorie: json['basic_colorie_intake'].toString());
}
