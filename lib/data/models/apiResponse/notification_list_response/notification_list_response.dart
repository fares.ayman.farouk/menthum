NotificationResponse notificationResponseFromJson(Map<String, dynamic> str) =>
    NotificationResponse.fromJson(str);

class NotificationResponse {
  NotificationResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  List<NotifiData>? data;

  factory NotificationResponse.fromJson(Map<String, dynamic> json) =>
      NotificationResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: List<NotifiData>.from(
            json["data"].map((x) => NotifiData.fromJson(x))),
      );
}

class NotifiData {
  NotifiData({
    this.id,
    this.title,
    this.body,
    this.notificationType,
    this.metaData,
    this.isRead,
    this.createdAt,
  });

  int? id;
  String? title;
  String? body;
  int? notificationType;
  MetaData? metaData;
  bool? isRead;
  int? createdAt;

  factory NotifiData.fromJson(Map<String, dynamic> json) {
    return NotifiData(
      id: json["id"],
      title: json["title"],
      body: json["body"],
      notificationType: json["notification_type"],
      metaData: json["meta_data"] == null
          ? null
          : MetaData.fromJson(json["meta_data"]),
      isRead: json["is_read"],
      createdAt: json["created_at"],
    );
  }
}

class MetaData {
  MetaData({
    this.callId,
    this.subscribeId,
    this.planId
  });

  int? callId;
  int? planId;
  int? subscribeId;


  factory MetaData.fromJson(Map<String, dynamic> json) => MetaData(
    callId: json["call_id"] != null ? int.parse(json["call_id"].toString()) : null,
    subscribeId: json["subscription_id"] != null ? int.parse(json["subscription_id"].toString()) : null,
    planId: json["plan_id"] != null ? int.parse(json["plan_id"].toString()) : null,
      );
}
