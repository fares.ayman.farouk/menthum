import '../../../../domain/models/pediatric/pediatric_head_response.dart';

class PediatricHeadResponse {
  PediatricHeadResponse({
    this.status,
    this.message,
    this.code,
    this.pediatricData,
  });

  bool? status;
  String? message;
  int? code;
  PediatricHeadModel? pediatricData;

  factory PediatricHeadResponse.fromJson(Map<String, dynamic> json) =>
      PediatricHeadResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        pediatricData: PediatricHeadModel.fromJson(json['data']),
      );
}

class PediatricHeadModel extends PediatricHeadResponseEntity {
  PediatricHeadModel(
      {required super.message, required super.pediatricHeadModelResponse});

  factory PediatricHeadModel.fromJson(Map<String, dynamic> json) =>
      PediatricHeadModel(
        message: json['message'],
        pediatricHeadModelResponse:
            PediatricHeadModelModel.fromJson(json['model']),
      );
}

class PediatricHeadModelModel extends PediatricHeadModelEntity {
  PediatricHeadModelModel(
      {required super.id,
      required super.minCircum,
      required super.avgCircum,
      required super.maxCircum,
      required super.gender,
      required super.program_action_id,
      required super.created_at,
      required super.updated_at});

  factory PediatricHeadModelModel.fromJson(Map<String, dynamic> json) =>
      PediatricHeadModelModel(
        id: json['id']??0,
        minCircum: json['min_circum']??"",
        avgCircum: json['avg_circum']??"",
        maxCircum: json['max_circum']??"",
        gender: json['gender']??0,
        program_action_id: json['program_action_id']??0,
        created_at: json['created_at']??"",
        updated_at: json['updated_at']??"",
      );
}
