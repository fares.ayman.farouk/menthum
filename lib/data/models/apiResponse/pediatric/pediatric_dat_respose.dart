import 'package:patientapp/domain/models/pediatric/pediatric_data_entity.dart';

class PediatricDataResponse {
  PediatricDataResponse({
    this.status,
    this.message,
    this.code,
    this.pediatricData,
  });

  bool? status;
  String? message;
  int? code;
  List<PediatricDataModel>? pediatricData;

  factory PediatricDataResponse.fromJson(Map<String, dynamic> json) =>
      PediatricDataResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        pediatricData: List<PediatricDataModel>.from(
            json["data"]['events'].map((x) => PediatricDataModel.fromJson(x))),
      );
}

class PediatricDataModel extends PediatricDataEntity {
  PediatricDataModel({
    required super.enrollAt,
    required super.date,
    required super.actionableType,
    required super.patientMedicalEventId,
    required super.done,
    required super.pediatricCondition,
  });

  factory PediatricDataModel.fromJson(Map<String, dynamic> json) =>
      PediatricDataModel(
          actionableType: json['actionable_type'],
          date: json['date'],
          done: json['done'],
          enrollAt: json['enroll_at'],
          patientMedicalEventId: json['patient_medical_event_id'],
          pediatricCondition:
              selectCondition(json['actionable_type'], json['condition']));
}

selectCondition(String actionableType, Map<String, dynamic> json) {
  if (actionableType == "vaccin") {
    return VaccinConditionModel.fromJson(json);
  } else if (actionableType == "head_circumference") {
    return HeadCircumFerenceModel.fromJson(json);
  } else {
    return WeightConditionsModel.fromJson(json);
  }
}

class VaccinConditionModel extends VaccinCondition {
  VaccinConditionModel(
      {required super.id,
      required super.name,
      required super.description,
      required super.type});

  factory VaccinConditionModel.fromJson(Map<String, dynamic> json) =>
      VaccinConditionModel(
        id: json['vaccin_id'],
        name: json['name'],
        type: json['type'],
        description: json['description'] ?? "",
      );
}

class HeadCircumFerenceModel extends HeadCircumFerence {
  HeadCircumFerenceModel(
      {required super.id,
      required super.minCircum,
      required super.avgCircum,
      required super.maxCircum});

  factory HeadCircumFerenceModel.fromJson(Map<String, dynamic> json) =>
      HeadCircumFerenceModel(
        id: json['head_circumference_id'],
        maxCircum: json['max_circum'],
        avgCircum: json['avg_circum'],
        minCircum: json['max_circum'],
      );
}

class WeightConditionsModel extends WeightConditions {
  WeightConditionsModel(
      {required super.id,
      required super.normalFrom,
      required super.normalTo,
      required super.overweight,
      required super.severelyUnderweight,
      required super.underweightFrom,
      required super.underweightTo});

  factory WeightConditionsModel.fromJson(Map<String, dynamic> json) =>
      WeightConditionsModel(
        id: json['weight_baby_id'],
        severelyUnderweight: json['severely_underweight'].toString(),
        underweightFrom: json['underweight_from'].toString(),
        underweightTo: json['underweight_to'].toString(),
        normalFrom: json['normal_from'].toString(),
        normalTo: json['normal_to'].toString(),
        overweight: json['overweight'].toString(),
      );
}
