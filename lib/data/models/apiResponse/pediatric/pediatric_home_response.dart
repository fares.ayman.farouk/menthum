import '../../../../domain/models/pediatric/pediatric_home.dart';

class PediatricHomeResponse {
  PediatricHomeResponse({
    this.status,
    this.message,
    this.code,
    this.pediatricData,
  });

  bool? status;
  String? message;
  int? code;
  List<PediatricHomeModel>? pediatricData;

  factory PediatricHomeResponse.fromJson(Map<String, dynamic> json) =>
      PediatricHomeResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        pediatricData: List<PediatricHomeModel>.from(
            json["data"].map((x) => PediatricHomeModel.fromJson(x))),
      );
}

class PediatricHomeModel extends PediatricHomeEntity {
  PediatricHomeModel({required super.name, required super.type});

  factory PediatricHomeModel.fromJson(Map<String, dynamic> json) =>
      PediatricHomeModel(
        name: json['name'],
        type: json['type'],
      );
}
