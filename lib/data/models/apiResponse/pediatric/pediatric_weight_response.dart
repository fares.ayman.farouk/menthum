import '../../../../domain/models/pediatric/pediatric_weight_response.dart';

class PediatricWeightResponse {
  PediatricWeightResponse({
    this.status,
    this.message,
    this.code,
    this.pediatricData,
  });

  bool? status;
  String? message;
  int? code;
  PediatricWeightResponseModel? pediatricData;

  factory PediatricWeightResponse.fromJson(Map<String, dynamic> json) =>
      PediatricWeightResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        pediatricData: PediatricWeightResponseModel.fromJson(json['data']),
      );
}

class PediatricWeightResponseModel extends PediatricWeightResponseEntity {
  PediatricWeightResponseModel(
      {required super.message, required super.pediatricWeightModelResponse});

  factory PediatricWeightResponseModel.fromJson(Map<String, dynamic> json) =>
      PediatricWeightResponseModel(
          message: json['message'],
          pediatricWeightModelResponse:
              PediatricWeightModel.fromJson(json['model']));
}

class PediatricWeightModel extends PediatricWeightModelResponse {
  PediatricWeightModel(
      {required super.id,
      required super.severely_underweight,
      required super.underweightFrom,
      required super.underweightTo,
      required super.normalFrom,
      required super.normalTo,
      required super.overweight,
      required super.gender,
      required super.program_action_id,
      required super.created_at,
      required super.updated_at});

  factory PediatricWeightModel.fromJson(Map<String, dynamic> json) =>
      PediatricWeightModel(
        id: json['id'],
        severely_underweight: json['severely_underweight'] ?? "",
        underweightFrom: json['underweight_from'] ?? "",
        underweightTo: json['underweight_to'] ?? "",
        normalFrom: json['normal_from'] ?? "",
        normalTo: json['normal_to'] ?? "",
        overweight: json['overweight'] ?? "",
        gender: json['gender'] ?? 0,
        program_action_id: json['program_action_id'] ?? 0,
        created_at: json['created_at'] ?? "",
        updated_at: json['updated_at'] ?? "",
      );
}
