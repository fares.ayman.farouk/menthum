import 'package:patientapp/data/models/apiResponse/pediatric/pediatric_dat_respose.dart';

import '../../../../domain/models/pediatric/history_entity.dart';

class PediatricHistoryResponse {
  PediatricHistoryResponse({
    this.status,
    this.message,
    this.code,
    this.pediatricData,
  });

  bool? status;
  String? message;
  int? code;
  PediatricHistoryResponseModel? pediatricData;

  factory PediatricHistoryResponse.fromJson(Map<String, dynamic> json) =>
      PediatricHistoryResponse(
          status: json["status"],
          message: json["message"],
          code: json["code"],
          pediatricData: PediatricHistoryResponseModel.fromJson(json['data']));
}

class PediatricHistoryResponseModel extends PediatricHistoryEntity {
  PediatricHistoryResponseModel(
      {required super.pediatricData, required super.latPage});

  factory PediatricHistoryResponseModel.fromJson(Map<String, dynamic> json) =>
      PediatricHistoryResponseModel(
          pediatricData: List<PediatricDataModel>.from(
              json['events'].map((x) => PediatricDataModel.fromJson(x))),
          latPage: json['links']['last_page']);
}
