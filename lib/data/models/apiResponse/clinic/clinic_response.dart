import 'dart:convert';

ClinicsResponse getClinicsResponseFromJson(Map<String, dynamic> data) {
  return ClinicsResponse.fromJson(data);
}

String getClinicsResponseToJson(ClinicsResponse data) =>
    json.encode(data.toJson());

class ClinicsResponse {
  ClinicsResponse({
    this.status,
    this.message,
    this.code,
    this.data,
    this.meta,
  });

  bool? status;
  String? message;
  int? code;
  List<Clinic>? data;
  Meta? meta;

  factory ClinicsResponse.fromJson(Map<String, dynamic> json) {
    return ClinicsResponse(
      status: json["status"],
      message: json["message"],
      code: json["code"],
      data: List<Clinic>.from(json["data"].map((x) => Clinic.fromJson(x))),
      meta: Meta.fromJson(json["meta"]),
    );
  }

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "code": code,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "meta": meta?.toJson(),
      };
}

class Clinic {
  Clinic({
    this.id,
    this.name,
    this.description,
    this.address,
    this.logo,
    this.lat,
    this.lng,
    this.consultationFees,
    this.consultationTime,
    this.specialty,
    this.workingShifts,
    this.date,
    this.images,
    this.specialtyDescription,
  });

  String? id;
  String? name;
  String? description;
  String? address;
  String? logo;
  String? lat;
  String? lng;
  List<Image>? images;
  int? consultationFees;
  int? consultationTime;
  Specialty? specialty;
  List<WorkingShift>? workingShifts;
  int? date;
  String? specialtyDescription;

  factory Clinic.fromJson(Map<String, dynamic> json) {
    try {
      return Clinic(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        address: json["address"],
        logo: json["logo"],
        lat: json["lat"].toString(),
        lng: json["lng"].toString(),
        consultationFees: json["consultation_fees"],
        specialtyDescription: json["specialty_description"],
        consultationTime: json["consultation_time"],
        images: List<Image>.from(json["images"].map((x) => Image.fromJson(x))),
        specialty: Specialty.fromJson(json["specialty"]),
        workingShifts: List<WorkingShift>.from(
            json["working_shifts"].map((x) => WorkingShift.fromJson(x))),
        date: json["date"],
      );
    } catch (e) {
      print("#################////$e");
      throw e.toString();
    }
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "address": address,
        "logo": logo,
        "lat": lat,
        "lng": lng,
        "consultation_fees": consultationFees,
        "consultation_time": consultationTime,
        "images": List<dynamic>.from(images!.map((x) => x.toJson())),
        "specialty": specialty?.toJson(),
        "working_shifts":
            List<dynamic>.from(workingShifts!.map((x) => x.toJson())),
        "date": date,
      };
}

class Image {
  Image({
    this.id,
    this.originalUrl,
    this.fileName,
  });

  int? id;
  String? originalUrl;
  String? fileName;

  factory Image.fromJson(Map<String, dynamic> json) {
    try{
      return Image(
        id: json["id"],
        originalUrl: json["original_url"],
        fileName: json["file_name"],
      );
    }catch(e){
      print('********************************$e');
      throw e.toString();
    }
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "original_url": originalUrl,
        "file_name": fileName,
      };
}

class Specialty {
  Specialty({
    this.id,
    this.name,
  });

  String? id;
  String? name;

  factory Specialty.fromJson(Map<String, dynamic> json) => Specialty(
        id: json["id"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };
}

class WorkingShift {
  WorkingShift({
    this.id,
    this.weekDay,
    this.timeFrom,
    this.timeTo,
    this.day,
  });

  int? id;
  int? weekDay;
  int? timeFrom;
  int? timeTo;
  DateTime? day;

  factory WorkingShift.fromJson(Map<String, dynamic> json) => WorkingShift(
        id: json["id"],
        weekDay: json["week_day"],
        timeFrom: json["time_from"],
        timeTo: json["time_to"],
        day: DateTime.parse(json["day"].toString()),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "week_day": weekDay,
        "time_from": timeFrom,
        "time_to": timeTo,
        "day":
            "${day?.year.toString().padLeft(4, '0')}-${day?.month.toString().padLeft(2, '0')}-${day?.day.toString().padLeft(2, '0')}",
      };
}

class Meta {
  Meta({
    this.currentPage,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.nextPageUrl,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  int? currentPage;
  int? lastPage;
  int? perPage;
  int? from;
  int? to;
  int? total;
  String? firstPageUrl;
  String? lastPageUrl;
  String? nextPageUrl;
  String? prevPageUrl;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        currentPage: json["current_page"],
        firstPageUrl: json["first_page_url"],
        from: json["from"],
        lastPage: json["last_page"],
        lastPageUrl: json["last_page_url"],
        nextPageUrl: json["next_page_url"],
        perPage: json["per_page"],
        prevPageUrl: json["prev_page_url"],
        to: json["to"],
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "first_page_url": firstPageUrl,
        "from": from,
        "last_page": lastPage,
        "last_page_url": lastPageUrl,
        "next_page_url": nextPageUrl,
        "per_page": perPage,
        "prev_page_url": prevPageUrl,
        "to": to,
        "total": total,
      };
}
