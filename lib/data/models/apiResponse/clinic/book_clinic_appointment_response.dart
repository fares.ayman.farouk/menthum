import 'dart:convert';

BookAppointmentResponse bookClinicAppointmentResponseFromJson(
        Map<String,dynamic> data) =>
    BookAppointmentResponse.fromJson(data);

String bookClinicAppointmentResponseToJson(
        BookAppointmentResponse data) =>
    json.encode(data.toJson());

class BookAppointmentResponse {
  BookAppointmentResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  String? data;

  factory BookAppointmentResponse.fromJson(Map<String, dynamic> json) =>
      BookAppointmentResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: json["data"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "code": code,
        "data": data,
      };
}
