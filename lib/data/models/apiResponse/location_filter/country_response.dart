// To parse this JSON data, do
//
//     final countryResponse = countryResponseFromJson(jsonString);

import 'dart:convert';

CountryResponse countryResponseFromJson(Map<String, dynamic> data) =>
    CountryResponse.fromJson(data);

String countryResponseToJson(CountryResponse data) =>
    json.encode(data.toJson());

class CountryResponse {
  CountryResponse({
    this.countries,
  });

  List<Country>? countries;

  factory CountryResponse.fromJson(Map<String, dynamic> json) =>
      CountryResponse(
        countries:
            List<Country>.from(json["data"].map((x) => Country.fromJson(x))),
      );
  factory CountryResponse.fromJsonForSharedPref(List<dynamic> data) =>
      CountryResponse(
        countries:
        List<Country>.from(data.map((x) => Country.fromJson(x))),
      );



  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(countries!.map((x) => x.toJson())),
      };
}

class Country {
  Country({
    this.id,
    this.name,
    this.nationality,
    this.alpha2Code,
    this.alpha3Code,
    this.status,
  });

  int? id;
  String? name;
  String? nationality;
  String? alpha2Code;
  String? alpha3Code;
  int? status;

  factory Country.fromJson(Map<String, dynamic> json) => Country(
        id: json["id"],
        name: json["name"],
        nationality: json["nationality"],
        alpha2Code: json["alpha_2_code"],
        alpha3Code: json["alpha_3_code"],
        status: json["status"],
      );


  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "nationality": nationality,
        "alpha_2_code": alpha2Code,
        "alpha_3_code": alpha3Code,
        "status": status,
      };
}
