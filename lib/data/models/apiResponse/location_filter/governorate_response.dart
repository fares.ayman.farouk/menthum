// To parse this JSON data, do
//
//     final governorateResponse = governorateResponseFromJson(jsonString);

import 'dart:convert';

GovernorateResponse governorateResponseFromJson(Map<String, dynamic> data) =>
    GovernorateResponse.fromJson(data);

String governorateResponseToJson(GovernorateResponse data) =>
    json.encode(data.toJson());

class GovernorateResponse {
  GovernorateResponse({
    this.data,
  });

  List<Governorate>? data;

  factory GovernorateResponse.fromJson(Map<String, dynamic> json) =>
      GovernorateResponse(
        data: List<Governorate>.from(
            json["data"].map((x) => Governorate.fromJson(x))),
      );

  factory GovernorateResponse.fromJsonForSharedPref(List<dynamic> data) =>
      GovernorateResponse(
        data: List<Governorate>.from(data.map((x) => Governorate.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Governorate {
  Governorate({
    this.id,
    this.name,
    this.countryId,
  });

  int? id;
  String? name;
  int? countryId;

  factory Governorate.fromJson(Map<String, dynamic> json) => Governorate(
        id: json["id"],
        name: json["name"],
        countryId: json["country_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "country_id": countryId,
      };
}
