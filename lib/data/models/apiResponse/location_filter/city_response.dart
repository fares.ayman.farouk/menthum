import 'dart:convert';

CityResponse cityResponseFromJson(Map<String, dynamic> data) =>
    CityResponse.fromJson(data);

String cityResponseToJson(CityResponse data) => json.encode(data.toJson());

class CityResponse {
  CityResponse({
    this.data,
  });

  List<City>? data;

  factory CityResponse.fromJson(Map<String, dynamic> json) => CityResponse(
        data: List<City>.from(json["data"].map((x) => City.fromJson(x))),
      );

  factory CityResponse.fromJsonForSharedPref(List<dynamic> data) =>
      CityResponse(
        data: List<City>.from(data.map((x) => City.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class City {
  City({
    this.id,
    this.name,
    this.governorateId,
  });

  int? id;
  String? name;
  int? governorateId;

  factory City.fromJson(Map<String, dynamic> json) => City(
        id: json["id"],
        name: json["name"],
        governorateId: json["governorate_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "governorate_id": governorateId,
      };
}
