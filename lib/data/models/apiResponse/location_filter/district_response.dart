// To parse this JSON data, do
//
//     final districtResponse = districtResponseFromJson(jsonString);

import 'dart:convert';

DistrictResponse districtResponseFromJson(Map<String, dynamic> data) =>
    DistrictResponse.fromJson(data);

String districtResponseToJson(DistrictResponse data) =>
    json.encode(data.toJson());

class DistrictResponse {
  DistrictResponse({
    this.districts,
  });

  List<District>? districts;

  factory DistrictResponse.fromJson(Map<String, dynamic> json) =>
      DistrictResponse(
        districts:
            List<District>.from(json["data"].map((x) => District.fromJson(x))),
      );

  factory DistrictResponse.fromJsonForSharedPref(List<dynamic> data) =>
      DistrictResponse(
        districts: List<District>.from(data.map((x) => District.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(districts!.map((x) => x.toJson())),
      };
}

class District {
  District({
    this.id,
    this.name,
    this.cityId,
  });

  int? id;
  String? name;
  int? cityId;

  factory District.fromJson(Map<String, dynamic> json) => District(
        id: json["id"],
        name: json["name"],
        cityId: json["city_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "city_id": cityId,
      };
}
