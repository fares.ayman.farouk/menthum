import 'package:patientapp/data/models/apiResponse/specialties_response.dart';
import 'package:patientapp/data/models/apiResponse/tests_response.dart';
import 'package:patientapp/data/models/apiResponse/upcoming_response.dart';

VisitDetailResponse visitDetailResponseFromJson(Map<String, dynamic> data) =>
    VisitDetailResponse.fromJson(data);

class VisitDetailResponse {
  VisitDetailResponse({
    this.status,
    this.message,
    this.code,
    this.visitDetailsData,
  });

  bool? status;
  String? message;
  int? code;
  VisitDetailsData? visitDetailsData;

  factory VisitDetailResponse.fromJson(Map<String, dynamic> json) {

    return VisitDetailResponse(
      status: json["status"],
      message: json["message"],
      code: json["code"],
      visitDetailsData: VisitDetailsData.fromJson(json["data"]),
    );
  }
}

class VisitDetailsData {
  VisitDetailsData({
    this.id,
    this.name,
    this.address,
    this.logo,
    this.lat,
    this.lng,
    this.doctor,
    this.callRecommendationsSpecialty,
    this.callRecommendationsLaboratory,
    this.callRecommendationsRadiology,
    this.callReport,
    this.date,
    required this.appointmentClinic,
    required this.appointmentLaboratory,
    required this.appointmentRadiologyBranch,
  });

  int? id;
  String? name;
  String? address;
  String? logo;
  double? lat;
  double? lng;
  Doctor? doctor;
  Speciality? callRecommendationsSpecialty;
  List<Test>? callRecommendationsLaboratory;
  List<Test>? callRecommendationsRadiology;
  String? callReport;
  int? date;
  List<AppointmentItem> appointmentClinic;
  List<AppointmentItem> appointmentLaboratory;
  List<AppointmentItem> appointmentRadiologyBranch;

  factory VisitDetailsData.fromJson(Map<String, dynamic> json) {

    return VisitDetailsData(
      id: json["id"],
      name: json["name"],
      address: json["address"],
      logo: json["logo"],
      lat: json["lat"] != null ? double.parse(json["lat"].toString()) : null,
      lng: json["lat"] != null ? double.parse(json["lng"].toString()) : null,
      doctor: json["doctor"] != null ? Doctor.fromJson(json["doctor"]) : null,
      callRecommendationsSpecialty:
          json["call_recommendations_specialty"] != null
              ? Speciality.fromJson(json["call_recommendations_specialty"])
              : null,
      callRecommendationsLaboratory: List<Test>.from(
          json["call_recommendations_laboratory"].map((x) => Test.fromJson(x))),
      callRecommendationsRadiology: List<Test>.from(
          json["call_recommendations_radiology"].map((x) => Test.fromJson(x))),
      callReport: json["callReport"],
      date: json["date"],
      appointmentClinic:
          List<AppointmentItem>.from(json["appointment_clinic"].map((x) => AppointmentItem.fromJson(x))),
      appointmentLaboratory: List<AppointmentItem>.from(
          json["appointment_laboratory"]
              .map((x) => AppointmentItem.fromJson(x))),
      appointmentRadiologyBranch: List<AppointmentItem>.from(
          json["appointment_radiology_branch"]
              .map((x) => AppointmentItem.fromJson(x))),
    );
  }
}

class Doctor {
  Doctor({
    this.fullname,
    this.avatar,
    this.position,
  });

  String? fullname;
  String? avatar;
  String? position;

  factory Doctor.fromJson(Map<String, dynamic> json) {
    return Doctor(
      fullname: json["fullname"],
      avatar: json["avatar"],
      position: json["position"],
    );
  }
}
