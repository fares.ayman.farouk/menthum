
ConsultationsResponse consultationsResponseFromJson(Map<String, dynamic> str) =>
    ConsultationsResponse.fromJson(str);

class ConsultationsResponse {
  ConsultationsResponse({
    this.status,
    this.message,
    this.code,
    this.data,
    this.meta,
  });

  bool? status;
  String? message;
  int? code;
  List<ConsultationItem>? data;
  Meta? meta;

  factory ConsultationsResponse.fromJson(Map<String, dynamic> json) =>
      ConsultationsResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: List<ConsultationItem>.from(
            json["data"].map((x) => ConsultationItem.fromJson(x))),
        meta: Meta.fromJson(json["meta"]),
      );
}

class ConsultationItem {
  ConsultationItem({
    this.id,
    this.name,
    this.address,
    this.logo,
    this.date,
    this.lat,
    this.lng
  });

  int? id;
  String? name;
  String? address;
  String? logo;
  int? date;
  double? lat;
  double? lng;


  factory ConsultationItem.fromJson(Map<String, dynamic> json) =>
      ConsultationItem(
        id: json["id"],
        name: json["name"],
        address: json["address"],
        logo: json["logo"],
        date: json["date"],
        lat: json["lat"] == null ? null : double.parse(json["lat"].toString()),
        lng: json["lng"] == null ? null : double.parse(json["lng"].toString()),
      );
}

class Meta {
  Meta({
    this.currentPage,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.nextPageUrl,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  int? currentPage;
  int? lastPage;
  int? perPage;
  int? from;
  int? to;
  int? total;
  String? firstPageUrl;
  String? lastPageUrl;
  String? nextPageUrl;
  String? prevPageUrl;


  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
    currentPage: json["current_page"],
    firstPageUrl: json["first_page_url"],
    from: json["from"],
    lastPage: json["last_page"],
    lastPageUrl: json["last_page_url"],
    nextPageUrl: json["next_page_url"],
    perPage: json["per_page"],
    prevPageUrl: json["prev_page_url"],
    to: json["to"],
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "current_page": currentPage,
    "first_page_url": firstPageUrl,
    "from": from,
    "last_page": lastPage,
    "last_page_url": lastPageUrl,
    "next_page_url": nextPageUrl,
    "per_page": perPage,
    "prev_page_url": prevPageUrl,
    "to": to,
    "total": total,
  };
}
