import 'package:patientapp/data/models/apiResponse/clinic/clinic_response.dart';

ClinicProfileResponse clinicProfileResponseFromJson(Map<String, dynamic> str) =>
    ClinicProfileResponse.fromJson(str);

class ClinicProfileResponse {
  ClinicProfileResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  ClinicProfile? data;

  factory ClinicProfileResponse.fromJson(Map<String, dynamic> json) =>
      ClinicProfileResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: ClinicProfile.fromJson(json["data"]),
      );
}

class ClinicProfile {
  ClinicProfile({
    this.id,
    this.name,
    this.description,
    this.address,
    this.logo,
    this.lat,
    this.lng,
    this.consultationFees,
    this.consultationTime,
    this.specialty,
    this.workingShifts,
    this.hospitals,
    this.date,
    this.images,
    this.website,
    this.phone,
  });

  String? id;
  String? name;
  String? description;
  String? address;
  String? logo;
  String? lat;
  String? lng;
  int? consultationFees;
  int? consultationTime;
  String? website;
  Specialty? specialty;
  List<ClinicImage>? images;
  List<WorkingShift>? workingShifts;
  List<Hospital>? hospitals;
  int? date;
  List<String>? phone;

  factory ClinicProfile.fromJson(Map<String, dynamic> json) {
    try {
      return ClinicProfile(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        address: json["address"],
        website: json["website"],
        logo: json["logo"],
        lat: json["lat"].toString(),
        phone: json["phone"].toString().split('-').toList(),
        lng: json["lng"].toString(),
        consultationFees: json["consultation_fees"],
        consultationTime: json["consultation_time"],
        specialty: Specialty.fromJson(json["specialty"]),
        images: List<ClinicImage>.from(
            json["images"].map((x) => ClinicImage.fromJson(x))),
        workingShifts: List<WorkingShift>.from(
            json["working_shifts"].map((x) => WorkingShift.fromJson(x))),
        hospitals: List<Hospital>.from(
            json["hospitals"].map((x) => Hospital.fromJson(x))),
        date: json["date"],
      );
    } catch (e) {
      throw e;
    }
  }
}

class ClinicImage {
  ClinicImage({
    required this.id,
    required this.originalUrl,
    required this.fileName,
  });

  int id;
  String originalUrl;
  String fileName;

  factory ClinicImage.fromJson(Map<String, dynamic> json) => ClinicImage(
        id: json["id"],
        originalUrl: json["original_url"],
        fileName: json["file_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "original_url": originalUrl,
        "file_name": fileName,
      };
}

class Hospital {
  Hospital({
    this.id,
    this.name,
  });

  String? id;
  String? name;

  factory Hospital.fromJson(Map<String, dynamic> json) =>
      Hospital(id: json["id"], name: json["name"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };
}
