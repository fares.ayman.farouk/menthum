GetDependantAccountsResponse getDependantAccountsResponseFromJson(
        Map<String, dynamic> str) =>
    GetDependantAccountsResponse.fromJson(str);

class GetDependantAccountsResponse {
  GetDependantAccountsResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  List<DependantAccountData>? data;

  factory GetDependantAccountsResponse.fromJson(Map<String, dynamic> json) =>
      GetDependantAccountsResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: List<DependantAccountData>.from(
            json["data"].map((x) => DependantAccountData.fromJson(x))),
      );
}

class DependantAccountData {
  DependantAccountData({
    this.id,
    this.fullname,
    this.image,
  });

  String? id;
  String? fullname;
  String? image;

  factory DependantAccountData.fromJson(Map<String, dynamic> json) =>
      DependantAccountData(
        id: json["id"],
        fullname: json["fullname"],
        image: json["image"],
      );
}
