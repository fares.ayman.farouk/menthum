import '../../../../domain/models/programs/program_entity.dart';

class ProgramModel extends Program {
  ProgramModel({
    required super.id,
    required super.title,
    required super.icon,
    required super.route,
  });

  factory ProgramModel.fromJson(Map<String, dynamic> json) => ProgramModel(
        id: json['id'],
        title: json['name'],
        icon: json['image'],
        route: json['type'],
      );
}
