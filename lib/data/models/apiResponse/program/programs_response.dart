import 'package:patientapp/data/models/apiResponse/program/program_model.dart';

class ProgramsResponse {
  bool? status;
  String? message;
  int? code;
  List<ProgramModel>? data;

  ProgramsResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  factory ProgramsResponse.fromJson(Map<String, dynamic> json) =>
      ProgramsResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: List<ProgramModel>.from(
            json["data"].map((x) => ProgramModel.fromJson(x))),
      );
}
