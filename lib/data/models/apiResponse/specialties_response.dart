import 'dart:convert';

SpecialtiesResponse specialtiesResponseFromJson(Map<String, dynamic> data) =>
    SpecialtiesResponse.fromJson(data);

String specialtiesResponseToJson(SpecialtiesResponse data) =>
    json.encode(data.toJson());

class SpecialtiesResponse {
  SpecialtiesResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  List<Speciality>? data;

  factory SpecialtiesResponse.fromJson(Map<String, dynamic> json) =>
      SpecialtiesResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: List<Speciality>.from(
            json["data"].map((x) => Speciality.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "code": code,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Speciality {
  Speciality({this.id, this.name, this.logo});

  String? id;
  String? name;
  String? logo;

  factory Speciality.fromJson(Map<String, dynamic> json) => Speciality(
        id: json["id"],
        name: json["name"],
        logo: json["logo"],
      );

  Map<String, dynamic> toJson() => {"id": id, "name": name, "logo": logo};
}
