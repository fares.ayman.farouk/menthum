import '../../../../domain/models/questionnaire/questionnaire_entity.dart';

class QuestionnaireQuestionResponse {
  QuestionnaireQuestionResponse({
    required this.status,
    required this.message,
    required this.code,
    this.questions,
  });

  bool status;
  String message;
  int code;
  List<QuestionnaireQuestionModel>? questions;

  factory QuestionnaireQuestionResponse.fromJson(Map<String, dynamic> json) =>
      QuestionnaireQuestionResponse(
          status: json["status"],
          message: json["message"],
          code: json["code"],
          questions: json["status"] as bool
              ? List<QuestionnaireQuestionModel>.from(json["data"]
                  .map((x) => QuestionnaireQuestionModel.fromJson(x)))
              : []);
}

class QuestionnaireQuestionModel extends QuestionnaireQuestionEntity {
  QuestionnaireQuestionModel(
      {required super.id,
      required super.question,
      required super.options,
      required super.type,
      super.answer});

  factory QuestionnaireQuestionModel.fromJson(Map<String, dynamic> json) =>
      QuestionnaireQuestionModel(
          id: json['id'],
          question: json['question'],
          type: json['type'],
          options: List<QuestionnaireOptionModel>.from(
            json["option"].map(
              (x) => QuestionnaireOptionModel.fromJson(x),
            ),
          ),
          answer: json['answer']);

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "answer": answer,
    };
  }
}

class QuestionnaireOptionModel extends QuestionnaireOptionEntity {
  QuestionnaireOptionModel({required super.id, required super.text});

  factory QuestionnaireOptionModel.fromJson(Map<String, dynamic> json) =>
      QuestionnaireOptionModel(id: json['id'], text: json['text']);
}
