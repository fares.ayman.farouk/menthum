import 'package:patientapp/domain/models/questionnaire/questionnaire_item_entity.dart';

class QuestionnaireItemResponse {
  QuestionnaireItemResponse({
    this.status,
    this.message,
    this.code,
    this.questionnaireItems,
  });

  bool? status;
  String? message;
  int? code;
  List<QuestionnaireItemModel>? questionnaireItems;

  factory QuestionnaireItemResponse.fromJson(Map<String, dynamic> json) =>
      QuestionnaireItemResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        questionnaireItems: List<QuestionnaireItemModel>.from(
            json["data"].map((x) => QuestionnaireItemModel.fromJson(x))),
      );
}

class QuestionnaireItemModel extends QuestionnaireItemEntity {
  QuestionnaireItemModel(
      {required super.id,
      required super.slug,
      required super.name,
      required super.description});

  factory QuestionnaireItemModel.fromJson(Map<String, dynamic> json) =>
      QuestionnaireItemModel(
        name: json['name'],
        id: json['id'],
        slug: json['slug'],
        description: json['description'],
      );
}
