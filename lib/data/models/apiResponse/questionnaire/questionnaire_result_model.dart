import '../../../../domain/models/questionnaire/questionnaire_result_entity.dart';

class QuestionnaireResultResponse {
  QuestionnaireResultResponse({
    required this.status,
    required this.message,
    required this.code,
    this.questionnaireResult,
  });

  bool status;
  String message;
  int code;
  QuestionnaireResultModel? questionnaireResult;

  factory QuestionnaireResultResponse.fromJson(Map<String, dynamic> json) =>
      QuestionnaireResultResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        questionnaireResult: QuestionnaireResultModel.fromJson(
          json['data'],
        ),
      );
}

class QuestionnaireResultModel extends QuestionnaireResultEntity {
  QuestionnaireResultModel({required super.value, required super.message});

  factory QuestionnaireResultModel.fromJson(Map<String, dynamic> json) =>
      QuestionnaireResultModel(
          value: json['value'], message: json['message'] ?? "");
}
