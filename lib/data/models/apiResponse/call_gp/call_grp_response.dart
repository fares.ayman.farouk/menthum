CallGpResponse callGpResponseFromJson(Map<String, dynamic> str) =>
    CallGpResponse.fromJson(str);

class CallGpResponse {
  CallGpResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  CallData? data;

  factory CallGpResponse.fromJson(Map<String, dynamic> json) => CallGpResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: CallData.fromJson(json["data"]),
      );
}

class CallData {
  CallData(
      {this.videoProvider, this.callId, this.token, this.room, this.coupon});

  String? videoProvider;
  int? callId;
  String? token;
  String? room;
  String? coupon;

  factory CallData.fromJson(Map<String, dynamic> json) => CallData(
        videoProvider: json["video_provider"],
        callId: int.parse(json["call_id"].toString()),
        token: json["token"],
        room: json["room"],
        coupon: json["coupon"],
      );

  Map<String, dynamic> toJson() => {
        "video_provider": videoProvider,
        "call_id": callId,
        "token": token,
        "room": room,
        "coupon": coupon,
      };
}
