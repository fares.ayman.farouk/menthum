
import 'dart:convert';

TermsPageApiResponse termsPageApiResponseFromJson(Map<String,dynamic> str) =>
    TermsPageApiResponse.fromJson(str);

String termsPageApiResponseToJson(TermsPageApiResponse data) => json.encode(data.toJson());

class TermsPageApiResponse {
  TermsPageApiResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  Data? data;

  factory TermsPageApiResponse.fromJson(Map<String, dynamic> json) => TermsPageApiResponse(
    status: json["status"],
    message: json["message"],
    code: json["code"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "code": code,
    "data": data!.toJson(),
  };
}

class Data {
  Data({
    this.title,
    this.type,
    this.keywords,
    this.description,
    this.html,
  });

  String? title;
  int? type;
  String? keywords;
  String? description;
  String? html;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    title: json["title"],
    type: json["type"],
    keywords: json["keywords"],
    description: json["description"],
    html: json["html"],
  );

  Map<String, dynamic> toJson() => {
    "title": title,
    "type": type,
    "keywords": keywords,
    "description": description,
    "html": html,
  };
}
