import 'package:patientapp/data/models/apiResponse/menstrual_cycle/menstrual_cycle_model.dart';

class MenstrualCycleList {
  MenstrualCycleList({
    required this.status,
    required this.message,
    required this.code,
    required this.data,
  });

  bool status;
  String message;
  int code;
  List<MenstrualCycleModel> data;

  factory MenstrualCycleList.fromJson(Map<String, dynamic> json) =>
      MenstrualCycleList(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: List<MenstrualCycleModel>.from(
            json["data"].map((x) => MenstrualCycleModel.fromJson(x))),
      );
}
