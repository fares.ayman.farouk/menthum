import '../../../../domain/models/menstrual_cycle/menstrual_cycle.dart';

class MenstrualCycleModel extends MenstrualCycle {
  MenstrualCycleModel({
    required super.id,
    required super.periodLength,
    required super.cycleLength,
    required super.currentDate,
    required super.endCycleDate,
    required super.bleedingNotes,
    required super.endPeriodDate,
    required super.startPeriodDate,
    required super.ovulationDate,
    required super.endPreOvulationDate,
    required super.startPreOvulationDate,
    required super.startPostOvulationDate,
    required super.endPostOvulationDate,
  });

  factory MenstrualCycleModel.fromJson(Map<String, dynamic> json) =>
      MenstrualCycleModel(
        id: json['id'],
        bleedingNotes: json['bleeding_notes'],
        startPeriodDate: DateTime.parse(json['start_at']),
        endCycleDate: DateTime.parse(json['end_at']),
        ovulationDate: DateTime.parse(json['ovulation']),
        startPreOvulationDate: DateTime.parse(json['preOvulation'][0]),
        endPreOvulationDate: DateTime.parse(json['preOvulation'][1]),
        startPostOvulationDate: DateTime.parse(json['postOvulation'][0]),
        endPostOvulationDate: DateTime.parse(json['postOvulation'][1]),
        endPeriodDate: DateTime.parse(json['bleeding_end_at']),
        cycleLength: DateTime.parse(json['end_at'])
            .difference(DateTime.parse(json['start_at']))
            .inDays
            .toDouble(),
        periodLength: DateTime.parse(json['bleeding_end_at'])
            .difference(DateTime.parse(json['start_at']))
            .inDays
            .toDouble(),
        currentDate: DateTime.now()
                        .compareTo(DateTime.parse(json['start_at'])) >=
                    0 &&
                DateTime.now().compareTo(DateTime.parse(json['end_at'])) <= 0
            ? DateTime(
                DateTime.now().year, DateTime.now().month, DateTime.now().day)
            : DateTime.parse(json['start_at']),
      );
}
