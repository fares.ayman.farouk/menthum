QuestionAnswersResponse resendOtpResponseFromJson(Map<String, dynamic> str) =>
    QuestionAnswersResponse.fromJson(str);

class QuestionAnswersResponse {
  QuestionAnswersResponse({this.status, this.message, this.code, this.data});

  bool? status;
  String? message;
  int? code;
  CallAnswersResponse? data;

  factory QuestionAnswersResponse.fromJson(Map<String, dynamic> json) =>
      QuestionAnswersResponse(
          status: json["status"],
          message: json["message"],
          code: json["code"],
          data: CallAnswersResponse.fromJson(json["data"]));
}

class CallAnswersResponse {
  String? videoProvider;
  int? callId;
  String? token;
  String? roomId;
  int? type;
  int? waitingNumber;
  String? waitingMessage;

  CallAnswersResponse(
      {this.videoProvider,
      this.callId,
      this.token,
      this.roomId,
      this.type,
      this.waitingNumber,
      this.waitingMessage});

  factory CallAnswersResponse.fromJson(Map<String, dynamic> json) =>
      CallAnswersResponse(
        videoProvider: json["video_provider"],
        callId: json["call_id"],
        token: json["token"],
        roomId: json["room"],
        type: json["type"],
        waitingNumber: json["waiting_number"],
        waitingMessage: json["message"],
      );
}
