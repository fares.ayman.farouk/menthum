
QuestionResponse questionResponseFromJson(Map<String, dynamic> str) =>
    QuestionResponse.fromJson(str);


class QuestionResponse {
  QuestionResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  List<QuestionResponseItem>? data;

  factory QuestionResponse.fromJson(Map<String, dynamic> json)=>
      QuestionResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: List<QuestionResponseItem>.from(
            json["data"].map((x) => QuestionResponseItem.fromJson(x))),
      );
}

class QuestionResponseItem {
  QuestionResponseItem({
    this.id,
    this.question,
    this.options,
    this.type,
    this.answer,
  });

  String? id;
  String? question;
  List<Option>? options;
  int? type;
  dynamic answer;

  factory QuestionResponseItem.fromJson(Map<String, dynamic> json) =>
      QuestionResponseItem(
        id: json["id"],
        question: json["question"],
        options:
            List<Option>.from(json["option"].map((x) => Option.fromJson(x))),
        type: json["type"],
        answer: json["answer"],
      );
}

class Option {
  Option({
    this.id,
    this.text,
  });

  int? id;
  String? text;

  factory Option.fromJson(Map<String, dynamic> json) => Option(
        id: json["id"],
        text: json["text"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "text": text,
      };
}
