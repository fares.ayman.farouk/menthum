import 'package:patientapp/data/models/apiResponse/specialties_response.dart';

import '../../../../domain/models/diabetes/next_visit.dart';

class NextVisitModel extends NextVisit {
  NextVisitModel({
    required super.id,
    required super.name,
    required super.date,
    required super.speciality,
  });

  factory NextVisitModel.fromJson(Map<String, dynamic> json) => NextVisitModel(
        id: json['patient_medical_event_id'],
        name: json['title'],
        date: json['date'],
        speciality: Speciality.fromJson(json['speciality']),
      );
}
