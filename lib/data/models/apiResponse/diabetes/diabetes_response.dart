import 'diabetes_data_model.dart';

class DiabetesResponse {
  bool? status;
  String? message;
  int? code;
  DiabetesDataModel? data;

  DiabetesResponse({
    this.status,
    this.code,
    this.message,
    this.data,
  });

  factory DiabetesResponse.fromJson(Map<String, dynamic> json) {
    return DiabetesResponse(
      status: json["status"],
      message: json["message"],
      code: json["code"],
      data: DiabetesDataModel.fromJson(json["data"]),
    );
  }
}
