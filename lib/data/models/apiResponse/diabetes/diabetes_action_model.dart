import 'package:patientapp/data/models/apiResponse/diabetes/reading_model.dart';

import '../../../../domain/models/diabetes/diabetes_action.dart';

class DiabetesActionModel extends DiabetesAction {
  DiabetesActionModel({
    required super.id,
    required super.name,
    required super.done,
    super.date,
    super.reading,
    super.value,
  });

  factory DiabetesActionModel.fromJson(Map<String, dynamic> json) =>
      DiabetesActionModel(
        id: json['patient_medical_event_id'],
        name: json['title'],
        done: json['done'],
        value: json['value'],
        date: json.containsKey('date') && json['date'] != null
            ? json['date']
            : null, 
        reading: json.containsKey('condition') && json['condition'] != null
            ? ReadingModel.fromJson(json['condition'])
            : null,
      );
}
