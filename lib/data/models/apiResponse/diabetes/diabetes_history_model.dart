import '../../../../domain/models/diabetes/diabetes_history.dart';
import 'diabetes_action_model.dart';

class DiabetesHistoryModel extends DiabetesHistory {
  DiabetesHistoryModel({
    required super.diabetesAction,
    required super.lastPage,
  });

  factory DiabetesHistoryModel.fromJson(Map<String, dynamic> json) =>
      DiabetesHistoryModel(
        diabetesAction: List<DiabetesActionModel>.from(
            json["events"].map((x) => DiabetesActionModel.fromJson(x))),
        lastPage: json['links']['last_page'],
      );
}
