import 'diabetes_history_model.dart';

class DiabetesHistoryResponse {
  bool? status;
  String? message;
  int? code;
  DiabetesHistoryModel? data;

  DiabetesHistoryResponse({
    this.status,
    this.code,
    this.message,
    this.data,
  });

  factory DiabetesHistoryResponse.fromJson(Map<String, dynamic> json) {
    return DiabetesHistoryResponse(
      status: json["status"],
      message: json["message"],
      code: json["code"],
      data: DiabetesHistoryModel.fromJson(json["data"]),
    );
  }
}
