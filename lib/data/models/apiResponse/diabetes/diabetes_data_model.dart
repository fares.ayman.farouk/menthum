import 'package:patientapp/domain/models/diabetes/diabetes_data.dart';

import 'diabetes_action_model.dart';
import 'next_visit_model.dart';
class DiabetesDataModel extends DiabetesData {
  DiabetesDataModel({required super.diabetesAction, required super.nextVisit});

  factory DiabetesDataModel.fromJson(Map<String, dynamic> json) =>
      DiabetesDataModel(
        diabetesAction: List<DiabetesActionModel>.from(
            json["events"].map((x) => DiabetesActionModel.fromJson(x))),
        nextVisit: NextVisitModel.fromJson(json['next_visit']),
      );
}
