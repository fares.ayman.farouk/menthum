import '../../../../domain/models/diabetes/reading.dart';

class ReadingModel extends Reading {
  ReadingModel(
      {required super.id,
      required super.name,
      required super.unit,
      required super.min,
      required super.max});

  factory ReadingModel.fromJson(Map<String, dynamic> json) => ReadingModel(
        id: json['id'],
        name: json['name'],
        unit: json['unit'],
        max: json['max'],
        min: json['min'],
      );
}
