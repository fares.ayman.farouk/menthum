
MyRecordResultsResponse labResultsResponseFromJson( Map<String, dynamic> str) =>
    MyRecordResultsResponse.fromJson(str);

class MyRecordResultsResponse {
  MyRecordResultsResponse({
    this.status,
    this.message,
    this.code,
    this.myRecordResultItems,
  });

  bool? status;
  String? message;
  int? code;
  List<FileResultItem>? myRecordResultItems;

  factory MyRecordResultsResponse.fromJson(Map<String, dynamic> json) =>
      MyRecordResultsResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        myRecordResultItems: List<FileResultItem>.from(json["data"].map((x) => FileResultItem.fromJson(x))),
      );
}

class FileResultItem {
  FileResultItem({
    this.name,
    this.url,
    this.createdAt,
  });

  String? name;
  String? url;
  int? createdAt;

  factory FileResultItem.fromJson(Map<String, dynamic> json) => FileResultItem(
        name: json["name"],
        url: json["url"],
        createdAt: json["created_at"],
      );
}
