import 'package:patientapp/data/models/apiResponse/clinic/clinic_response.dart';
import 'package:patientapp/data/models/apiResponse/tests_response.dart';

import 'dart:convert';

LaboratoryResponse laboratoryResponseFromJson(Map<String, dynamic> data) =>
    LaboratoryResponse.fromJson(data);

String laboratoryResponseToJson(LaboratoryResponse data) =>
    json.encode(data.toJson());

class LaboratoryResponse {
  LaboratoryResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  List<LaboratoryItem>? data;

  factory LaboratoryResponse.fromJson(Map<String, dynamic> json) =>
      LaboratoryResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: List<LaboratoryItem>.from(
            json["data"].map((x) => LaboratoryItem.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "code": code,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class LaboratoryItem {
  LaboratoryItem({
    this.id,
    this.name,
    this.description,
    this.logo,
    this.website,
    this.phone,
    this.inHome,
    this.availableTests,
    this.notAvailableTests,
    this.totalCost,
    this.time,
    this.laboratoryBranches,
  });

  String? id;
  String? name;
  String? description;
  String? logo;
  String? website;
  String? phone;
  int? inHome;
  List<Test>? availableTests;
  List<Test>? notAvailableTests;
  int? totalCost;
  int? time;
  List<LaboratoryBranch>? laboratoryBranches;

  factory LaboratoryItem.fromJson(Map<String, dynamic> json) {
    return LaboratoryItem(
      id: json["id"],
      name: json["name"],
      description: json["description"],
      logo: json["logo"],
      website: json["website"],
      phone: json["phone"],
      inHome: json["in_home"],
      availableTests:
          List<Test>.from(json["availableTests"].map((x) => Test.fromJson(x))),
      notAvailableTests: List<Test>.from(
          json["notAvailableTests"].map((x) => Test.fromJson(x))),
      totalCost: json["total_cost"],
      time: json["time"],
      laboratoryBranches: List<LaboratoryBranch>.from(
          json["laboratory_branches"].map((x) => LaboratoryBranch.fromJson(x))),
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "logo": logo,
        "website": website,
        "phone": phone,
        "in_home": inHome,
        "availableTests": List<Test>.from(availableTests!.map((x) => x)),
        "notAvailableTests": List<Test>.from(notAvailableTests!.map((x) => x)),
        "total_cost": totalCost,
        "time": time,
        "laboratory_branches":
            List<dynamic>.from(laboratoryBranches!.map((x) => x.toJson())),
      };
}

class LaboratoryBranch {
  LaboratoryBranch({
    this.id,
    this.name,
    this.address,
    this.logo,
    this.workingShifts,
  });

  String? id;
  String? name;
  String? address;
  String? logo;
  List<WorkingShift>? workingShifts;

  factory LaboratoryBranch.fromJson(Map<String, dynamic> json) =>
      LaboratoryBranch(
        id: json["id"],
        name: json["name"],
        address: json["address"],
        logo: json["logo"],
        workingShifts: List<WorkingShift>.from(
            json["working_shifts"].map((x) => WorkingShift.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "address": address,
        "logo": logo,
        "working_shifts":
            List<dynamic>.from(workingShifts!.map((x) => x.toJson())),
      };
}
