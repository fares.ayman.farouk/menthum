import 'package:patientapp/data/models/apiResponse/laboratory/laboratory_response.dart';

LabProfileResponse labProfileResponseFromJson(Map<String, dynamic> data) =>
    LabProfileResponse.fromJson(data);

class LabProfileResponse {
  LabProfileResponse({
    this.status,
    this.message,
    this.code,
    required this.laboratoryItem,
  });

  bool? status;
  String? message;
  int? code;
  LaboratoryItem laboratoryItem;

  factory LabProfileResponse.fromJson(Map<String, dynamic> json) =>
      LabProfileResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        laboratoryItem: LaboratoryItem.fromJson(json["data"]),
      );
}
