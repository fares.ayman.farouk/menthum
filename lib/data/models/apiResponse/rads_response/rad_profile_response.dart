import 'package:patientapp/data/models/apiResponse/rads_response/rads_response.dart';

RadProfileResponse radProfileResponseFromJson(Map<String, dynamic> data) =>
    RadProfileResponse.fromJson(data);

class RadProfileResponse {
  RadProfileResponse({
    this.status,
    this.message,
    this.code,
    required this.radioloyBranch,
  });

  bool? status;
  String? message;
  int? code;
  RadiologyBranch radioloyBranch;

  factory RadProfileResponse.fromJson(Map<String, dynamic> json) =>
      RadProfileResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        radioloyBranch: RadiologyBranch.fromJson(json["data"]),
      );
}
