
import 'dart:convert';

import 'package:patientapp/data/models/apiResponse/tests_response.dart';

RadsResponse radsResponseFromJson(Map<String, dynamic> data) =>
    RadsResponse.fromJson(data);

String radsResponseToJson(RadsResponse data) => json.encode(data.toJson());

class RadsResponse {
  RadsResponse({
    this.status,
    this.message,
    this.code,
    this.radioloyBranchs,
  });

  bool? status;
  String? message;
  int? code;
  List<RadiologyBranch>? radioloyBranchs;

  factory RadsResponse.fromJson(Map<String, dynamic> json) => RadsResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        radioloyBranchs: List<RadiologyBranch>.from(
            json["data"].map((x) => RadiologyBranch.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "code": code,
        "data": List<dynamic>.from(radioloyBranchs!.map((x) => x.toJson())),
      };
}

class RadiologyBranch {
  RadiologyBranch({
    this.id,
    this.name,
    this.logo,
    this.address,
    this.availableTests,
    this.notAvailableTests,
    this.totalCost,
    this.time,
    this.radiology,
  });

  String? id;
  String? name;
  String? logo;
  String? address;
  List<Test>? availableTests;
  List<Test>? notAvailableTests;
  int? totalCost;
  int? time;
  Radiology? radiology;

  factory RadiologyBranch.fromJson(Map<String, dynamic> json) => RadiologyBranch(
        id: json["id"],
        name: json["name"],
        logo: json["logo"],
        address: json["address"],
        availableTests: List<Test>.from(
            json["availableTests"].map((x) => Test.fromJson(x))),
        notAvailableTests: List<Test>.from(
            json["notAvailableTests"].map((x) => Test.fromJson(x))),
        totalCost: json["total_cost"],
        time: json["time"],
        radiology: Radiology.fromJson(json["radiology"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "logo": logo,
        "address": address,
        "availableTests":
            List<dynamic>.from(availableTests!.map((x) => x.toJson())),
        "notAvailableTests":
            List<dynamic>.from(notAvailableTests!.map((x) => x.toJson())),
        "total_cost": totalCost,
        "time": time,
        "radiology": radiology!.toJson(),
      };
}

class Radiology {
  Radiology({
    this.id,
    this.name,
    this.description,
    this.website,
    this.phone,
  });

  String? id;
  String? name;
  String? description;
  String? website;
  dynamic? phone;

  factory Radiology.fromJson(Map<String, dynamic> json) => Radiology(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        website: json["website"],
        phone: json["phone"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "website": website,
        "phone": phone,
      };
}

