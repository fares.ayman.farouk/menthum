import 'dart:convert';

import 'package:patientapp/data/models/apiResponse/auth_response/login_api_response.dart';

AccountNotVerifiedResponse accountNotVerifiedResponseFromJson(String str) =>
    AccountNotVerifiedResponse.fromJson(json.decode(str));

String accountNotVerifiedResponseToJson(AccountNotVerifiedResponse data) =>
    json.encode(data.toJson());

class AccountNotVerifiedResponse {
  AccountNotVerifiedResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  UserData? data;

  factory AccountNotVerifiedResponse.fromJson(Map<String, dynamic> json) =>
      AccountNotVerifiedResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: UserData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "code": code,
        "data": data?.toJson(),
      };
}
