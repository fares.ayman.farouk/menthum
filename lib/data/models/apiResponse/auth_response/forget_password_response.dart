import 'dart:convert';

ForgetPasswordResponse forgetPasswordResponseFromJson(Map<String, dynamic> str) => ForgetPasswordResponse.fromJson(str);

String resendOtpResponseToJson(ForgetPasswordResponse data) =>
    json.encode(data.toJson());

class ForgetPasswordResponse {
  ForgetPasswordResponse({this.status, this.message, this.code, this.data});

  bool? status;
  String? message;
  int? code;
  dynamic? data;

  factory ForgetPasswordResponse.fromJson(Map<String, dynamic> json) =>
      ForgetPasswordResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "code": code,
      };
}
