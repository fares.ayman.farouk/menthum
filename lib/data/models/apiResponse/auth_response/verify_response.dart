import 'dart:convert';

import 'package:patientapp/data/models/apiResponse/auth_response/login_api_response.dart';

VerifyResponse verifyResponseFromJson(Map<String, dynamic> data) =>
    VerifyResponse.fromJson(data);

String verifyResponseToJson(VerifyResponse data) => json.encode(data.toJson());

class VerifyResponse {
  VerifyResponse({
    required this.status,
    required this.message,
    required this.code,
    required this.data,
  });

  bool status;
  String message;
  int code;
  UserData data;

  factory VerifyResponse.fromJson(Map<String, dynamic> json) {
    return VerifyResponse(
      status: json["status"],
      message: json["message"],
      code: json["code"],
      data: UserData.fromJson(json["data"]),
    );
  }

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "code": code,
        "data": data.toJson(),
      };
}

