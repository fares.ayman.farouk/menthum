// To parse this JSON data, do
//
//     final resendOtpResponse = resendOtpResponseFromJson(jsonString);

import 'dart:convert';

ResendOtpResponse resendOtpResponseFromJson(Map<String, dynamic> str) =>
    ResendOtpResponse.fromJson(str);

String resendOtpResponseToJson(ResendOtpResponse data) =>
    json.encode(data.toJson());

class ResendOtpResponse {
  ResendOtpResponse({this.status, this.message, this.code, this.data});

  bool? status;
  String? message;
  int? code;
  dynamic? data;

  factory ResendOtpResponse.fromJson(Map<String, dynamic> json) =>
      ResendOtpResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "code": code,
      };
}
