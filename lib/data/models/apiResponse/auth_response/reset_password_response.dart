import 'dart:convert';

import 'login_api_response.dart';

ResetPasswordResponse resetPasswordResponseFromJson(
        Map<String, dynamic> data) =>
    ResetPasswordResponse.fromJson(data);

class ResetPasswordResponse {
  ResetPasswordResponse({
    required this.status,
    required this.message,
    required this.code,
    this.data,
  });

  bool status;
  String message;
  int code;
  UserData? data;

  factory ResetPasswordResponse.fromJson(Map<String, dynamic> json) =>
      ResetPasswordResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: UserData.fromJson(json["data"]),
      );
}

