import 'dart:convert';

LoginResponse loginResponseFromJson(Map<String, dynamic> data) =>
    LoginResponse.fromJson(data);

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    required this.status,
    required this.message,
    required this.code,
    required this.data,
  });

  bool status;
  String message;
  int code;
  UserData? data;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: UserData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "code": code,
        "data": data?.toJson(),
      };
}

class UserData {
  UserData(
      {this.id,
      this.fullname,
      this.email,
      this.token,
      this.mobile,
      this.mobileVerifiedAt,
      this.wallet,
      this.countryCode,
      this.phone,
      this.avatar,
      this.hasPassword,
      this.maritalStatus,
      this.isMobileVerified,
      this.gender,
      this.isTester,
      this.subscriptions = const [],
      this.birthOfDate,
      this.deviceToken});

  String? id;
  String? fullname;
  String? email;
  dynamic? gender;
  dynamic birthOfDate;
  int? maritalStatus;
  String? token;
  String? mobile;
  String? mobileVerifiedAt;
  String? avatar;
  int? wallet;
  String? countryCode;
  String? phone;
  String? deviceToken;
  bool? hasPassword;
  bool? isMobileVerified;

  bool? isTester;
  List<Subscription> subscriptions;

  factory UserData.fromJson(Map<String, dynamic> json) {
    try {
      return UserData(
        id: json.containsKey("id") ? json["id"] : "",
        fullname: json.containsKey("fullname") ? json["fullname"] : "",
        email: json.containsKey("email") ? json["email"] : "",
        token: json.containsKey("token") ? json["token"] : "",
        mobile: json.containsKey("mobile") ? json["mobile"] : "",
        mobileVerifiedAt: json.containsKey("mobile_verified_at")
            ? json["mobile_verified_at"]
            : "",
        wallet: json.containsKey("wallet") ? json["wallet"] : "",
        countryCode:
            json.containsKey("country_code") ? json["country_code"] : "",
        phone: json.containsKey("phone") ? json["phone"] : "",
        avatar: json.containsKey("avatar") ? json["avatar"] : "",
        hasPassword:
            json.containsKey("has_password") ? json["has_password"] : true,
        isMobileVerified: json.containsKey("is_mobile_verified")
            ? json["is_mobile_verified"]
            : true,
        deviceToken:
            json.containsKey("device_token") ? json["device_token"] : "",
        birthOfDate: json.containsKey("birthdate") ? json["birthdate"] : 0,
        gender: json.containsKey("gender") ? json["gender"] : 0,
        maritalStatus:
            json.containsKey("marital_status") ? json["marital_status"] : 0,
        isTester: json.containsKey("testing") ? json["testing"] : false,
        subscriptions: json.containsKey("subscriptions")? List<Subscription>.from(
            json["subscriptions"].map((x) => Subscription.fromJson(x))):[],
      );
    } catch (e) {
      print("EEEEEEEEEEEEEEEEEEE${e.toString()}");
      throw (e);
    }
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "fullname": fullname,
        "email": email,
        "token": token,
        "mobile": mobile,
        "mobile_verified_at": mobileVerifiedAt,
        "wallet": wallet,
        "country_code": countryCode,
        "phone": phone,
        "avatar": avatar,
        "has_password": hasPassword,
        "birthdate": birthOfDate,
        "gender": gender,
        "marital_status": maritalStatus,
        "is_mobile_verified": isMobileVerified,
        "subscriptions":
            List<dynamic>.from(subscriptions.map((x) => x.toJson())),
      };
}

class Subscription {
  Subscription({
    required this.id,
    required this.startAt,
    required this.endAt,
    required this.renewsAt,
    required this.plan,
  });

  int id;
  int startAt;
  int endAt;
  int renewsAt;
  Plan plan;

  factory Subscription.fromJson(Map<String, dynamic> json) => Subscription(
        id: json["id"],
        startAt: json["start_at"],
        endAt: json["end_at"],
        renewsAt: json["renews_at"],
        plan: Plan.fromJson(json["plan"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "start_at": startAt,
        "end_at": endAt,
        "renews_at": renewsAt,
        "plan": plan.toJson(),
      };
}

class Plan {
  Plan({
    required this.id,
    required this.name,
    required this.description,
    required this.price,
    required this.callsCount,
    required this.interval,
    required this.intervalCount,
  });

  int id;
  String name;
  String description;
  int price;
  int callsCount;
  int interval;
  int intervalCount;

  factory Plan.fromJson(Map<String, dynamic> json) => Plan(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        price: json["price"],
        callsCount: json["calls_count"],
        interval: json["interval"],
        intervalCount: json["interval_count"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "price": price,
        "calls_count": callsCount,
        "interval": interval,
        "interval_count": intervalCount,
      };
}
