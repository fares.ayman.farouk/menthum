GPWorkingShiftResponse getGPWorkingShiftResponse(Map<String, dynamic> str) =>
    GPWorkingShiftResponse.fromJson(str);

class GPWorkingShiftResponse {
  bool? status;
  String? message;
  int? code;
  List<GPWorkingShifts>? gpWorkingShifts;

  GPWorkingShiftResponse(
      {this.status, this.code, this.message, this.gpWorkingShifts});

  factory GPWorkingShiftResponse.fromJson(Map<String, dynamic> json) =>
      GPWorkingShiftResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        gpWorkingShifts: List<GPWorkingShifts>.from(
            json["data"].map((x) => GPWorkingShifts.fromJson(x))),
      );

  factory GPWorkingShiftResponse.fromJsonForSharedPref(List<dynamic> data) =>
      GPWorkingShiftResponse(
        gpWorkingShifts: List<GPWorkingShifts>.from(
            data.map((x) => GPWorkingShifts.fromJson(x))),
      );
}

class GPWorkingShifts {
  final String day;
  final int from, to;

  GPWorkingShifts({required this.day, required this.from, required this.to});

  factory GPWorkingShifts.fromJson(Map<String, dynamic> json) =>
      GPWorkingShifts(day: json['day'], from: json['from'], to: json['to']);

  Map<String, dynamic> toJson() => {
        "day": day,
        "from": from,
        "to": to,
      };
}
