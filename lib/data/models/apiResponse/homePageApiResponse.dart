import 'dart:convert';

HomePageApiResponse homePageApiResponseFromJson(Map<String, dynamic> str) =>
    HomePageApiResponse.fromJson(str);

String homePageApiResponseToJson(HomePageApiResponse data) =>
    json.encode(data.toJson());

class HomePageApiResponse {
  HomePageApiResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  List<DataListItem>? data;

  factory HomePageApiResponse.fromJson(Map<String, dynamic> json) =>
      HomePageApiResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: List<DataListItem>.from(
            json["data"].map((x) => DataListItem.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "code": code,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class DataListItem {
  DataListItem({
    this.name,
    this.value,
    this.extra,
    this.callCost,
  });

  String? name;
  String? value;
  int? callCost;
  Extra? extra;

  factory DataListItem.fromJson(Map<String, dynamic> json) => DataListItem(
        name: json["name"],
        value: json["value"],
        extra: Extra.fromJson(json["extra"]),
        callCost: json["call_cost"] != null
            ? int.parse(json["call_cost"].toString())
            : 0,
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "value": value,
        "extra": extra?.toJson(),
      };
}

class Extra {
  String? type;
  String? callToAction;
  String? phone;
  String? coupon;
  int? planId;

  Extra(
      {this.type, this.callToAction, this.phone, this.coupon, this.planId});

  factory Extra.fromJson(Map<String, dynamic> json) => Extra(
        type: json["type"],
        callToAction:
            json.containsKey("call_to_action") ? json["call_to_action"] : null,
        phone: json.containsKey("phone") ? json["phone"] : null,
        coupon: json.containsKey("coupon") ? json["coupon"] : null,
    planId:
            json.containsKey("plan_id") ? json["plan_id"] : null,
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "call_to_action": callToAction,
        "phone": phone,
        "coupon": coupon,
      };
}
