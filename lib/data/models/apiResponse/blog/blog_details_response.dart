import '../../../../domain/models/blog_details_entity.dart';

BlogDetailsResponse blogDetailsResponseFromJson(Map<String, dynamic> str) =>
    BlogDetailsResponse.fromJson(str);

class BlogDetailsResponse {
  BlogDetailsResponse({
    this.status,
    this.message,
    this.code,
    this.blogData,
  });

  bool? status;
  String? message;
  int? code;
  BlogDetailsItem? blogData;

  factory BlogDetailsResponse.fromJson(Map<String, dynamic> json) =>
      BlogDetailsResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        blogData: BlogDetailsItem.fromJson(json["data"]),
      );
}

class BlogDetailsItem extends BlogDetailsEntity {
  BlogDetailsItem({
    required super.title,
    required super.image,
    required super.body,
    required super.createdAt,
  });

  factory BlogDetailsItem.fromJson(Map<String, dynamic> json) =>
      BlogDetailsItem(
          title: json['title'] ?? "",
          image: json['image'] ?? "",
          body: json['body'] ?? "",
          createdAt: json['created_at'] ?? 0);
}
