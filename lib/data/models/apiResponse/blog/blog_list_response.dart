import '../../../../domain/models/blog_entity.dart';

BlogListResponse blogsListResponseFromJson(Map<String, dynamic> str) =>
    BlogListResponse.fromJson(str);

class BlogListResponse {
  BlogListResponse({
    this.status,
    this.message,
    this.code,
    this.blogItems,
  });

  bool? status;
  String? message;
  int? code;
  List<BlogItem>? blogItems;

  factory BlogListResponse.fromJson(Map<String, dynamic> json) =>
      BlogListResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        blogItems:
            List<BlogItem>.from(json["data"].map((x) => BlogItem.fromJson(x))),
      );
}

class BlogItem extends BlogEntity {
  BlogItem(
      {required super.id,
      required super.title,
      required super.image,
      required super.description,
      required super.slug,
      required super.createdAt});

  factory BlogItem.fromJson(Map<String, dynamic> json) => BlogItem(
      id: json['id'],
      title: json['title'] ?? "",
      image: json['image'] ?? "",
      description: json['description'] ?? "",
      slug: json['slug'] ?? "",
      createdAt: json['created_at'] ?? 0);
}
