import 'package:patientapp/domain/models/vitals/response/vital_entity.dart';

import '../../../../domain/models/vitals/response/reading_condition_entity.dart';

VitalResponse vitalsResponseFromJson(Map<String, dynamic> data) =>
    VitalResponse.fromJson(data);

class VitalResponse {
  bool status;
  String message;
  int code;
  List<VitalModel> vitals;

  VitalResponse({
    required this.status,
    required this.message,
    required this.code,
    required this.vitals,
  });

  factory VitalResponse.fromJson(Map<String, dynamic> json) => VitalResponse(
        status: json['status'],
        message: json['message'],
        code: json['code'],
        vitals: List<VitalModel>.from(
            json["data"].map((x) => VitalModel.fromJson(x))),
      );
}

class VitalModel extends VitalEntity {
  VitalModel({
    required super.id,
    required super.title,
    required super.value,
    required super.canCall,
    required super.unit,
    required super.readingAt,
    required super.min,
    required super.max,
    required super.coupon,
    required super.readingConditionEntity,
  });

  factory VitalModel.fromJson(Map<String, dynamic> json) => VitalModel(
      id: json['id'],
      title: json['name'] ?? "",
      unit: json['unit'] ?? "",
      value: json['value'] ?? "",
      canCall: json['has_call'] ?? false,
      readingAt: json['reading_at'] ?? 0,
      min: json['min'] ?? "",
      max: json['max'] ?? "",
      coupon: json['coupon'],
      readingConditionEntity: List<ReadingConditionModel>.from(
        json["readingConditions"].map((x) => ReadingConditionModel.fromJson(x)),
      ));

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": title,
        "reading_at": readingAt,
        "value": value,
      };

  factory VitalModel.obj(VitalEntity vital) => VitalModel(
        id: vital.id,
        title: vital.title,
        unit: vital.unit,
        value: vital.value,
        canCall: vital.canCall,
        readingAt: vital.readingAt,
        min: vital.min,
        max: vital.max,
        coupon: vital.coupon,
        readingConditionEntity: vital.readingConditionEntity,
      );
}

class ReadingConditionModel extends ReadingConditionEntity {
  ReadingConditionModel({required super.id, required super.name});

  factory ReadingConditionModel.fromJson(Map<String, dynamic> json) =>
      ReadingConditionModel(id: json['id'], name: json['name']);

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };
}
