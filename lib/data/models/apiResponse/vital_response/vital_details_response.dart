import 'package:patientapp/domain/models/vitals/response/vital_details_entity.dart';

VitalDetailsResponse vitalDetailsResponseFromJson(Map<String, dynamic> data) =>
    VitalDetailsResponse.fromJson(data);

class VitalDetailsResponse {
  bool status;
  String message;
  int code;
  List<VitalDetailsModel> vitalDetailsModel;

  VitalDetailsResponse({
    required this.status,
    required this.message,
    required this.code,
    required this.vitalDetailsModel,
  });

  factory VitalDetailsResponse.fromJson(Map<String, dynamic> json) =>
      VitalDetailsResponse(
        status: json['status'],
        message: json['message'],
        code: json['code'],
        vitalDetailsModel: List<VitalDetailsModel>.from(
            json["data"].map((x) => VitalDetailsModel.fromJson(x))),
      );
}

class VitalDetailsModel extends VitalDetailsEntity {
  VitalDetailsModel({
    required super.name,
    required super.value,
    required super.hasCall,
    required super.unit,
    required super.readingAt,
    required super.humanMinimum,
    required super.humanMaximum,
    required super.coupon,
    required super.createdBy,
  });

  factory VitalDetailsModel.fromJson(Map<String, dynamic> json) =>
      VitalDetailsModel(
        name: json['name'] ?? "",
        unit: json['unit'] ?? "",
        value: json['value'] ?? "",
        hasCall: json['has_call'] ?? false,
        readingAt: json['reading_at'] ?? 0,
        humanMaximum: json['human_minimum'] ?? "",
        humanMinimum: json['human_maximum'] ?? "",
        createdBy: json["created_by"] ?? "",
        coupon: json['coupon'],
      );
}
