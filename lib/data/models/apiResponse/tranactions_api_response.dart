
import 'dart:convert';

TransactionApiResponse transactionResponseFromJson(Map<String, dynamic>  str) =>
    TransactionApiResponse.fromJson(str);


String transactionResponseToJson(TransactionApiResponse data) => json.encode(data.toJson());

class TransactionApiResponse {
  TransactionApiResponse({
    this.status,
    this.message,
    this.code,
    this.data,
    this.meta,
  });

  bool? status;
  String? message;
  int? code;
  List<Transaction>? data;
  Meta? meta;

  factory TransactionApiResponse.fromJson(Map<String, dynamic> json) => TransactionApiResponse(
    status: json["status"],
    message: json["message"],
    code: json["code"],
    data: List<Transaction>.from(json["data"].map((x) => Transaction.fromJson(x))),
    meta: Meta.fromJson(json["meta"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "code": code,
    "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    "meta": meta?.toJson(),
  };
}

class Transaction {
  Transaction({
    this.amount,
    this.paymentMethod,
    this.type,
    this.date,
  });

  String? amount;
  int? paymentMethod;
  int? type;
  int? date;

  factory Transaction.fromJson(Map<String, dynamic> json) => Transaction(
    amount: json["amount"].toString(),
    paymentMethod: json["payment_method"],
    type: json["type"],
    date: json["date"],
  );

  Map<String, dynamic> toJson() => {
    "amount": amount,
    "payment_method": paymentMethod,
    "type": type,
    "date": date,
  };
}

class Meta {
  Meta({
    this.currentPage,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.nextPageUrl,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  int? currentPage;
  int? lastPage;
  int? perPage;
  int? from;
  int? to;
  int? total;
  String? firstPageUrl;
  String? lastPageUrl;
  String? nextPageUrl;
  String? prevPageUrl;


  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
    currentPage: json["current_page"],
    firstPageUrl: json["first_page_url"],
    from: json["from"],
    lastPage: json["last_page"],
    lastPageUrl: json["last_page_url"],
    nextPageUrl: json["next_page_url"],
    perPage: json["per_page"],
    prevPageUrl: json["prev_page_url"],
    to: json["to"],
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "current_page": currentPage,
    "first_page_url": firstPageUrl,
    "from": from,
    "last_page": lastPage,
    "last_page_url": lastPageUrl,
    "next_page_url": nextPageUrl,
    "per_page": perPage,
    "prev_page_url": prevPageUrl,
    "to": to,
    "total": total,
  };
}

