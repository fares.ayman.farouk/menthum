import 'package:patientapp/data/models/apiResponse/clinic/clinic_response.dart';

NanoClinicResponse nanoClinicResponseFromJson(Map<String, dynamic> data) =>
    NanoClinicResponse.fromJson(data);

class NanoClinicResponse {
  NanoClinicResponse({
    required this.status,
    required this.message,
    required this.code,
    required this.nanoClinics,
  });

  bool status;
  String message;
  int code;
  List<NanoClinic> nanoClinics;

  factory NanoClinicResponse.fromJson(Map<String, dynamic> json) =>
      NanoClinicResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        nanoClinics: List<NanoClinic>.from(
            json["data"].map((x) => NanoClinic.fromJson(x))),
      );
}

class NanoClinic {
  NanoClinic({
    required this.id,
    required this.name,
    required this.announcement,
    required this.lat,
    required this.lng,
    required this.address,
    required this.phone,
    this.distance,
    required this.workingShifts,
  });

  String id;
  String name;
  String announcement;
  double lat;
  double lng;
  String address;
  String phone;
  String? distance;
  List<WorkingShift> workingShifts;

  factory NanoClinic.fromJson(Map<String, dynamic> json) => NanoClinic(
        id: json["id"],
        name: json["name"],
        announcement: json["announcement"],
        lat: double.parse(json["lat"].toString()),
        lng: double.parse(json["lng"].toString()),
        address: json["address"],
        phone: json["phone"],
        workingShifts: List<WorkingShift>.from(
            json["working_shifts"].map((x) => WorkingShift.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "announcement": announcement,
        "lat": lat,
        "lng": lng,
        "address": address,
        "phone": phone,
        "working_shifts":
            List<dynamic>.from(workingShifts.map((x) => x.toJson())),
      };
}
