import 'dart:convert';

CheckCouponApiResponse checkCouponApiResponseFromJson(
        Map<String, dynamic> str) =>
    CheckCouponApiResponse.fromJson(str);

String checkCouponApiResponseToJson(CheckCouponApiResponse data) =>
    json.encode(data.toJson());

class CheckCouponApiResponse {
  CheckCouponApiResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  DataItem? data;

  factory CheckCouponApiResponse.fromJson(Map<String, dynamic> json) =>
      CheckCouponApiResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: DataItem.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "code": code,
        "data": data!.toJson(),
      };
}

class DataItem {
  DataItem({
    this.isValid,
    this.message,
    this.codeCost
  });

  bool? isValid;
  String? message;
  int? codeCost;

  factory DataItem.fromJson(Map<String, dynamic> json) => DataItem(
      isValid: json["is_valid"],
      message: json["message"],
      codeCost: json["code_cost"]
  );

  Map<String, dynamic> toJson() =>
      {"is_valid": isValid, "message": message, "code_cost": codeCost};
}
