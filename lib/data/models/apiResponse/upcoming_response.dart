import 'dart:convert';

import 'package:patientapp/application/utils.dart';
import 'package:patientapp/data/models/apiResponse/clinic/clinic_response.dart';
import 'package:patientapp/data/models/apiResponse/tests_response.dart';

UpcomingAppointmentsResponse upcomingResultsFromJson(
        Map<String, dynamic> data) =>
    UpcomingAppointmentsResponse.fromJson(data);

String upcomingResultsToJson(UpcomingAppointmentsResponse data) =>
    json.encode(data.toJson());

class UpcomingAppointmentsResponse {
  UpcomingAppointmentsResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  List<AppointmentItem>? data;

  factory UpcomingAppointmentsResponse.fromJson(Map<String, dynamic> json) =>
      UpcomingAppointmentsResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: List<AppointmentItem>.from(
            json["data"].map((x) => AppointmentItem.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "code": code,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class AppointmentItem {
  AppointmentItem(
      {this.id,
      this.name,
      this.address,
      this.type,
      this.tests,
      this.specialty,
      this.logo,
      this.inHome,
      this.timeFrom,
      this.lat,
      this.lng,
      this.timeTo,
      this.date,
      this.time,
      this.totalCost,
      this.totalTime,
      this.phone,
      this.status,
      this.statusId});

  String? id;
  String? name;
  String? phone;
  int? type;
  String? address;
  List<Test>? tests;
  Specialty? specialty;
  String? logo;
  int? inHome;
  double? lat;
  double? lng;
  String? date;
  int? timeFrom;
  int? timeTo;
  String? time;
  String? status;
  String? statusId;
  int? totalCost;
  int? totalTime;

  factory AppointmentItem.fromJson(Map<String, dynamic> json) {
    //add the date and time if exists
    try {
      String dateFromTimestamp = "", timeFromTimestamp = "";
      if (json["time_from"] != null) {
        dateFromTimestamp = timeStampToDateEDMY(json["time_from"]);
      }

      if (json["time_at"] != null && (json['specialty'] == 1 || json['in_home'] == 1)) {
        timeFromTimestamp = timeStampToTimeWorkingShifts(json["time_at"]);
      } else {
        if (json["time_from"] != null && json["time_to"] != null) {
          timeFromTimestamp =
              timeStampToTime(json["time_from"], json["time_to"]);
        }
      }
      return AppointmentItem(
        id: json["id"],
        name: json["name"],
        address: json["address"] == null ? null : json["address"],
        type: json["type"],
        lat: json["lat"] == null ? null : double.parse(json["lat"].toString()),
        lng: json["lng"] == null ? null : double.parse(json["lng"].toString()),
        tests: json["items"] == null
            ? null
            : List<Test>.from(json["items"].map((x) => Test.fromJson(x))),
        specialty: json["specialty"] == null
            ? null
            : Specialty.fromJson(json["specialty"]),
        logo: json["logo"],
        status: json["status"],
        statusId: json["status_id"].toString(),
        phone: json["phone"],
        inHome: json["in_home"],
        totalCost: json["cost"],
        totalTime: json["time"],
        timeFrom: json["time_from"],
        timeTo: json["time_to"],
        date: dateFromTimestamp,
        time: timeFromTimestamp,
      );
    } catch (e) {
      print("Appointments $e");
      return AppointmentItem();
    }
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "status_id": statusId,
        "address": address == null ? null : address,
        "type": type,
        "tests": tests == null
            ? null
            : List<dynamic>.from(tests!.map((x) => x.toJson())),
        "lat": lat == null ? null : lat,
        "lng": lng == null ? null : lng,
        "specialty": specialty == null ? null : specialty?.toJson(),
        "logo": logo,
        "in_home": inHome,
        "time_from": timeFrom == null ? null : timeFrom,
        "time_to": timeTo == null ? null : timeTo,
        "status": status
      };
}
