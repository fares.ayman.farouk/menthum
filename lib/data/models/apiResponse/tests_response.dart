import 'dart:convert';

TestsResponse testsResponseFromJson(Map<String, dynamic> data) =>
    TestsResponse.fromJson(data);

String testsResponseToJson(TestsResponse data) => json.encode(data.toJson());

class TestsResponse {
  TestsResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  List<Test>? data;

  factory TestsResponse.fromJson(Map<String, dynamic> json) => TestsResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: List<Test>.from(json["data"].map((x) => Test.fromJson(x))),
      );

  factory TestsResponse.fromJsonForSharedPref(List<dynamic> data) =>
      TestsResponse(
        data: List<Test>.from(data.map((x) => Test.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "code": code,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Test {
  Test({
    this.id,
    this.name,
    this.cost,
    this.time,
  });

  String? id;
  String? name;
  dynamic? cost;
  dynamic? time;

  factory Test.fromJson(Map<String, dynamic> json) => Test(
        id: json["id"],
        name: json["name"],
        cost: json["cost"],
        time: json["time"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "cost": cost,
        "time": time,
      };
}
