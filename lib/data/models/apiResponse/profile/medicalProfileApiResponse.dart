// To parse this JSON data, do
//
//     final medicalProfileApiResponse = medicalProfileApiResponseFromJson(jsonString);

import 'dart:convert';

MedicalProfileApiResponse medicalProfileApiResponseFromJson(
    Map<String, dynamic> str) =>
    MedicalProfileApiResponse.fromJson(str);

String medicalProfileApiResponseToJson(MedicalProfileApiResponse data) =>
    json.encode(data.toJson());

class MedicalProfileApiResponse {
  MedicalProfileApiResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  Data? data;

  factory MedicalProfileApiResponse.fromJson(Map<String, dynamic> json) =>
      MedicalProfileApiResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() =>
      {
        "status": status,
        "message": message,
        "code": code,
        "data": data!.toJson(),
      };
}

class Data {
  Data({
    this.medicalConditions,
    this.bloodType,
    this.weight,
    this.height,
    this.immunization,
    this.food_allergies,
    this.medication_allergies,
    this.chronic_medical_conditions,
    this.chronic_medication,
    this.operations_hospitalization,
    this.family_history_of_diseases,
    this.last_check_up,
    this.social_habits,
    this.exercise_habits,
    this.diet,
    this.general_health,
    this.emergency_contact,
    this.occupation,
    this.report
  });

  String? medicalConditions;
  int? bloodType;
  String? weight;
  String? height;
  String? immunization;
  String? food_allergies;
  String? medication_allergies;
  String? chronic_medical_conditions;
  String? chronic_medication;
  String? operations_hospitalization;
  String? family_history_of_diseases;
  String? last_check_up;
  String? social_habits;
  String? exercise_habits;
  String? diet;
  String? general_health;
  String? emergency_contact;
  String? occupation;
  String? report;

  factory Data.fromJson(Map<String, dynamic> json) =>
      Data(
          medicalConditions: json["medical_conditions"],
          bloodType: json["blood_type"],
          weight: json["weight"],
          height: json["height"],
          immunization: json["immunization"],
          food_allergies: json["food_allergies"],
          medication_allergies: json["medication_allergies"],
          chronic_medical_conditions: json["chronic_medical_conditions"],
          chronic_medication: json["chronic_medication"],
          operations_hospitalization: json["operations_hospitalization"],
          family_history_of_diseases: json["family_history_of_diseases"],
          last_check_up: json["last_check_up"],
          social_habits: json["social_habits"],
          exercise_habits: json["exercise_habits"],
          diet: json["diet"],
          report: json["report"],
          general_health: json["general_health"],
          occupation: json["occupation"],
          emergency_contact: json["emergency_contact"]);

  Map<String, dynamic> toJson() =>
      {
        "medical_conditions": medicalConditions,
        "blood_type": bloodType,
        "weight": weight,
        "height": height,
        "immunization": immunization,
        "food_allergies": food_allergies,
        "medication_allergies": medication_allergies,
        "chronic_medical_conditions": chronic_medical_conditions,
        "chronic_medication": chronic_medication,
        "operations_hospitalization": operations_hospitalization,
        "family_history_of_diseases": family_history_of_diseases,
        "last_check_up": last_check_up,
        "social_habits": social_habits,
        "exercise_habits": exercise_habits,
        "diet": diet,
        "general_health": general_health,
        "occupation": occupation,
        "emergency_contact": emergency_contact
      };
}
