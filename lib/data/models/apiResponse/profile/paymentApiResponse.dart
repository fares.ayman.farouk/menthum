import 'dart:convert';

PaymentApiResponse paymentApiResponseFromJson(Map<String, dynamic> str) =>
    PaymentApiResponse.fromJson(str);


String paymentApiResponseToJson(PaymentApiResponse data) => json.encode(data.toJson());

class PaymentApiResponse {
  PaymentApiResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  Data? data;

  factory PaymentApiResponse.fromJson(Map<String, dynamic> json) => PaymentApiResponse(
    status: json["status"],
    message: json["message"],
    code: json["code"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "code": code,
    "data": data!.toJson(),
  };
}

class Data {
  Data({
    this.url,
  });

  String? url;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    url: json["url"],
  );

  Map<String, dynamic> toJson() => {
    "url": url,
  };
}
