import 'dart:convert';

PendingPrescriptionsApiResponse pendingPrescriptionsResponseFromJson(
        Map<String, dynamic> data) =>
    PendingPrescriptionsApiResponse.fromJson(data);

String pendingPrescriptionsResponseToJson(
        PendingPrescriptionsApiResponse data) =>
    json.encode(data.toJson());

class PendingPrescriptionsApiResponse {
  PendingPrescriptionsApiResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  List<PendingPrescriptions>? data;

  factory PendingPrescriptionsApiResponse.fromJson(Map<String, dynamic> json) =>
      PendingPrescriptionsApiResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: List<PendingPrescriptions>.from(
            json["data"].map((x) => PendingPrescriptions.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "code": code,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class PendingPrescriptions {
  PendingPrescriptions(
      {this.id,
      this.fileUrl,
      this.reportUrl,
      this.name,
      this.isPending,
      this.createdAt,
      this.isOrdered});

  String? id;
  String? fileUrl;
  String? reportUrl;
  String? name;

  // List<int>? image;
  bool? isPending;
  bool? isOrdered;
  int? createdAt;

  factory PendingPrescriptions.fromJson(Map<String, dynamic> json) =>
      PendingPrescriptions(
          id: json['id'],
          name: json["name"],
          reportUrl: json["report_url"],
          fileUrl: json["file_path"],
          isPending: json["is_pending"] ?? false,
          isOrdered: json["is_ordered"] ?? false,
          createdAt: json["created_at"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "file_path": fileUrl,
        "reportUrl": reportUrl,
        "name": name,
        "created_at": createdAt,
      };
}
