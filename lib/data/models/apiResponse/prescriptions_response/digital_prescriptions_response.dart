DigitalPrescriptionResponse digitalPrescriptionResponseFromJson(
        Map<String, dynamic> data) =>
    DigitalPrescriptionResponse.fromJson(data);

class DigitalPrescriptionResponse {
  DigitalPrescriptionResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  List<DigitalPrescriptions>? data;

  factory DigitalPrescriptionResponse.fromJson(Map<String, dynamic> json) =>
      DigitalPrescriptionResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: List<DigitalPrescriptions>.from(
            json["data"].map((x) => DigitalPrescriptions.fromJson(x))),
      );
}

class DigitalPrescriptions {
  DigitalPrescriptions(
      {this.id,
      this.name,
      this.filePath,
      this.digitalPrescriptions,
      this.createdAt});

  String? id;
  String? name;
  String? filePath;
  List<Drug>? digitalPrescriptions;
  int? createdAt;

  factory DigitalPrescriptions.fromJson(Map<String, dynamic> json) =>
      DigitalPrescriptions(
        id: json["id"],
        name: json["name"],
        filePath: json["file_path"],
        createdAt: json["created_at"],
        digitalPrescriptions: List<Drug>.from(
            json["digital_prescriptions"].map((x) => Drug.fromJson(x))),
      );
}

class Drug {
  Drug(
      {this.logo,
      this.name,
      this.description,
      this.interval,
      this.intervalCount,
      this.frequency,
      this.route,
      this.duration,
      this.comment});

  String? logo;
  String? name;
  String? description;
  int? interval;

  ///هتستمر علي العلاج اد ايه يوم - اسبوع - شهر

  int? intervalCount;

  /// عدد المرات * الاسمترار

  int? frequency;

  /// يومي - اسبوعي - شهري

  int? duration;

  ///عدد المرات ف المره

  int? route;

  /// هيتاخد فين

  String? comment;

  factory Drug.fromJson(Map<String, dynamic> json) => Drug(
        logo: json["logo"],
        name: json["name"],
        description: json["description"],
        frequency: json["frequency"],
        interval: json["for_interval"],
        intervalCount: json["for_interval_count"],
        duration: json["duration"],
        route: json["route"],
        comment: json["comment"],
      );
}
