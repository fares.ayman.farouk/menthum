
BaseResponse baseResponseFromJson(Map<String, dynamic> data) =>
    BaseResponse.fromJson(data);

class BaseResponse {
  BaseResponse({
    this.status,
    this.message,
    this.code,
    this.data,
  });

  bool? status;
  String? message;
  int? code;
  dynamic data;

  factory BaseResponse.fromJson(Map<String, dynamic> json) => BaseResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        data: json["data"],
      );
}
