
import 'package:patientapp/data/models/apiResponse/auth_response/login_api_response.dart';

PlansResponse plansResponseFromJson(Map<String, dynamic> data) =>
    PlansResponse.fromJson(data);

class PlansResponse {
  PlansResponse({
    this.status,
    this.message,
    this.code,
    this.plans,
  });

  bool? status;
  String? message;
  int? code;
  List<Plan>? plans;

  factory PlansResponse.fromJson(Map<String, dynamic> json) => PlansResponse(
        status: json["status"],
        message: json["message"],
        code: json["code"],
        plans: List<Plan>.from(json["data"].map((x) => Plan.fromJson(x))),
      );
}
