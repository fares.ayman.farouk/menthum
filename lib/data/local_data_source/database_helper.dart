import 'dart:developer';
import 'package:patientapp/data/models/call_logs/call_logs_model.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

class DatabaseDataSource {
  DatabaseDataSource._privateConstructor();

  static final DatabaseDataSource instance =
      DatabaseDataSource._privateConstructor();
  static Database? _database;

  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    log("<<initialization of DB>>");
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'patient.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE callLog(
          id TEXT PRIMARY KEY,
          call_id INTEGER,
          event INTEGER,
          comment TEXT,
          date TEXT,
          num_of_tries INTEGER DEFAULT 0 not null
      )
      ''');
  }

  Future<List<CallLogsModel>> getCallLogsList() async {
    Database db = await database;
    var callLogs = await db.query('callLog', orderBy: 'date DESC');

    List<CallLogsModel> callLogsList = callLogs.isNotEmpty
        ? callLogs.map((c) => CallLogsModel.fromJson(c)).toList()
        : [];
    return callLogsList;
  }

  Future<int> saveCallLogData(CallLogsModel callLogsModel) async {
    Database db = await database;
    // return await db.insert('callLog', callLogData);
    int value = await db.rawInsert(
        "insert or replace into callLog (id, call_id, event, comment, date,num_of_tries) values "
        "((select id from callLog where call_id = '${callLogsModel.callId}' and event = '${callLogsModel.event}'), "
        "'${callLogsModel.callId}','${callLogsModel.event}', '${callLogsModel.comment}', '${callLogsModel.date}'"
        ",(select num_of_tries from callLog where call_id = '${callLogsModel.callId}' and event = '${callLogsModel.event}')+1)");

    log("Value inserted: $value");
    return value;
  }

  Future<int> removeCallLogData(CallLogsModel callLogsModel) async {
    Database db = await database;
    return await db.rawDelete(
        'DELETE FROM callLog WHERE call_id = "${callLogsModel.callId}" and event = "${callLogsModel.event}"');
  }

  Future<int> updateCallLogData(CallLogsModel callLogsModel) async {
    Database db = await database;
    return await db.update('callLog', callLogsModel.toJson(),
        where: "call_id = ?,event = ?",
        whereArgs: [callLogsModel.callId, callLogsModel.event]);
  }
}
