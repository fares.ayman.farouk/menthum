import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:patientapp/data/models/apiResponse/base_response.dart';
import 'package:patientapp/data/models/apiResponse/my_records_response/lab_results_response.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/repositories/my_records_repository.dart';

import '../../application/app_preference.dart';
import '../models/apiError/login_api_error.dart';
import '../network/error_handler.dart';
import '../network/network_info.dart';
import '../network/web_services/my_records_web_services.dart';

class MyRecorderRepositoryImp extends MyRecordsRepositories {
  final NetWorkInfo _netWorkInfo;
  final AppPreferences appPreferences;

  final MyRecordsWebServices myRecordsWebServices;

  MyRecorderRepositoryImp(
      this._netWorkInfo, this.appPreferences, this.myRecordsWebServices);

  @override
  Future<Either<Failure, MyRecordResultsResponse>> getLabResults() async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await myRecordsWebServices.getLabResults();

        if (response.statusCode == 200) {
          return Right(labResultsResponseFromJson(response.data));
        } else {
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,

            ///todo edit fail
            body: loginApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, MyRecordResultsResponse>> getRadsResults() async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await myRecordsWebServices.getRadsResults();

        if (response.statusCode == 200) {
          return Right(labResultsResponseFromJson(response.data));
        } else {
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,

            ///todo edit fail
            body: loginApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  /// upload lab file
  @override
  Future<Either<Failure, BaseResponse>> uploadRecordFile(
       File file,String type) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await myRecordsWebServices.uploadRecordFile(file,type);

        if (response.statusCode == 200) {
          return Right(baseResponseFromJson(response.data));
        } else {
          print("Faild ${response.data}");
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,

            ///todo edit fail
            body: loginApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }



  /// get Specialist results
  @override
  Future<Either<Failure, MyRecordResultsResponse>> getSpecialistResults(String type) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await myRecordsWebServices.getReports(type);

        if (response.statusCode == 200) {
          return Right(labResultsResponseFromJson(response.data));
        } else {
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,

            ///todo edit fail
            body: loginApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }


  /// get Tikshif results
  @override
  Future<Either<Failure, MyRecordResultsResponse>> getTikshifResults(String type) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await myRecordsWebServices.getReports(type);

        if (response.statusCode == 200) {
          return Right(labResultsResponseFromJson(response.data));
        } else {
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,

            ///todo edit fail
            body: loginApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
