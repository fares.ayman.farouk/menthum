import 'dart:developer';
import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:patientapp/application/constants.dart';
import 'package:patientapp/data/caching/local_data_source.dart';
import 'package:patientapp/data/models/apiRequest/clinic/book_clinic_appointment_req.dart';
import 'package:patientapp/data/models/apiRequest/clinic/clinic_request.dart';
import 'package:patientapp/data/models/apiResponse/clinic/book_clinic_appointment_response.dart';
import 'package:patientapp/data/models/apiResponse/clinic/clinic_response.dart';
import 'package:patientapp/data/models/apiResponse/consultations_responses/clinic_profile_response.dart';
import 'package:patientapp/data/models/apiResponse/consultations_responses/consultations_response.dart';
import 'package:patientapp/data/models/apiResponse/homePageApiResponse.dart';
import 'package:patientapp/data/models/apiResponse/laboratory/lab_profile_response.dart';
import 'package:patientapp/data/models/apiResponse/location_filter/city_response.dart';
import 'package:patientapp/data/models/apiResponse/location_filter/governorate_response.dart';
import 'package:patientapp/data/models/apiResponse/prescriptions_response/digital_prescriptions_response.dart';
import 'package:patientapp/data/models/apiResponse/prescriptions_response/pending_prescriptions_response.dart';
import 'package:patientapp/data/models/apiResponse/rads_response/rad_profile_response.dart';
import 'package:patientapp/data/models/apiResponse/specialties_response.dart';
import 'package:patientapp/data/models/apiResponse/tikshif_locations/nano_clinic_response.dart';
import 'package:patientapp/data/models/apiResponse/tranactions_api_response.dart';
import 'package:patientapp/data/models/apiResponse/upcoming_response.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/data/network/web_services/consultations_web_services.dart';

import '../../application/app_preference.dart';
import '../../domain/repositories/consultations_repositories.dart';
import '../models/apiError/login_api_error.dart';
import '../models/apiError/register_api_error.dart';
import '../models/apiResponse/base_response.dart';
import '../models/apiResponse/plans_response.dart';
import '../models/apiResponse/profile/paymentApiResponse.dart';
import '../models/apiResponse/visit_detail_response/visitDetailResponse.dart';
import '../network/error_handler.dart';
import '../network/network_info.dart';

class ConsultationsRepositoriesImp extends ConsultationsRepositories {
  final ConsultationsWebServices consultationsWebServices;
  final NetWorkInfo _netWorkInfo;
  final AppPreferences appPreferences;
  final LocalDataSource _localDataSource;

  ConsultationsRepositoriesImp(this.consultationsWebServices, this._netWorkInfo,
      this.appPreferences, this._localDataSource);

  @override
  Future<Either<Failure, ConsultationsResponse>> getConsultations(
      int page) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await consultationsWebServices.getConsultation(page);

        if (response.statusCode == 200) {
          final tikshifReports = consultationsResponseFromJson(response.data);

          return Right(tikshifReports);
        } else {
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,

            ///todo edit fail
            body: loginApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, UpcomingAppointmentsResponse>>
      upcomingAppointments() async {
    try {
      /// get response from cache.
      final response = await _localDataSource.getUpcomingAppointments();
      return Right(response);
    } catch (cacheError) {
      ///cache is not existing or invalid.
      ///it is the time to get from the API side.
      if (await _netWorkInfo.isConnected) {
        try {
          final response =
              await consultationsWebServices.upcomingAppointments();

          if (response.statusCode == 200) {
            final upcomingResponse = upcomingResultsFromJson(response.data);

            /// save upcoming appointments to cache.
            _localDataSource.saveUpcomingAppointmentsToCash(upcomingResponse);

            return Right(upcomingResponse);
          } else {
            return Left(
              Failure(
                ApiInternalStatus.FAILURE,
                response.statusMessage,
              ),
            );
          }
        } catch (error) {
          return Left(ErrorHandler.handle(error).failure);
        }
      } else {
        return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
      }
    }
  }

  @override
  Future<Either<Failure, SpecialtiesResponse>> getSpecialties(
      String query) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await consultationsWebServices.getSpecialties(query);

        if (response.statusCode == 200) {
          return Right(specialtiesResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, ClinicProfileResponse>> getClinicProfile(
      String id) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await consultationsWebServices.getClinicProfile(id);

        if (response.statusCode == 200) {
          return Right(clinicProfileResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        print("#######${error.toString()}");
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, ClinicsResponse>> getClinics(
      ClinicRequest request, int page) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await consultationsWebServices.getClinics(request, page);

        if (response.statusCode == 200) {
          print('200');
          return Right(getClinicsResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        print(error);
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, GovernorateResponse>> getGovernorates(
      String countryId) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await consultationsWebServices.getGovernorates(countryId);

        if (response.statusCode == 200) {
          return Right(governorateResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, CityResponse>> getCities(String governorateId) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await consultationsWebServices.getCities(governorateId);

        if (response.statusCode == 200) {
          return Right(cityResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, PendingPrescriptionsApiResponse>>
      getPrescriptions() async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await consultationsWebServices.getPrescriptions();

        if (response.statusCode == 200) {
          return Right(pendingPrescriptionsResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, List<int>>> getPrescriptionsImage(
      String imagePath) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await consultationsWebServices.getPrescriptionImage(imagePath);

        if (response.statusCode == 200) {
          print("${response.data}");

          return Right(response.data);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, BookAppointmentResponse>> makeAppointment(
      BookAppointmentReq bookClinicAppointmentReq) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await consultationsWebServices
            .makeAppointment(bookClinicAppointmentReq);

        if (response.statusCode == 200) {
          print("################################");

          /// remove the upcoming Appointment. to get the new data.
          _localDataSource
              .removeFromCache(AppConstants.UPCOMING_APPOINTMENT_KEY);
          print("${response.data}");

          return Right(bookClinicAppointmentResponseFromJson(response.data));
        } else {
          return Left(
            Failure(ApiInternalStatus.FAILURE, response.statusMessage,
                body: registerApiErrorResponseFromJson(response.data)),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  /// get visit details
  @override
  Future<Either<Failure, VisitDetailResponse>> getVisitDetails(int id) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await consultationsWebServices.getVisitDetails(id);

        if (response.statusCode == 200) {
          return Right(visitDetailResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        print("#################${error.toString()}");
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, TransactionApiResponse>> getTransactions(
      int page) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await consultationsWebServices.getTransactions(page);

        if (response.statusCode == 200) {
          return Right(transactionResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        print("#################${error.toString()}");
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  Future<Either<Failure, BaseResponse>> uploadPresFiles(File file) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await consultationsWebServices.uploadPresFiles(file);

        if (response.statusCode == 200) {
          return Right(baseResponseFromJson(response.data));
        } else {
          print("Faild ${response.data}");
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,

            ///todo edit fail
            body: loginApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  ///get digital prescriptions
  @override
  Future<Either<Failure, DigitalPrescriptionResponse>>
      getDigitalPrescriptions() async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await consultationsWebServices.getPrescriptions();

        if (response.statusCode == 200) {
          return Right(digitalPrescriptionResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  /// delete pescrip
  @override
  Future<Either<Failure, BaseResponse>> deletePendingPrescription(
      String id) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await consultationsWebServices.deletePendingPrescription(id);

        if (response.statusCode == 200) {
          return Right(baseResponseFromJson(response.data));
        } else {
          print("Faild ${response.data}");
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
            body: loginApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  /// order online
  @override
  Future<Either<Failure, BaseResponse>> orderOnline(String id) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await consultationsWebServices.orderOnlinePrescription(id);

        if (response.statusCode == 200) {
          return Right(baseResponseFromJson(response.data));
        } else {
          print("Faild ${response.data}");
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
            body: loginApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, PaymentApiResponse>> payment(int amount) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await consultationsWebServices.payment(amount);

        if (response.statusCode == 200) {
          return Right(paymentApiResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  ///update prescription file
  @override
  Future<Either<Failure, BaseResponse>> updatePresFiles(
      File file, String id) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await consultationsWebServices.updatePresFiles(file, id);

        if (response.statusCode == 200) {
          return Right(baseResponseFromJson(response.data));
        } else {
          print("Faild ${response.data}");
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,

            ///todo edit fail
            body: loginApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, LabProfileResponse>> getLabProfile(
      String labId) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await consultationsWebServices.getLabProfile(labId);

        if (response.statusCode == 200) {
          return Right(labProfileResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        print("#######${error.toString()}");
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, RadProfileResponse>> getRadProfile(
      String radId) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await consultationsWebServices.getRadProfile(radId);

        if (response.statusCode == 200) {
          return Right(radProfileResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        print("#######${error.toString()}");
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, NanoClinicResponse>> getNanoClinicsLocations() async {
    try {
      /// get response from cache.
      final response = await _localDataSource.getTikshifBoothLocations();
      return Right(response);
    } catch (cacheError) {
      if (await _netWorkInfo.isConnected) {
        try {
          final response =
              await consultationsWebServices.getNanoClinicsLocations();

          if (response.statusCode == 200) {
            /// save Nano Clinics Locations to cache.
            _localDataSource.saveTikshifBoothLocations(
                nanoClinicResponseFromJson(response.data));

            return Right(nanoClinicResponseFromJson(response.data));
          } else {
            return Left(
              Failure(
                ApiInternalStatus.FAILURE,
                response.statusMessage,
              ),
            );
          }
        } catch (error) {
          print("#######${error.toString()}");
          return Left(ErrorHandler.handle(error).failure);
        }
      } else {
        return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
      }
    }
  }

  @override
  Future<Either<Failure, HomePageApiResponse>> homePage() async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await consultationsWebServices.homePage();

        if (response.statusCode == 200) {
          final HomePageApiResponse homeResponse =
              homePageApiResponseFromJson(response.data);
          if (homeResponse.data != null && homeResponse.data!.isNotEmpty) {
            await appPreferences
                .addCallCost(homeResponse.data![0].callCost ?? 0);
          }
          return Right(homeResponse);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, PlansResponse>> getPlans() async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await consultationsWebServices.getPlans();

        if (response.statusCode == 200) {
          return Right(plansResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        log("RError:$error");
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, PaymentApiResponse>> subscribe(int planId) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await consultationsWebServices.subscribe(planId);

        if (response.statusCode == 200) {
          return Right(paymentApiResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, PaymentApiResponse>> renew(
      int planId, int subscriptionId) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await consultationsWebServices.renew(planId, subscriptionId);

        if (response.statusCode == 200) {
          return Right(paymentApiResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
