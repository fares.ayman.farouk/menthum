import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:dio/src/response.dart';
import 'package:patientapp/data/models/apiRequest/pediatric/pediatric_done_model.dart';

import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/models/pediatric/enroll_entity.dart';
import 'package:patientapp/domain/models/pediatric/history_entity.dart';
import 'package:patientapp/domain/models/pediatric/history_request.dart';
import 'package:patientapp/domain/models/pediatric/pediatric_calculate_request_Entity.dart';

import 'package:patientapp/domain/models/pediatric/pediatric_data_entity.dart';
import 'package:patientapp/domain/models/pediatric/pediatric_done.dart';
import 'package:patientapp/domain/models/pediatric/pediatric_head_response.dart';
import 'package:patientapp/domain/models/pediatric/pediatric_home.dart';
import 'package:patientapp/domain/models/pediatric/pediatric_weight_response.dart';

import '../../../domain/repositories/programs/pediatric_repository.dart';
import '../../models/apiRequest/diabetes/pediatric_history_request.dart';
import '../../models/apiRequest/pediatric/enroll_pediatric_request.dart';
import '../../models/apiRequest/pediatric/pediatric_calculate_request_model.dart';
import '../../models/apiResponse/pediatric/PediatricHistoryResponseModel.dart';
import '../../models/apiResponse/pediatric/pediatric_dat_respose.dart';
import '../../models/apiResponse/pediatric/pediatric_head_response.dart';
import '../../models/apiResponse/pediatric/pediatric_home_response.dart';
import '../../models/apiResponse/pediatric/pediatric_weight_response.dart';
import '../../network/error_handler.dart';
import '../../network/network_info.dart';
import '../../network/web_services/programs/pediatric_web_service.dart';

class PediatricRepositoryImp implements PediatricRepository {
  final NetWorkInfo _netWorkInfo;
  final PediatricWebService pediatricWebService;

  PediatricRepositoryImp(this._netWorkInfo, this.pediatricWebService);

  ///pediatric home
  @override
  Future<Either<Failure, List<PediatricHomeEntity>>> pediatricHome(
      String id) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await pediatricWebService.pediatricHome(id);
        if (response.statusCode == 200) {
          PediatricHomeResponse data =
              PediatricHomeResponse.fromJson(response.data);

          return Right(data.pediatricData!);
        } else if (response.statusCode == 400) {
          return Left(
            Failure(
              ResponseCode.BAD_REQUEST,
              response.data['message'],
            ),
          );
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        log("Error: $error");
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, List<PediatricDataEntity>>> today(String id) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await pediatricWebService.todayAction(id);
        if (response.statusCode == 200) {
          PediatricDataResponse data =
              PediatricDataResponse.fromJson(response.data);

          return Right(data.pediatricData!);
        } else if (response.statusCode == 400) {
          return Left(
            Failure(
              ResponseCode.BAD_REQUEST,
              response.data['data']['description'],
            ),
          );
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        log("Error: $error");
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  /// enroll
  @override
  Future<Either<Failure, Response>> enroll(
      EnrollRequestEntity enrollRequestEntity) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await pediatricWebService
            .enroll(EnrollRequestModel.obj(enrollRequestEntity));
        if (response.statusCode == 200) {
          return Right(response);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  /// done
  @override
  Future<Either<Failure, Response>> done(
      PediatricDoneEntity pediatricDoneEntity) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await pediatricWebService
            .done(PediatricDoneModel.obj(pediatricDoneEntity));
        if (response.statusCode == 200) {
          return Right(response);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  /// weight
  @override
  Future<Either<Failure, PediatricWeightResponseEntity>> calculateWeight(
      PediatricCalculateRequestEntity pediatricCalculateRequestEntity) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await pediatricWebService.pediatricCalculate(
            PediatricCalculateRequestModel.obj(
                pediatricCalculateRequestEntity));
        if (response.statusCode == 200) {
          PediatricWeightResponse data =
              PediatricWeightResponse.fromJson(response.data);

          return Right(data.pediatricData!);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  /// head
  @override
  Future<Either<Failure, PediatricHeadResponseEntity>> calculateHead(
      PediatricCalculateRequestEntity pediatricCalculateRequestEntity) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await pediatricWebService.pediatricCalculate(
            PediatricCalculateRequestModel.obj(
                pediatricCalculateRequestEntity));
        if (response.statusCode == 200) {
          PediatricHeadResponse data =
              PediatricHeadResponse.fromJson(response.data);

          return Right(data.pediatricData!);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  /// history
  @override
  Future<Either<Failure, PediatricHistoryEntity>> history(
      PediatricHistoryRequest pediatricHistoryRequest) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await pediatricWebService.historyAction(
            PediatricHistoryRequestModel.obj(pediatricHistoryRequest));
        if (response.statusCode == 200) {
          PediatricHistoryResponse data =
              PediatricHistoryResponse.fromJson(response.data);
          return Right(data.pediatricData!);
        } else if (response.statusCode == 400) {
          return Left(
            Failure(
              ResponseCode.BAD_REQUEST,
              response.data['data']['description'],
            ),
          );
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        log("Error: $error");
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
