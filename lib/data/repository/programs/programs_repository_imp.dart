import 'package:dartz/dartz.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/models/programs/program_entity.dart';

import '../../../domain/repositories/programs/programs_repository.dart';
import '../../models/apiResponse/program/programs_response.dart';
import '../../network/error_handler.dart';
import '../../network/network_info.dart';
import '../../network/web_services/programs_web_service.dart';

class ProgramsRepositoryImp implements ProgramsRepository {
  final NetWorkInfo _netWorkInfo;
  final ProgramsWebService programsWebService;

  ProgramsRepositoryImp(this._netWorkInfo, this.programsWebService);

  @override
  Future<Either<Failure, List<Program>>> programsList() async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await programsWebService.getProgramsList();

        if (response.statusCode == 200) {
          ProgramsResponse data = ProgramsResponse.fromJson(response.data);
          return Right(data.data ?? []);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
