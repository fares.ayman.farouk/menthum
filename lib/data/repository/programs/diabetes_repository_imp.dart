import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:dio/src/response.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/models/diabetes/diabetes_data.dart';
import 'package:patientapp/domain/models/diabetes/diabetes_done.dart';
import 'package:patientapp/domain/repositories/programs/diabetes_repository.dart';

import '../../../domain/models/diabetes/diabetes_history.dart';
import '../../../domain/models/diabetes/enroll_diabetes.dart';
import '../../models/apiRequest/diabetes/enroll_diabetes_model.dart';
import '../../models/apiRequest/diabtes_done_model.dart';
import '../../models/apiResponse/diabetes/diabetes_history_response.dart';
import '../../models/apiResponse/diabetes/diabetes_response.dart';
import '../../network/error_handler.dart';
import '../../network/network_info.dart';
import '../../network/web_services/programs/diabetes_web_services.dart';

class DiabetesRepositoryImp implements DiabetesRepository {
  final NetWorkInfo _netWorkInfo;
  final DiabetesWebService diabetesWebService;

  DiabetesRepositoryImp(this._netWorkInfo, this.diabetesWebService);

  @override
  Future<Either<Failure, DiabetesData>> today() async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await diabetesWebService.todayAction();
        if (response.statusCode == 200) {
          DiabetesResponse data = DiabetesResponse.fromJson(response.data);
          return Right(data.data!);
        } else if (response.statusCode == 400) {
          return Left(
            Failure(
              ResponseCode.BAD_REQUEST,
              response.data['data']['description'],
            ),
          );
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        log("Error: $error");
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, DiabetesHistory>> history(int nextPage) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await diabetesWebService.historyAction( nextPage);
        if (response.statusCode == 200) {
          DiabetesHistoryResponse data =
              DiabetesHistoryResponse.fromJson(response.data);
          return Right(data.data!);
        } else if (response.statusCode == 400) {
          return Left(
            Failure(
              ResponseCode.BAD_REQUEST,
              response.data['data']['description'],
            ),
          );
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        log("Error: $error");
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, Response>> enroll(
      EnrollDiabetes enrollDiabetes) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await diabetesWebService
            .enroll(EnrollDiabetesModel.obj(enrollDiabetes));
        if (response.statusCode == 200) {
          return Right(response);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, Response>> done(DiabetesDone diabetesDone) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await diabetesWebService.done(DiabetesDoneModel.obj(diabetesDone));
        if (response.statusCode == 200) {
          return Right(response);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
