import 'package:dartz/dartz.dart';
import 'package:patientapp/data/models/apiRequest/vitals/add_read_request_model.dart';
import 'package:patientapp/data/models/apiResponse/base_response.dart';

import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/models/vitals/response/vital_details_entity.dart';

import 'package:patientapp/domain/models/vitals/response/vital_entity.dart';

import '../../application/app_preference.dart';
import '../../domain/models/vitals/request/add_read_request.dart';
import '../../domain/models/vitals/request/reading_request.dart';
import '../../domain/repositories/vital_repository.dart';
import '../models/apiRequest/vitals/reading_request_model.dart';
import '../models/apiResponse/vital_response/vital_details_response.dart';
import '../models/apiResponse/vital_response/vital_response.dart';
import '../network/error_handler.dart';
import '../network/network_info.dart';
import '../network/web_services/vital_web_services.dart';

class VitalRepositoryImp implements VitalRepository {
  final VitalWebServices vitalWebServices;
  final NetWorkInfo netWorkInfo;
  final AppPreferences appPreferences;

  VitalRepositoryImp(
      this.netWorkInfo, this.vitalWebServices, this.appPreferences);

  @override
  Future<Either<Failure, List<VitalEntity>>> getPatientReadings() async {
    if (await netWorkInfo.isConnected) {
      try {
        final response = await vitalWebServices.getPatientReadings();

        if (response.statusCode == 200) {
          VitalResponse res = vitalsResponseFromJson(response.data);
          return Right(res.vitals);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, String>> addRead(AddReadRequestEntity addReadRequestEntity) async {
    if (await netWorkInfo.isConnected) {
      try {
        final response =
            await vitalWebServices.addRad(AddReadRequestModel.obj(addReadRequestEntity));

        if (response.statusCode == 200) {
          BaseResponse res = BaseResponse.fromJson(response.data);
          return Right(res.message ?? "");
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  ///get Vital Details
  @override
  Future<Either<Failure, List<VitalDetailsEntity>>> getVitalDetails(
      ReadingRequestEntity readingRequestEntity) async {
    if (await netWorkInfo.isConnected) {
      try {
        final response = await vitalWebServices
            .getVitalDetails(ReadingRequestModel.obj(readingRequestEntity));

        if (response.statusCode == 200) {
          VitalDetailsResponse res =
              vitalDetailsResponseFromJson(response.data);
          return Right(res.vitalDetailsModel);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
