import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:patientapp/data/models/apiRequest/menstrual_list/add_new_cycle_model.dart';
import 'package:patientapp/data/models/apiResponse/menstrual_cycle/menstrual_cycle_model.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/models/menstrual_cycle/menstrual_cycle.dart';
import 'package:patientapp/domain/repositories/menstrual_repository.dart';

import '../../application/app_preference.dart';
import '../../domain/models/menstrual_cycle/add_new_cycle.dart';
import '../../domain/models/menstrual_cycle/edit_cycle.dart';
import '../models/apiRequest/menstrual_list/edit_cycle_model.dart';
import '../models/apiResponse/menstrual_cycle/menstrual_cycle_list.dart';
import '../network/error_handler.dart';
import '../network/network_info.dart';
import '../network/web_services/menstrual_web_services.dart';

class MenstrualRepositoryImp extends MenstrualRepository {
  final NetWorkInfo netWorkInfo;
  final AppPreferences appPreferences;
  final MenstrualWebServices menstrualWebServices;

  MenstrualRepositoryImp(
      this.netWorkInfo, this.appPreferences, this.menstrualWebServices);

  @override
  Future<Either<Failure, List<MenstrualCycle>>> getMenstrualCycle() async {
    if (await netWorkInfo.isConnected) {
      try {
        final response = await menstrualWebServices.getMenstrualCycle();

        return Right(MenstrualCycleList.fromJson(response.data).data);
      } catch (error) {
        log("!!!!!!!!!!!!!!!!!!!!!!!--- ${error.toString()}");
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, MenstrualCycleModel>> addMenstrualCycle(
      AddNewCycle addNewCycle) async {
    if (await netWorkInfo.isConnected) {
      try {
        final response = await menstrualWebServices
            .addMenstrualCycle(AddNewCycleModel.obj(addNewCycle));
        log("Logg: ${response.data['data']}");
        return Right(MenstrualCycleModel.fromJson(response.data['data']));
      } catch (error) {
        log("!!!!!!!!!!!!!!!!!!!!!!!--- ${error.toString()}");
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, MenstrualCycleModel>> editMenstrualCycle(
      EditCycle editCycle) async {
    if (await netWorkInfo.isConnected) {
      try {
        final response = await menstrualWebServices
            .editMenstrualCycle(EditCycleModel.obj(editCycle));
        log("Logg: ${response.data['data']}");
        return Right(MenstrualCycleModel.fromJson(response.data['data']));
      } catch (error) {
        log("!!!!!!!!!!!!!!!!!!!!!!!--- ${error.toString()}");
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
