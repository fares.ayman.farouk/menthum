import 'package:dartz/dartz.dart';
import 'package:patientapp/application/constants.dart';

import 'package:patientapp/data/models/apiResponse/base_response.dart';

import 'package:patientapp/data/models/apiResponse/notification_list_response/notification_list_response.dart';

import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/data/network/web_services/notifications_web_services.dart';

import '../../application/app_preference.dart';
import '../../domain/repositories/notification_repositories.dart';
import '../caching/local_data_source.dart';
import '../network/error_handler.dart';
import '../network/network_info.dart';

class NotificationRepositoriesImp extends NotificationRepositories {
  final NotificationWebServices notificationWebServices;
  final NetWorkInfo _netWorkInfo;
  final AppPreferences appPreferences;
  final LocalDataSource _localDataSource;

  NotificationRepositoriesImp(
      this.notificationWebServices, this._netWorkInfo, this.appPreferences,this._localDataSource);

  @override
  Future<Either<Failure, NotificationResponse>> getListNotifications() async {
    try {
      /// get response from cache.
      final response =  _localDataSource.getNotificationList();
      return Right(response);
    }catch(cacheError){
      if (await _netWorkInfo.isConnected) {
        try {
          final response = await notificationWebServices.getListNotification();

          if (response.statusCode == 200) {
            _localDataSource.saveNotificationList(notificationResponseFromJson(response.data));

            return Right(notificationResponseFromJson(response.data));

          } else {
            return Left(
              Failure(
                ApiInternalStatus.FAILURE,
                response.statusMessage,
              ),
            );
          }
        } catch (error) {
          return Left(ErrorHandler.handle(error).failure);
        }
      } else {
        return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
      }
    }



  }

  @override
  Future<Either<Failure, BaseResponse>> setNotificationRead(int id) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await notificationWebServices.setNotificationRead(id);

        if (response.statusCode == 200) {
          _localDataSource.removeFromCache(AppConstants.NOTIFICATION_LOCAL_KEY);

          return Right(baseResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
