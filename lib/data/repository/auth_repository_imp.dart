
import 'package:dartz/dartz.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/application/utils.dart';
import 'package:patientapp/data/models/apiError/reset_password_api_error.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/EditProfileApiRequest.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/UpadePasswordApiRequest.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/forget_password_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/login_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/login_with_provider_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/register_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/resend_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/reset_password_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/verify_request.dart';
import 'package:patientapp/data/models/apiRequest/dependant_account_request/add_dependant_account_request.dart';
import 'package:patientapp/data/models/apiResponse/TermsPageApiResponse.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/forget_password_response.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/logout_response.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/register_response.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/reset_password_response.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/verify_response.dart';
import 'package:patientapp/data/models/apiResponse/base_response.dart';
import 'package:patientapp/data/models/apiResponse/dependant_account_responses/get_dependant_accounts_response.dart';
import 'package:patientapp/domain/repositories/auth_repositories.dart';

import '../models/apiError/login_api_error.dart';
import '../models/apiError/register_api_error.dart';
import '../models/apiRequest/auth_requests/EditMedicalProfileApiRequest.dart';
import '../models/apiResponse/auth_response/login_api_response.dart';
import '../models/apiResponse/auth_response/resend_response.dart';
import '../models/apiResponse/profile/medicalProfileApiResponse.dart';
import '../network/error_handler.dart';
import '../network/failure.dart';
import '../network/network_info.dart';
import '../network/web_services/login_web_services.dart';

class AuthRepositoryImp implements AuthRepository {
  final AuthWebServices authWebServices;
  final NetWorkInfo _netWorkInfo;
  final AppPreferences appPreferences;

  AuthRepositoryImp(
      this.authWebServices, this._netWorkInfo, this.appPreferences);

  // login
  @override
  Future<Either<Failure, LoginResponse>> login(
      LoginRequest loginRequest) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await authWebServices.login(loginRequest);

        if (response.statusCode == 200) {
          //save user data in shared pref..
          //local data.
          await appPreferences.addUser(response.data["data"]);
          return Right(loginResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
            body: loginApiErrorResponseFromJson(response.data.toString()),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, LoginResponse>> loginWithProvider(
      LoginWithProviderRequest loginRequest) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await authWebServices.loginWithProvider(loginRequest);

        if (response.statusCode == 200) {
          //save user data in shared pref..
          //local data.
          appPreferences.addUser(response.data["data"]);
          return Right(loginResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
            body: loginApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  //register
  @override
  Future<Either<Failure, RegisterResponse>> register(
      RegisterRequest registerRequest) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await authWebServices.register(registerRequest);

        if (response.statusCode == 201) {
          //success
          //return Either right

          return Right(registerResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
            body: registerApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, ResendOtpResponse>> resendOTP(
      ResendOtpRequest resendOtpRequest) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await authWebServices.resendOTP(resendOtpRequest);

        if (response.statusCode == 200) {
          //success
          //return Either right
          return Right(resendOtpResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
            body: registerApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, VerifyResponse>> verify(
      VerifyRequest verifyRequest) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await authWebServices.verifyCode(verifyRequest);

        if (response.statusCode == 200) {
          //save user data in shared pref..
          //local data.
          appPreferences.addUser(response.data["data"]);
          return Right(verifyResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
            body: loginApiErrorResponseFromJson(response.data.toString()),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, ResetPasswordResponse>> resetPassword(
      ResetPasswordRequest resetPasswordRequest) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await authWebServices.resetPassword(resetPasswordRequest);

        if (response.statusCode == 200) {
          return Right(resetPasswordResponseFromJson(response.data));
        } else {
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
            body: resetPasswordApiErrorFromJson(response.data.toString()),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, ForgetPasswordResponse>> forgetPassword(
      ForgetPasswordRequest forgetPasswordRequest) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await authWebServices.forgetPassword(forgetPasswordRequest);

        if (response.statusCode == 200) {
          //success
          //return Either right

          return Right(forgetPasswordResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
            body: loginApiErrorResponseFromJson(response.data.toString()),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, LogoutResponse>> logout() async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await authWebServices.logout();

        if (response.statusCode == 200) {
          clearUserData();
          return Right(logoutResponseFromJson(response.data));
        }
        // else if (response.statusCode == 401) {
        //   clearUserData();
        //   print(
        //       "******${response.statusMessage} -  ${ResponseCode.UNAUTORISED}");
        //   return Left(Failure(
        //     ResponseCode.UNAUTORISED,
        //     response.statusMessage,
        //   ));
        // }
        else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, LoginResponse>> getUserProfileData(
      {String? token}) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await authWebServices.getUserProfileData(qrToken: token);

        if (response.statusCode == 200) {
          //save user data in shared pref..
          //local data.

          await appPreferences.addUser(response.data["data"]);

          return Right(loginResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
            body: loginApiErrorResponseFromJson(response.data.toString()),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, LoginResponse>> editUserProfile(
      EditProfileApiRequest editProfileApiRequest) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await authWebServices.editUserProfile(editProfileApiRequest);

        if (response.statusCode == 200) {
          await appPreferences.addUser(response.data["data"]);
          return Right(loginResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
            body: registerApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, BaseResponse>> updatePassword(
      UpdatePasswordApiRequest updatePasswordApiRequest) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await authWebServices.updatePassword(updatePasswordApiRequest);

        if (response.statusCode == 200) {
          return Right(baseResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
            body: updatePasswordApiRequestFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, MedicalProfileApiResponse>> getMedicalProfile() async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await authWebServices.getMedicalProfile();

        if (response.statusCode == 200) {
          /// add medical profile to shared preference.
          await appPreferences.addMedicalProfile(response.data["data"]);
          return Right(medicalProfileApiResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, MedicalProfileApiResponse>> editMedicalProfile(
      EditMedicalProfileApiRequest medicalProfileApiRequest) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await authWebServices.editMedicalProfile(medicalProfileApiRequest);

        if (response.statusCode == 200) {
          return Right(medicalProfileApiResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        print("#################${error.toString()}");
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  Future<Either<Failure, TermsPageApiResponse>> termsPage(String url) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await authWebServices.termPage(url);

        if (response.statusCode == 200) {
          return Right(termsPageApiResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  Future<Either<Failure, LoginResponse>> setPassword(
      {required String password, required String confirmPassword}) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await authWebServices.setPassword(password, confirmPassword);

        if (response.statusCode == 200) {
          await appPreferences.addUser(response.data["data"]);
          return Right(loginResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, LoginResponse>> changePhone(
      String phone, String countryCode) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await authWebServices.changePhone(phone, countryCode);

        if (response.statusCode == 200) {
          //save user data in shared pref..
          //local data.
          await appPreferences.addUser(response.data["data"]);
          return Right(loginResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
            body: loginApiErrorResponseFromJson(response.data.toString()),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  /// dependant account

  // add dependant account
  @override
  Future<Either<Failure, LoginResponse>> addDependantAccount(
      AddDependantAccountRequest dependantAccountRequest) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await authWebServices.addDependantAccount(dependantAccountRequest);

        if (response.statusCode == 200) {
          return Right(loginResponseFromJson(response.data));
        } else {
          return Left(
            Failure(ApiInternalStatus.FAILURE, response.statusMessage,
                body: registerApiErrorResponseFromJson(response.data)),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  // get dependant accounts
  @override
  Future<Either<Failure, GetDependantAccountsResponse>>
      getDependantAccounts() async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await authWebServices.getDependantAccounts();

        if (response.statusCode == 200) {
          return Right(getDependantAccountsResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  // show dependant account details
  @override
  Future<Either<Failure, LoginResponse>> showDependantAccount(String id) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await authWebServices.showDependantAccount(id);

        if (response.statusCode == 200) {
          await appPreferences.addUser(response.data["data"]);
          return Right(loginResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  int addDependant(AddDependantAccountRequest dependantAccountRequest) {
    return 1000;
  }
}
