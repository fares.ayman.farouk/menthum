import 'package:dartz/dartz.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/data/models/apiRequest/rads_request/rads_request.dart';
import 'package:patientapp/data/models/apiResponse/rads_response/rads_response.dart';
import 'package:patientapp/data/models/apiResponse/tests_response.dart';
import 'package:patientapp/data/network/error_handler.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/data/network/network_info.dart';
import 'package:patientapp/data/network/web_services/rads_web_services.dart';
import 'package:patientapp/domain/repositories/rads_repositories.dart';

class RadsRepositoryImp extends RadsRepositories {
  final RadsWebServices radsWebServices;
  final NetWorkInfo _netWorkInfo;
  final AppPreferences appPreferences;

  RadsRepositoryImp(
      this.radsWebServices, this._netWorkInfo, this.appPreferences);

  @override
  Future<Either<Failure, RadsResponse>> getListOfRads(
      RadsRequest radsRequest) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await radsWebServices.getListOfRads(radsRequest);

        if (response.statusCode == 200) {
          return Right(radsResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, TestsResponse>> getAllRadsTests() async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await radsWebServices.getRadsList(2);

        if (response.statusCode == 200) {
          return Right(testsResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
