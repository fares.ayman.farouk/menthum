import 'package:dartz/dartz.dart';
import 'package:patientapp/data/models/apiResponse/questionnaire/questionnaire_item_response.dart';
import 'package:patientapp/data/models/apiResponse/questionnaire/questionnaire_response.dart';

import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/models/questionnaire/answer_questionnaire_entity.dart';

import 'package:patientapp/domain/models/questionnaire/questionnaire_entity.dart';

import 'package:patientapp/domain/models/questionnaire/questionnaire_item_entity.dart';

import '../../application/app_preference.dart';
import '../../domain/models/questionnaire/questionnaire_result_entity.dart';
import '../../domain/repositories/questionnaire_repository.dart';
import '../models/apiRequest/questionnaire_request/answer_questionnaire_request.dart';
import '../models/apiResponse/questionnaire/questionnaire_result_model.dart';
import '../network/error_handler.dart';
import '../network/network_info.dart';
import '../network/web_services/questionnaire_web_services.dart';

class QuestionnaireRepositoryImp implements QuestionnaireRepository {
  final QuestionnaireWebServices questionnaireWebServices;
  final NetWorkInfo netWorkInfo;
  final AppPreferences appPreferences;

  QuestionnaireRepositoryImp(
      this.netWorkInfo, this.questionnaireWebServices, this.appPreferences);

  /// list questionnaires
  @override
  Future<Either<Failure, List<QuestionnaireItemEntity>>>
      getQuestionnairesItems() async {
    if (await netWorkInfo.isConnected) {
      try {
        final response =
            await questionnaireWebServices.getQuestionnairesItems();
        if (response.statusCode == 200) {
          QuestionnaireItemResponse res =
              QuestionnaireItemResponse.fromJson(response.data);
          return Right(res.questionnaireItems ?? []);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  /// questions
  @override
  Future<Either<Failure, List<QuestionnaireQuestionEntity>>>
      getQuestionnaireQuestions(String questionnaireSlug) async {
    if (await netWorkInfo.isConnected) {
      try {
        final response = await questionnaireWebServices
            .getQuestionnaireQuestions(questionnaireSlug);
        if (response.statusCode == 200) {
          QuestionnaireQuestionResponse res =
              QuestionnaireQuestionResponse.fromJson(response.data);
          return Right(res.questions ?? []);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  /// save answers
  @override
  Future<Either<Failure, QuestionnaireResultEntity>> saveAnswers(
      AnswerQuestionnaireEntity answerQuestionnaireEntity, String slug) async {
    if (await netWorkInfo.isConnected) {
      try {
        final response = await questionnaireWebServices.saveAnswers(
            AnswerQuestionnaireRequest.obj(answerQuestionnaireEntity), slug);
        if (response.statusCode == 200) {
          QuestionnaireResultResponse res =
              QuestionnaireResultResponse.fromJson(response.data);
          return Right(res.questionnaireResult!);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
