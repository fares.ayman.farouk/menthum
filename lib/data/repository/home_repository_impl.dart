import 'package:dartz/dartz.dart';
import 'package:patientapp/data/caching/local_data_source.dart';
import 'package:patientapp/data/models/apiResponse/home_response/gp_working_shifts_response.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/data/network/network_info.dart';
import 'package:patientapp/domain/repositories/home_repository.dart';

import '../network/error_handler.dart';
import '../network/web_services/home_web_services.dart';

class HomeRepositoryImpl implements HomeRepository {
  final HomeWebServices homeWebServices;
  final NetWorkInfo _netWorkInfo;
  final LocalDataSource _localDataSource;

  HomeRepositoryImpl(
      this.homeWebServices, this._netWorkInfo, this._localDataSource);

  @override
  Future<Either<Failure, GPWorkingShiftResponse>> getGpWorkingShifts() async {
    try {
      /// get response from cache.
      final response = _localDataSource.getGPWorkingShiftResponse();
      return Right(response);
    } catch (cacheError) {
      if (await _netWorkInfo.isConnected) {
        try {
          final response = await homeWebServices.getGpWorkingShifts();

          if (response.statusCode == 200) {
            final gpWorkingShifts = getGPWorkingShiftResponse(response.data);

            /// save gp working shifts to cache.
            _localDataSource.saveGPWorkingShiftResponse(gpWorkingShifts);

            return Right(gpWorkingShifts);
          } else {
            return Left(Failure(
                ApiInternalStatus.FAILURE, response.statusMessage,
                body: response.data));
          }
        } catch (error) {
          return Left(ErrorHandler.handle(error).failure);
        }
      } else {
        return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
      }
    }
  }
}
