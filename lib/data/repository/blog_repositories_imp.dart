import 'package:dartz/dartz.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/models/blog_details_entity.dart';
import 'package:patientapp/domain/models/blog_entity.dart';
import 'package:patientapp/domain/repositories/blog_repositories.dart';

import '../models/apiResponse/blog/blog_details_response.dart';
import '../models/apiResponse/blog/blog_list_response.dart';
import '../network/error_handler.dart';
import '../network/network_info.dart';
import '../network/web_services/blog_web_services.dart';

class BlogsRepositoryImp extends BlogRepository {
  final NetWorkInfo _netWorkInfo;
  final BlogsWebServices blogsWebServices;

  BlogsRepositoryImp(this._netWorkInfo, this.blogsWebServices);

  /// get blogs list
  @override
  Future<Either<Failure, List<BlogEntity>>> getBlogsList() async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await blogsWebServices.getBlogsList();

        if (response.statusCode == 200) {
          BlogListResponse data = blogsListResponseFromJson(response.data);
          return Right(data.blogItems ?? []);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  /// get blog by id
  @override
  Future<Either<Failure, BlogDetailsEntity>> getBlog(String  slug) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await blogsWebServices.getBlog(slug);

        if (response.statusCode == 200) {
          BlogDetailsResponse data = blogDetailsResponseFromJson(response.data);
          return Right(data.blogData!);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
