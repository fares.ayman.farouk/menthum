import 'package:dartz/dartz.dart';
import 'package:patientapp/data/models/call_logs/call_logs_request.dart';
import 'package:patientapp/data/models/apiRequest/question_requests/question_answers_request.dart';
import 'package:patientapp/data/models/apiResponse/base_response.dart';
import 'package:patientapp/data/models/apiResponse/call_gp/call_grp_response.dart';
import 'package:patientapp/data/models/apiResponse/check_coupon_api_response.dart';
import 'package:patientapp/data/models/apiResponse/question_responses/question_answers_response.dart';
import 'package:patientapp/data/models/apiResponse/question_responses/question_response.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/data/network/web_services/questions_web_services.dart';
import 'package:patientapp/domain/repositories/questions_repositories.dart';
import 'package:patientapp/domain/services/local_db_handler/local_db_handler.dart';

import '../../application/app_preference.dart';
import '../models/apiError/login_api_error.dart';
import '../models/apiError/register_api_error.dart';
import '../network/error_handler.dart';
import '../network/network_info.dart';

class QuestionRepositoriesImp extends QuestionRepositories {
  final QuestionWebServices questionWebServices;
  final NetWorkInfo _netWorkInfo;
  final AppPreferences appPreferences;

  QuestionRepositoriesImp(
    this.questionWebServices,
    this._netWorkInfo,
    this.appPreferences,
  );

  @override
  Future<Either<Failure, QuestionResponse>> getQuestions() async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await questionWebServices.getQuestions();

        if (response.statusCode == 200) {
          return Right(questionResponseFromJson(response.data));
        } else {
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,

            ///todo edit fail
            body: loginApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, QuestionAnswersResponse>> sendQuestionsAnswers(
      List<QuestionAnswers> answers, String? coupon) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await questionWebServices.sendQuestionsAnswers(
            QuestionAnswersRequest(answers: answers, coupon: coupon));

        if (response.statusCode == 200) {
          return Right(QuestionAnswersResponse.fromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,

            /// todo edit it
            body: loginApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, CheckCouponApiResponse>> checkCoupon(
      String coupon) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await questionWebServices.checkCoupon(coupon);

        if (response.statusCode == 200) {
          return Right(checkCouponApiResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
            body: registerApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  /// get call info
  @override
  Future<Either<Failure, CallGpResponse>> getCallInfo(
      int callId, String? coupon) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await questionWebServices.getCallInfo(callId, coupon);

        if (response.statusCode == 200) {
          return Right(callGpResponseFromJson(response.data));
        } else {
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
            body: loginApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  ///send Call Log
  @override
  Future<Either<Failure, BaseResponse>> sendCallLog(
      CallLogsRequest callLogsRequest) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await questionWebServices.sendCallLog(callLogsRequest);

        if (response.statusCode == 200) {
          return Right(baseResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,

            /// todo edit it
            body: null,
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      ///TODO:: save to local data source.
      /// save/update to db.
      LocalDBHandler.addFailedLogToDB(callLogsRequest);
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  ///remove call queue
  @override
  Future<Either<Failure, BaseResponse>> removeCallFromQueue(int callId) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await questionWebServices.removeCallFromQueue(callId);

        if (response.statusCode == 200) {
          return Right(baseResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,

            /// todo edit it
            body: null,
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  ///notify Me
  @override
  Future<Either<Failure, BaseResponse>> notifyMe(int callId) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await questionWebServices.notifyMe(callId);

        if (response.statusCode == 200) {
          return Right(baseResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,

            body: null,
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  ///triedCall
  @override
  Future<Either<Failure, BaseResponse>> triedCall() async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await questionWebServices.triedCall();

        if (response.statusCode == 200) {
          return Right(baseResponseFromJson(response.data));
        } else {
          //data failure
          //return Either left
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,
          ));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      // network failure
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
