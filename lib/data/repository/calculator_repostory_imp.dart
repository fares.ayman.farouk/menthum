import 'package:dartz/dartz.dart';
import 'package:patientapp/data/models/apiResponse/calculation/bmi/BmiResponse.dart';
import 'package:patientapp/data/models/apiResponse/calculation/calories/calories_response.dart';

import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/models/calculation/bmi/bmi_request_entity.dart';
import 'package:patientapp/domain/models/calculation/bmi/bmi_response_entity.dart';
import 'package:patientapp/domain/models/calculation/calories/calories_request_entity.dart';

import '../../application/app_preference.dart';
import '../../domain/models/calculation/pregnancy/pregnancy_response.dart';
import '../../domain/repositories/calculation_repository.dart';
import '../models/apiRequest/calculations/bmi/bmi_request_model.dart';
import '../models/apiRequest/calculations/calories/calories_request_model.dart';
import '../models/apiResponse/calculation/calculation_types_response.dart';
import '../models/apiResponse/calculation/pregnancy_response/pregnancy_response.dart';
import '../network/error_handler.dart';
import '../network/network_info.dart';
import '../network/web_services/calculation_web_services.dart';

class CalculationRepositoryImp implements CalculationRepository {
  final CalculationWebServices calculationWebServices;
  final NetWorkInfo netWorkInfo;
  final AppPreferences appPreferences;

  CalculationRepositoryImp(
      this.netWorkInfo, this.calculationWebServices, this.appPreferences);

  /// calculation list
  @override
  Future<Either<Failure, List<String>>> getCalculationsList() async {
    if (await netWorkInfo.isConnected) {
      try {
        final response = await calculationWebServices.getCalculationsList();

        if (response.statusCode == 200) {
          CalculationTypesResponse res =
              CalculationTypesResponse.fromJson(response.data);
          return Right(res.types ?? []);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  ///  BMI
  @override
  Future<Either<Failure, BmiResponseEntity>> getBmi(
      BmiRequestEntity bmiRequestEntity) async {
    if (await netWorkInfo.isConnected) {
      try {
        final response = await calculationWebServices
            .getBmi(BmiRequestModel.obj(bmiRequestEntity));

        if (response.statusCode == 200) {
          BmiResponse res = BmiResponse.fromJson(response.data);
          return Right(res.bmiModel!);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  /// calories
  @override
  Future<Either<Failure, String>> getCalorie(
      CaloriesRequestEntity caloriesRequestEntity) async {
    if (await netWorkInfo.isConnected) {
      try {
        final response = await calculationWebServices
            .getCalorie(CaloriesRequestModel.obj(caloriesRequestEntity));

        if (response.statusCode == 200) {
          CalorieResponse res = CalorieResponse.fromJson(response.data);
          return Right(res.calories!.basCalorie!);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  ///get Birth Day
  @override
  Future<Either<Failure, PregnancyResponseEntity >> getPregnancyData(int data) async {
    if (await netWorkInfo.isConnected) {
      try {
        final response = await calculationWebServices.getPregnancyData(data);

        if (response.statusCode == 200) {
          PregnancyResponse res = PregnancyResponse.fromJson(response.data);
          return Right(res.pregnancy!);
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
