import 'package:dartz/dartz.dart';
import 'package:patientapp/data/models/apiRequest/laboratory/laboratory_request.dart';
import 'package:patientapp/data/models/apiResponse/laboratory/laboratory_response.dart';
import 'package:patientapp/data/models/apiResponse/tests_response.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/repositories/laboratory_repositories.dart';

import '../../application/app_preference.dart';
import '../models/apiError/login_api_error.dart';
import '../network/error_handler.dart';
import '../network/network_info.dart';
import '../network/web_services/laboratory_web_services.dart';

class LaboratoryRepositoryImp extends LaboratoriesRepositories {
  final NetWorkInfo _netWorkInfo;
  final AppPreferences appPreferences;

  final LaboratoryWebServices _aboratoryWebServices;

  LaboratoryRepositoryImp(
      this._aboratoryWebServices, this._netWorkInfo, this.appPreferences);

  @override
  Future<Either<Failure, LaboratoryResponse>> getLaboratories(
      LaboratoryRequest laboratoryRequest) async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response =
            await _aboratoryWebServices.getLaboratories(laboratoryRequest);

        if (response.statusCode == 200) {
          return Right(laboratoryResponseFromJson(response.data));
        } else {
          return Left(Failure(
            ApiInternalStatus.FAILURE,
            response.statusMessage,

            ///todo edit fail
            body: loginApiErrorResponseFromJson(response.data),
          ));
        }
      } catch (error) {
        print("!!!!!!!!!!!!!!!!!!!!!!!--- ${error.toString()}");
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, TestsResponse>> getAllLabsTests() async {
    if (await _netWorkInfo.isConnected) {
      try {
        final response = await _aboratoryWebServices.getLabsTestsList(1);

        if (response.statusCode == 200) {
          return Right(testsResponseFromJson(response.data));
        } else {
          return Left(
            Failure(
              ApiInternalStatus.FAILURE,
              response.statusMessage,
            ),
          );
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
