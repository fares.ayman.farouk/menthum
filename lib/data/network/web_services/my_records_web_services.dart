import 'dart:io';

import 'package:dio/dio.dart';

import '../../../application/app_preference.dart';
import '../../../application/constants.dart';

class MyRecordsWebServices {
  late Dio dio;
  late final AppPreferences _appPreferences;

  MyRecordsWebServices(this.dio, this._appPreferences);

  Future<Response> getLabResults() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.labResults,
      );
      print(
          "---- ${response.statusCode} , ${response.statusMessage} , ${response.data.toString()}");
      return response;
    } catch (e) {
      print(
          "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${e.toString()}");
      throw (e);
    }
  }

  Future<Response> getRadsResults() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.radsResults,
      );
      print(
          "---- ${response.statusCode} , ${response.statusMessage} , ${response.data.toString()}");
      return response;
    } catch (e) {
      print(
          "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${e.toString()}");
      throw (e);
    }
  }

  Future<Response> getReports(String type) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.getReports+type,
      );
      print(
          "---- ${response.statusCode} , ${response.statusMessage} , ${response.data.toString()}");
      return response;
    } catch (e) {
      print(
          "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${e.toString()}");
      throw (e);
    }
  }


/// upload record file
  Future<Response> uploadRecordFile(File file,String type) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    String fileName = file.path.split('/').last;
    String name = file.path.split('/').last.split('.').first;


    FormData formData = FormData.fromMap({
      "name":name.length>AppConstants.maxStringLength?  name.substring(0, AppConstants.maxStringLength):name,
      "patient_file":
          await MultipartFile.fromFile(file.path, filename: fileName),
    });

    try {
      Response response =
          await dio.post(APIsConstants.uploadRecordReport+type, data: formData);
      print(
          "---- ${response.statusCode} , ${response.statusMessage} , ${response.data.toString()}");
      return response;
    } catch (e) {
      print(
          "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${e.toString()}");
      throw (e);
    }
  }
}
