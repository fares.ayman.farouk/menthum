import 'package:dio/dio.dart';
import 'package:patientapp/data/models/apiRequest/vitals/add_read_request_model.dart';

import '../../../application/app_preference.dart';
import '../../../application/constants.dart';
import '../../models/apiRequest/vitals/reading_request_model.dart';

class VitalWebServices {
  late Dio dio;
  late final AppPreferences _appPreferences;

  VitalWebServices(this.dio, this._appPreferences);

  /// get patient Reading
  Future<Response> getPatientReadings() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.allReadingType,
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  ///addRad

  Future<Response> addRad(AddReadRequestModel addReadRequestEntity) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(APIsConstants.readingsStore,
          data: addReadRequestEntity.toJson());
      return response;
    } catch (e) {
      rethrow;
    }
  }

  /// get patient Reading
  Future<Response> getVitalDetails(
      ReadingRequestModel readingRequestModel) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(APIsConstants.readings,
          queryParameters: readingRequestModel.toJson());
      return response;
    } catch (e) {
      rethrow;
    }
  }
}
