import 'package:dio/dio.dart';

import '../../../application/app_preference.dart';
import '../../../application/constants.dart';

class BlogsWebServices {
  late Dio dio;
  late final AppPreferences _appPreferences;

  BlogsWebServices(this.dio, this._appPreferences);

  Future<Response> getBlogsList() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.getBlogsList,
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<Response> getBlog(String slug) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.getBlog+slug,
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }
}
