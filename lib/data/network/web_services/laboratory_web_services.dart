import 'package:dio/dio.dart';
import 'package:patientapp/application/constants.dart';
import 'package:patientapp/data/models/apiRequest/laboratory/laboratory_request.dart';

import '../../../application/app_preference.dart';

class LaboratoryWebServices {
  late Dio dio;
  late final AppPreferences _appPreferences;

  LaboratoryWebServices(this.dio, this._appPreferences);

  Future<Response> getLaboratories(LaboratoryRequest laboratoryRequest) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(APIsConstants.laboratorySerch,
          queryParameters: laboratoryRequest.toJson());
      print(
          "---- ${response.statusCode} , ${response.statusMessage} , ${response.data.toString()}");
      return response;
    } catch (e) {
      print(
          "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${e.toString()}");
      throw (e);
    }
  }

  Future<Response> getLabsTestsList(int type) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.getTests,
        queryParameters: {"type": type},
      );
      print(
          "---- ${response.statusCode} , ${response.statusMessage} , ${response.data.toString()}");
      return response;
    } catch (e) {
      print(
          "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${e.toString()}");
      throw (e);
    }
  }
}
