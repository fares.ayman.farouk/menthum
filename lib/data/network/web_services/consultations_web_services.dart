import 'dart:io';

import 'package:dio/dio.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/EditMedicalProfileApiRequest.dart';
import 'package:patientapp/data/models/apiRequest/clinic/book_clinic_appointment_req.dart';
import 'package:patientapp/data/models/apiRequest/clinic/clinic_request.dart';

import '../../../application/app_preference.dart';
import '../../../application/constants.dart';

class ConsultationsWebServices {
  late Dio dio;
  late final AppPreferences _appPreferences;

  ConsultationsWebServices(this.dio, this._appPreferences);

  Future<Response> getConsultation(int page) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.getResult + page.toString(),
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> upcomingAppointments() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.upcomingAppointments,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> homePage() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.homePage,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> getSpecialties(String query) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.getSpecialties + query,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> getPendingPrescriptions() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.getPendPrescriptions,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> getPrescriptions() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.getPrescription,
      );
      return response;
    } catch (e) {
      print(
          "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${e.toString()}");
      throw (e);
    }
  }

  Future<Response> getPrescriptionImage(imageUrl) async {
    String? token = await _appPreferences.getParentUserToken();

    try {
      Response response = await Dio().get(
        imageUrl,
        options: Options(
            responseType: ResponseType.bytes,
            headers: {"Authorization": "Bearer ${token.toString()}"}),
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> getClinicProfile(String id) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.clinicProfile + id,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> getLabProfile(String labId) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.laboratory + labId,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> getRadProfile(String radId) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.radiologyBranches + radId,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> getMedicalProfile() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.medicalProfile,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> editMedicalProfile(
      EditMedicalProfileApiRequest medicalProfileApiRequest) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(APIsConstants.saveMedicalProfile,
          data: medicalProfileApiRequest.toJson());
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> getClinics(ClinicRequest request, int page) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(APIsConstants.clinic + page.toString(),
          queryParameters: request.toJson());
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> getGovernorates(String countryId) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;
    dio.options.baseUrl = APIsConstants.baseUrlWithoutV;

    try {
      Response response = await dio.get(
          APIsConstants.getGovernorates,
          queryParameters: {"country_id": countryId});
      dio.options.baseUrl = APIsConstants.baseUrl;

      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> getCities(String governorateId) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    dio.options.baseUrl = APIsConstants.baseUrlWithoutV;
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(APIsConstants.getCities,
          queryParameters: {"governorate_id": governorateId});
      dio.options.baseUrl = APIsConstants.baseUrl;

      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> makeAppointment(
      BookAppointmentReq bookClinicAppointmentReq) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(APIsConstants.makeAppointment,
          data: bookClinicAppointmentReq.toJson());

      return response;
    } catch (e) {
      throw (e);
    }
  }

  /// get visit details
  Future<Response> getVisitDetails(int id) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.getOneResult + id.toString(),
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> getTransactions(int page) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers['Authorization'] = 'Bearer $token';
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      return await dio.get(APIsConstants.getTransactions + page.toString());
    } catch (e) {
      throw (e);
    }
  }

  /// upload pres
  Future<Response> uploadPresFiles(File file) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";

    String fileName = file.path.split('/').last;
    String name = file.path.split('/').last.split('.').first;

    FormData formData = FormData.fromMap({
      "name": name.length > AppConstants.maxStringLength
          ? name.substring(0, AppConstants.maxStringLength)
          : name,
      "patient_file":
          await MultipartFile.fromFile("${file.path}", filename: fileName),
    });

    try {
      Response response =
          await dio.post(APIsConstants.uploadRadPrescription, data: formData);
      return response;
    } catch (e) {
      throw (e);
    }
  }

  /// update pres

  Future<Response> updatePresFiles(File file, String id) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";

    String fileName = file.path.split('/').last;
    FormData formData = FormData.fromMap({
      "name": fileName.split('.').first,
      "prescription_id": id,
      "patient_file":
          await MultipartFile.fromFile(file.path, filename: fileName),
    });

    try {
      Response response =
          await dio.post(APIsConstants.updatePrescriptionFile, data: formData);
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> deletePendingPrescription(String id) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";

    FormData formData = FormData.fromMap(
      {
        "prescription_id": id,
      },
    );

    try {
      Response response =
          await dio.post(APIsConstants.deletePrescription, data: formData);
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> orderOnlinePrescription(String id) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";

    FormData formData = FormData.fromMap(
      {
        "prescription_id": id,
      },
    );

    try {
      Response response =
          await dio.post(APIsConstants.orderOnline, data: formData);
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> payment(int amount) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response =
          await dio.post(APIsConstants.payment, data: {"amount": amount});
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> getNanoClinicsLocations() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(APIsConstants.nanoClinicsInfo);
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> getPlans() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(APIsConstants.getPlans);
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> subscribe(int planId) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response =
          await dio.post(APIsConstants.subscribe, data: {"plan_id": planId});
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> renew(int planId, int subscriptionId) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(APIsConstants.renew,
          data: {"plan_id": planId, "subscription_id": subscriptionId});
      return response;
    } catch (e) {
      throw (e);
    }
  }
}
