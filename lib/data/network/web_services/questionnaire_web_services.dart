import 'package:dio/dio.dart';

import '../../../application/app_preference.dart';
import '../../../application/constants.dart';
import '../../models/apiRequest/questionnaire_request/answer_questionnaire_request.dart';

class QuestionnaireWebServices {
  late Dio dio;
  late final AppPreferences _appPreferences;

  QuestionnaireWebServices(this.dio, this._appPreferences);

  /// questionnaire items list
  Future<Response> getQuestionnairesItems() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.questionnaire,
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  /// questions
  Future<Response> getQuestionnaireQuestions(String slug) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        "${APIsConstants.questionnaire}/$slug",
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  /// save Answers
  Future<Response> saveAnswers(
      AnswerQuestionnaireRequest answerQuestions, String slug) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        "${APIsConstants.questionnaire}/$slug/answers",
        data: answerQuestions.toJson(),
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }
}
