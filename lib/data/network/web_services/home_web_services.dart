import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

import '../../../application/app_preference.dart';
import '../../../application/constants.dart';

class HomeWebServices{
  late Dio dio;
  late final AppPreferences _appPreferences;

  HomeWebServices(this.dio, this._appPreferences);

  /// get GpWorking Shifts
  Future<Response> getGpWorkingShifts() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;
    try {
      Response response = await dio.get(
        APIsConstants.getGpWorkingShifts,
      );
      debugPrint("!!!!!!!!!!!!!!!!!!!!!!!!!!!!getGpWorkingShifts");
      return response;
    } catch (e) {
      throw (e);
    }
  }


}