import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/application/constants.dart';
import 'package:patientapp/data/models/apiRequest/menstrual_list/add_new_cycle_model.dart';

import '../../models/apiRequest/menstrual_list/edit_cycle_model.dart';

class MenstrualWebServices {
  late Dio dio;
  late final AppPreferences _appPreferences;

  MenstrualWebServices(this.dio, this._appPreferences);

  Future<Response> getMenstrualCycle() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(APIsConstants.menstrual);
      log("---- ${response.statusCode} , ${response.statusMessage} , ${response.data.toString()}");
      return response;
    } catch (e) {
      log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${e.toString()}");
      rethrow;
    }
  }

  Future<Response> addMenstrualCycle(AddNewCycleModel addNewCycleModel) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(APIsConstants.addMenstrual,
          data: addNewCycleModel.toJson());
      log("---- ${response.statusCode} , ${response.statusMessage} , ${response.data.toString()}");
      return response;
    } catch (e) {
      log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${e.toString()}");
      rethrow;
    }
  }

  Future<Response> editMenstrualCycle(EditCycleModel editCycleModel) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.put(
          "${APIsConstants.menstrual}/${editCycleModel.id}/update",
          data: editCycleModel.toJson());
      log("---- ${response.statusCode} , ${response.statusMessage} , ${response.data.toString()}");
      return response;
    } catch (e) {
      log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${e.toString()}");
      rethrow;
    }
  }
}
