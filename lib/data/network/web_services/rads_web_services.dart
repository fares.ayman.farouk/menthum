import 'package:dio/dio.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/application/constants.dart';
import 'package:patientapp/data/models/apiRequest/rads_request/rads_request.dart';

class RadsWebServices {
  late Dio dio;
  late final AppPreferences _appPreferences;

  RadsWebServices(this.dio, this._appPreferences);

  Future<Response> getListOfRads(RadsRequest request) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.getRadiologies,
        queryParameters: request.toJson(),
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> getRadsList(int type) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.getTests,
        queryParameters: {"type": type},
      );
      print(
          "---- ${response.statusCode} , ${response.statusMessage} , ${response.data.toString()}");
      return response;
    } catch (e) {
      print(
          "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${e.toString()}");
      throw (e);
    }
  }
}
