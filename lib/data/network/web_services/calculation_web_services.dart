import 'package:dio/dio.dart';
import 'package:patientapp/data/models/apiRequest/calculations/calories/calories_request_model.dart';

import '../../../application/app_preference.dart';
import '../../../application/constants.dart';
import '../../models/apiRequest/calculations/bmi/bmi_request_model.dart';

class CalculationWebServices {
  late Dio dio;
  late final AppPreferences _appPreferences;

  CalculationWebServices(this.dio, this._appPreferences);

  /// get calculation list
  Future<Response> getCalculationsList() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.calculationTypes,
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  /// get Bmi
  Future<Response> getBmi(BmiRequestModel bmiRequestModel) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(APIsConstants.calculationBmi,
          data: bmiRequestModel.toJson());
      return response;
    } catch (e) {
      rethrow;
    }
  }

  /// get calories
  Future<Response> getCalorie(CaloriesRequestModel caloriesRequestModel) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.calculationCalories,
        data: caloriesRequestModel.toJson(),
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  /// get calories
  Future<Response> getPregnancyData(int date) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.calculationPregnancy,
        data: {"first_day_last_period": date},
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }
}
