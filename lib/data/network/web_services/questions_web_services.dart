import 'package:dio/dio.dart';
import 'package:patientapp/data/models/apiRequest/question_requests/question_answers_request.dart';

import '../../../application/app_preference.dart';
import '../../../application/constants.dart';
import '../../models/call_logs/call_logs_request.dart';

class QuestionWebServices {
  late Dio dio;
  late final AppPreferences _appPreferences;

  QuestionWebServices(this.dio, this._appPreferences);

  Future<Response> getQuestions() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.getQuestions,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> sendQuestionsAnswers(QuestionAnswersRequest request) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.sendQuestionAnswers,
        data: request.toJson(),
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> checkCoupon(String coupon) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.checkCoupon + coupon,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  ///  getCall info
  Future<Response> getCallInfo(int callId, String? coupon) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    String url = "${APIsConstants.callGp}$callId";
    if (coupon != null) {
      url = "${APIsConstants.callGp}$callId&code=$coupon";
    }

    try {
      Response response = await dio.get(url);
      print(
          "---- ${response.statusCode} , ${response.statusMessage} , ${response.data.toString()}");
      return response;
    } catch (e) {
      print(
          "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${e.toString()}");
      throw (e);
    }
  }

  /// send call logs
  Future<Response> sendCallLog(CallLogsRequest callLogsRequest) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.createCallLog,
        data: callLogsRequest.toJson(),
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  /// remove Call From Queue
  Future<Response> removeCallFromQueue(int callId) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.removeCallFromQueue,
        queryParameters: {"call_id": callId},
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  /// notify Me
  Future<Response> notifyMe(int callId) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.notifyMe,
        queryParameters: {"call_id": callId},
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  /// Tried Call
  Future<Response> triedCall() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.triedCall,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }
}
