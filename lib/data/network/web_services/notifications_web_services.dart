import 'package:dio/dio.dart';

import '../../../application/app_preference.dart';
import '../../../application/constants.dart';

class NotificationWebServices {
  late Dio dio;
  late final AppPreferences _appPreferences;

  NotificationWebServices(this.dio, this._appPreferences);

  Future<Response> getListNotification() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.listNotifications,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> setNotificationRead(int id) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        "${APIsConstants.setNotificationRead}$id",
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }


}
