import 'package:dio/dio.dart';

import '../../../../application/app_preference.dart';
import '../../../../application/constants.dart';
import '../../../models/apiRequest/diabetes/pediatric_history_request.dart';
import '../../../models/apiRequest/pediatric/enroll_pediatric_request.dart';
import '../../../models/apiRequest/pediatric/pediatric_calculate_request_model.dart';
import '../../../models/apiRequest/pediatric/pediatric_done_model.dart';

class PediatricWebService {
  late Dio dio;
  late final AppPreferences _appPreferences;

  PediatricWebService(this.dio, this._appPreferences);

  /// pediatric Home
  Future<Response> pediatricHome(String id) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.pediatricHome + id,
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  /// today
  Future<Response> todayAction(String id) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.pediatricToday + id,
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  /// enroll
  Future<Response> enroll(EnrollRequestModel enrollPediatricModel) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.pediatricEnroll,
        data: enrollPediatricModel.toJson(),
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  ///done
  Future<Response> done(PediatricDoneModel doneModel) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.pediatricDone,
        data: doneModel.toJson(),
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  ///calculate Weight
  Future<Response> pediatricCalculate(
      PediatricCalculateRequestModel calculateModel) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.pediatricCalculate,
        data: calculateModel.toJson(),
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  /// history
  Future<Response> historyAction(
      PediatricHistoryRequestModel pediatricHistoryRequestModel) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;
    try {
      Response response = await dio.get(
        APIsConstants.pediatricHistory,
        queryParameters: pediatricHistoryRequestModel.toJson(),
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }
}
