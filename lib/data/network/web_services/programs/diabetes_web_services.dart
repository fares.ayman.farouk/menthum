import 'package:dio/dio.dart';
import 'package:patientapp/data/models/apiRequest/diabtes_done_model.dart';

import '../../../../application/app_preference.dart';
import '../../../../application/constants.dart';
import '../../../models/apiRequest/diabetes/enroll_diabetes_model.dart';

class DiabetesWebService {
  late Dio dio;
  late final AppPreferences _appPreferences;

  DiabetesWebService(this.dio, this._appPreferences);

  Future<Response> todayAction() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.diabetesToday,
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<Response> historyAction(int nextPage) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.diabetesIndex + nextPage.toString(),
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<Response> enroll(EnrollDiabetesModel enrollDiabetesModel) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.diabetesEnroll,
        data: enrollDiabetesModel.toJson(),
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<Response> done(DiabetesDoneModel doneModel) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.diabetesDone,
        data: doneModel.toJson(),
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }
}
