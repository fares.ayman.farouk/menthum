import 'package:dio/dio.dart';

import '../../../application/app_preference.dart';
import '../../../application/constants.dart';

class ProgramsWebService {
  late Dio dio;
  late final AppPreferences _appPreferences;

  ProgramsWebService(this.dio, this._appPreferences);

  Future<Response> getProgramsList() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.programs,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }
}
