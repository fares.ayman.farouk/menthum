import 'package:dio/dio.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/EditMedicalProfileApiRequest.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/login_with_provider_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/register_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/reset_password_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/verify_request.dart';

import '../../../application/app_preference.dart';
import '../../../application/constants.dart';
import '../../models/apiRequest/auth_requests/EditProfileApiRequest.dart';
import '../../models/apiRequest/auth_requests/UpadePasswordApiRequest.dart';
import '../../models/apiRequest/auth_requests/forget_password_request.dart';
import '../../models/apiRequest/auth_requests/login_request.dart';
import '../../models/apiRequest/auth_requests/resend_request.dart';
import '../../models/apiRequest/dependant_account_request/add_dependant_account_request.dart';

class AuthWebServices {
  late Dio dio;
  late final AppPreferences _appPreferences;

  AuthWebServices(this.dio, this._appPreferences);

  Future<Response> login(LoginRequest request) async {
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.loginRoute,
        data: request.toJson(),
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  /// change Phone
  Future<Response> changePhone(String phone, String countryCode) async {
    Map<String, dynamic> data = {
      "mobile": phone,
      "country_code": countryCode,
    };

    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";

    try {
      Response response = await dio.post(
        APIsConstants.changeMobileNumber,
        data: data,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> register(RegisterRequest request) async {
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.registerRoute,
        data: request.toJson(),
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> resendOTP(ResendOtpRequest request) async {
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.resendOtpRoute,
        data: request.toJson(),
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> verifyCode(VerifyRequest request) async {
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.verifyURL,
        data: request.toJson(),
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> resetPassword(ResetPasswordRequest request) async {
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.resetPassword,
        data: request.toJson(),
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  /// forget password
  Future<Response> forgetPassword(ForgetPasswordRequest request) async {
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.forgetPassword,
        data: request.toJson(),
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> logout() async {
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";

    try {
      Response response = await dio.post(APIsConstants.logout);
      return response;
    }
    // on DioError catch (e) {
    //   print(
    //       "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${e.response?.statusCode}  - ");
    //   return e.response!;
    //   //throw (e);
    // }
    catch (e) {
      throw (e);
    }
  }

  Future<Response> loginWithProvider(LoginWithProviderRequest request) async {
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.loginWithProvider,
        data: request.toJson(),
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> termPage(String pageURLType) async {
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.termsPage + pageURLType,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  /// get user data
  Future<Response> getUserProfileData({String? qrToken}) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = qrToken == null
        ? "Bearer $token"
        : "Bearer $qrToken"; //"Bearer ${qrToken ?? token}";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.profile,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  /// edit user data
  Future<Response> editUserProfile(
      EditProfileApiRequest editProfileApiRequest) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";

    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(APIsConstants.editProfile,
          data: editProfileApiRequest.toJson());
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> getMedicalProfile() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";

    try {
      Response response = await dio.get(
        APIsConstants.medicalProfile,
      );
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> editMedicalProfile(
      EditMedicalProfileApiRequest medicalProfileApiRequest) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";

    try {
      Response response = await dio.post(APIsConstants.saveMedicalProfile,
          data: medicalProfileApiRequest.toJson());
      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> updatePassword(
      UpdatePasswordApiRequest updatePasswordApiRequest) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = await _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(APIsConstants.updatePassword,
          data: updatePasswordApiRequest.toJson());

      return response;
    } catch (e) {
      throw (e);
    }
  }

  Future<Response> setPassword(String password, String confirmPassword) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(APIsConstants.setPassword, data: {
        "password": password,
        "password_confirmation": confirmPassword,
      });
      return response;
    } catch (e) {
      rethrow;
    }
  }

  /// dependant account
  // add dependant account
  Future<Response> addDependantAccount(
      AddDependantAccountRequest accountRequest) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.post(
        APIsConstants.addDependantAccount,
        data: accountRequest.toJson(),
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  // get dependant account
  Future<Response> getDependantAccounts() async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.getDependantAccounts,
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  // show dependant account
  Future<Response> showDependantAccount(String id) async {
    String? token = await _appPreferences.getParentUserToken();
    dio.options.headers["Authorization"] = "Bearer $token";
    String? lang = _appPreferences.getAppLanguage();
    dio.options.headers["language"] = lang;

    try {
      Response response = await dio.get(
        APIsConstants.showDependantAccount + id,
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }
}
