import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../../application/app_preference.dart';
import '../../application/constants.dart';
import 'dart:io' as getPlatform;

import '../../application/environment.dart';

const String APPLICATION_JSON = "application/json";
const String CONTENT_TYPE = "content-type";
const String ACCEPT = "accept";
const String AUTHORIZATION = "Authorization";
const String DEFAULT_LANGUAGE = "language";
const String PLATFORM = "platform";
const String APP_VERSION = "app-version";

class DioFactory {
  AppPreferences _appPreferences;

  DioFactory(this._appPreferences);

  Future<Dio> getDio() async {
    Dio dio = Dio();

    String language = _appPreferences.getAppLanguage();

    String platform =
        getPlatform.Platform.isAndroid == true ? "android" : "ios";

    Map<String, String> headers = {
      CONTENT_TYPE: APPLICATION_JSON,
      ACCEPT: APPLICATION_JSON,
      DEFAULT_LANGUAGE: language,
      PLATFORM: platform,
      APP_VERSION: appVersion
    };

    dio.options = BaseOptions(
        baseUrl: APIsConstants.baseUrl,
        headers: headers,
        receiveTimeout: APIsConstants.apiTimeOut,
        sendTimeout: APIsConstants.apiTimeOut);

    if (!kReleaseMode) {
      // its debug mode so print app logs
      dio.interceptors.add(PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseHeader: true,
          error: true,
          responseBody: true,
          compact: true));
    }

    return dio;
  }
}
