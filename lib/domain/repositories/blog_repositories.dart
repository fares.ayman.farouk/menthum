import 'package:dartz/dartz.dart';
import 'package:patientapp/domain/models/blog_details_entity.dart';
import 'package:patientapp/domain/models/blog_entity.dart';

import '../../data/network/failure.dart';

abstract class BlogRepository {
  Future<Either<Failure, List<BlogEntity>>> getBlogsList();
  Future<Either<Failure, BlogDetailsEntity>> getBlog(String  slug);
}
