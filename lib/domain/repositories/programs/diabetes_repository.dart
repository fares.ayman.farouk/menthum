import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/models/diabetes/diabetes_data.dart';

import '../../models/diabetes/diabetes_done.dart';
import '../../models/diabetes/diabetes_history.dart';
import '../../models/diabetes/enroll_diabetes.dart';

abstract class DiabetesRepository {
  Future<Either<Failure, DiabetesData>> today();

  Future<Either<Failure, DiabetesHistory>> history(int nextPage);

  Future<Either<Failure, Response>> enroll(EnrollDiabetes enrollDiabetes);

  Future<Either<Failure, Response>> done(DiabetesDone diabetesDone);
}
