import 'package:dartz/dartz.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/models/programs/program_entity.dart';

abstract class ProgramsRepository {
  Future<Either<Failure,List<Program>>> programsList();
}