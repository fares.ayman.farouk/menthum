import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

import '../../../data/network/failure.dart';
import '../../models/pediatric/enroll_entity.dart';
import '../../models/pediatric/history_entity.dart';
import '../../models/pediatric/history_request.dart';
import '../../models/pediatric/pediatric_calculate_request_Entity.dart';
import '../../models/pediatric/pediatric_data_entity.dart';
import '../../models/pediatric/pediatric_done.dart';
import '../../models/pediatric/pediatric_head_response.dart';
import '../../models/pediatric/pediatric_home.dart';
import '../../models/pediatric/pediatric_weight_response.dart';

abstract class PediatricRepository {
  Future<Either<Failure, List<PediatricDataEntity>>> today(String id);

  Future<Either<Failure, List<PediatricHomeEntity>>> pediatricHome(String id);

  Future<Either<Failure, Response>> enroll(
      EnrollRequestEntity enrollRequestEntity);

  Future<Either<Failure, Response>> done(
      PediatricDoneEntity pediatricDoneEntity);

  Future<Either<Failure, PediatricWeightResponseEntity>> calculateWeight(
      PediatricCalculateRequestEntity pediatricCalculateRequestEntity);

  Future<Either<Failure, PediatricHeadResponseEntity>> calculateHead(
      PediatricCalculateRequestEntity pediatricCalculateRequestEntity);

  Future<Either<Failure, PediatricHistoryEntity>> history(
    PediatricHistoryRequest pediatricHistoryRequest,
  );
}
