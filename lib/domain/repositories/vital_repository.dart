import 'package:dartz/dartz.dart';
import 'package:patientapp/domain/models/vitals/response/vital_details_entity.dart';
import 'package:patientapp/domain/models/vitals/response/vital_entity.dart';

import '../../data/network/failure.dart';
import '../models/vitals/request/add_read_request.dart';
import '../models/vitals/request/reading_request.dart';

abstract class VitalRepository {
  Future<Either<Failure, List<VitalEntity>>> getPatientReadings();

  Future<Either<Failure, String>> addRead(AddReadRequestEntity vitalEntity);

  Future<Either<Failure, List<VitalDetailsEntity>>> getVitalDetails(ReadingRequestEntity readingRequestEntity);
}
