import 'package:dartz/dartz.dart';
import 'package:patientapp/data/models/apiRequest/rads_request/rads_request.dart';
import 'package:patientapp/data/models/apiResponse/rads_response/rads_response.dart';
import 'package:patientapp/data/models/apiResponse/tests_response.dart';
import 'package:patientapp/data/network/failure.dart';

abstract class RadsRepositories {
  Future<Either<Failure, RadsResponse>> getListOfRads(RadsRequest radsRequest);

  Future<Either<Failure, TestsResponse>> getAllRadsTests();
}
