import 'package:dartz/dartz.dart';
import 'package:patientapp/data/models/apiRequest/laboratory/laboratory_request.dart';
import 'package:patientapp/data/models/apiResponse/laboratory/laboratory_response.dart';
import 'package:patientapp/data/models/apiResponse/tests_response.dart';

import '../../data/network/failure.dart';

abstract class LaboratoriesRepositories {
  Future<Either<Failure, LaboratoryResponse>> getLaboratories(
      LaboratoryRequest laboratoryRequest);

  Future<Either<Failure, TestsResponse>> getAllLabsTests();
}
