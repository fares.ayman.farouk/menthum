import 'package:dartz/dartz.dart';
import 'package:patientapp/domain/models/calculation/calories/calories_request_entity.dart';

import '../../data/network/failure.dart';
import '../models/calculation/bmi/bmi_request_entity.dart';
import '../models/calculation/bmi/bmi_response_entity.dart';
import '../models/calculation/pregnancy/pregnancy_response.dart';

abstract class CalculationRepository {
  Future<Either<Failure, List<String>>> getCalculationsList();

  Future<Either<Failure, BmiResponseEntity>> getBmi(
    BmiRequestEntity bmiRequestEntity,
  );

  Future<Either<Failure, String>> getCalorie(
    CaloriesRequestEntity caloriesRequestEntity,
  );

  Future<Either<Failure, PregnancyResponseEntity>> getPregnancyData(int data);
}
