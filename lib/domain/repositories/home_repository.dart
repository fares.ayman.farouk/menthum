import 'package:dartz/dartz.dart';
import 'package:patientapp/data/models/apiResponse/home_response/gp_working_shifts_response.dart';
import 'package:patientapp/data/network/failure.dart';

abstract class HomeRepository {
  Future<Either<Failure, GPWorkingShiftResponse>> getGpWorkingShifts();
}
