import 'package:dartz/dartz.dart';
import 'package:patientapp/domain/models/questionnaire/questionnaire_entity.dart';
import 'package:patientapp/domain/models/questionnaire/questionnaire_item_entity.dart';

import '../../data/network/failure.dart';
import '../models/questionnaire/answer_questionnaire_entity.dart';
import '../models/questionnaire/questionnaire_result_entity.dart';

abstract class QuestionnaireRepository{
  Future<Either<Failure, List<QuestionnaireItemEntity>>> getQuestionnairesItems();
  Future<Either<Failure, List<QuestionnaireQuestionEntity>>> getQuestionnaireQuestions(String questionnaireSlug);

  Future<Either<Failure, QuestionnaireResultEntity>> saveAnswers(AnswerQuestionnaireEntity answerQuestionnaireEntity,String slug);

}