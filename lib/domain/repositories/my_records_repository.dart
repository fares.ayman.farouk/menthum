import 'dart:io';

import 'package:dartz/dartz.dart';
import '../../data/models/apiResponse/base_response.dart';
import '../../data/models/apiResponse/my_records_response/lab_results_response.dart';
import '../../data/network/failure.dart';

abstract class MyRecordsRepositories {
  Future<Either<Failure, MyRecordResultsResponse>> getLabResults();

  Future<Either<Failure, MyRecordResultsResponse>> getRadsResults();

  Future<Either<Failure, MyRecordResultsResponse>> getTikshifResults(String type);

  Future<Either<Failure, MyRecordResultsResponse>> getSpecialistResults(String type);

  Future<Either<Failure, BaseResponse>> uploadRecordFile(File file,String type);

}
