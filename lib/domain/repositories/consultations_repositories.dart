import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:patientapp/data/models/apiRequest/clinic/book_clinic_appointment_req.dart';
import 'package:patientapp/data/models/apiRequest/clinic/clinic_request.dart';
import 'package:patientapp/data/models/apiResponse/clinic/book_clinic_appointment_response.dart';
import 'package:patientapp/data/models/apiResponse/clinic/clinic_response.dart';
import 'package:patientapp/data/models/apiResponse/laboratory/lab_profile_response.dart';
import 'package:patientapp/data/models/apiResponse/location_filter/city_response.dart';
import 'package:patientapp/data/models/apiResponse/location_filter/governorate_response.dart';
import 'package:patientapp/data/models/apiResponse/plans_response.dart';
import 'package:patientapp/data/models/apiResponse/rads_response/rad_profile_response.dart';
import 'package:patientapp/data/models/apiResponse/specialties_response.dart';
import 'package:patientapp/data/models/apiResponse/consultations_responses/consultations_response.dart';
import 'package:patientapp/data/models/apiResponse/tikshif_locations/nano_clinic_response.dart';
import 'package:patientapp/data/models/apiResponse/tranactions_api_response.dart';
import 'package:patientapp/data/models/apiResponse/upcoming_response.dart';

import '../../data/models/apiResponse/base_response.dart';
import '../../data/models/apiResponse/consultations_responses/clinic_profile_response.dart';
import '../../data/models/apiResponse/homePageApiResponse.dart';
import '../../data/models/apiResponse/prescriptions_response/digital_prescriptions_response.dart';
import '../../data/models/apiResponse/prescriptions_response/pending_prescriptions_response.dart';
import '../../data/models/apiResponse/profile/paymentApiResponse.dart';
import '../../data/models/apiResponse/visit_detail_response/visitDetailResponse.dart';
import '../../data/network/failure.dart';

abstract class ConsultationsRepositories {
  Future<Either<Failure, ConsultationsResponse>> getConsultations(int page);

  Future<Either<Failure, UpcomingAppointmentsResponse>> upcomingAppointments();

  Future<Either<Failure, HomePageApiResponse>> homePage();

  Future<Either<Failure, SpecialtiesResponse>> getSpecialties(String query);

  Future<Either<Failure, PendingPrescriptionsApiResponse>> getPrescriptions();

  Future<Either<Failure, DigitalPrescriptionResponse>>
      getDigitalPrescriptions();

  Future<Either<Failure, List<int>>> getPrescriptionsImage(String imagePath);

  Future<Either<Failure, ClinicsResponse>> getClinics(
      ClinicRequest request, int page);

  Future<Either<Failure, GovernorateResponse>> getGovernorates(
      String countryId);

  Future<Either<Failure, CityResponse>> getCities(String governorateId);

  Future<Either<Failure, ClinicProfileResponse>> getClinicProfile(String id);

  Future<Either<Failure, BookAppointmentResponse>> makeAppointment(
      BookAppointmentReq bookAppointmentReq);

  Future<Either<Failure, LabProfileResponse>> getLabProfile(String labId);

  Future<Either<Failure, RadProfileResponse>> getRadProfile(String radId);

  Future<Either<Failure, VisitDetailResponse>> getVisitDetails(int id);

  Future<Either<Failure, TransactionApiResponse>> getTransactions(int page);

  Future<Either<Failure, BaseResponse>> uploadPresFiles(File file);

  Future<Either<Failure, BaseResponse>> deletePendingPrescription(String id);

  Future<Either<Failure, BaseResponse>> orderOnline(String prescriptionId);

  Future<Either<Failure, BaseResponse>> updatePresFiles(File file, String id);

  Future<Either<Failure, PaymentApiResponse>> payment(int amount);

  Future<Either<Failure, NanoClinicResponse>> getNanoClinicsLocations();

  Future<Either<Failure, PlansResponse>> getPlans();

  Future<Either<Failure, PaymentApiResponse>> subscribe(int planId);

  Future<Either<Failure, PaymentApiResponse>> renew(
      int planId, int subscriptionId);
}
