import 'package:dartz/dartz.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/forget_password_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/login_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/login_with_provider_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/register_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/reset_password_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/verify_request.dart';
import 'package:patientapp/data/models/apiResponse/TermsPageApiResponse.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/login_api_response.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/logout_response.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/register_response.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/reset_password_response.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/verify_response.dart';
import 'package:patientapp/data/models/apiResponse/base_response.dart';

import '../../data/models/apiRequest/auth_requests/EditMedicalProfileApiRequest.dart';
import '../../data/models/apiRequest/auth_requests/EditProfileApiRequest.dart';
import '../../data/models/apiRequest/auth_requests/UpadePasswordApiRequest.dart';
import '../../data/models/apiRequest/auth_requests/resend_request.dart';
import '../../data/models/apiRequest/dependant_account_request/add_dependant_account_request.dart';
import '../../data/models/apiResponse/auth_response/forget_password_response.dart';
import '../../data/models/apiResponse/auth_response/resend_response.dart';
import '../../data/models/apiResponse/dependant_account_responses/get_dependant_accounts_response.dart';
import '../../data/models/apiResponse/profile/medicalProfileApiResponse.dart';
import '../../data/network/failure.dart';

abstract class AuthRepository {
  Future<Either<Failure, LoginResponse>> login(LoginRequest loginRequest);

  Future<Either<Failure, LoginResponse>> loginWithProvider(
      LoginWithProviderRequest loginRequest);

  Future<Either<Failure, RegisterResponse>> register(
      RegisterRequest registerRequest);

  Future<Either<Failure, TermsPageApiResponse>> termsPage(String url);

  Future<Either<Failure, VerifyResponse>> verify(VerifyRequest verifyRequest);

  Future<Either<Failure, ResendOtpResponse>> resendOTP(
      ResendOtpRequest resendOtpRequest);

  Future<Either<Failure, ForgetPasswordResponse>> forgetPassword(
      ForgetPasswordRequest forgetPasswordRequest);

  Future<Either<Failure, ResetPasswordResponse>> resetPassword(
      ResetPasswordRequest resetPasswordRequest);

  Future<Either<Failure, LoginResponse>> setPassword(
      {required String password, required String confirmPassword});

  Future<Either<Failure, LogoutResponse>> logout();

  Future<Either<Failure, LoginResponse>> changePhone(
      String phone, String countryCode);

  Future<Either<Failure, LoginResponse>> getUserProfileData({String? token});

  Future<Either<Failure, BaseResponse>> updatePassword(
      UpdatePasswordApiRequest updatePasswordApiRequest);

  Future<Either<Failure, LoginResponse>> editUserProfile(
      EditProfileApiRequest editProfileApiRequest);

  Future<Either<Failure, MedicalProfileApiResponse>> getMedicalProfile();

  Future<Either<Failure, MedicalProfileApiResponse>> editMedicalProfile(
      EditMedicalProfileApiRequest medicalProfileApiRequest);

  /// dependant account

  //  Get Dependant Accounts
  Future<Either<Failure, GetDependantAccountsResponse>> getDependantAccounts();

  // show dependant account data
  Future<Either<Failure, LoginResponse>> showDependantAccount(String id);

  //add dependant account
  Future<Either<Failure, LoginResponse>> addDependantAccount(
      AddDependantAccountRequest dependantAccountRequest);

  //add dependant account
  int addDependant(AddDependantAccountRequest dependantAccountRequest) {
     int x=100;
    print("Hello");
    return x;
  }
}
