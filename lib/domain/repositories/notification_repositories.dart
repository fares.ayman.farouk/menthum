

import 'package:dartz/dartz.dart';
import 'package:patientapp/data/models/apiResponse/base_response.dart';

import '../../data/models/apiResponse/notification_list_response/notification_list_response.dart';
import '../../data/network/failure.dart';

abstract class NotificationRepositories {
  Future<Either<Failure, NotificationResponse >> getListNotifications();
  Future<Either<Failure, BaseResponse>> setNotificationRead(int id);
}
