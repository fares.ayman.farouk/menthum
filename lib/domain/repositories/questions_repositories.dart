import 'package:dartz/dartz.dart';
import 'package:patientapp/data/models/apiResponse/check_coupon_api_response.dart';
import '../../data/models/call_logs/call_logs_request.dart';
import '../../data/models/apiRequest/question_requests/question_answers_request.dart';
import '../../data/models/apiResponse/base_response.dart';
import '../../data/models/apiResponse/call_gp/call_grp_response.dart';
import '../../data/models/apiResponse/question_responses/question_answers_response.dart';
import '../../data/models/apiResponse/question_responses/question_response.dart';
import '../../data/network/failure.dart';

abstract class QuestionRepositories{
  Future<Either<Failure, QuestionResponse>> getQuestions();
  Future<Either<Failure, QuestionAnswersResponse>> sendQuestionsAnswers(List<QuestionAnswers> answers, String ? coupon);
  Future<Either<Failure, CallGpResponse>> getCallInfo(int callId, String? coupon);
  Future<Either<Failure, BaseResponse>> sendCallLog(CallLogsRequest callLogsRequest);
  Future<Either<Failure, CheckCouponApiResponse>> checkCoupon(String coupon);
  Future<Either<Failure,BaseResponse>> removeCallFromQueue(int callId);
  Future<Either<Failure,BaseResponse>> notifyMe(int callId);
  Future<Either<Failure,BaseResponse>> triedCall();
}