import 'package:dartz/dartz.dart';
import 'package:patientapp/data/models/apiResponse/menstrual_cycle/menstrual_cycle_model.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/models/menstrual_cycle/menstrual_cycle.dart';

import '../models/menstrual_cycle/add_new_cycle.dart';
import '../models/menstrual_cycle/edit_cycle.dart';

abstract class MenstrualRepository {
  Future<Either<Failure, List<MenstrualCycle>>> getMenstrualCycle();

  Future<Either<Failure, MenstrualCycleModel>> addMenstrualCycle(
      AddNewCycle addNewCycle);

  Future<Either<Failure, MenstrualCycleModel>> editMenstrualCycle(
      EditCycle editCycle);
}
