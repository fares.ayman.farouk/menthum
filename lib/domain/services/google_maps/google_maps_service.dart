import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:patientapp/presentation/resources/assets_manager.dart';
import 'package:permission_handler/permission_handler.dart'
    as PermissionHandler;

class GoogleMapsServices {
  static BitmapDescriptor? tikshifSelectedMarker;
  static BitmapDescriptor? tikshifNotSelectedMarker;
  static LocationData? currentLocation;

  static initializeIcons() async {
    tikshifNotSelectedMarker ??= await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(size: Size(100, 100)),
        ImageAssets.tikshifNotSelectedMarker);
    tikshifSelectedMarker ??= await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(size: Size(100, 100)),
        ImageAssets.tikshifSelectedMarker);
  }

  static listenForPermissionStatus() async {
    // PermissionHandler.PermissionStatus locationPermission =
    //     await PermissionHandler.Permission.location.request();
    // if (locationPermission.isDenied) {
    //   listenForPermissionStatus();
    // } else if (locationPermission.isPermanentlyDenied) {
    //   Fluttertoast.showToast(msg: AppStrings.permissionError.tr());
    //   print("--Permission location: ${PermissionHandler.Permission.location.status.isPermanentlyDenied}");
    //   //PermissionHandler.openAppSettings();
    // }
    // currentLocation = await Location().getLocation();
    // print("**Location Permission: ${await locationPermission.isPermanentlyDenied}");
    // print("**Location Permission: ${await PermissionHandler.Permission.locationAlways.status.isGranted}");
    // print("**Location Permission: ${await PermissionHandler.Permission.locationWhenInUse.status.isGranted}");
    // print("**Location Permission: ${await PermissionHandler.Permission.accessMediaLocation.status.isGranted}");
    Location location = await Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      print(
          "**Location Permission: ${_permissionGranted == PermissionStatus.denied}");
      _permissionGranted = await Location().requestPermission();
      // _serviceEnabled = await location.requestService();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      } else if (_permissionGranted == PermissionStatus.deniedForever) {
        PermissionHandler.openAppSettings();
      }
    }
    currentLocation = await location.getLocation();
  }
}
