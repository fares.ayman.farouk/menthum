import 'dart:developer';

import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';

import '../../../main.dart';

class DynamicLinkService {
  Future handleDynamicLinks() async {
    // 1. Get the initial dynamic link if the app is opened with a dynamic link
    log("DynamicLinkService handleDynamicLinks");
    final PendingDynamicLinkData? data =
        await FirebaseDynamicLinks.instance.getInitialLink();

    // 2. handle link that has been retrieved
    if (data != null) _handleDeepLink(data);

    // 3. Register a link callback to fire if the app is opened up from the background
    // using a dynamic link.
    FirebaseDynamicLinks.instance.onLink.listen((dynamicLink) {
      _handleDeepLink(dynamicLink);
    });
  }

  void _handleDeepLink(PendingDynamicLinkData data) {
    final Uri deepLink = data.link;
    if (deepLink != null) {
      print('_handleDeepLink | deeplink: $deepLink');

      _handleRoutes(deepLink);
    }
  }

  void _handleRoutes(Uri deepLink) {
    log("Uri: $deepLink");
    log("path ${deepLink.path} ");
    log("queryParameters ${deepLink.queryParameters}");
    log("data ${deepLink.data}");
    log("authority ${deepLink.authority}");
    log("fragment ${deepLink.fragment}");
    log("host ${deepLink.host}");
    log("origin ${deepLink.origin}");

    navigatorKey.currentState
        ?.pushNamed(deepLink.path, arguments: deepLink.queryParameters);
  }
}
