import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:patientapp/application/utils.dart';

import 'local_notification.dart';

/// firebase Messaging Background Handler
Future<void> _firebaseMessagingBackgroundHandler(message) async {
  await Firebase.initializeApp();
  await setupFlutterNotifications();

  showFlutterNotification(message);
}

/// back ground
void onBackgroundNotification() {
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
}

/// when app is working in front
void onMessageNotification() {
  FirebaseMessaging.onMessage.listen((event) {
    if (event.data["notification_type"] == "7") {
      /// refresh user data.
      getUserProfileData();
    } else {
      showFlutterNotification(event);
    }
  });
}

/// in front ground
void frontBackgroundNotification() {
  FirebaseMessaging.onMessageOpenedApp.listen((event) {
    onSelectedNotification(event.data.toString());
  });
}

/// deviceToken
Future<String> createDeviceToken() async {
  try {
    var deviceToken = await FirebaseMessaging.instance.getToken();

    return deviceToken.toString();
  } catch (e) {
    debugPrint("error in create device token:${e.toString()}");
    return "";
  }
}

/// delete token
Future<void> deleteDeviceToken() async {
  await FirebaseMessaging.instance.deleteToken();
}
