import 'dart:developer';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:patientapp/application/constants.dart';
import 'package:patientapp/data/models/apiResponse/home_response/gp_working_shifts_response.dart';
import 'package:patientapp/domain/business_logic/subscription_plans/subscription_plans_cubit.dart';

import '../../../application/di.dart';
import '../../../application/utils.dart';
import '../../../data/repository/consultations_repositories_imp.dart';
import '../../../data/repository/notification_repository_imp.dart';
import '../../../main.dart';
import '../../../presentation/resources/routes_manger.dart';
import '../../business_logic/notification_list/notification_list_bloc.dart';
import '../../models/digital_prescriptions_mapper.dart';

/// Initialize the [FlutterLocalNotificationsPlugin] package.
late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
late AndroidNotificationChannel channel;
bool isFlutterLocalNotificationsInitialized = false;

/// show notification
void showFlutterNotification(RemoteMessage message) {
  RemoteNotification? notification = message.notification;
  Map<String, dynamic> data = message.data;

  log('show data ::  ${data}');
  AndroidNotification? android = message.notification?.android;
  if (notification != null && android != null && !kIsWeb) {
    flutterLocalNotificationsPlugin.show(
        notification.hashCode,
        notification.title,
        notification.body,
        NotificationDetails(
          android: AndroidNotificationDetails(
            channel.id,
            channel.name,
            channelDescription: channel.description,
            icon: android.smallIcon,
          ),
          iOS: const IOSNotificationDetails(
              presentAlert: true, presentSound: true),
        ),
        payload: data.toString());
  }
}

/// setup Flutter Notifications
Future<void> setupFlutterNotifications() async {
  if (isFlutterLocalNotificationsInitialized) {
    return;
  }
  channel = const AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    description:
        'This channel is used for important notifications.', // description
    importance: Importance.high,
  );

  flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  /// Create an Android Notification Channel.
  ///
  /// We use this channel in the `AndroidManifest.xml` file to override the
  /// default FCM channel to enable heads up notifications.
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  // await flutterLocalNotificationsPlugin
  //     .resolvePlatformSpecificImplementation<
  //     IOSFlutterLocalNotificationsPlugin>()
  //     ?.requestPermissions(
  //   alert: true,
  //   badge: true,
  //   sound: true,
  // );
  /// heads up notifications.
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  /// settings for ios
  await FirebaseMessaging.instance.requestPermission(
    alert: true,
    announcement: false,
    badge: true,
    carPlay: false,
    criticalAlert: false,
    provisional: false,
    sound: true,
  );

  isFlutterLocalNotificationsInitialized = true;
}

/// initial Settings
var initialSettingAndroid =
    const AndroidInitializationSettings('@mipmap/ic_launcher');
IOSInitializationSettings initializationIOS = const IOSInitializationSettings(
  requestAlertPermission: false,
  requestBadgePermission: false,
  requestSoundPermission: false,
);
InitializationSettings initializationSettings = InitializationSettings(
    android: initialSettingAndroid, iOS: initializationIOS);

/// onSelectedNotification
Future<dynamic> onSelectedNotification(dynamic payload) async {
  log('on Tap :::: $payload');
  NotificationData notificationData =
      NotificationData.fromJson(stringToJson(payload));
  if (notificationData.notificationType != 8) {
    NotificationRepositoriesImp repository =
        instance<NotificationRepositoriesImp>();
    NotificationListBloc notificationListBloc =
        NotificationListBloc(repository);

    notificationListBloc.add(SetNotificationReadEvent(
        int.parse(notificationData.notificationId.toString())));
  }

  handleNotificationNavigation(notificationData);
}

/// handle notification navigation
handleNotificationNavigation(NotificationData notificationData) async {
  /// visit notification
  if (notificationData.notificationType! <= 3) {
    Navigator.pushNamed(navigatorKey.currentState!.context, Routes.visitDetails,
        arguments: {
          AppKeyConstants.RESULTS_ID: int.parse(notificationData.callId ?? ''),
          AppKeyConstants.NOTIFICATION_TYPE: notificationData.notificationType
        });
  }

  /// subscription Plans List  => 4
  else if (notificationData.notificationType == 4) {
    Navigator.pushNamed(
        navigatorKey.currentState!.context, Routes.subscriptionPlansScreen);
  }

  /// subscription Payment  => 5
  else if (notificationData.notificationType == 5) {
    SubscriptionPlansCubit subscriptionPlansCubit =
        SubscriptionPlansCubit(instance<ConsultationsRepositoriesImp>());
    if (notificationData.planId != null) {
      await subscriptionPlansCubit.subscribe(notificationData.planId!);
      if (subscriptionPlansCubit.state.subscribeState == RequestState.loaded) {
        Navigator.pushNamed(
            navigatorKey.currentState!.context, Routes.paymentScreen,
            arguments: subscriptionPlansCubit.state.paymentUrl);
      } else if (subscriptionPlansCubit.state.subscribeState ==
          RequestState.error) {
        handleApiError(
            context: navigatorKey.currentState!.context,
            failure: subscriptionPlansCubit.state.failure!);
      }
    }
  }

  /// renew subscription => 6
  else if (notificationData.notificationType == 6) {
    Navigator.pushNamed(
        navigatorKey.currentState!.context, Routes.renewSubscription,
        arguments: {
          "subscriptionId": notificationData.subscribeId,
          "planId": notificationData.planId,
        });
  }

  /// renew subscription => 7
  else if (notificationData.notificationType == 7) {
    getUserProfileData();
  }

  /// notify me when doctor available  => 8
  else if (notificationData.notificationType == 8) {
    log("Notification Type 8 $notificationData.");
    List<GPWorkingShifts>? gpWorkingShifts =
        await appPreferences.getGPWorkingShifts();
    if (gpWorkingShifts != null) {
      log("The working shifts is available try to make call.");
      /* CallScenario.callGpHandlingFunc(
          navigatorKey.currentState!.context, gpWorkingShifts); */
    } else {
      log("The working shifts is null so we need to load it.");
    }
  }
}

/// convert string to json
Map<String, dynamic> stringToJson(String data) {
  List<String> str = data.replaceAll('{', '').replaceAll('}', '').split(',');
  Map<String, dynamic> result = {};
  for (int i = 0; i < str.length; i++) {
    List<String> s = str[i].split(':');
    result.putIfAbsent(s[0].trim(), () => s[1].trim());
  }

  log('result : $result');
  return result;
}

class NotificationData {
  String? callId;
  String? notificationVisitType;
  String? notificationId;
  int? notificationType;
  int? planId;
  int? subscribeId;

  NotificationData(
      {this.callId,
      this.notificationVisitType,
      this.notificationId,
      this.notificationType,
      this.planId,
      this.subscribeId});

  factory NotificationData.fromJson(Map<String, dynamic> json) {
    return NotificationData(
      callId: json.containsKey('call_id') ? json['call_id'] : null,
      notificationVisitType: json.containsKey('visit_notification_type')
          ? json['visit_notification_type']
          : null,
      notificationId:
          json.containsKey('notification_id') ? json['notification_id'] : null,
      notificationType: json.containsKey('notification_type')
          ? int.parse(json['notification_type'])
          : null,
      planId: json.containsKey('plan_id') ? int.parse(json['plan_id']) : null,
      subscribeId: json.containsKey('subscription_id')
          ? int.parse(json['subscription_id'])
          : null,
    );
  }
}
