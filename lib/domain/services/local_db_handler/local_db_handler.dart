import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/application/utils.dart';
import 'package:patientapp/data/local_data_source/database_helper.dart';
import 'package:patientapp/data/models/apiResponse/base_response.dart';
import 'package:patientapp/data/models/call_logs/call_logs_model.dart';
import 'package:patientapp/data/models/call_logs/call_logs_request.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/data/repository/question_repositories_imp.dart';

class LocalDBHandler {
  static final DatabaseDataSource _localDB = instance<DatabaseDataSource>();

  static void addFailedLogToDB(CallLogsRequest callLogsRequest) {
    ///check if this call log exists if not save it else update with increase tries number
    final callLogs = CallLogsModel(
        comment: callLogsRequest.comment,
        event: callLogsRequest.event,
        numOfTries: 0,
        callId: callLogsRequest.callId,
        date: convertDateTimeToTimestamp(DateTime.now()).toString());

    _localDB.saveCallLogData(callLogs);
  }

  static void loopForSendingEvents() async {
    log("local DB>>>> fun loopForSendingEvents: Check for failed calls..");
    List<CallLogsModel> callList = await _localDB.getCallLogsList();
    log("local DB>>>> num of missed calls ${callList.length}");

    for (int i = 0; i < callList.length; i++) {
      if (callList[i].numOfTries < 3) {
        resendCallLogs(callList[i]);
      } else {
        int removed = await _localDB.removeCallLogData(callList[i]);
        log("local DB>>>>  fun loopForSendingEvents:  Success in removed from db because num of tries greater than 3 : $removed");
      }
    }
  }

  static void resendCallLogs(CallLogsModel callLogsModel) async {
    log("local DB>>>> fun resendCallLogs: Send Call Log Request: ${callLogsModel.event} - ${callLogsModel.comment}");
    final repository = instance<QuestionRepositoriesImp>();
    try {
      Either<Failure, BaseResponse> response =
          await repository.sendCallLog(callLogsModel);

      response.fold((failure) {
        log("local DB>>>> fun resendCallLogs: Failure in Send Call Log Request: ${failure.message}");

        ///The _repository saves the if exists add to num of tries...
      }, (success) async {
        log("local DB>>>> fun resendCallLogs: Success in Send Call Log Request : ${success.message}");

        ///remove from db.
        int removed = await _localDB.removeCallLogData(callLogsModel);
        log("local DB>>>> fun resendCallLogs: Success in removed from db  : $removed");
      });
    } catch (e) {
      log("local DB>>>> fun resendCallLogs: sendCallLog -- ${e.toString()}");
    }
  }
}
