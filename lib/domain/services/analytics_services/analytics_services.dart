import 'dart:developer';
import 'dart:io' as getPlatform;

import 'package:app_tracking_transparency/app_tracking_transparency.dart';
import 'package:facebook_app_events/facebook_app_events.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
// import 'package:kochava_tracker/kochava_tracker.dart';
import 'package:patientapp/application/constants.dart';
import 'package:patientapp/application/environment.dart';
import 'package:patientapp/domain/services/analytics_services/base_analytics_event_name.dart';

import '../../../application/di.dart';

class AnalyticsServices {
  final FirebaseAnalytics _firebaseAnalytics = FirebaseAnalytics.instance;
  final FacebookAppEvents _facebookAnalytics = instance<FacebookAppEvents>();

  AnalyticsServices() {
    try {
      init();
      log("AnalyticsServices=> initialization finished");
      if (useAnalyticsInReleaseMode) startKochavaSdk();
    } catch (e) {
      log("Error happened in AnalyticsServices initialization ?? $e");
    }
  }

  startKochavaSdk() async {
    // try {
      // String trackId = const Uuid().v4();

    //   if (useAnalyticsInReleaseMode) {
    //     KochavaTracker.instance.setLogLevel(KochavaTrackerLogLevel.Info);
    //   } else {
    //     KochavaTracker.instance.setLogLevel(KochavaTrackerLogLevel.Trace);
    //   }
    //   if (getPlatform.Platform.isAndroid) {
    //     KochavaTracker.instance
    //         .registerAndroidAppGuid(KochavaConstants.androidGUIDDebug);
    //   } else if (getPlatform.Platform.isIOS) {
    //     KochavaTracker.instance
    //         .registerIosAppGuid(KochavaConstants.iosGUIDDebug);
    //   }
    //   KochavaTracker.instance.start();
    //
    //   String deviceId = await KochavaTracker.instance.getDeviceId();
    //
    //   log("Device Id : $deviceId");
    //   // log("Track Id : $trackId");
    //   log("AnalyticsServices=> startKochavaSdk finished");
    // } catch (e) {
    //   log("Error happened in AnalyticsServices startKochavaSdk initialization ?? $e");
    // }
  }

  Future<void> init() async {
    if (getPlatform.Platform.isAndroid) {
      await _facebookTracking();
    } else if (getPlatform.Platform.isIOS) {
      await _iosValidation();
    }
  }

  Future<void> _iosValidation() async {
    // If the system can show an authorization request dialog
    TrackingStatus status =
        await AppTrackingTransparency.trackingAuthorizationStatus;
    log("Tracking permission is : $status");

    if (status != TrackingStatus.authorized) {
      // Request system's tracking authorization dialog
      status = await AppTrackingTransparency.requestTrackingAuthorization();
    }
    log("Tracking permission is : $status");

    await _facebookTracking();
  }

  _facebookTracking() async {
    await _facebookAnalytics.setAutoLogAppEventsEnabled(true);
    await _facebookAnalytics.setAdvertiserTracking(enabled: true);
  }

  FirebaseAnalyticsObserver getAnalyticsObserver() =>
      FirebaseAnalyticsObserver(analytics: _firebaseAnalytics);

  addEventAnalytics(
      {required String eventName, required String parameterName}) async {
    ///facebook in-app events
    await _facebookAnalytics.logEvent(
      name: BaseAnalyticsEventName.getAnalyticName(eventName),
      parameters: {
        EventsAnalytics.itemName: parameterName,
      },
    );

    ///firebase analysis
    await FirebaseAnalytics.instance.logEvent(
      name: BaseAnalyticsEventName.getAnalyticName(eventName),
      parameters: {
        EventsAnalytics.itemName: parameterName,
      },
    );

    ///Kochava Tracker
    // log("Kochava Log Event: ${BaseAnalyticsEventName.getAnalyticName(eventName)} - $parameterName");
    // KochavaTrackerEvent event = KochavaTracker.instance.buildEventWithEventName(
    //     BaseAnalyticsEventName.getAnalyticName(eventName));
    // event.setCustomStringValue(
    //     BaseAnalyticsEventName.getAnalyticName(eventName), parameterName);
    // event.send();
  }

  addScreenTrackingAnalytics(
      {required String parameterScreenName,
      required String parameterScreenClass}) async {
    ///firebase analysis
    await FirebaseAnalytics.instance.logScreenView(
      screenName: BaseAnalyticsEventName.getAnalyticName(parameterScreenName),
      screenClass: BaseAnalyticsEventName.getAnalyticName(parameterScreenClass),
    );

    ///add Screen Tracking Analytics
    // log("Kochava Log Event: ${BaseAnalyticsEventName.getAnalyticName(parameterScreenName)} - $parameterScreenClass");
    // KochavaTrackerEvent event = KochavaTracker.instance.buildEventWithEventName(
    //     BaseAnalyticsEventName.getAnalyticName(parameterScreenName));
    // event.setCustomStringValue(
    //     BaseAnalyticsEventName.getAnalyticName(parameterScreenName),
    //     parameterScreenClass);
    // event.send();
    // await FirebaseAnalytics.instance.setCurrentScreen(screenName: eventName);
  }
}
