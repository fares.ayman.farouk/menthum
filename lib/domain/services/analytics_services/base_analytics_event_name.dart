import '../../../application/environment.dart';

class BaseAnalyticsEventName {
  static String getAnalyticName(String eventName) {
    if (!useAnalyticsInReleaseMode) {
      return "test_$eventName";
    } else {
      return eventName;
    }
  }
}
