import 'dart:developer';

import 'package:patientapp/application/constants.dart';
import 'package:pusher_client/pusher_client.dart';

import '../../../application/environment.dart';

class PusherServices {
  late PusherClient _pusher;
  late Channel _channel;

  void firePusher(String channelName) {
    initPusher();
    connectPusher();
    subscribeChannel(channelName);
  }

  /// init Pusher
  void initPusher() {
    try {
      _pusher = PusherClient(
        getPusherKey(),
        PusherOptions(
          cluster: PusherConstants.cluster,
        ),
      );
    } catch (e) {
      log("Catch Error  $e");
    }
  }

  /// subscription channel
  subscribeChannel(String channelName) {
    _channel = _pusher.subscribe(channelName);
  }

  /// connect
  void connectPusher() {
    try {
      _pusher.connect();
    } catch (e) {
      log(e.toString());
    }
  }

  /// onConnection State Change
  void onConnectionStateChange() {
    _pusher.onConnectionStateChange((state) {
      log("previousState: ${state?.previousState}, currentState: ${state?.currentState}");
    });
  }

  /// onConnection Error
  void onConnectionError() {
    _pusher.onConnectionError((error) {
      log("error: ${error?.message}");
    });
  }

  /// bind event
  void bindEvent(String eventName, Function(PusherEvent? event) onEvent) {
    _channel.bind(eventName, onEvent);
  }

  /// un bind
  void unbindEvent(String eventName) {
    _channel.unbind(eventName);
  }

  /// close channel
  void closeChannel(String channel) {
    // Unsubscribe from channel
    _pusher.unsubscribe(channel);

    // Disconnect from pusher service
    _pusher.disconnect();
  }
}
