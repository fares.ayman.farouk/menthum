import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/data/models/apiRequest/clinic/book_clinic_appointment_req.dart';
import 'package:patientapp/data/models/apiResponse/clinic/book_clinic_appointment_response.dart';

import '../../../data/models/apiResponse/consultations_responses/clinic_profile_response.dart';
import '../../../data/network/failure.dart';
import '../../repositories/consultations_repositories.dart';

part 'clinic_profile_event.dart';

part 'clinic_profile_state.dart';

class ClinicProfileBloc extends Bloc<ClinicProfileEvent, ClinicProfileState> {
  final ConsultationsRepositories repository;
  ClinicProfile? clinicProfile;

  ClinicProfileBloc(this.repository) : super(ClinicProfileInitial()) {
    on<ClinicProfileEvent>((event, emit) {});

    on<GetClinicProfile>((event, emit) async {
      emit(ClinicProfileLoading());
      Either<Failure, ClinicProfileResponse> response =
          await repository.getClinicProfile(event.id);

      response.fold((failure) {
        emit(ClinicProfileFailed(failure));
      }, (consultationResponse) {
        clinicProfile = consultationResponse.data!;
        emit(ClinicProfileSuccess(consultationResponse.data));
      });
    });
    on<MakeAppointmentEvent>((event, emit) async {
      emit(LoadingBookAppointment());
      Either<Failure, BookAppointmentResponse> response =
          await repository.makeAppointment(event.bookClinicAppointmentReq);

      response.fold((failure) {
        emit(FailedMakeAppointmentState(failure));
      }, (bookClinicAppointmentResponse) {
        emit(SuccessMakeAppointmentState(
            bookClinicAppointmentResponse.data.toString()));
      });
    });
  }
}
