part of 'clinic_profile_bloc.dart';


abstract class ClinicProfileEvent {}
class GetClinicProfile extends ClinicProfileEvent {
  String id;

  GetClinicProfile({required this.id});
}

class MakeAppointmentEvent extends ClinicProfileEvent {
  final BookAppointmentReq bookClinicAppointmentReq;

  MakeAppointmentEvent({required this.bookClinicAppointmentReq});
}

