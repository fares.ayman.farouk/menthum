part of 'clinic_profile_bloc.dart';

abstract class ClinicProfileState {}

class ClinicProfileInitial extends ClinicProfileState {}

class ClinicProfileLoading extends ClinicProfileState {}

class ClinicProfileSuccess extends ClinicProfileState {
  ClinicProfile? response;

  ClinicProfileSuccess(this.response);
}

class ClinicProfileFailed extends ClinicProfileState {
  Failure failure;

  ClinicProfileFailed(this.failure);
}

class LoadingBookAppointment extends ClinicProfileState {}

class FailedMakeAppointmentState extends ClinicProfileState {
  Failure failure;

  FailedMakeAppointmentState(this.failure);
}

class SuccessMakeAppointmentState extends ClinicProfileState {
  final String response;

  SuccessMakeAppointmentState(this.response);
}
