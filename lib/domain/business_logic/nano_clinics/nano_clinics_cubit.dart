import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/data/models/apiResponse/tikshif_locations/nano_clinic_response.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/repositories/consultations_repositories.dart';

part 'nano_clinics_state.dart';

class NanoClinicsCubit extends Cubit<NanoClinicsState> {
  ConsultationsRepositories consultationsRepository;

  NanoClinicsCubit(this.consultationsRepository) : super(NanoClinicsInitial());

  getNanoClinicsInfo() async {
    emit(LoadingNanoClinicsState());
    Either<Failure, NanoClinicResponse> response =
        await consultationsRepository.getNanoClinicsLocations();

    response.fold((failure) {
      emit(FailedNanoClinicsState(failure));
    }, (nanoClinicResponse) {
      if(nanoClinicResponse.nanoClinics!=null && nanoClinicResponse.nanoClinics.isNotEmpty)
      emit(LoadedNanoClinicsState(nanoClinicResponse.nanoClinics));
      else emit(EmptyNanoClinicsState());
    });
  }
}
