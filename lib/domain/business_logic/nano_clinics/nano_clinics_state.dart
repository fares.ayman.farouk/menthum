part of 'nano_clinics_cubit.dart';

abstract class NanoClinicsState {}

class NanoClinicsInitial extends NanoClinicsState {}

class LoadingNanoClinicsState extends NanoClinicsState {}
class EmptyNanoClinicsState extends NanoClinicsState {}

class LoadedNanoClinicsState extends NanoClinicsState {
  List<NanoClinic> nanoClinics;

  LoadedNanoClinicsState(this.nanoClinics);
}

class FailedNanoClinicsState extends NanoClinicsState {
  Failure failure;

  FailedNanoClinicsState(this.failure);
}
