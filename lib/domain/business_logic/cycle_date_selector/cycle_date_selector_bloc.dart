import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/domain/models/menstrual_cycle/menstrual_cycle.dart';
import 'package:rxdart/rxdart.dart';

import '../../../presentation/resources/color_manger.dart';
import '../../models/digital_prescriptions_mapper.dart';
import '../../models/menstrual_cycle/date_selectorI_entity.dart';
import '../menstrual_cycle/menstrual_cycle_bloc.dart';

part 'cycle_date_selector_event.dart';
part 'cycle_date_selector_state.dart';

class CycleDateSelectorBloc
    extends Bloc<CycleDateSelectorEvent, CycleDateSelectorState> {
  MenstrualCycle menstrualCycle;
  final MenstrualCycleBloc menstrualCycleBloc;
  StreamSubscription<List<MenstrualCycleState>>? _menstrualCycleSubscription;

  CycleDateSelectorBloc({
    required this.menstrualCycle,
    required this.menstrualCycleBloc,
  }) : super(CycleDateSelectorState(
            getCycleDateState: RequestState.initiate, datesList: [])) {
    _menstrualCycleSubscription = menstrualCycleBloc.stream
        .startWith(menstrualCycleBloc.state)
        .pairwise()
        .listen((pair) {
      final p = pair.first;
      final c = pair.last;
      if (p.getChartDesignDataState != c.getChartDesignDataState &&
          c.getChartDesignDataState == RequestState.loaded &&
          menstrualCycle.endPeriodDate.compareTo(c.chartData!.endPeriodDate) !=
              0) {
        log("new menstrualCycle");
        add(InitiateDateSelector(menstrualCycle: c.chartData!));
      }
    });

    on<InitiateDateSelector>(_initiateDateSelector);
    on<ChangeDateSelector>(_changeDateSelector);
  }

  FutureOr<void> _changeDateSelector(
      ChangeDateSelector event, Emitter<CycleDateSelectorState> emit) {
    emit(state.copyWith(getCycleDateState: RequestState.loading));
    DateTime previousSelected = menstrualCycle.currentDate;
    menstrualCycle.currentDate = event.currentSelectedDate;

    state.datesList
        .where((element) => element.date == previousSelected)
        .toList()[0]
        .bgColor = (getCurrentColor(previousSelected));

    state.datesList
        .where((element) => element.date == menstrualCycle.currentDate)
        .toList()[0]
        .bgColor = (getCurrentColor(menstrualCycle.currentDate));

    emit(state.copyWith(getCycleDateState: RequestState.loaded));
  }

  FutureOr<void> _initiateDateSelector(
      InitiateDateSelector event, Emitter<CycleDateSelectorState> emit) {
    menstrualCycle = event.menstrualCycle;

    emit(state.copyWith(getCycleDateState: RequestState.loading));

    List<DateSelectorEntity> listOfDateSelectorEntitiesBetweenDates =
        _getListOfDateSelectorEntitiesBetweenDates(
      startDate: menstrualCycle.startPeriodDate,
      currentSelected: menstrualCycle.currentDate,
      endDate: menstrualCycle.endCycleDate,
    );

    emit(state.copyWith(
        getCycleDateState: RequestState.loaded,
        datesList: listOfDateSelectorEntitiesBetweenDates));
  }

  List<DateSelectorEntity> _getListOfDateSelectorEntitiesBetweenDates({
    required DateTime startDate,
    required DateTime currentSelected,
    required DateTime endDate,
  }) {
    List<DateSelectorEntity> dateSelectorList = [];
    for (int i = 0; i <= endDate.difference(startDate).inDays; i++) {
      final date = DateTime(startDate.year, startDate.month, startDate.day + i);
      dateSelectorList
          .add(DateSelectorEntity(date: date, bgColor: getCurrentColor(date)));
    }

    return dateSelectorList;
  }

  Color getCurrentColor(DateTime date) {
    Color bgColor = Colors.grey.withOpacity(.4);

    if (date.compareTo(menstrualCycle.startPeriodDate) >= 0 &&
        date.compareTo(menstrualCycle.endPeriodDate) <= 0) {
      // log("getCurrentColor SD: ${menstrualCycle.startPeriodDate} --- CD: ${menstrualCycle.currentDate}"
      //     "  --- date: $date --- ED: ${menstrualCycle.endPeriodDate} ");

      bgColor = date.compareTo(menstrualCycle.currentDate) == 0
          ? ColorManager.borderColor
          : ColorManager.borderColor.withOpacity(.6);
    } else if (date.compareTo(menstrualCycle.startPreOvulationDate) >= 0 &&
        date.compareTo(menstrualCycle.endPreOvulationDate) <= 0) {
      // log("getCurrentColor SPRED: ${menstrualCycle.startPreOvulationDate} --- CD: ${menstrualCycle.currentDate}  --- date: $date --- EPRED: ${menstrualCycle.endPreOvulationDate} ");

      bgColor = date.compareTo(menstrualCycle.currentDate) == 0
          ? ColorManager.primaryLight
          : ColorManager.primaryLight.withOpacity(.6);
    } else if (date.compareTo(menstrualCycle.ovulationDate
                .subtract(const Duration(days: 1))) >=
            0 &&
        date.compareTo(
                menstrualCycle.ovulationDate.add(const Duration(days: 1))) <=
            0) {
      // log("getCurrentColor ovulation: ${menstrualCycle.ovulationDate} --- CD: ${menstrualCycle.currentDate}  --- date: $date ");
      bgColor = date.compareTo(menstrualCycle.currentDate) == 0
          ? ColorManager.primary
          : ColorManager.primary.withOpacity(.6);
    } else if (date.compareTo(menstrualCycle.startPostOvulationDate) >= 0 &&
        date.compareTo(menstrualCycle.endPostOvulationDate) <= 0) {
      // log("getCurrentColor SPOSTD: ${menstrualCycle.startPostOvulationDate} --- CD: ${menstrualCycle.currentDate} --- date: $date   --- EPOSTD: ${menstrualCycle.endPostOvulationDate} ");
      bgColor = date.compareTo(menstrualCycle.currentDate) == 0
          ? ColorManager.primaryLight
          : ColorManager.primaryLight.withOpacity(.6);
    }
    return bgColor;
  }

  @override
  Future<void> close() {
    _menstrualCycleSubscription?.cancel();
    return super.close();
  }
}
