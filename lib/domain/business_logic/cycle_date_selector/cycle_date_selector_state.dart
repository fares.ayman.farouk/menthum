part of 'cycle_date_selector_bloc.dart';

class CycleDateSelectorState {
  List<DateSelectorEntity> datesList;
  RequestState getCycleDateState;

  CycleDateSelectorState({
    this.datesList = const [],
    this.getCycleDateState = RequestState.initiate,
  });

  CycleDateSelectorState copyWith({
    List<DateSelectorEntity>? datesList,
    RequestState? getCycleDateState,
  }) =>
      CycleDateSelectorState(
        datesList: datesList ?? this.datesList,
        getCycleDateState: getCycleDateState ?? this.getCycleDateState,
      );
}
