part of 'cycle_date_selector_bloc.dart';

abstract class CycleDateSelectorEvent {}

class InitiateDateSelector extends CycleDateSelectorEvent {
  MenstrualCycle menstrualCycle;

  InitiateDateSelector({required this.menstrualCycle});
}

class ChangeDateSelector extends CycleDateSelectorEvent {
  DateTime currentSelectedDate;

  ChangeDateSelector({required this.currentSelectedDate});
}
