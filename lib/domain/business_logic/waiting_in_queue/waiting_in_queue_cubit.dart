import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/data/models/apiResponse/base_response.dart';
import 'package:patientapp/data/network/failure.dart';

import '../../repositories/questions_repositories.dart';

part 'waiting_in_queue_state.dart';

class WaitingInQueueCubit extends Cubit<WaitingInQueueState> {
  final QuestionRepositories questionRepositories;

  WaitingInQueueCubit({required this.questionRepositories})
      : super(WaitingInQueueState(requestState: RequestState.init));

  removeCallFromQueue({required int callId}) async {
    emit(state.copyWith(requestState: RequestState.loading));

    Either<Failure, BaseResponse> response =
        await questionRepositories.removeCallFromQueue(callId);

    response.fold(
      (failure) => emit(
          state.copyWith(requestState: RequestState.failed, failure: failure)),
      (success) => emit(
        state.copyWith(
          requestState: RequestState.exitScreen,
        ),
      ),
    );
  }

  notifyMe({required int callId}) async {
    emit(state.copyWith(requestState: RequestState.loading));

    Either<Failure, BaseResponse> response =
        await questionRepositories.notifyMe(callId);

    response.fold(
      (failure) => emit(
          state.copyWith(requestState: RequestState.failed, failure: failure)),
      (success) => emit(
        state.copyWith(
          requestState: RequestState.exitScreen,
        ),
      ),
    );
  }
}
