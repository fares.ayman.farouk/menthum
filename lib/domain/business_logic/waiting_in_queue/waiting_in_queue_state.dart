part of 'waiting_in_queue_cubit.dart';

class WaitingInQueueState {
  final RequestState requestState;
  final Failure? failure;

  WaitingInQueueState({
    this.requestState = RequestState.init,
    this.failure,
  });

  copyWith({RequestState? requestState, Failure? failure}) {
    return WaitingInQueueState(
      requestState: requestState ?? this.requestState,
      failure: failure ?? this.failure,
    );
  }
}

enum RequestState {
  init,
  loading,
  exitScreen,
  failed, loaded, error,
}
