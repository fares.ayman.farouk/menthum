import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';
import 'package:patientapp/presentation/transaction_history/transaction_history_page.dart';

import '../../../data/models/apiResponse/auth_response/login_api_response.dart';
import '../../../presentation/screens/home/home_screen.dart';
import '../../models/main_screen_tabs.dart';

part 'main_screen_navigator_state.dart';

class MainScreenNavigatorCubit extends Cubit<MainScreenNavigatorState> {
  //To know witch one to display on the screen.
  String screenName = AppStrings.home;
  bool isSideMenuOpen = false;

  MainTabs currentTab = MainTabs.home;
  PageController? pageController;
  AppPreferences appPreferences = instance<AppPreferences>();
  UserData userData = UserData();
  int callCost = 50;
  MainScreenNavigatorCubit() : super(MainScreenNavigatorInitial()) {
    pageController =
        PageController(initialPage: currentTab.index /*MainTabs.home.index*/);
    getUserData();
  }

  getUserData() async {
    userData = (await appPreferences.getUser())!;
  }

  final tabs = [
    HomeScreen(),
    TransactionHistoryPage(),
    //ProfileScreen(backButtonExist: false),
  ];

  changeTab(int index) {
    print("faressssssssssssssssssssssssssssss");
    print(index);
    currentTab = MainTabs.values.elementAt(index);
    print(currentTab);
    pageController!.jumpToPage(index);
    //print(currentTab.name);
    emit(MainNavigationChange(index));
  }

  callDoctor() {
    state is ShowCallDoctorState
        ? emit(HideCallDoctorState())
        : emit(ShowCallDoctorState());
  }

  showSideMenu(String screenName) {
    this.screenName = screenName;
    isSideMenuOpen = true;

    switch (screenName) {
      case AppStrings.home:
        emit(ShowSideMenuHomeState());
        return;
      case AppStrings.myVitals:
        emit(ShowSideMenuVitalsState());
        return;
      case AppStrings.myProfile:
        emit(ShowSideMenuProfileState());
        return;
      case AppStrings.prescriptions:
        emit(ShowSideMenuPrescriptionsState());
        return;
      case AppStrings.myRecords:
        emit(ShowSideMenuRecordsState());
        return;
      case AppStrings.myResult:
        emit(ShowSideMenuMyResultsState());
        return;
      case AppStrings.myPrograms:
        emit(ShowSideMenuMyProgramsState());
        return;
    }
  }

  hideSideMenu(String screenName) {
    this.screenName = screenName;
    isSideMenuOpen = false;

    switch (screenName) {
      case AppStrings.home:
        emit(HideSideMenuHomeState());
        return;
      case AppStrings.myVitals:
        emit(HideSideMenuVitalsState());
        return;
      case AppStrings.myProfile:
        emit(HideSideMenuProfileState());
        return;
      case AppStrings.Prescriptions:
        emit(HideSideMenuPrescriptionsState());
        return;
      case AppStrings.myRecords:
        emit(HideSideMenuRecordsState());
        return;
      case AppStrings.myResult:
        emit(HideSideMenuMyResultsState());
        return;
      case AppStrings.myPrograms:
        emit(HideSideMenuMyProgramsState());
        return;
    }
  }
}
