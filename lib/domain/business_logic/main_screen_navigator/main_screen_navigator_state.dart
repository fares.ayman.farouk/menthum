part of 'main_screen_navigator_cubit.dart';

@immutable
abstract class MainScreenNavigatorState {}

class MainScreenNavigatorInitial extends MainScreenNavigatorState {}

class MainNavigationChange extends MainScreenNavigatorState {
  final int index;

  MainNavigationChange(this.index);
}

class ShowCallDoctorState extends MainScreenNavigatorState {}

class HideCallDoctorState extends MainScreenNavigatorState {}

// class ShowSideMenuState extends MainScreenNavigatorState {}
//
// class HideSideMenuState extends MainScreenNavigatorState {}

//MainScreenContent
class ShowSideMenuHomeState extends MainScreenNavigatorState {}

class HideSideMenuHomeState extends MainScreenNavigatorState {}

//VitalsScreenContent
class ShowSideMenuVitalsState extends MainScreenNavigatorState {}

class HideSideMenuVitalsState extends MainScreenNavigatorState {}

//ProfileScreenContent
class ShowSideMenuProfileState extends MainScreenNavigatorState {}

class HideSideMenuProfileState extends MainScreenNavigatorState {}

//Prescriptions ScreenContent
class ShowSideMenuPrescriptionsState extends MainScreenNavigatorState {}

class HideSideMenuPrescriptionsState extends MainScreenNavigatorState {}

//Records ScreenContent
class ShowSideMenuRecordsState extends MainScreenNavigatorState {}

class HideSideMenuRecordsState extends MainScreenNavigatorState {}

//my Results ScreenContent
class ShowSideMenuMyResultsState extends MainScreenNavigatorState {}

class HideSideMenuMyResultsState extends MainScreenNavigatorState {}

/// My Program ScreenContent
class ShowSideMenuMyProgramsState extends MainScreenNavigatorState {}

class HideSideMenuMyProgramsState extends MainScreenNavigatorState {}
