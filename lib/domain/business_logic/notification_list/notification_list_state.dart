part of 'notification_list_bloc.dart';

abstract class NotificationListState {}

class NotificationListInitial extends NotificationListState {}

class NotificationListSucess extends NotificationListState {
  List<NotifiData> notifications;
  int? unReadCount=0;
  NotificationListSucess({required this.notifications,this.unReadCount});
}


class NotificationListLoading extends NotificationListState {}
class NotificationListEmpty extends NotificationListState {}

class NotificationListFailed extends NotificationListState {
  Failure failure;

  NotificationListFailed(this.failure);
}
