part of 'notification_list_bloc.dart';

abstract class NotificationListEvent {}

class GetNotificationListEvent extends NotificationListEvent {}

class SetNotificationReadEvent extends NotificationListEvent {
  int id;

  SetNotificationReadEvent(this.id);
}
