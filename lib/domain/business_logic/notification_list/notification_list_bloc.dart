import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/data/models/apiResponse/base_response.dart';
import 'package:patientapp/data/network/failure.dart';

import '../../../data/models/apiResponse/notification_list_response/notification_list_response.dart';
import '../../repositories/notification_repositories.dart';

part 'notification_list_event.dart';

part 'notification_list_state.dart';

class NotificationListBloc
    extends Bloc<NotificationListEvent, NotificationListState> {
  NotificationRepositories repository;

  List<NotifiData>? notificationListData=[];

  NotificationListBloc(this.repository) : super(NotificationListInitial()) {
    on<NotificationListEvent>((event, emit) {});

    /// get notification
    on<GetNotificationListEvent>((event, emit) async {
      emit(NotificationListLoading());
      Either<Failure, NotificationResponse> response =
          await repository.getListNotifications();
      response.fold((failure) {
        emit(NotificationListFailed(failure));
      }, (notificationList) {
        List<NotifiData>? data = notificationList.data;
        notificationListData=notificationList.data;

        if (data != null && data.isNotEmpty) {
          emit(NotificationListSucess(
              notifications: data, unReadCount: _setCounter(data)));
        } else {
          emit(NotificationListEmpty());
        }
      });
    });

    /// set Notification read
    on<SetNotificationReadEvent>((event, emit) async {
      // emit(NotificationListLoading());

      Either<Failure, BaseResponse> response =
          await repository.setNotificationRead(event.id);
      response.fold((failure) {
        emit(NotificationListFailed(failure));
      }, (sucessRes) {
        add(GetNotificationListEvent());
      });
    });
  }

  int _setCounter(List<NotifiData> data) {
    int count = 0;
    for (int i = 0; i < data.length; i++) {
      if (data[i].isRead == false) {
        count++;
      }
    }
    return count;
  }
}
