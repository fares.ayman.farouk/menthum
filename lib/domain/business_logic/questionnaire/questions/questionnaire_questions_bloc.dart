import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/domain/models/questionnaire/questionnaire_entity.dart';

import '../../../../data/network/failure.dart';
import '../../../models/digital_prescriptions_mapper.dart';
import '../../../models/questionnaire/answer_questionnaire_entity.dart';
import '../../../models/questionnaire/questionnaire_result_entity.dart';
import '../../../repositories/questionnaire_repository.dart';

part 'questionnaire_questions_event.dart';

part 'questionnaire_questions_state.dart';

class QuestionnaireQuestionsBloc
    extends Bloc<QuestionnaireQuestionsEvent, QuestionnaireQuestionsState> {
  QuestionnaireRepository questionnaireRepository;

  QuestionnaireQuestionsBloc(this.questionnaireRepository)
      : super(QuestionnaireQuestionsState(
            questionsState: RequestState.initiate)) {
    on<GetQuestions>((event, emit) async {
      emit(state.copyWith(questionsState: RequestState.loading));

      final response =
          await questionnaireRepository.getQuestionnaireQuestions(event.slug);
      response.fold((failure) {
        emit(state.copyWith(
            failure: failure, questionsState: RequestState.error));
      }, (data) {
        emit(
          state.copyWith(
            questionsState: RequestState.loaded,
            questionsList: data,
          ),
        );
      });
    });

    on<AnswerQuestionsEvent>((event, emit) async {
      emit(state.copyWith(answerState: RequestState.loading));

      final response =
          await questionnaireRepository.saveAnswers(event.answers, event.slug);
      response.fold((failure) {
        emit(state.copyWith(failure: failure, answerState: RequestState.error));
      }, (data) {
        emit(
          state.copyWith(
            answerState: RequestState.loaded,
            questionnaireResult: data,
          ),
        );
      });
    });
  }
}
