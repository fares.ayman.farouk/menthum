part of 'questionnaire_questions_bloc.dart';

class QuestionnaireQuestionsState {
  RequestState? questionsState;
  RequestState? answerState;
  Failure? failure;
  List<QuestionnaireQuestionEntity>? questionsList;
  QuestionnaireResultEntity? questionnaireResult;

  QuestionnaireQuestionsState(
      {this.questionsState,
      this.questionsList,
      this.answerState,
      this.questionnaireResult,
      this.failure});

  QuestionnaireQuestionsState copyWith(
      {RequestState? questionsState,
      RequestState? answerState,
      Failure? failure,
      List<QuestionnaireQuestionEntity>? questionsList,
      QuestionnaireResultEntity? questionnaireResult}) {
    return QuestionnaireQuestionsState(
        failure: failure ?? this.failure,
        questionsState: questionsState ?? this.questionsState,
        answerState: answerState ?? this.answerState,
        questionsList: questionsList ?? this.questionsList,
        questionnaireResult: questionnaireResult ?? this.questionnaireResult);
  }
}
