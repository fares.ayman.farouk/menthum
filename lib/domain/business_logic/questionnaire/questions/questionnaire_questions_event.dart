part of 'questionnaire_questions_bloc.dart';

abstract class QuestionnaireQuestionsEvent {}

class GetQuestions extends QuestionnaireQuestionsEvent {
  String slug;

  GetQuestions(this.slug);
}

class AnswerQuestionsEvent extends QuestionnaireQuestionsEvent {
  AnswerQuestionnaireEntity answers;
  String slug;
  AnswerQuestionsEvent({required this.answers,required this.slug});
}
