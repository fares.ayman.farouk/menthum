part of 'questionnaires_bloc.dart';

class QuestionnairesState {
  RequestState? questionnairesState;
  Failure? failure;
  List<QuestionnaireItemEntity>? questionnaireItems;

  QuestionnairesState(
      {this.questionnairesState, this.questionnaireItems, this.failure});

  QuestionnairesState copyWith({
    RequestState? questionnairesState,
    Failure? failure,
    List<QuestionnaireItemEntity>? questionnaireItems,
  }) {
    return QuestionnairesState(
      failure: failure ?? this.failure,
      questionnairesState: questionnairesState ?? this.questionnairesState,
      questionnaireItems: questionnaireItems ?? this.questionnaireItems,
    );
  }
}
