import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/domain/models/questionnaire/questionnaire_item_entity.dart';

import '../../../../data/network/failure.dart';
import '../../../models/digital_prescriptions_mapper.dart';
import '../../../repositories/questionnaire_repository.dart';

part 'questionnaires_event.dart';

part 'questionnaires_state.dart';

class QuestionnairesBloc
    extends Bloc<QuestionnairesEvent, QuestionnairesState> {
  QuestionnaireRepository questionnaireRepository;

  QuestionnairesBloc(this.questionnaireRepository)
      : super(QuestionnairesState(questionnairesState: RequestState.initiate)) {


    on<GetQuestionnairesItems>((event, emit) async {
      emit(state.copyWith(questionnairesState: RequestState.loading));

      final response = await questionnaireRepository.getQuestionnairesItems();
      response.fold((failure) {
        emit(state.copyWith(
            failure: failure, questionnairesState: RequestState.error));
      }, (data) {
        emit(
          state.copyWith(
            questionnairesState: RequestState.loaded,
            questionnaireItems: data,
          ),
        );
      });
    });


  }
}
