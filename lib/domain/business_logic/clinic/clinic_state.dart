part of 'clinic_bloc.dart';

abstract class ClinicState {}

class ClinicInitial extends ClinicState {}

class LoadingClinicState extends ClinicState {}

class LoadingMoreClinicState extends ClinicState {}


class EmptyClinicState extends ClinicState {}

class SuccessClinicState extends ClinicState {
  List<Clinic> clinicsList;
  Meta meta;

  SuccessClinicState(this.clinicsList, this.meta);
}

class SuccessMoreClinicState extends ClinicState {
  List<Clinic> clinicsList;
  Meta meta;

  SuccessMoreClinicState(this.clinicsList, this.meta);
}

class FailedClinicState extends ClinicState {
  Failure failure;

  FailedClinicState(this.failure);
}



