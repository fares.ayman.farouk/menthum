import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/application/di.dart';

import '../../../../data/network/failure.dart';
import '../../repositories/consultations_repositories.dart';

import 'package:bloc/bloc.dart';
import 'package:patientapp/data/models/apiRequest/clinic/clinic_request.dart';
import 'package:patientapp/data/models/apiResponse/clinic/clinic_response.dart';
import 'package:patientapp/data/models/apiResponse/location_filter/city_response.dart';
import 'package:patientapp/data/models/apiResponse/location_filter/governorate_response.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/repositories/consultations_repositories.dart';

part 'clinic_event.dart';

part 'clinic_state.dart';

class ClinicBloc extends Bloc<ClinicEvent, ClinicState> {
  final ConsultationsRepositories repository;

  //default val
  String countryId = "1";

  final AppPreferences _appPreferences = instance<AppPreferences>();

  List<Governorate> governoratesList = [];

  List<City> citiesList = [];
  List<Clinic> clinicsList = [];

  ClinicBloc(this.repository) : super(ClinicInitial()) {
    on<GetClinicsEvent>((event, emit) async {
      emit(LoadingClinicState());
      try {
        Either<Failure, ClinicsResponse> response =
            await repository.getClinics(event.clinicRequest, event.page);

        /// check if the shared preference is empty or not
        citiesList = await _appPreferences.getCities() ?? [];
        governoratesList = await _appPreferences.getGovernorates() ?? [];

        if (citiesList.isEmpty || governoratesList.isEmpty) {
          await getListOfGovernorates("1");
          await getListOfCities(governoratesList[0].id.toString());

        }
        response.fold((failure) {
          clinicsList = [];
          emit(FailedClinicState(failure));
        }, (clinicResponse) async {
          if (clinicResponse.data?.isEmpty == true) {
            clinicsList = [];
            emit(EmptyClinicState());
          } else {
            clinicsList = clinicResponse.data ?? [];
            emit(SuccessClinicState(clinicsList, clinicResponse.meta!));
          }
        });
      } catch (e) {
        print(e);
      }
    });


    on<GetClinicsMoreEvent>((event, emit) async {
      emit(LoadingMoreClinicState());
      try {
        Either<Failure, ClinicsResponse> response =
            await repository.getClinics(event.clinicRequest, event.page);

        response.fold((failure) {
          clinicsList = [];
          emit(FailedClinicState(failure));
        }, (clinicResponse) async {
          clinicsList = clinicResponse.data ?? [];
          emit(SuccessMoreClinicState(clinicsList, clinicResponse.meta!));
        });
      } catch (e) {
        print(e);
      }
    });
  }

  getListOfCities(String governorateId) async {
    try {
      Either<Failure, CityResponse> response =
          await repository.getCities(governorateId);

      response.fold((failure) {
        citiesList = [];
      }, (cityResponse) async {
        if (cityResponse.data?.isEmpty == true) {
          citiesList = [];
        } else {
          citiesList = cityResponse.data ?? [];
          await _appPreferences.addCities(citiesList);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  getListOfGovernorates(String countryId) async {
    try {
      Either<Failure, GovernorateResponse> response =
          await repository.getGovernorates(countryId);

      response.fold((failure) {
        governoratesList = [];
      }, (governorateResponse) async {
        if (governorateResponse.data?.isEmpty == true) {
          governoratesList = [];
        } else {
          governoratesList = governorateResponse.data ?? [];
          await _appPreferences.addGovernorates(governoratesList);
        }
      });
    } catch (e) {
      print(e);
    }
  }
}
