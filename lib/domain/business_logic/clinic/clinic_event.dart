part of 'clinic_bloc.dart';

abstract class ClinicEvent {}

class GetClinicsEvent extends ClinicEvent {
  ClinicRequest clinicRequest;
  int page;

  GetClinicsEvent(this.clinicRequest, this.page);
}

class GetClinicsMoreEvent extends ClinicEvent {
  ClinicRequest clinicRequest;
  int page;

  GetClinicsMoreEvent(this.clinicRequest, this.page);
}
