part of 'pediatric_bloc.dart';

abstract class PediatricEvent {}

class TodayVaccinesPediatricEvent extends PediatricEvent {
  final String id;

  TodayVaccinesPediatricEvent(this.id);
}

class HomePediatricEvent extends PediatricEvent {
  final String id;

  HomePediatricEvent(this.id);
}

class EnrollPediatricEvent extends PediatricEvent {
  EnrollRequestEntity enrollRequestEntity;

  EnrollPediatricEvent({required this.enrollRequestEntity});
}

class DonePediatricEvent extends PediatricEvent {
  PediatricDoneEntity pediatricDoneEntity;

  DonePediatricEvent(this.pediatricDoneEntity);
}

class CalculatePediatricWeightEvent extends PediatricEvent {
  PediatricCalculateRequestEntity pediatricCalculateRequestEntity;
  CalculatePediatricWeightEvent(this.pediatricCalculateRequestEntity);
}

class CalculatePediatricHeadEvent extends PediatricEvent {
  PediatricCalculateRequestEntity pediatricCalculateRequestEntity;
  CalculatePediatricHeadEvent(this.pediatricCalculateRequestEntity);
}

class HistoryPediatricEvent extends PediatricEvent {
  PediatricHistoryRequest pediatricHistoryRequest;

  HistoryPediatricEvent(this.pediatricHistoryRequest);
}

class ShowMoreReadingHistory extends PediatricEvent {
  PediatricHistoryRequest pediatricHistoryRequest;

  ShowMoreReadingHistory(this.pediatricHistoryRequest);
}
