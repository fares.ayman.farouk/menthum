part of 'pediatric_bloc.dart';

class PediatricState {
  Failure? failure;
  List<PediatricDataEntity>? pediatricData;
  List<PediatricDataEntity>? historyPediatricData;
  List<PediatricHomeEntity>? pediatricHomeData;
  PediatricWeightResponseEntity? pediatricWeightResponse;
  PediatricHeadResponseEntity? pediatricHeadResponse;
  RequestState? homePediatricState;
  RequestState? todayVaccinesPediatricState;
  RequestState? addReadingPediatricState;
  RequestState? historyPediatricState;
  RequestState? enrollState;
  bool? enrollMode;
  bool loadingMore;

  PediatricState({
    this.homePediatricState = RequestState.initiate,
    this.todayVaccinesPediatricState = RequestState.initiate,
    this.enrollState = RequestState.initiate,
    this.addReadingPediatricState = RequestState.initiate,
    this.historyPediatricState = RequestState.initiate,
    this.pediatricHomeData,
    this.pediatricData,
    this.historyPediatricData,
    this.pediatricWeightResponse,
    this.pediatricHeadResponse,
    this.failure,
    this.enrollMode = false,
    this.loadingMore = false,
  });

  PediatricState copyWith({
    RequestState? homePediatricState,
    RequestState? todayVaccinesPediatricState,
    RequestState? enrollState,
    RequestState? addReadingPediatricState,
    RequestState? historyPediatricState,
    List<PediatricDataEntity>? pediatricData,
    List<PediatricDataEntity>? historyPediatricData,
    List<PediatricHomeEntity>? pediatricHomeData,
    PediatricWeightResponseEntity? pediatricWeightResponse,
    PediatricHeadResponseEntity? pediatricHeadResponse,
    Failure? failure,
    bool? enrollMode,
    bool? loadingMore,
  }) =>
      PediatricState(
        failure: failure ?? this.failure,
        pediatricData: pediatricData ?? this.pediatricData,
        pediatricHomeData: pediatricHomeData ?? this.pediatricHomeData,
        historyPediatricData: historyPediatricData ?? this.historyPediatricData,
        addReadingPediatricState:
            addReadingPediatricState ?? this.addReadingPediatricState,
        todayVaccinesPediatricState:
            todayVaccinesPediatricState ?? this.todayVaccinesPediatricState,
        historyPediatricState:
            historyPediatricState ?? this.historyPediatricState,
        homePediatricState: homePediatricState ?? this.homePediatricState,
        pediatricWeightResponse:
            pediatricWeightResponse ?? this.pediatricWeightResponse,
        pediatricHeadResponse:
            pediatricHeadResponse ?? this.pediatricHeadResponse,
        enrollState: enrollState ?? this.enrollState,
        enrollMode: enrollMode ?? this.enrollMode,
        loadingMore: loadingMore ?? this.loadingMore,
      );
}
