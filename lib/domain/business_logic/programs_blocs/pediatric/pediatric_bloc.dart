import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../data/network/error_handler.dart';
import '../../../../data/network/failure.dart';
import '../../../models/digital_prescriptions_mapper.dart';
import '../../../models/pediatric/enroll_entity.dart';
import '../../../models/pediatric/history_entity.dart';
import '../../../models/pediatric/history_request.dart';
import '../../../models/pediatric/pediatric_calculate_request_Entity.dart';
import '../../../models/pediatric/pediatric_data_entity.dart';
import '../../../models/pediatric/pediatric_done.dart';
import '../../../models/pediatric/pediatric_head_response.dart';
import '../../../models/pediatric/pediatric_home.dart';
import '../../../models/pediatric/pediatric_weight_response.dart';
import '../../../repositories/programs/pediatric_repository.dart';

part 'pediatric_event.dart';

part 'pediatric_state.dart';

class PediatricBloc extends Bloc<PediatricEvent, PediatricState> {
  PediatricRepository repository;

  int currentPage = 1, lastPage = 1;
  bool isMoreLoadRunning = false;
  ScrollController scrollController = ScrollController();

  PediatricBloc(this.repository)
      : super(PediatricState(
            todayVaccinesPediatricState: RequestState.initiate)) {
    /// scroll Controller
    scrollController.addListener(() {
      _onScroll();
    });

    on<HomePediatricEvent>((event, emit) async {
      emit(state.copyWith(
          homePediatricState: RequestState.loading, enrollMode: false));

      Either<Failure, List<PediatricHomeEntity>> response =
          await repository.pediatricHome(event.id);
      response.fold((failure) {
        if (failure.code == ResponseCode.BAD_REQUEST) {
          /// enroll mode
          emit(state.copyWith(
            homePediatricState: RequestState.error,
            enrollMode: true,
            failure: failure,
          ));
        } else {
          emit(state.copyWith(
              homePediatricState: RequestState.error, failure: failure));
        }
      }, (pediatricHomeData) {
        emit(
          state.copyWith(
            homePediatricState: RequestState.loaded,
            pediatricHomeData: pediatricHomeData,
            enrollMode: false,
          ),
        );
      });
    });

    /// today Vaccines
    on<TodayVaccinesPediatricEvent>((event, emit) async {
      emit(state.copyWith(
        todayVaccinesPediatricState: RequestState.loading,
        enrollMode: false,
      ));

      Either<Failure, List<PediatricDataEntity>> response =
          await repository.today(event.id);
      response.fold((failure) {
        // if (failure.code == ResponseCode.BAD_REQUEST) {
        //   /// enroll mode
        //   emit(state.copyWith(
        //     todayVaccinesPediatricState: RequestState.error,
        //     enrollMode: true,
        //     failure: failure,
        //   ));
        // } else
        // {
        emit(state.copyWith(
            todayVaccinesPediatricState: RequestState.error, failure: failure));
        // }
      }, (pediatricData) {
        emit(state.copyWith(
            todayVaccinesPediatricState: RequestState.loaded,
            pediatricData: pediatricData,
            enrollMode: false));
      });
    });

    /// Enroll
    on<EnrollPediatricEvent>((event, emit) async {
      emit(state.copyWith(
        enrollState: RequestState.loading,
        todayVaccinesPediatricState: RequestState.initiate,
      ));
      final response = await repository.enroll(event.enrollRequestEntity);
      response.fold((failure) {
        emit(state.copyWith(enrollState: RequestState.error, failure: failure));
      }, (response) {
        emit(state.copyWith(
          todayVaccinesPediatricState: RequestState.initiate,
          homePediatricState: RequestState.initiate,
          enrollState: RequestState.loaded,
        ));
      });
    });

    /// done
    on<DonePediatricEvent>((event, emit) async {
      emit(state.copyWith(
        addReadingPediatricState: RequestState.loading,
      ));
      final response = await repository.done(event.pediatricDoneEntity);
      response.fold((failure) {
        emit(state.copyWith(
            addReadingPediatricState: RequestState.error, failure: failure));
      }, (response) {
        emit(state.copyWith(
          addReadingPediatricState: RequestState.loaded,
        ));
      });
    });

    /// Calculate Pediatric
    // calculate weight
    on<CalculatePediatricWeightEvent>((event, emit) async {
      emit(state.copyWith(
        addReadingPediatricState: RequestState.loading,
      ));
      final response = await repository
          .calculateWeight(event.pediatricCalculateRequestEntity);
      response.fold((failure) {
        emit(state.copyWith(
            addReadingPediatricState: RequestState.error, failure: failure));
      }, (pediatricWeightResponse) {
        emit(
          state.copyWith(
            addReadingPediatricState: RequestState.loaded,
            pediatricWeightResponse: pediatricWeightResponse,
          ),
        );
      });
    });
    // calculate head
    on<CalculatePediatricHeadEvent>((event, emit) async {
      emit(state.copyWith(
        addReadingPediatricState: RequestState.loading,
      ));
      final response = await repository
          .calculateHead(event.pediatricCalculateRequestEntity);
      response.fold((failure) {
        emit(state.copyWith(
            addReadingPediatricState: RequestState.error, failure: failure));
      }, (pediatricHeadResponse) {
        emit(
          state.copyWith(
            addReadingPediatricState: RequestState.loaded,
            pediatricHeadResponse: pediatricHeadResponse,
          ),
        );
      });
    });

    /// history

    on<HistoryPediatricEvent>((event, emit) async {
      emit(state.copyWith(
        historyPediatricState: RequestState.loading,
      ));

      Either<Failure, PediatricHistoryEntity> response =
          await repository.history(event.pediatricHistoryRequest);

      response.fold((failure) {
        emit(state.copyWith(
            historyPediatricState: RequestState.error, failure: failure));
      }, (pediatricHistory) {
        lastPage = pediatricHistory.latPage;
        emit(state.copyWith(
          historyPediatricState: RequestState.loaded,
          historyPediatricData: pediatricHistory.pediatricData,
        ));
      });
    });

    /// history More
    on<ShowMoreReadingHistory>((event, emit) async {
      Either<Failure, PediatricHistoryEntity> response =
          await repository.history(event.pediatricHistoryRequest);
      response.fold((failure) {
        emit(state.copyWith(
            historyPediatricState: RequestState.error, failure: failure));
      }, (pediatricHistory) {
        lastPage = pediatricHistory.latPage;

        List<PediatricDataEntity> pediatricAllData =
            (state.historyPediatricData)! + (pediatricHistory.pediatricData);

        pediatricHistory.pediatricData = pediatricAllData;

        emit(state.copyWith(
          loadingMore: false,
          historyPediatricData: pediatricAllData,
        ));
      });
    });
  }

  void _onScroll() {
    double maxScroll = scrollController.position.maxScrollExtent;
    double currentScroll = scrollController.position.pixels;
    if (currentScroll == maxScroll) {
      currentPage++;
      if (currentPage <= lastPage) {
        emit(state.copyWith(loadingMore: true));
      }
    }
  }

  @override
  Future<void> close() {
    scrollController.dispose();
    return super.close();
  }
}
