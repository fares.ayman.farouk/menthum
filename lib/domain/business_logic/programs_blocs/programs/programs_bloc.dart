import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../data/network/failure.dart';
import '../../../models/digital_prescriptions_mapper.dart';
import '../../../models/programs/program_entity.dart';
import '../../../repositories/programs/programs_repository.dart';


part 'programs_event.dart';

part 'programs_state.dart';

class ProgramsBloc extends Bloc<ProgramsEvent, ProgramsState> {
  final ProgramsRepository programsRepository;

  ProgramsBloc(this.programsRepository)
      : super(ProgramsState(getProgramsState: RequestState.initiate)) {
    on<GetProgramsList>(_getProgramsList);
  }

  Future<FutureOr<void>> _getProgramsList(
      GetProgramsList event, Emitter<ProgramsState> emit) async {
    emit(state.copyWith(getProgramsState: RequestState.loading));
    Either<Failure, List<Program>> response =
        await programsRepository.programsList();

    response.fold((failure) {
      emit(state.copyWith(
        getProgramsState: RequestState.error,
        failure: failure,
      ));
    }, (programsList) {
      emit(state.copyWith(
          programsList: programsList, getProgramsState: RequestState.loaded));
    });
  }
}
