part of 'programs_bloc.dart';

class ProgramsState {
  RequestState getProgramsState;
  List<Program> programsList;
  Failure? failure;

  ProgramsState({
    this.getProgramsState = RequestState.initiate,
    this.programsList = const [],
    this.failure,
  });

  ProgramsState copyWith({
    RequestState? getProgramsState,
    List<Program>? programsList,
    Failure? failure,
  }) =>
      ProgramsState(
        getProgramsState: getProgramsState ?? this.getProgramsState,
        programsList: programsList ?? this.programsList,
        failure: failure ?? this.failure,
      );
}
