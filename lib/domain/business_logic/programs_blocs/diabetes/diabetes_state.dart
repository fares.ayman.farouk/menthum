part of 'diabetes_bloc.dart';

class DiabetesState {
  RequestState addReadingDiabetesState;
  RequestState enrollDiabetesState;
  RequestState todayDiabetesState;
  RequestState historyDiabetesState;
  bool loadingMore;
  Failure? failure;
  DiabetesData? diabetesData;
  DiabetesHistory? historyDiabetesData;
  EnrollToProgram? enrollToProgram;

  DiabetesState({
    this.addReadingDiabetesState = RequestState.initiate,
    this.enrollDiabetesState = RequestState.initiate,
    this.todayDiabetesState = RequestState.initiate,
    this.historyDiabetesState = RequestState.initiate,
    this.enrollToProgram,
    this.historyDiabetesData,
    this.diabetesData,
    this.loadingMore = false,
    this.failure,
  });

  DiabetesState copyWith({
    RequestState? addReadingDiabetesState,
    RequestState? enrollDiabetesState,
    RequestState? todayDiabetesState,
    RequestState? historyDiabetesState,
    EnrollToProgram? enrollToProgram,
    DiabetesHistory? historyDiabetesData,
    DiabetesData? diabetesData,
    bool? loadingMore,
    Failure? failure,
  }) =>
      DiabetesState(
        failure: failure ?? this.failure,
        loadingMore: loadingMore ?? this.loadingMore,
        diabetesData: diabetesData ?? this.diabetesData,
        addReadingDiabetesState:
            addReadingDiabetesState ?? this.addReadingDiabetesState,
        historyDiabetesData: historyDiabetesData ?? this.historyDiabetesData,
        enrollToProgram: enrollToProgram ?? this.enrollToProgram,
        enrollDiabetesState: enrollDiabetesState ?? this.enrollDiabetesState,
        todayDiabetesState: todayDiabetesState ?? this.todayDiabetesState,
        historyDiabetesState: historyDiabetesState ?? this.historyDiabetesState,
      );
}
