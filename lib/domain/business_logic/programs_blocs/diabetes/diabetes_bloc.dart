import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/domain/models/diabetes/diabetes_data.dart';

import '../../../../data/network/error_handler.dart';
import '../../../../data/network/failure.dart';
import '../../../models/diabetes/diabetes_action.dart';
import '../../../models/diabetes/diabetes_done.dart';
import '../../../models/diabetes/diabetes_history.dart';
import '../../../models/diabetes/enroll_diabetes.dart';
import '../../../models/diabetes/enroll_to_program.dart';
import '../../../models/digital_prescriptions_mapper.dart';
import '../../../repositories/programs/diabetes_repository.dart';



part 'diabetes_event.dart';

part 'diabetes_state.dart';

class DiabetesBloc extends Bloc<DiabetesEvent, DiabetesState> {
  final DiabetesRepository repository;
  int currentPage = 1, lastPage = 1;
  bool isMoreLoadRunning = false;
  ScrollController scrollController = ScrollController();

  DiabetesBloc(this.repository)
      : super(DiabetesState(
            addReadingDiabetesState: RequestState.initiate,
            historyDiabetesState: RequestState.initiate,
            todayDiabetesState: RequestState.initiate,
            enrollDiabetesState: RequestState.initiate)) {
    /// scroll Controller
    scrollController.addListener(() {
      _onScroll();
    });

    on<EnrollDiabetesEvent>(_enrollDiabetes);
    on<TodayDiabetesEvent>(_todayDiabetesEvent);
    on<ShowReadingHistory>(_showReadingHistoryEvent);
    on<AddReadingEvent>(_addReadingEvent);
    on<ShowMoreReadingHistory>(_showMoreReading);
  }

  FutureOr<void> _enrollDiabetes(
      EnrollDiabetesEvent event, Emitter<DiabetesState> emit) async {
    emit(state.copyWith(
      enrollDiabetesState: RequestState.loading,
      todayDiabetesState: RequestState.initiate,
      addReadingDiabetesState: RequestState.initiate,
    ));
    final response = await repository.enroll(event.enrollDiabetes);
    response.fold((failure) {
      emit(state.copyWith(
          enrollDiabetesState: RequestState.error, failure: failure));
    }, (response) {
      emit(state.copyWith(
        todayDiabetesState: RequestState.initiate,
        enrollDiabetesState: RequestState.loaded,
      ));
      add(TodayDiabetesEvent());
    });
  }

  FutureOr<void> _todayDiabetesEvent(
      TodayDiabetesEvent event, Emitter<DiabetesState> emit) async {
    emit(state.copyWith(
        addReadingDiabetesState: RequestState.initiate,
        todayDiabetesState: RequestState.loading,
        enrollDiabetesState: RequestState.initiate));
    Either<Failure, DiabetesData> response = await repository.today();
    response.fold((failure) {
      if (failure.code == ResponseCode.BAD_REQUEST) {
        final enrolled =
            EnrollToProgram.fromJson(jsonDecode(failure.body)['data']);
        emit(state.copyWith(
            todayDiabetesState: RequestState.error,
            enrollDiabetesState: RequestState.initiate,
            failure: failure,
            enrollToProgram: enrolled));
      } else {
        emit(state.copyWith(
            todayDiabetesState: RequestState.error, failure: failure));
      }
    }, (diabetesData) {
      emit(state.copyWith(
        todayDiabetesState: RequestState.loaded,
        enrollDiabetesState: RequestState.initiate,
        diabetesData: diabetesData,
      ));
    });
  }

  FutureOr<void> _showReadingHistoryEvent(
      ShowReadingHistory event, Emitter<DiabetesState> emit) async {
    currentPage = 1;
    emit(state.copyWith(
        addReadingDiabetesState: RequestState.initiate,
        historyDiabetesState: RequestState.loading,
        enrollDiabetesState: RequestState.initiate));
    Either<Failure, DiabetesHistory> response =
        await repository.history(currentPage);
    response.fold((failure) {
      emit(state.copyWith(
          historyDiabetesState: RequestState.error, failure: failure));
    }, (diabetesHistory) {
      log("LAST PAGE ${diabetesHistory.lastPage}");
      lastPage = diabetesHistory.lastPage;
      emit(state.copyWith(
        historyDiabetesState: RequestState.loaded,
        enrollDiabetesState: RequestState.initiate,
        historyDiabetesData: diabetesHistory,
      ));
      log("State historyDiabetesState: ${state.historyDiabetesState} --//n todayDiabetesState: ${state.todayDiabetesState}  //n--enrollDiabetesState:  ${state.enrollDiabetesState}//n addReadingDiabetesState: ${state.addReadingDiabetesState}");
    });
  }

  FutureOr<void> _addReadingEvent(
      AddReadingEvent event, Emitter<DiabetesState> emit) async {
    emit(state.copyWith(
      addReadingDiabetesState: RequestState.loading,
    ));
    final response = await repository.done(event.diabetesDone);
    response.fold((failure) {
      emit(state.copyWith(
          addReadingDiabetesState: RequestState.error, failure: failure));
    }, (response) {
      emit(state.copyWith(
        addReadingDiabetesState: RequestState.loaded,
      ));
      if (event.fromHistory) {
        add(ShowReadingHistory());
      } else {
        add(TodayDiabetesEvent());
      }
    });
  }

  void _onScroll() {
    double maxScroll = scrollController.position.maxScrollExtent;
    double currentScroll = scrollController.position.pixels;
    if (currentScroll == maxScroll) {
      currentPage++;
      if (currentPage <= lastPage) {
        log("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE $currentPage $lastPage");

        add(ShowMoreReadingHistory(
          nextPage: currentPage,
        ));
      }
    }
  }

  @override
  Future<void> close() {
    scrollController.dispose();
    return super.close();
  }

  FutureOr<void> _showMoreReading(
      ShowMoreReadingHistory event, Emitter<DiabetesState> emit) async {
    emit(state.copyWith(loadingMore: true));

    Either<Failure, DiabetesHistory> response =
        await repository.history(currentPage);
    response.fold((failure) {
      emit(state.copyWith(
          historyDiabetesState: RequestState.error, failure: failure));
    }, (diabetesHistory) {
      log("LAST PAGE ${diabetesHistory.lastPage}");

      lastPage = diabetesHistory.lastPage;
      log("old length: ${state.historyDiabetesData!.diabetesAction.length}");

      log("new length: ${diabetesHistory.diabetesAction.length}");

      List<DiabetesAction> diabetesAction =
          (state.historyDiabetesData!.diabetesAction) +
              (diabetesHistory.diabetesAction);

      diabetesHistory.diabetesAction = diabetesAction;
      log("final length: ${diabetesHistory.diabetesAction.length}");
      emit(state.copyWith(
        loadingMore: false,
        historyDiabetesData: diabetesHistory,
      ));
    });
  }
}
