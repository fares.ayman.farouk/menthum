part of 'diabetes_bloc.dart';

abstract class DiabetesEvent {}

class EnrollDiabetesEvent extends DiabetesEvent {
  EnrollDiabetes enrollDiabetes;

  EnrollDiabetesEvent({required this.enrollDiabetes});
}

class TodayDiabetesEvent extends DiabetesEvent {}

class AddReadingEvent extends DiabetesEvent {
  final DiabetesDone diabetesDone;
  final bool fromHistory;

  AddReadingEvent({
    required this.diabetesDone,
    this.fromHistory = false,
  });
}

class ShowReadingHistory extends DiabetesEvent {}

class ShowMoreReadingHistory extends DiabetesEvent {
  final int nextPage;

  ShowMoreReadingHistory({required this.nextPage});
}
