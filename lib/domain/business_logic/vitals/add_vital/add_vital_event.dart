part of 'add_vital_bloc.dart';

abstract class AddVitalEvent {}

class AddRead extends AddVitalEvent {
  AddReadRequestEntity addReadRequestEntity;
  AddRead(this.addReadRequestEntity);
}
