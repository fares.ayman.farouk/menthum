import 'dart:developer';

import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../data/network/failure.dart';
import '../../../models/digital_prescriptions_mapper.dart';
import '../../../models/vitals/request/add_read_request.dart';
import '../../../repositories/vital_repository.dart';

part 'add_vital_event.dart';

part 'add_vital_state.dart';

class AddVitalBloc extends Bloc<AddVitalEvent, AddVitalState> {
  VitalRepository vitalRepository;

  AddVitalBloc(this.vitalRepository)
      : super(AddVitalState(addReadState: RequestState.initiate)) {
    on<AddRead>((event, emit) async {
      emit(state.copyWith(addReadState: RequestState.loading));
      final response =
          await vitalRepository.addRead(event.addReadRequestEntity);
      response.fold((failure) {
        emit(
            state.copyWith(failure: failure, addReadState: RequestState.error));
      }, (data) {
        emit(
          state.copyWith(
            addReadState: RequestState.loaded,
          ),
        );
      });
    });
  }

  @override
  Future<void> close() {
    log("CLosssee BLOC");
    return super.close();
  }
}
