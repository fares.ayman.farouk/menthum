part of 'add_vital_bloc.dart';

class AddVitalState {
  RequestState? addReadState;
  Failure? failure;

  AddVitalState({this.addReadState, this.failure});

  AddVitalState copyWith({
    RequestState? addReadState,
    Failure? failure,
  }) {
    return AddVitalState(
      failure: failure ?? this.failure,
      addReadState: addReadState ?? this.addReadState,
    );
  }
}
