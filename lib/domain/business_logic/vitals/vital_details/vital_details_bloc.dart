import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/domain/models/vitals/response/vital_details_entity.dart';

import '../../../../data/network/failure.dart';
import '../../../models/digital_prescriptions_mapper.dart';
import '../../../models/vitals/request/reading_request.dart';
import '../../../repositories/vital_repository.dart';

part 'vital_details_event.dart';

part 'vital_details_state.dart';

class VitalDetailsBloc extends Bloc<VitalDetailsEvent, VitalDetailsState> {
  VitalRepository vitalRepository;

  VitalDetailsBloc(this.vitalRepository)
      : super(VitalDetailsState(vitalsState: RequestState.initiate)) {

    /// get readings
    on<GetVitalDetails>((event, emit) async {
      emit(state.copyWith(vitalsState: RequestState.loading));
      final response =
          await vitalRepository.getVitalDetails(event.readingRequestEntity);
      response.fold((failure) {

        emit(state.copyWith(failure: failure, vitalsState: RequestState.error));
      }, (data) {
        emit(
          state.copyWith(
            vitalsState: RequestState.loaded,
            vitals: data,
          ),
        );
      });
    });
  }
}
