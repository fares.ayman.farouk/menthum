part of 'vital_details_bloc.dart';

abstract class VitalDetailsEvent {}

class GetVitalDetails extends VitalDetailsEvent {
  ReadingRequestEntity readingRequestEntity;

  GetVitalDetails({required this.readingRequestEntity});
}
