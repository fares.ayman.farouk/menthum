part of 'vital_details_bloc.dart';

class VitalDetailsState {
  RequestState? vitalsState;
  Failure? failure;
  List<VitalDetailsEntity>? vitals;

  VitalDetailsState({this.vitalsState, this.vitals, this.failure});

  VitalDetailsState copyWith({
    RequestState? vitalsState,
    Failure? failure,
    List<VitalDetailsEntity>? vitals,
  }) {
    return VitalDetailsState(
      failure: failure ?? this.failure,
      vitalsState: vitalsState ?? this.vitalsState,
      vitals: vitals ?? this.vitals,
    );
  }
}
