part of 'vital_bloc.dart';

class VitalState {
  RequestState? vitalsState;
  RequestState? addReadState;
  Failure? failure;
  List<VitalEntity>? vitals;

  VitalState({this.vitalsState, this.vitals, this.addReadState,this.failure});

  VitalState copyWith({
    RequestState? vitalsState,
    RequestState? addReadState,
    Failure? failure,
    List<VitalEntity>? vitals,
  }) {
    return VitalState(
      failure: failure ?? this.failure,
      vitalsState: vitalsState ?? this.vitalsState,
      addReadState: addReadState ?? this.addReadState,
      vitals: vitals ?? this.vitals,
    );
  }
}
