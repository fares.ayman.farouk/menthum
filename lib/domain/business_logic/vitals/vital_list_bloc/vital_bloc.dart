import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rxdart/rxdart.dart';

import '../../../../data/network/failure.dart';
import '../../../models/digital_prescriptions_mapper.dart';
import '../../../models/vitals/response/vital_entity.dart';
import '../../../repositories/vital_repository.dart';
import '../add_vital/add_vital_bloc.dart';

part 'vital_event.dart';

part 'vital_state.dart';

class VitalBloc extends Bloc<VitalEvent, VitalState> {
  VitalRepository vitalRepository;
  final AddVitalBloc addVitalBloc;
  StreamSubscription<List<AddVitalState>>? _addVitalStates;

  VitalBloc(this.vitalRepository, this.addVitalBloc)
      : super(VitalState(vitalsState: RequestState.initiate)) {
    _addVitalStates = addVitalBloc.stream
        .startWith(addVitalBloc.state)
        .pairwise()
        .listen((pair) {
      final p = pair.first;
      final c = pair.last;

      if (p.addReadState != c.addReadState &&
          c.addReadState == RequestState.loaded) {

        add(GetPatientReadings());
      }
    });

    /// get readings
    on<GetPatientReadings>((event, emit) async {
      if (state.vitalsState != RequestState.loaded) {
        emit(state.copyWith(vitalsState: RequestState.loading));
      }
      final response = await vitalRepository.getPatientReadings();
      response.fold((failure) {
        emit(state.copyWith(failure: failure, vitalsState: RequestState.error));
      }, (data) {
        emit(
          state.copyWith(
            vitalsState: RequestState.loaded,
            vitals: data,
          ),
        );
      });
    });
  }

  @override
  Future<void> close() {
    _addVitalStates?.cancel();
    return super.close();
  }
}
