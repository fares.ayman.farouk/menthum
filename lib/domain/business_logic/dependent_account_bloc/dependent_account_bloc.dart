import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../application/app_preference.dart';
import '../../../application/di.dart';
import '../../../data/models/apiRequest/dependant_account_request/add_dependant_account_request.dart';
import '../../../data/models/apiResponse/auth_response/login_api_response.dart';
import '../../../data/models/apiResponse/dependant_account_responses/get_dependant_accounts_response.dart';
import '../../../data/network/failure.dart';
import '../../repositories/auth_repositories.dart';

part 'dependent_account_event.dart';

part 'dependent_account_state.dart';

class DependentAccountBloc
    extends Bloc<DependentAccountEvent, DependentAccountState> {
  final AuthRepository repository;
  final AppPreferences _appPreferences = instance<AppPreferences>();

  //List<DependantAccountData> dependantAccounts = [];
  bool mainSwitchVisible = false;

  DependentAccountBloc(this.repository) : super(DependentAccountInitial()) {
    ///add dependant account
    on<AddDependantAccount>((event, emit) async {
      emit(LoadingDependantState());
      Either<Failure, LoginResponse> response =
          await repository.addDependantAccount(event.accountRequest);
      response.fold((failure) {
        emit(FailureDependantState(failure));
      }, (loginResponse) {
        emit(SuccessAddDependantAccountState(loginResponse.data!));
        add(GetDependantAccounts());

      });
    });

    /// get dependant accounts
    on<GetDependantAccounts>((event, emit) async {
      emit(LoadingGetDependantState());
      Either<Failure, GetDependantAccountsResponse> response =
          await repository.getDependantAccounts();

      mainSwitchVisible = await parentSwitched();
      //print("mainSwitchVisible $mainSwitchVisible");

      response.fold((failure) {
        emit(FailureGetDependantState(failure));
      }, (dependantAccountsResponse) {
        if (dependantAccountsResponse.data != null) {
          //dependantAccounts = dependantAccountsResponse.data!;
          emit(SuccessGetDependantAccountsState(
              dependantAccountsResponse.data!));
        }
      });
    });

    /// show and set dependant account
    on<ShowDependantAccount>((event, emit) async {
      emit(LoadingDependantState());

      Either<Failure, LoginResponse> response =
          await repository.showDependantAccount(event.id);
      response.fold((failure) {
        emit(FailureDependantState(failure));
      }, (loginResponse) {
        _appPreferences.getParentUserToken().then((parentToken) {
          //print(parentToken);
          _appPreferences.addTemporaryToken(parentToken!);

          //print(loginResponse.data?.token);
          _appPreferences
              .addParentUserToken(loginResponse.data?.token ?? "")
              .then((value) => addToken(loginResponse));
        });
      });
    });
  }

  parentSwitched() async {
    return await _appPreferences.hasParent().then((value) => value);
  }

  addToken(loginResponse) {
    emit(SuccessDependantAccountState(loginResponse));
  }
}
