part of 'dependent_account_bloc.dart';

abstract class DependentAccountState {}

class DependentAccountInitial extends DependentAccountState {}

class LoadingGetDependantState extends DependentAccountState {}

class LoadingDependantState extends DependentAccountState {}

class SuccessDependantAccountState extends DependentAccountState {
  LoginResponse loginResponse;

  SuccessDependantAccountState(this.loginResponse);
}

class SuccessAddDependantAccountState extends DependentAccountState {
  UserData data;

  SuccessAddDependantAccountState(this.data);

}

class EmptyDependantAccountsState extends DependentAccountState {

}

class SuccessGetDependantAccountsState extends DependentAccountState {
  List<DependantAccountData> dependantAccounts;
  SuccessGetDependantAccountsState(this.dependantAccounts);
}

class FailureDependantState extends DependentAccountState {
  Failure failure;

  FailureDependantState(this.failure);
}
class FailureGetDependantState extends DependentAccountState {
  Failure failure;

  FailureGetDependantState(this.failure);
}

