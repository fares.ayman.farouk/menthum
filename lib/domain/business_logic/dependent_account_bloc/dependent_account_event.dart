part of 'dependent_account_bloc.dart';

abstract class DependentAccountEvent {}

class AddDependantAccount extends DependentAccountEvent {
  AddDependantAccountRequest accountRequest;
  AddDependantAccount(this.accountRequest);
}

class GetDependantAccounts extends DependentAccountEvent {}

class ShowDependantAccount extends DependentAccountEvent {
  String id;

  ShowDependantAccount(this.id);
}
