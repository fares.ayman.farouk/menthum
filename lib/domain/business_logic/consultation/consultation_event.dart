part of 'consultation_bloc.dart';

abstract class ConsultationEvent {}

class GetConsultationList extends ConsultationEvent {
  int page;

  GetConsultationList(this.page);
}

class GetMoreConsultationList extends ConsultationEvent {
  int page;

  GetMoreConsultationList(this.page);
}


