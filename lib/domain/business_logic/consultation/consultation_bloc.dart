import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/data/models/apiResponse/upcoming_response.dart';
import 'package:patientapp/domain/repositories/consultations_repositories.dart';

import '../../../data/models/apiResponse/consultations_responses/consultations_response.dart';
import '../../../data/network/failure.dart';

part 'consultation_event.dart';

part 'consultation_state.dart';

class ConsultationBloc extends Bloc<ConsultationEvent, ConsultationState> {
  final ConsultationsRepositories repository;


  List<ConsultationItem> consultationItem = [];
  //late Meta meta;
  int currentPage = 1, lastPage = 1;
  bool isMoreLoadRunning = false;

  ConsultationBloc(this.repository) : super(ConsultationInitial()) {
    on<GetConsultationList>((event, emit) async {
      emit(ConsultationLoading());
      Either<Failure, ConsultationsResponse> response =
          await repository.getConsultations(event.page);

      response.fold((failure) {
        emit(ConsultationFails(failure));
      }, (consultationResponse) {
        if (consultationResponse.data == null ||
            consultationResponse.data!.isEmpty) {
          emit(ConsultationEmpty());
        } else {
          //print ('bloc before emit success');
          emit(ConsultationSuccess());
          consultationItem = consultationResponse.data!;

          currentPage = consultationResponse.meta!.currentPage!;
          lastPage = consultationResponse.meta!.lastPage!;

          //print ('bloc after emit success');
        }
      });
    });

    on<GetMoreConsultationList>((event, emit) async {
      isMoreLoadRunning = true;
      emit(ConsultationMoreLoading());

      Either<Failure, ConsultationsResponse> response =
          await repository.getConsultations(event.page);

      response.fold((failure) {
        emit(ConsultationFails(failure));
      }, (consultationResponse) {
        isMoreLoadRunning = false;
        emit(ConsultationMoreSuccess());

        consultationItem.addAll(consultationResponse.data!);

        currentPage = consultationResponse.meta!.currentPage!;
        lastPage = consultationResponse.meta!.lastPage!;
      });
    });
  }
}
