part of 'consultation_bloc.dart';

abstract class ConsultationState {}

class ConsultationInitial extends ConsultationState {}

class ConsultationLoading extends ConsultationState {}

class ConsultationMoreLoading extends ConsultationState {}

class ConsultationSuccess extends ConsultationState {
  ConsultationSuccess();
}

class ConsultationMoreSuccess extends ConsultationState {
  ConsultationMoreSuccess();
}

class ConsultationFails extends ConsultationState {
  final Failure failure;

  ConsultationFails(this.failure);
}

class ConsultationEmpty extends ConsultationState {}


