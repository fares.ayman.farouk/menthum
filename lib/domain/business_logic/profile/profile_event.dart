part of 'profile_bloc.dart';


abstract class ProfileEvent{}

class GetProfileEvent extends ProfileEvent{}

class GetMedicalProfileEvent extends ProfileEvent{}

class EditMedicalProfileEvent extends ProfileEvent{
  final EditMedicalProfileApiRequest editMedicalProfileApiRequest;

  EditMedicalProfileEvent(this.editMedicalProfileApiRequest);
}

class EditProfileEvent extends ProfileEvent{
  final EditProfileApiRequest editProfileApiRequest;

  EditProfileEvent(this.editProfileApiRequest);
}

class UpdatePasswordEvent extends ProfileEvent{
  final UpdatePasswordApiRequest updatePasswordApiRequest;

  UpdatePasswordEvent(this.updatePasswordApiRequest);
}