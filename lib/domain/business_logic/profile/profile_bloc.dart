import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/UpadePasswordApiRequest.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/login_api_response.dart';
import 'package:patientapp/data/models/apiResponse/base_response.dart';
import 'package:patientapp/domain/repositories/auth_repositories.dart';
import 'package:patientapp/domain/services/analytics_services/analytics_services.dart';

import '../../../application/constants.dart';
import '../../../data/models/apiRequest/auth_requests/EditMedicalProfileApiRequest.dart';
import '../../../data/models/apiRequest/auth_requests/EditProfileApiRequest.dart';
import '../../../data/models/apiResponse/profile/medicalProfileApiResponse.dart';
import '../../../data/network/failure.dart';

part 'profile_event.dart';

part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final AuthRepository repository;
  late Data medicalData;

  ProfileBloc(this.repository) : super(MedicalInitial()) {
    on<GetMedicalProfileEvent>((event, emit) async {
      emit(LoadingState());

      try {
        Either<Failure, MedicalProfileApiResponse> response =
            await repository.getMedicalProfile();

        response.fold((failure) {
          emit(FailedState(failure));
        }, (response) {
          medicalData = response.data!;
          emit(SuccessMedicalState(medicalData));
        });
      } catch (e) {
        print(e);
      }
    });

    on<GetProfileEvent>((event, emit) async {
      emit(LoadingState());

      ///get trigger
      bool finishButtonPressed =
          instance<AppPreferences>().getTriggerForFinishPayment() ?? false;
      int wallet = 0;
      log("Payment Event: trigger : $finishButtonPressed - wallet: $wallet");

      ///get previous wallet if is finish button pressed for payment
      if (finishButtonPressed) {
        wallet = instance<AppPreferences>().getUser()?.wallet ?? 0;
        log("Payment Event: get wallet value $wallet");
      }

      try {
        Either<Failure, LoginResponse> response =
            await repository.getUserProfileData();

        try {
          response.fold((failure) {
            emit(FailedState(failure));
          }, (response) async {
            ///check if the wallet has changed
            ///and the trigger is true for finish payment.

            if (finishButtonPressed && (response.data!.wallet! > wallet)) {
              ///send event and change the finish button pressed to false.
              log("Payment Event: send event and change the finish button pressed to false");
              instance<AnalyticsServices>().addEventAnalytics(
                eventName: EventsAnalytics.finishFawrySuccessfully,
                parameterName: EventsAnalytics.patientFinishFawrySuccessfully,
              );
              instance<AppPreferences>().addTriggerForFinishPayment(false);
            } else {
              instance<AppPreferences>().addTriggerForFinishPayment(false);
            }

            emit(SuccessProfileState(response.data!));
          });
        } catch (e, s) {
          print(s);
        }
      } catch (e) {
        print(e);
      }
    });

    on<EditMedicalProfileEvent>((event, emit) async {
      emit(LoadingEditState());

      try {
        Either<Failure, MedicalProfileApiResponse> response = await repository
            .editMedicalProfile(event.editMedicalProfileApiRequest);

        response.fold((failure) {
          emit(FailedEditMedicalState(failure));
        }, (response) {
          medicalData = response.data!;
          emit(SuccessEditMedicalState(medicalData));
        });
      } catch (e) {
        print(e);
      }
    });

    on<EditProfileEvent>((event, emit) async {
      emit(LoadingEditState());

      try {
        Either<Failure, LoginResponse> response =
            await repository.editUserProfile(event.editProfileApiRequest);

        response.fold((failure) {
          emit(FailedEditProfileState(failure));
        }, (response) {
          emit(SuccessEditProfileState(response.data!));
        });
      } catch (e) {
        print(e);
      }
    });

    on<UpdatePasswordEvent>((event, emit) async {
      emit(LoadingEditState());

      try {
        Either<Failure, BaseResponse> response =
            await repository.updatePassword(event.updatePasswordApiRequest);

        response.fold((failure) {
          emit(FailedUpdatePasswordState(failure));
        }, (response) {
          String msg = response.message!;
          emit(SuccessUpdatePasswordState(msg));
        });
      } catch (e) {
        print(e);
      }
    });
  }
}
