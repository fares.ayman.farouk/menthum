part of 'profile_bloc.dart';


abstract class ProfileState{}


class MedicalInitial extends ProfileState {}

class LoadingState extends ProfileState {}

class LoadingEditState extends ProfileState {}

class SuccessMedicalState extends ProfileState {
  final Data medicalProfileData;

  SuccessMedicalState(this.medicalProfileData);
}

class SuccessUpdatePasswordState extends ProfileState {
  final String  msg;
  SuccessUpdatePasswordState(this.msg);
}

class SuccessEditMedicalState extends ProfileState {
  final Data medicalProfileData;

  SuccessEditMedicalState(this.medicalProfileData);
}

class SuccessProfileState extends ProfileState {
  final UserData user;

  SuccessProfileState(this.user);
}

class SuccessEditProfileState extends ProfileState {
  final UserData user;

  SuccessEditProfileState(this.user);
}

class FailedState extends ProfileState {
  final Failure failure;

  FailedState(this.failure);
}

class FailedEditMedicalState extends ProfileState {
  final Failure failure;

  FailedEditMedicalState(this.failure);
}

class FailedEditProfileState extends ProfileState {
  final Failure failure;

  FailedEditProfileState(this.failure);
}

class FailedUpdatePasswordState extends ProfileState {
  final Failure failure;

  FailedUpdatePasswordState(this.failure);
}


