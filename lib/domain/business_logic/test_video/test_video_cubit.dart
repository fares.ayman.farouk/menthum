import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
// import 'package:fluttertoast/fluttertoast.dart';
import 'package:patientapp/domain/business_logic/application_life_cycle/application_life_cycle_cubit.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:rxdart/rxdart.dart';

import '../../../presentation/resources/strings_manger.dart';

part 'test_video_state.dart';

class TestVideoCubit extends Cubit<TestVideoState> {
  ApplicationLifeCycleCubit applicationLifeCycle;
  StreamSubscription<Iterable<ApplicationLifeCycleState>>?
      _appLifeCycleSubscription;

  TestVideoCubit({required this.applicationLifeCycle})
      : super(TestVideoState()) {
    checkStatusPermissions();
    _appLifeCycleSubscription = applicationLifeCycle.stream
        .startWith(applicationLifeCycle.state)
        .pairwise()
        .listen((pair) async {
      final previous = pair.first;
      final current = pair.last;
      if (previous.isResumed != current.isResumed && current.isResumed) {
        await checkStatusPermissions();
      }
    });
  }

  @override
  Future<void> close() async {
    await _appLifeCycleSubscription?.cancel();
    super.close();
  }

  checkStatusPermissions() async {
    List<PermissionStatus> statuses = [
      await Permission.camera.status,
      await Permission.microphone.status,
      await Permission.bluetoothConnect.status,
    ];

    log("${statuses[0].toString()} - ${statuses[1].toString()} - ${statuses[2].toString()}");

    emit(state.copyWith(
      cameraPermissionStatus: statuses[0],
      micPermissionStatus: statuses[1],
      bluetoothPermissionStatus:
          Platform.isAndroid ? statuses[2] : PermissionStatus.granted,
    ));
    if (state.hasPermissionDenied) {
      showPermissionDialog();
      log("showPermissionDialog ${state.displayPermissionDialog}");
    }
    if (state.displayOpenAppSettingsDialog && state.allPermissionGranted) {
      emit(state.copyWith(displayOpenAppSettingsDialog: false));
    }
    if (state.displayPermissionDialog && state.allPermissionGranted) {
      emit(state.copyWith(displayPermissionDialog: false));
    }
  }

  requestCameraPermission() async {
    PermissionStatus cameraPermissionStatus = await Permission.camera.request();

    emit(state.copyWith(
      cameraPermissionStatus: cameraPermissionStatus,
    ));
    if (cameraPermissionStatus.isPermanentlyDenied) {
      // Fluttertoast.showToast(msg: AppStrings.cameraPermissionError.tr());
      openAppSettingsDialog(AppStrings.cameraPermissionError.tr());
    } else {
      hidePermissionDialog();
    }
  }

  requestMicPermission() async {
    PermissionStatus micPermissionStatus =
        await Permission.microphone.request();
    emit(state.copyWith(
      micPermissionStatus: micPermissionStatus,
    ));
    if (micPermissionStatus.isPermanentlyDenied) {
      // Fluttertoast.showToast(msg: AppStrings.micPermissionError.tr());
      openAppSettingsDialog(AppStrings.micPermissionError.tr());
    } else {
      hidePermissionDialog();
    }
  }

  requestBluetoothConnect() async {
    PermissionStatus bluetoothPermissionStatus =
        await Permission.bluetoothConnect.request();

    emit(state.copyWith(
      bluetoothPermissionStatus: bluetoothPermissionStatus,
    ));
    if (bluetoothPermissionStatus.isPermanentlyDenied) {
      // Fluttertoast.showToast(msg: AppStrings.bluePermissionError.tr());

      openAppSettingsDialog(AppStrings.bluePermissionError.tr());
    } else {
      hidePermissionDialog();
    }
  }

  showPermissionDialog() {
    emit(state.copyWith(displayPermissionDialog: true));
  }

  hidePermissionDialog() {
    bool allPermissionGranted = state.allPermissionGranted;
    log("hidePermissionDialog $allPermissionGranted");
    if (allPermissionGranted) {
      emit(state.copyWith(displayPermissionDialog: false));
    }
  }

  openAppSettingsDialog(String message) {
    emit(state.copyWith(
        displayOpenAppSettingsDialog: true, appSettingsMessage: message));
  }

  hideAppSettingsDialog() {
    emit(state.copyWith(displayOpenAppSettingsDialog: false));
  }
}
