part of 'test_video_cubit.dart';

class TestVideoState {
  final PermissionStatus cameraPermissionStatus;
  final PermissionStatus micPermissionStatus;
  final PermissionStatus bluetoothPermissionStatus;
  final bool displayPermissionDialog;
  final bool displayOpenAppSettingsDialog;
  final String appSettingsMessage;

  TestVideoState(
      {this.cameraPermissionStatus = PermissionStatus.denied,
      this.micPermissionStatus = PermissionStatus.denied,
      this.bluetoothPermissionStatus = PermissionStatus.denied,
      this.appSettingsMessage = '',
      this.displayPermissionDialog = false,
      this.displayOpenAppSettingsDialog = false});

  bool get allPermissionGranted =>
      cameraPermissionStatus.isGranted &&
      micPermissionStatus.isGranted &&
      bluetoothPermissionStatus.isGranted;

  bool get hasPermissionDenied =>
      cameraPermissionStatus.isDenied ||
      micPermissionStatus.isDenied ||
      bluetoothPermissionStatus.isDenied;

  bool get hasPermissionDeniedForever =>
      cameraPermissionStatus.isPermanentlyDenied ||
      micPermissionStatus.isPermanentlyDenied ||
      bluetoothPermissionStatus.isPermanentlyDenied;

  TestVideoState copyWith({
    PermissionStatus? cameraPermissionStatus,
    PermissionStatus? micPermissionStatus,
    PermissionStatus? bluetoothPermissionStatus,
    bool? displayPermissionDialog,
    bool? displayOpenAppSettingsDialog,
    String? appSettingsMessage,
  }) {
    return TestVideoState(
      cameraPermissionStatus:
          cameraPermissionStatus ?? this.cameraPermissionStatus,
      micPermissionStatus: micPermissionStatus ?? this.micPermissionStatus,
      bluetoothPermissionStatus:
          bluetoothPermissionStatus ?? this.bluetoothPermissionStatus,
      displayPermissionDialog:
          displayPermissionDialog ?? this.displayPermissionDialog,
      displayOpenAppSettingsDialog:
          displayOpenAppSettingsDialog ?? this.displayOpenAppSettingsDialog,
      appSettingsMessage: appSettingsMessage ?? this.appSettingsMessage,
    );
  }
}
