part of 'question_bloc.dart';

abstract class QuestionEvent {}

class GetQuestionsEvent extends QuestionEvent {}

class SendQuestionAnswersEvent extends QuestionEvent {
  final bool isCameraClosed;

  SendQuestionAnswersEvent({required this.isCameraClosed});
}
