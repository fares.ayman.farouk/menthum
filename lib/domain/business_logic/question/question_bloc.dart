import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/application/utils.dart';
import 'package:patientapp/data/models/apiResponse/question_responses/question_response.dart';
import 'package:patientapp/data/network/error_handler.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';

import '../../../application/app_preference.dart';
import '../../../data/models/apiError/login_api_error.dart';
import '../../../data/models/apiRequest/question_requests/question_answers_request.dart';
import '../../../data/models/apiResponse/question_responses/question_answers_response.dart';
import '../../../data/network/failure.dart';
import 'package:quiver/iterables.dart';

import '../../repositories/questions_repositories.dart';
import '../../services/pusher_services/pusher_services.dart';

part 'question_event.dart';

part 'question_state.dart';

Iterable<List<QuestionResponseItem>> chunk(
    List<QuestionResponseItem> questionsList) {
  Iterable<List<QuestionResponseItem>> questions = partition(questionsList, 5);
  return questions;
}

class QuestionBloc extends Bloc<QuestionEvent, QuestionState> {
  double valueSlider = 0;
  final PageController pageController = PageController();
  int currentPage = 0;
  String? coupon;
  PusherServices pusherServices = instance();
  AppPreferences appPreferences = instance();
  String userId = "";

  final QuestionRepositories repository;

  late Iterable<List<QuestionResponseItem>> questionsList;

  QuestionBloc(this.repository) : super(QuestionInitial()) {
    on<QuestionEvent>((event, emit) async {});

    /// get Questions
    on<GetQuestionsEvent>((event, emit) async {
      emit(QuestionLoading());
      Either<Failure, QuestionResponse> response =
          await repository.getQuestions();

      response.fold((failure) {
        emit(QuestionFails(failure));
      }, (questionResponse) {
        if (questionResponse.data == null || questionResponse.data!.isEmpty) {
          emit(QuestionEmpty());
        } else {
          emit(QuestionSuccess());

          questionsList = chunk(questionResponse.data!);
        }
      });
    });

    /// send Answers

    on<SendQuestionAnswersEvent>((event, emit) async {
      List<QuestionAnswers> answers = fillAnswersList();
      initPusher();

      Either<Failure, QuestionAnswersResponse> response =
          await repository.sendQuestionsAnswers(answers, coupon);

      response.fold((failure) {
        closePusher();

        if (failure.code == ResponseCode.NO_INTERNET_CONNECTION) {
          showToast(
              message: AppStrings.noInternetConnection.tr(), success: false);
        } else {
          LoginApiErrorResponse r =
              loginApiErrorResponseFromJson(failure.body.toString());

          Fluttertoast.showToast(
              msg: r.message ?? AppStrings.noAvailableGp.tr());
        }
      }, (questionResponse) {
        if (questionResponse.data!.type == 1) {
          closePusher();
        }
        emit(AnswersQuestionSuccess(questionResponse, event.isCameraClosed));
      });
    });
  }

  /// init pusher

  void initPusher() {
    userId = appPreferences.getUserId().replaceAll('-', '_');
    log("UserID: $userId");
    pusherServices.firePusher(userId);
  }

  closePusher() {
    pusherServices.closeChannel(userId);
  }

  /// fill Answers List

  List<QuestionAnswers> fillAnswersList() {
    List<QuestionAnswers> answers = [];
    for (int i = 0; i < questionsList.length; i++) {
      for (int x = 0; x < questionsList.elementAt(i).length; x++) {
        answers.add(QuestionAnswers(
            id: questionsList.elementAt(i).elementAt(x).id,
            type: questionsList.elementAt(i).elementAt(x).type,
            answer: questionsList.elementAt(i).elementAt(x).answer));
      }
    }

    return answers;
  }
}
