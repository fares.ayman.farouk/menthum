part of 'question_bloc.dart';

abstract class QuestionState {}

class QuestionInitial extends QuestionState {}

class QuestionLoading extends QuestionState {}

class AnswersQuestionLoading extends QuestionState {}

class QuestionSuccess extends QuestionState {}

class AnswersQuestionSuccess extends QuestionState {
  final bool isCameraClosed;
  QuestionAnswersResponse questionAnswersResponse;

  AnswersQuestionSuccess(this.questionAnswersResponse, this.isCameraClosed);
}

class QuestionFails extends QuestionState {
  final Failure failure;

  QuestionFails(this.failure);
}

class QuestionEmpty extends QuestionState {}
