part of 'transactions_bloc.dart';


abstract class TransactionsEvent{}

class GetTransactionsList extends TransactionsEvent{
  int page;
  GetTransactionsList(this.page);
}

class GetMoreTransactionsList extends TransactionsEvent{
  int page;
  GetMoreTransactionsList(this.page);
}

class RechargeBalanceEvent extends TransactionsEvent{
  int amount;
  RechargeBalanceEvent(this.amount);
}