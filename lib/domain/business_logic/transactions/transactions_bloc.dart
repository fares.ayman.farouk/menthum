import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/models/apiResponse/profile/paymentApiResponse.dart';
import '../../../data/models/apiResponse/tranactions_api_response.dart';
import '../../../data/network/failure.dart';
import '../../repositories/consultations_repositories.dart';

part 'transactions_state.dart';

part 'transactions_event.dart';

class TransactionsBloc extends Bloc<TransactionsEvent, TransactionsState> {
  final ConsultationsRepositories repository;
  late TransactionApiResponse transactionResponse;
  late String url;

  TransactionsBloc(this.repository) : super(TransactionsInitial()) {
    on<GetTransactionsList>((event, emit) async {
      emit(LoadingTransactionsState());

      try {
        Either<Failure, TransactionApiResponse> response =
        await repository.getTransactions(event.page);

        response.fold((failure) {
          emit(FailedTransactionsState(failure));
        }, (response) {
          transactionResponse = response;
          emit(SuccessTransactionsState(transactionResponse));
        });
      } catch (e) {
        print(e);
      }
    });

    on<GetMoreTransactionsList>((event, emit) async {
      emit(LoadingMoreTransactionsState());

      try {
        Either<Failure, TransactionApiResponse> response =
        await repository.getTransactions(event.page);

        response.fold((failure) {
          emit(FailedTransactionsState(failure));
        }, (response) {
          transactionResponse = response;
          emit(SuccessMoreTransactionsState(transactionResponse));
        });
      } catch (e) {
        print(e);
      }
    }
    );

    on<RechargeBalanceEvent>((event, emit) async {
      emit(LoadingTransactionsState());

      try {
        Either<Failure, PaymentApiResponse> response =
        await repository.payment(event.amount);

        response.fold((failure) {
          emit(FailedTransactionsState(failure));
        }, (response) {
          url = response.data!.url!;
          emit(SuccessRechargeState(url));
        });
      } catch (e) {
        print(e);
      }
    });
  }
}
