part of 'transactions_bloc.dart';


abstract class TransactionsState{}


class TransactionsInitial extends TransactionsState {}

class LoadingTransactionsState extends TransactionsState {}

class LoadingMoreTransactionsState extends TransactionsState {}

class SuccessTransactionsState extends TransactionsState {
  final TransactionApiResponse transactionResponse;

  SuccessTransactionsState(this.transactionResponse);
}

class SuccessMoreTransactionsState extends TransactionsState {
  final TransactionApiResponse transactionResponse;

  SuccessMoreTransactionsState(this.transactionResponse);
}

class SuccessRechargeState extends TransactionsState {
  final String url;

  SuccessRechargeState(this.url);
}

class FailedTransactionsState extends TransactionsState {
  final Failure failure;

  FailedTransactionsState(this.failure);
}
