part of 'menstrual_cycle_bloc.dart';

class MenstrualCycleState {
  Color circleColor;
  bool showArrow;
  String titleForDeign;
  String endDate;
  MenstrualCycle? chartData;
  Failure? failure;
  RequestState getChartDesignDataState;

  MenstrualCycleState({
    this.getChartDesignDataState = RequestState.initiate,
    this.circleColor = ColorManager.primaryLight,
    this.failure,
    this.titleForDeign = "",
    this.endDate = "",
    this.showArrow = false,
    this.chartData,
  });

  MenstrualCycleState copyWith({
    Color? circleColor,
    bool? showArrow,
    Failure? failure,
    String? titleForDeign,
    String? endDate,
    MenstrualCycle? chartData,
    RequestState? getChartDesignDataState,
  }) {
    return MenstrualCycleState(
      circleColor: circleColor ?? this.circleColor,
      showArrow: showArrow ?? this.showArrow,
      chartData: chartData ?? this.chartData,
      titleForDeign: titleForDeign ?? this.titleForDeign,
      endDate: endDate ?? this.endDate,
      failure: failure ?? this.failure,
      getChartDesignDataState:
          getChartDesignDataState ?? this.getChartDesignDataState,
    );
  }
}
