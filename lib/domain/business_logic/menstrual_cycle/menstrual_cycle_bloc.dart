import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/data/repository/menstrual_repository_imp.dart';
import 'package:patientapp/domain/models/menstrual_cycle/menstrual_cycle.dart';
import 'package:patientapp/domain/repositories/menstrual_repository.dart';
import 'package:patientapp/presentation/resources/color_manger.dart';
import 'package:patientapp/presentation/resources/strings_manger.dart';
import 'package:quiver/time.dart';

import '../../../application/utils.dart';
import '../../../data/network/error_handler.dart';
import '../../models/digital_prescriptions_mapper.dart';
import '../../models/menstrual_cycle/edit_cycle.dart';

part 'menstrual_cycle_event.dart';

part 'menstrual_cycle_state.dart';

class MenstrualCycleBloc
    extends Bloc<MenstrualCycleEvent, MenstrualCycleState> {
  MenstrualRepository menstrualRepository = instance<MenstrualRepositoryImp>();

  MenstrualCycleBloc()
      : super(MenstrualCycleState(
          getChartDesignDataState: RequestState.initiate,
        )) {
    on<EditPeriodDateEvent>(_editPeriodData);
    on<CheckPeriodInDayEvent>(_checkPeriodInDay);
    on<SetupPeriodDateEvent>(_setupPeriodData);
  }

  FutureOr<void> _checkPeriodInDay(
      CheckPeriodInDayEvent event, Emitter<MenstrualCycleState> emit) {
    emit(state.copyWith(
      getChartDesignDataState: RequestState.loading,
    ));
    if (state.chartData != null) {
      MenstrualCycle chartData = state.chartData!;
      chartData.currentDate = event.selectedDate;

      add(SetupPeriodDateEvent(data: chartData));
    } else {
      emit(state.copyWith(
        getChartDesignDataState: RequestState.error,
        failure: DataSource.DEFAULT.getFailure(),
      ));
    }
  }

  FutureOr<void> _setupPeriodData(
      SetupPeriodDateEvent event, Emitter<MenstrualCycleState> emit) {
    emit(state.copyWith(getChartDesignDataState: RequestState.loading));

    MenstrualCycle chartData = event.data;
    chartData.cycleLength = daysInMonth(
            chartData.startPeriodDate.year, chartData.startPeriodDate.month)
        .toDouble();
    bool showArrow = false;
    String titleForDeign = AppStrings.normalDay.tr();
    String endDate = dateTimeToDateEDMY(chartData.endCycleDate);
    Color bgColor = Colors.grey.withOpacity(.4);

    if (chartData.currentDate.compareTo(chartData.startPeriodDate) >= 0 &&
        chartData.currentDate.compareTo(chartData.endPeriodDate) <= 0) {
      // log("SD: ${chartData.startPeriodDate} --- CD: ${chartData.currentDate}  --- ED: ${chartData.endPeriodDate} ");
      showArrow = true;
      bgColor = ColorManager.borderColor;
      endDate = dateTimeToDateEDMY(chartData.endPeriodDate);
      titleForDeign = AppStrings.period.tr();
    } else if (chartData.currentDate
                .compareTo(chartData.startPreOvulationDate) >=
            0 &&
        chartData.currentDate.compareTo(chartData.endPreOvulationDate) <= 0) {
      // log("SPRED: ${chartData.startPreOvulationDate} --- CD: ${chartData.currentDate}  --- EPRED: ${chartData.endPreOvulationDate} ");
      showArrow = true;
      bgColor = ColorManager.primaryLight;
      endDate = dateTimeToDateEDMY(chartData.endPreOvulationDate);
      titleForDeign = AppStrings.preOvulation.tr();
    } else if (chartData.currentDate.compareTo(
                chartData.ovulationDate.subtract(Duration(days: 1))) >=
            0 &&
        chartData.currentDate
                .compareTo(chartData.ovulationDate.add(Duration(days: 1))) <=
            0) {
      // log("ovulation: ${chartData.ovulationDate} --- CD: ${chartData.currentDate}");
      showArrow = true;
      bgColor = ColorManager.primary;
      endDate =
          dateTimeToDateEDMY(chartData.ovulationDate.add(Duration(days: 1)));
      titleForDeign = AppStrings.ovulation.tr();
    } else if (chartData.currentDate
                .compareTo(chartData.startPostOvulationDate) >=
            0 &&
        chartData.currentDate.compareTo(chartData.endPostOvulationDate) <= 0) {
      // log("SPOSTD: ${chartData.startPostOvulationDate} --- CD: ${chartData.currentDate}  --- EPOSTD: ${chartData.endPostOvulationDate} ");
      showArrow = true;
      bgColor = ColorManager.primaryLight;
      endDate = dateTimeToDateEDMY(chartData.endPostOvulationDate);
      titleForDeign = AppStrings.postOvulation.tr();
    }

    emit(state.copyWith(
        getChartDesignDataState: RequestState.loaded,
        chartData: chartData,
        showArrow: showArrow,
        endDate: endDate,
        titleForDeign: titleForDeign,
        circleColor: bgColor));
  }

  FutureOr<void> _editPeriodData(
      EditPeriodDateEvent event, Emitter<MenstrualCycleState> emit) async {
    emit(state.copyWith(getChartDesignDataState: RequestState.loading));

    Either<Failure, MenstrualCycle> response =
        await menstrualRepository.editMenstrualCycle(event.editCycle);
    response.fold((failure) {
      emit(state.copyWith(
          getChartDesignDataState: RequestState.error, failure: failure));
    }, (menstrualCycle) {
      add(SetupPeriodDateEvent(data: menstrualCycle));
    });
  }
}
