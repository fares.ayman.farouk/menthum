part of 'menstrual_cycle_bloc.dart';

abstract class MenstrualCycleEvent {}

class CheckPeriodInDayEvent extends MenstrualCycleEvent {
  DateTime selectedDate;

  CheckPeriodInDayEvent({required this.selectedDate});
}

class SetupPeriodDateEvent extends MenstrualCycleEvent {
  MenstrualCycle data;

  SetupPeriodDateEvent({required this.data});
}

class EditPeriodDateEvent extends MenstrualCycleEvent {
  EditCycle editCycle;

  EditPeriodDateEvent({required this.editCycle});
}
