part of 'menstrual_list_bloc.dart';

class MenstrualListState {
  RequestState getMenstrualListState;
  List<MenstrualCycle> menstrualCycleList;
  Failure? failure;
  int? newCycleId;

  MenstrualListState({
    this.newCycleId,
    this.menstrualCycleList = const [],
    this.getMenstrualListState = RequestState.initiate,
    this.failure,
  });

  MenstrualListState copyWith({
    int? newCycleId,
    RequestState? getMenstrualListState,
    List<MenstrualCycle>? menstrualCycleList,
    Failure? failure,
  }) =>
      MenstrualListState(
        newCycleId: newCycleId ?? this.newCycleId,
        getMenstrualListState:
            getMenstrualListState ?? this.getMenstrualListState,
        menstrualCycleList: menstrualCycleList ?? this.menstrualCycleList,
        failure: failure ?? this.failure,
      );
}
