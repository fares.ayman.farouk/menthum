import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/data/repository/menstrual_repository_imp.dart';
import 'package:patientapp/domain/models/digital_prescriptions_mapper.dart';
import 'package:patientapp/domain/repositories/menstrual_repository.dart';

import '../../../data/network/failure.dart';
import '../../models/menstrual_cycle/add_new_cycle.dart';
import '../../models/menstrual_cycle/menstrual_cycle.dart';

part 'menstrual_list_event.dart';

part 'menstrual_list_state.dart';

class MenstrualListBloc extends Bloc<MenstrualListEvent, MenstrualListState> {
  MenstrualRepository menstrualRepository = instance<MenstrualRepositoryImp>();

  MenstrualListBloc()
      : super(
            MenstrualListState(getMenstrualListState: RequestState.initiate)) {
    on<GetMenstrualList>(_getMenstrualList);
    on<AddNewMenstrual>(_addNewMenstrual);
  }

  FutureOr<void> _getMenstrualList(
      GetMenstrualList event, Emitter<MenstrualListState> emit) async {
    emit(state.copyWith(
        getMenstrualListState: RequestState.loading, newCycleId: null));
    Either<Failure, List<MenstrualCycle>> response =
        await menstrualRepository.getMenstrualCycle();

    response.fold((failure) {
      emit(state.copyWith(
          getMenstrualListState: RequestState.error, failure: failure));
    }, (menstrualCycleList) {
      emit(state.copyWith(
          getMenstrualListState: RequestState.loaded,
          menstrualCycleList: menstrualCycleList,
          newCycleId: event.newCycleId));
    });
  }

  FutureOr<void> _addNewMenstrual(
      AddNewMenstrual event, Emitter<MenstrualListState> emit) async {
    emit(state.copyWith(
        getMenstrualListState: RequestState.loading, newCycleId: null));
    Either<Failure, MenstrualCycle> response =
        await menstrualRepository.addMenstrualCycle(event.addNewCycle);
    response.fold((failure) {
      emit(state.copyWith(
          getMenstrualListState: RequestState.error, failure: failure));
    }, (menstrualCycle) {
      /// add the id for the current added.

      add(GetMenstrualList(newCycleId: menstrualCycle.id));
    });
  }
}
