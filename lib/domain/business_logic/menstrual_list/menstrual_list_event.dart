part of 'menstrual_list_bloc.dart';

abstract class MenstrualListEvent {}

class GetMenstrualList extends MenstrualListEvent {
  int? newCycleId;

  GetMenstrualList({this.newCycleId});
}

class AddNewMenstrual extends MenstrualListEvent {
  AddNewCycle addNewCycle;

  AddNewMenstrual({required this.addNewCycle});
}
