part of 'otp_bloc.dart';

abstract class OTPState {}

class ResendOtpInitial extends OTPState {}

class SuccessResendOTPState extends OTPState {
  ResendOtpResponse resendOtpResponse;

  SuccessResendOTPState(this.resendOtpResponse);
}

class LoadingOTPState extends OTPState {}

class FailResendOTPState extends OTPState {
  final Failure failure;

  FailResendOTPState(this.failure);
}

class NoInterNetConnectionState extends OTPState {}

class InterNetConnectedState extends OTPState {}

//Verify

class SuccessVerifyOTPState extends OTPState {
  final VerifyResponse verifyResponse;

  SuccessVerifyOTPState({required this.verifyResponse});
}

class FailVerifyOTPState extends OTPState {
  final Failure failure;

  FailVerifyOTPState({required this.failure});
}
