part of 'otp_bloc.dart';

abstract class OTPEvent {}

class ResendOTPEvent extends OTPEvent {
  final ResendOtpRequest resendOtpRequest;

  ResendOTPEvent(this.resendOtpRequest);
}

class VerifyOTPEvent extends OTPEvent {
  final VerifyRequest verifyRequest;

  VerifyOTPEvent({required this.verifyRequest});
}
