import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/verify_request.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/resend_response.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/verify_response.dart';

import '../../../../data/models/apiRequest/auth_requests/resend_request.dart';
import '../../../../data/network/failure.dart';
import '../../../repositories/auth_repositories.dart';

part 'otp_event.dart';

part 'otp_state.dart';

class OTPBloc extends Bloc<OTPEvent, OTPState> {
  final AuthRepository repository;
  final AppPreferences _appPreferences = instance<AppPreferences>();

  OTPBloc(this.repository) : super(ResendOtpInitial()) {
    on<ResendOTPEvent>((event, emit) async {
      emit(LoadingOTPState());
      Either<Failure, dynamic> response =
          await repository.resendOTP(event.resendOtpRequest);

      response.fold((failure) {
        emit(FailResendOTPState(failure));
      }, (resentOtpResponse) {
        emit(SuccessResendOTPState(resentOtpResponse));
      });
    });

    on<VerifyOTPEvent>((event, emit) async {
      emit(LoadingOTPState());
      Either<Failure, VerifyResponse> response =
          await repository.verify(event.verifyRequest);

      response.fold((failure) {
        emit(FailVerifyOTPState(failure: failure));
      }, (verifyOtpResponse) {
        _appPreferences.addParentUserToken(verifyOtpResponse.data.token ?? "");

        emit(SuccessVerifyOTPState(verifyResponse: verifyOtpResponse));
      });
    });
  }
}
