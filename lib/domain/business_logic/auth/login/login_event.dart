part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class LoginWithPhoneEvent extends LoginEvent {
  LoginRequest loginRequest;

  LoginWithPhoneEvent(this.loginRequest);
}

class LogoutEvent extends LoginEvent {}

class LoginWithGoogleEvent extends LoginEvent {}

class LoginWithAppleEvent extends LoginEvent {}

class LoginWithFacebookEvent extends LoginEvent {}

class RegisterWithFacebookEvent extends LoginEvent {}

class ChangePhoneNum extends LoginEvent {
  String countryCode;
  String phone;

  ChangePhoneNum(this.phone, this.countryCode);
}

class LoginWithQrCodeWithFacebookEvent extends LoginEvent {
  final String token;

  LoginWithQrCodeWithFacebookEvent({required this.token});
}
