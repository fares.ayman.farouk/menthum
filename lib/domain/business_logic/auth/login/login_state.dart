part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class InitialLoginState extends LoginState {}

class LoadingLoginState extends LoginState {}

class SuccessLoginState extends LoginState {
  final LoginResponse loginResponse;

  SuccessLoginState(this.loginResponse);
}

class WrongCredentialsLoginState extends LoginState {
  final String? msg;

  WrongCredentialsLoginState(this.msg);
}

class FailLoginState extends LoginState {
  final Failure failure;

  FailLoginState(this.failure);
}

class LoadingLogoutState extends LoginState {}

class SuccessLogoutState extends LoginState {
  final LogoutResponse logoutResponse;

  SuccessLogoutState(this.logoutResponse);
}

class FailLogoutState extends LoginState {
  final Failure failure;

  FailLogoutState(this.failure);
}

class NoInterNetConnectionState extends LoginState {}

class InterNetConnectedState extends LoginState {}

class SuccessLoginProviderState extends LoginState {
  final LoginResponse response;

  SuccessLoginProviderState({required this.response});
}

class SuccessRegisterGoogleState extends LoginState {
  RegisterResponse response;

  SuccessRegisterGoogleState({required this.response});
}

class FailedRegisterGoogleState extends LoginState {
  final Failure failure;

  FailedRegisterGoogleState({required this.failure});
}

class FailedChangePhoneState extends LoginState {
  final Failure failure;

  FailedChangePhoneState({required this.failure});
}

class SuccessChangePhoneState extends LoginState {
  final LoginResponse response;

  SuccessChangePhoneState(this.response);
}
