import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/login_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/login_with_provider_request.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/login_api_response.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/logout_response.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/register_response.dart';
import 'package:patientapp/data/network/error_handler.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/repositories/auth_repositories.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

import '../../../../application/app_preference.dart';
import '../../../../application/di.dart';
import '../../../services/push_notification/firebase_notification.dart';

part 'login_event.dart';

part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthRepository repository;
  final GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ["email", "phone"]);

  LoginBloc(this.repository) : super(InitialLoginState()) {
    /// login by phone
    on<LoginWithPhoneEvent>((event, emit) async {
      emit(LoadingLoginState());

      Either<Failure, LoginResponse> response =
          await repository.login(event.loginRequest);
      response.fold((failure) {
        emit(FailLoginState(failure));
      }, (loginResponse) {
        if (loginResponse.status == false && loginResponse.code == 101) {
          emit(WrongCredentialsLoginState(loginResponse.message));
        } else if (loginResponse.status == true) {
          _appPreferences.addParentUserToken(loginResponse.data?.token ?? "");
          emit(SuccessLoginState(loginResponse));
        }
      });
    });

    /// logout
    on<LogoutEvent>((event, emit) async {
      emit(LoadingLogoutState());

      Either<Failure, LogoutResponse> response = await repository.logout();

      response.fold((failure) {
        emit(FailLogoutState(failure));
      }, (logoutResponse) {
        emit(SuccessLogoutState(logoutResponse));
      });
    });

    /// login by google
    on<LoginWithGoogleEvent>((event, emit) async {
      emit(LoadingLoginState());
      try {
        _userGoogle = await _googleSignIn.signIn();

        final googleAuth = await _userGoogle?.authentication;
        googleAuth?.accessToken;
        String deviceToken = await createDeviceToken();
        LoginWithProviderRequest loginWithProviderRequest =
            LoginWithProviderRequest(
                email: _googleSignIn.currentUser?.email ?? "",
                fullname: _googleSignIn.currentUser?.displayName ?? "",
                providerName: "google",
                providerToken: googleAuth?.accessToken ?? "",
                deviceToken: deviceToken);

        Either<Failure, LoginResponse> response =
            await repository.loginWithProvider(loginWithProviderRequest);

        response.fold((failure) async {
          if (failure.code == 422 ||
              failure.code == ResponseCode.NO_INTERNET_CONNECTION) {
            emit(FailLoginState(failure));
          } else {
            emit(FailLoginState(Failure(ResponseCode.DEFAULT, failure.message.toString())));
          }
        }, (loginResponse) {
          _appPreferences.addParentUserToken(loginResponse.data?.token ?? "");

          emit(SuccessLoginProviderState(response: loginResponse));
        });
      } catch (error) {
        emit(FailLoginState(Failure(ResponseCode.DEFAULT, error.toString())));
      }
    });

    /// login with apple

    on<LoginWithAppleEvent>((event, emit) async {
      emit(LoadingLoginState());
      try {
        final credential = await SignInWithApple.getAppleIDCredential(
          scopes: [
            AppleIDAuthorizationScopes.email,
            AppleIDAuthorizationScopes.fullName,
          ],
        );

        if (credential.identityToken != null) {
          String deviceToken = await createDeviceToken();

          LoginWithProviderRequest loginWithProviderRequest =
              LoginWithProviderRequest(
                  email: credential.email ?? "",
                  fullname: credential.givenName ?? "",
                  providerName: "apple",
                  providerToken: credential.authorizationCode,
                  deviceToken: deviceToken);

          Either<Failure, LoginResponse> response =
              await repository.loginWithProvider(loginWithProviderRequest);

          response.fold((failure) async {
            if (failure.code == 422 ||
                failure.code == ResponseCode.NO_INTERNET_CONNECTION) {
              emit(FailLoginState(failure));
            } else {
              emit(FailLoginState(Failure(ResponseCode.DEFAULT, failure.message.toString())));
            }
          }, (loginResponse) {
            _appPreferences.addParentUserToken(loginResponse.data?.token ?? "");

            emit(SuccessLoginProviderState(response: loginResponse));
          });
        } else {
          emit(FailLoginState(Failure(ResponseCode.DEFAULT, credential.state)));
        }
      } catch (e) {
        emit(FailLoginState(Failure(ResponseCode.DEFAULT, "Something went wrong, try again")));
      }
    });

    /// login by facebook

    on<LoginWithFacebookEvent>((event, emit) async {
      emit(LoadingLoginState());

      final result =
          await FacebookAuth.i.login(permissions: ["public_profile", "email"]);
      if (result.status == LoginStatus.success) {
        final userData = await FacebookAuth.i.getUserData(
          fields: "email,name",
        );
        String deviceToken = await createDeviceToken();

        LoginWithProviderRequest loginWithProviderRequest =
            LoginWithProviderRequest(
                email: userData['email'] ?? "",
                fullname: userData['name'] ?? "",
                providerName: "facebook",
                providerToken: result.accessToken?.token ?? "",
                deviceToken: deviceToken);

        Either<Failure, LoginResponse> response =
            await repository.loginWithProvider(loginWithProviderRequest);

        response.fold((failure) async {
          if (failure.code == 422 ||
              failure.code == ResponseCode.NO_INTERNET_CONNECTION) {
            emit(FailLoginState(failure));
          } else {
            emit(FailLoginState(Failure(ResponseCode.DEFAULT, failure.message.toString())));
          }
        }, (loginResponse) {
          _appPreferences.addParentUserToken(loginResponse.data?.token ?? "");

          emit(SuccessLoginProviderState(response: loginResponse));
        });
      } else {
        emit(FailLoginState(Failure(ResponseCode.DEFAULT, result.message)));
      }
    });

    ///login with qr code...
    on<LoginWithQrCodeWithFacebookEvent>((event, emit) async {
      emit(LoadingLoginState());
      try {
        Either<Failure, LoginResponse> response =
            await repository.getUserProfileData(token: event.token);

        response.fold((failure) {
          emit(FailLoginState(failure));
        }, (loginResponse) {
          _appPreferences.addParentUserToken(event.token);
          emit(SuccessLoginState(loginResponse));
        });
      } catch (error) {
        emit(FailLoginState(Failure(ResponseCode.DEFAULT, error.toString())));
      }
    });

    ///

    /// ChangePhoneNum
    on<ChangePhoneNum>((event, emit) async {
      emit(LoadingLoginState());

      Either<Failure, LoginResponse> response =
          await repository.changePhone(event.phone, event.countryCode);

      response.fold((failure) {
        emit(FailedChangePhoneState(failure: failure));
      }, (loginResponse) {
        emit(SuccessChangePhoneState(loginResponse));
      });
    });
  }

  GoogleSignInAccount? _userGoogle;

  final AppPreferences _appPreferences = instance<AppPreferences>();
}
