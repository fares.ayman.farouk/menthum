part of 'register_bloc.dart';

@immutable
abstract class RegisterEvent {}

class RegisterWithPhoneEvent extends RegisterEvent {
  RegisterRequest registerRequest;

  RegisterWithPhoneEvent(this.registerRequest);
}
