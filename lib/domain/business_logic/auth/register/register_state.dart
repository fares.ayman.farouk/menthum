part of 'register_bloc.dart';

@immutable
abstract class RegisterState {}

class RegisterInitial extends RegisterState {}

class LoadingRegisterState extends RegisterState {}

class SuccessRegisterState extends RegisterState {
  final RegisterResponse registerResponse;

  SuccessRegisterState(this.registerResponse);
}

class FailRegisterState extends RegisterState {
  final Failure failure;

  FailRegisterState(this.failure);
}

class NoInterNetConnectionState extends RegisterState {}

class InterNetConnectedState extends RegisterState {}
