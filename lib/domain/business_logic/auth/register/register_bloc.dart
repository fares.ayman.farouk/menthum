import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/register_request.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/register_response.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/repositories/auth_repositories.dart';

part 'register_event.dart';

part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final AuthRepository repository;


  RegisterBloc(this.repository) : super(RegisterInitial()) {
    on<RegisterWithPhoneEvent>((event, emit) async {
      emit(LoadingRegisterState());

      Either<Failure, RegisterResponse> response =
          await repository.register(event.registerRequest);

      response.fold((failure) {
        emit(FailRegisterState(failure));
      }, (registerResponse) {
        emit(SuccessRegisterState(registerResponse));
      });
    });
  }
}
