import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/forget_password_request.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/reset_password_request.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/forget_password_response.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/login_api_response.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/reset_password_response.dart';

import '../../../../data/network/failure.dart';
import '../../../repositories/auth_repositories.dart';

part 'forget_password_event.dart';

part 'forget_password_state.dart';

class PasswordBloc extends Bloc<PasswordEvents, PasswordStates> {
  final AuthRepository repository;

  PasswordBloc(this.repository) : super(ForgetPasswordInitial()) {
    on<ForgetPasswordEvent>((event, emit) async {
      emit(ForgetPasswordLoading());

      Either<Failure, ForgetPasswordResponse> response =
          await repository.forgetPassword(event.forgetPasswordRequest);

      response.fold((failure) {
        emit(ForgetPasswordFailed(failure));
      }, (forgetPasswordResponse) {
        emit(ForgetPasswordSuccess(forgetPasswordResponse));
      });
    });

    on<ResetPasswordEvent>((event, emit) async {
      emit(ResetPasswordLoading());

      Either<Failure, ResetPasswordResponse> response =
          await repository.resetPassword(event.resetPasswordRequest);

      response.fold((failure) {
        emit(ResetPasswordFailed(failure));
      }, (restPasswordResponse) {
        emit(ResetPasswordSuccess(restPasswordResponse));
      });
    });

    on<SetPasswordEvent>((event, emit) async {
      emit(SetPasswordLoading());

      Either<Failure, LoginResponse> response = await repository.setPassword(
          password: event.password, confirmPassword: event.confirmPassword);

      response.fold((failure) {
        emit(SetPasswordFailed(failure));
      }, (loginResponse) {
        emit(SetPasswordSuccess(loginResponse));
      });
    });
  }
}
