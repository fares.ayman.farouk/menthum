part of 'forget_password_bloc.dart';

@immutable
abstract class PasswordEvents {}

class ForgetPasswordEvent extends PasswordEvents {
  final ForgetPasswordRequest forgetPasswordRequest;

  ForgetPasswordEvent(this.forgetPasswordRequest);
}

class ResetPasswordEvent extends PasswordEvents {
  final ResetPasswordRequest resetPasswordRequest;

  ResetPasswordEvent(this.resetPasswordRequest);
}

class SetPasswordEvent extends PasswordEvents {
  final String password;
  final String confirmPassword;

  SetPasswordEvent({required this.password, required this.confirmPassword});
}
