part of 'forget_password_bloc.dart';

@immutable
abstract class PasswordStates {}

class ForgetPasswordInitial extends PasswordStates {}

class ForgetPasswordSuccess extends PasswordStates {
  final ForgetPasswordResponse forgetPasswordResponse;

  ForgetPasswordSuccess(this.forgetPasswordResponse);
}

class ForgetPasswordFailed extends PasswordStates {
  final Failure failure;

  ForgetPasswordFailed(this.failure);
}

class ForgetPasswordLoading extends PasswordStates {}

class ResetPasswordLoading extends PasswordStates {}

class ResetPasswordSuccess extends PasswordStates {
  final ResetPasswordResponse resetPasswordResponse;

  ResetPasswordSuccess(this.resetPasswordResponse);
}

class ResetPasswordFailed extends PasswordStates {
  final Failure failure;

  ResetPasswordFailed(this.failure);
}

class SetPasswordLoading extends PasswordStates {}

class SetPasswordSuccess extends PasswordStates {
  final LoginResponse response;

  SetPasswordSuccess(this.response);
}

class SetPasswordFailed extends PasswordStates {
  final Failure failure;

  SetPasswordFailed(this.failure);
}
