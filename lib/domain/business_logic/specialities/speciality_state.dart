part of 'speciality_bloc.dart';

abstract class SpecialityState {}

class SpecialityInitial extends SpecialityState {}

class LoadingSpecialityState extends SpecialityState {}

class LoadedSpecialityState extends SpecialityState {
  final List<Speciality> specialitiesList;

  LoadedSpecialityState(this.specialitiesList);
}

class FailedSpecialityState extends SpecialityState {
  final Failure failure;

  FailedSpecialityState(this.failure);
}
