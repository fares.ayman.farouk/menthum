import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/data/models/apiResponse/specialties_response.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/repositories/consultations_repositories.dart';

part 'speciality_event.dart';

part 'speciality_state.dart';

class SpecialityBloc extends Bloc<SpecialityEvent, SpecialityState> {
  final ConsultationsRepositories repository;

  SpecialityBloc(this.repository) : super(SpecialityInitial()) {
    on<GetSpecialityList>((event, emit) async {
      emit(LoadingSpecialityState());
      try {
        Either<Failure, SpecialtiesResponse> response =
            await repository.getSpecialties(event.specialityName ?? '');

        response.fold((failure) {
          emit(FailedSpecialityState(failure));
        }, (upcomingResponse) {
          emit(LoadedSpecialityState(upcomingResponse.data ?? []));
        });
      } catch (e) {
        print(e);
      }
    });
  }
}
