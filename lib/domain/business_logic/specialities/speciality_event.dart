part of 'speciality_bloc.dart';

abstract class SpecialityEvent {}

class GetSpecialityList extends SpecialityEvent {
  String? specialityName;

  GetSpecialityList({this.specialityName});
}
