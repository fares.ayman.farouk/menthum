part of 'upcoming_appointments_bloc.dart';

@immutable
abstract class UpcomingAppointmentsEvents {}

class UpcomingAppointmentsEvent extends UpcomingAppointmentsEvents {}
