part of 'upcoming_appointments_bloc.dart';

@immutable
abstract class UpcomingAppointmentsStates {}

class UpcommingAppointmentsInitial extends UpcomingAppointmentsStates {}

class SuccessUpcomingAppointmentsState extends UpcomingAppointmentsStates {
  final List<AppointmentItem> upcomingAppoitmentsList;

  SuccessUpcomingAppointmentsState(this.upcomingAppoitmentsList);
}

class FailedAppointmentsState extends UpcomingAppointmentsStates {
  final Failure failure;

  FailedAppointmentsState(this.failure);
}

class EmptyAppointmentsState extends UpcomingAppointmentsStates {}

class LoadingAppointmentsState extends UpcomingAppointmentsStates {}
