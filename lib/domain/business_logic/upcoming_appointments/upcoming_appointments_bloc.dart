import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:patientapp/data/models/apiResponse/upcoming_response.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/repositories/consultations_repositories.dart';

part 'upcoming_appointments_event.dart';

part 'upcoming_appointments_state.dart';

class UpcomingAppointmentsBloc
    extends Bloc<UpcomingAppointmentsEvents, UpcomingAppointmentsStates> {
  final ConsultationsRepositories repository;

  UpcomingAppointmentsBloc(this.repository)
      : super(UpcommingAppointmentsInitial()) {
    on<UpcomingAppointmentsEvent>((event, emit) async {
      emit(LoadingAppointmentsState());
      try {
        Either<Failure, UpcomingAppointmentsResponse> response =
            await repository.upcomingAppointments();

        response.fold((failure) {
          emit(FailedAppointmentsState(failure));
        }, (upcomingResponse) {
          if (upcomingResponse.data == null || upcomingResponse.data!.isEmpty) {
            emit(EmptyAppointmentsState());
          } else {
            /// add data to run time cash.
            emit(SuccessUpcomingAppointmentsState(upcomingResponse.data ?? []));
          }
        });
      } catch (e) {
        print(e);
      }
    });
  }
}
