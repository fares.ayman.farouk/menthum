part of 'coupon_bloc.dart';

abstract class CouponEvent {}

class CheckCouponEvent extends CouponEvent {
  final String coupon;

  CheckCouponEvent(this.coupon);
}
