import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/data/models/apiResponse/check_coupon_api_response.dart';
import '../../../data/network/failure.dart';
import '../../repositories/questions_repositories.dart';

part 'coupon_event.dart';

part 'coupon_state.dart';

class CouponBloc extends Bloc<CouponEvent, CouponState> {
  final QuestionRepositories repository;

  late DataItem dataItem;

  CouponBloc(this.repository) : super(CouponInitial()) {
    on<CheckCouponEvent>((event, emit) async {
      emit(CouponLoading());

      try {
        Either<Failure, CheckCouponApiResponse> response =
            await repository.checkCoupon(event.coupon);

        response.fold((failure) {
          emit(CouponFails(failure));
        }, (response) {
          dataItem = response.data!;
          emit(CouponSuccess());
        });
      } catch (e) {
        print(e);
      }
    });
  }
}
