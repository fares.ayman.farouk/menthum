part of 'coupon_bloc.dart';

abstract class CouponState {}

class CouponInitial extends CouponState {}

class CouponLoading extends CouponState {}

class CouponSuccess extends CouponState {
}

class CouponFails extends CouponState {
  final Failure failure;

  CouponFails(this.failure);
}
