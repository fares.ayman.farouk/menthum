import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/domain/models/calculation/bmi/bmi_request_entity.dart';
import 'package:patientapp/domain/models/calculation/bmi/bmi_response_entity.dart';
import 'package:patientapp/domain/models/digital_prescriptions_mapper.dart';

import '../../../../data/network/failure.dart';
import '../../../repositories/calculation_repository.dart';

part 'bmi_event.dart';

part 'bmi_state.dart';

class BmiBloc extends Bloc<BmiEvent, BmiState> {
  CalculationRepository calculationRepository;

  BmiBloc(this.calculationRepository)
      : super(BmiState(bmiState: RequestState.initiate)) {
    on<GetBmi>((event, emit) async {
      emit(state.copyWith(bmiState: RequestState.loading));

      final response =
          await calculationRepository.getBmi(event.bmiRequestEntity);
      response.fold((failure) {
        emit(state.copyWith(failure: failure, bmiState: RequestState.error));
      }, (data) {
        emit(
          state.copyWith(
            bmiState: RequestState.loaded,
            bmiData: data,
          ),
        );
      });
    });
  }
}
