part of 'bmi_bloc.dart';

class BmiState {
  RequestState? bmiState;
  Failure? failure;
  BmiResponseEntity? bmiData;

  BmiState({this.bmiState, this.bmiData, this.failure});

  BmiState copyWith({
    RequestState? bmiState,
    Failure? failure,
    BmiResponseEntity? bmiData,
  }) {
    return BmiState(
      failure: failure ?? this.failure,
      bmiState: bmiState ?? this.bmiState,
      bmiData: bmiData ?? this.bmiData,
    );
  }
}
