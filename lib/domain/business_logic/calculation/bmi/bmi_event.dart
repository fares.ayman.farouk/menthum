part of 'bmi_bloc.dart';

abstract class BmiEvent {}

class GetBmi extends BmiEvent {
  BmiRequestEntity bmiRequestEntity;

  GetBmi(this.bmiRequestEntity);
}
