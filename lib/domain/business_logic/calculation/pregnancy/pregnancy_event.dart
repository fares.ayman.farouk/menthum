part of 'pregnancy_bloc.dart';

abstract class PregnancyEvent {}

class GetBirthDate extends PregnancyEvent {
  int date;
  GetBirthDate(this.date);
}
