part of 'pregnancy_bloc.dart';

class PregnancyState {
  RequestState? pregnancyState;
  Failure? failure;
  PregnancyResponseEntity? pregnancyData;

  PregnancyState({this.pregnancyState, this.pregnancyData, this.failure});

  PregnancyState copyWith({
    RequestState? pregnancyState,
    Failure? failure,
    PregnancyResponseEntity? pregnancyData,
  }) {
    return PregnancyState(
      failure: failure ?? this.failure,
      pregnancyState: pregnancyState ?? this.pregnancyState,
      pregnancyData: pregnancyData ?? this.pregnancyData,
    );
  }
}
