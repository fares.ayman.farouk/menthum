import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../data/network/failure.dart';
import '../../../models/calculation/pregnancy/pregnancy_response.dart';
import '../../../models/digital_prescriptions_mapper.dart';
import '../../../repositories/calculation_repository.dart';

part 'pregnancy_event.dart';

part 'pregnancy_state.dart';

class PregnancyBloc extends Bloc<PregnancyEvent, PregnancyState> {
  CalculationRepository calculationRepository;

  PregnancyBloc(this.calculationRepository)
      : super(PregnancyState(pregnancyState: RequestState.initiate)) {


    on<GetBirthDate>((event, emit) async {
      emit(state.copyWith(pregnancyState: RequestState.loading));

      final response = await calculationRepository.getPregnancyData(event.date);
      response.fold((failure) {
        emit(
            state.copyWith(failure: failure, pregnancyState: RequestState.error));
      }, (data) {
        emit(
          state.copyWith(
            pregnancyState: RequestState.loaded,
            pregnancyData: data,
          ),
        );
      });
    });
  }
}
