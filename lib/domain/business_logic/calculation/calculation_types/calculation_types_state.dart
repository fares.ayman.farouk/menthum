part of 'calculation_types_bloc.dart';

class CalculationTypesState {
  RequestState? calcTypesState;
  Failure? failure;
  List<String>? calcTypes;

  CalculationTypesState({this.calcTypes, this.calcTypesState, this.failure});

  CalculationTypesState copyWith({
    RequestState? calcTypesState,
    Failure? failure,
    List<String>? calcTypes,
  }) {
    return CalculationTypesState(
      failure: failure ?? this.failure,
      calcTypesState: calcTypesState ?? this.calcTypesState,
      calcTypes: calcTypes ?? this.calcTypes,
    );
  }
}
