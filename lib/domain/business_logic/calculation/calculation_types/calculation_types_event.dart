part of 'calculation_types_bloc.dart';

abstract class CalculationTypesEvent {}

class GetCalculationTypes extends CalculationTypesEvent {}
