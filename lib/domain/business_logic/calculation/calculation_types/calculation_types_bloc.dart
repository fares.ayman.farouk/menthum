import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/domain/models/digital_prescriptions_mapper.dart';

import '../../../../data/network/failure.dart';
import '../../../repositories/calculation_repository.dart';

part 'calculation_types_event.dart';

part 'calculation_types_state.dart';

class CalculationTypesBloc
    extends Bloc<CalculationTypesEvent, CalculationTypesState> {
  CalculationRepository calculationRepository;

  CalculationTypesBloc(this.calculationRepository)
      : super(CalculationTypesState(calcTypesState: RequestState.initiate)) {
    /// Get Calculation Types
    on<GetCalculationTypes>((event, emit) async {
      emit(state.copyWith(calcTypesState: RequestState.loading));

      final response = await calculationRepository.getCalculationsList();
      response.fold((failure) {
        emit(state.copyWith(
            failure: failure, calcTypesState: RequestState.error));
      }, (data) {
        emit(
          state.copyWith(
            calcTypesState: RequestState.loaded,
            calcTypes: data,
          ),
        );
      });
    });
  }
}
