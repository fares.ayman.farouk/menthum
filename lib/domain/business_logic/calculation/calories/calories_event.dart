part of 'calories_bloc.dart';

abstract class CaloriesEvent {}

class GetCalories extends CaloriesEvent {
  final CaloriesRequestEntity caloriesRequestEntity;
  GetCalories(this.caloriesRequestEntity);
}
