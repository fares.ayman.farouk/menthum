import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/domain/models/calculation/calories/calories_request_entity.dart';

import '../../../../data/network/failure.dart';
import '../../../models/digital_prescriptions_mapper.dart';
import '../../../repositories/calculation_repository.dart';

part 'calories_event.dart';

part 'calories_state.dart';

class CaloriesBloc extends Bloc<CaloriesEvent, CaloriesState> {
  final CalculationRepository calculationRepository;

  CaloriesBloc(this.calculationRepository)
      : super(CaloriesState(calorieState: RequestState.initiate)) {

      on<GetCalories>((event, emit) async {
        emit(state.copyWith(calorieState: RequestState.loading));

        final response =
            await calculationRepository.getCalorie(event.caloriesRequestEntity);
        response.fold((failure) {
          emit(state.copyWith(
              failure: failure, calorieState: RequestState.error));
        }, (data) {
          emit(
            state.copyWith(
              calorieState: RequestState.loaded,
              calorieData: data,
            ),
          );
        });
      });

  }
}
