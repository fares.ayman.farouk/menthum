part of 'calories_bloc.dart';

class CaloriesState {
  RequestState? calorieState;
  Failure? failure;
  String? calorieData;

  CaloriesState({this.calorieState, this.calorieData, this.failure});

  CaloriesState copyWith({
    RequestState? calorieState,
    Failure? failure,
    String? calorieData,
  }) {
    return CaloriesState(
      failure: failure ?? this.failure,
      calorieState: calorieState ?? this.calorieState,
      calorieData: calorieData ?? this.calorieData,
    );
  }
}
