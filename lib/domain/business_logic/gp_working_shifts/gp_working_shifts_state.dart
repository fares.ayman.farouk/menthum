part of 'gp_working_shifts_bloc.dart';

abstract class GPWorkingShiftsState {}

class GPWorkingShiftsInitial extends GPWorkingShiftsState {}

class LoadingGPWorkingShiftsState extends GPWorkingShiftsState {}

class LoadedGPWorkingShiftsState extends GPWorkingShiftsState {
  final List<GPWorkingShifts> gpWorkingShifts;

  LoadedGPWorkingShiftsState({required this.gpWorkingShifts});
}

class FailedGPWorkingShiftsState extends GPWorkingShiftsState {
  final Failure failure;

  FailedGPWorkingShiftsState({required this.failure});
}
