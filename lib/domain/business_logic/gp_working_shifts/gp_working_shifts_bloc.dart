import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/data/models/apiResponse/home_response/gp_working_shifts_response.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/repositories/home_repository.dart';

part 'gp_working_shifts_event.dart';

part 'gp_working_shifts_state.dart';

class GpWorkingShiftsBloc
    extends Bloc<GPWorkingShiftsEvent, GPWorkingShiftsState> {
  final HomeRepository homeRepository;
  final AppPreferences _appPreferences = instance<AppPreferences>();

  GpWorkingShiftsBloc(this.homeRepository) : super(GPWorkingShiftsInitial()) {
    on<GPWorkingShiftsEvent>(_getGPWorkingShifts);
  }

  FutureOr<void> _getGPWorkingShifts(
      GPWorkingShiftsEvent event, Emitter<GPWorkingShiftsState> emit) async {
    emit(LoadingGPWorkingShiftsState());
    try {
      Either<Failure, GPWorkingShiftResponse> response =
          await homeRepository.getGpWorkingShifts();
      response.fold(
        (failure) => emit(FailedGPWorkingShiftsState(failure: failure)),
        (data) async {
          saveDataToShared(data);
        },
      );
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  saveDataToShared(GPWorkingShiftResponse data) async {
    await _appPreferences.addGPWorkingShifts(data.gpWorkingShifts!);
    emit(LoadedGPWorkingShiftsState(gpWorkingShifts: data.gpWorkingShifts!));
  }
}
