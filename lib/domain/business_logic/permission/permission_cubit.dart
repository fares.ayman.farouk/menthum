import 'dart:async';
import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:patientapp/data/network/network_info.dart';
import 'package:patientapp/domain/business_logic/application_life_cycle/application_life_cycle_cubit.dart';
import 'package:patientapp/domain/services/local_db_handler/local_db_handler.dart';
import 'package:patientapp/domain/services/permission/i_permission.dart';
import 'package:rxdart/rxdart.dart';

import '../../services/permission/location_permission_status.dart';

part 'permission_state.dart';

class PermissionCubit extends Cubit<PermissionState> {
  final NetWorkInfo netWorkInfo;
  final ApplicationLifeCycleCubit _applicationLifeCycleCubit;
  final IPermissionService _permissionService;
  StreamSubscription<bool>? _locationStatusSubscription;

  late final StreamSubscription<InternetConnectionStatus>
      internetCheckerSubscription;

  StreamSubscription<Iterable<ApplicationLifeCycleState>>?
      _appLifeCycleSubscription;

  PermissionCubit(this.netWorkInfo, this._applicationLifeCycleCubit,
      this._permissionService)
      : super(PermissionState()) {
    internetCheckerSubscription = netWorkInfo.getInternetConnectionStream
        .listen(_internetCheckerListener);

    _permissionService.isLocationPermissionGranted().then(
      (bool isLocationPermissionGranted) {
        emit(state.copyWith(
            isLocationPermissionGranted: isLocationPermissionGranted));
      },
    );

    _locationStatusSubscription = _permissionService
        .locationServicesStatusStream
        .listen((isLocationServicesEnabled) {
      emit(
          state.copyWith(isLocationServicesEnabled: isLocationServicesEnabled));
    });

    _appLifeCycleSubscription = _applicationLifeCycleCubit.stream
        .startWith(_applicationLifeCycleCubit.state)
        .pairwise()
        .listen((pair) async {
      final previous = pair.first;
      final current = pair.last;
      log("Previous app life cycle stateus is: ${previous.appLifecycleState.name} -- ${previous.isResumed}");
      log("current app life cycle stateus is: ${current.appLifecycleState.name} -- ${current.isResumed}");

      // if the user was not in the foreground and now comes to the foreground.
      if (previous.isResumed != current.isResumed && !current.isResumed) {
        internetCheckerSubscription.pause();
      } else if (previous.isResumed != current.isResumed && current.isResumed) {
        internetCheckerSubscription.resume();

        bool isGranted = await _permissionService.isLocationPermissionGranted();
        bool isServicesEnabled =
            await _permissionService.isLocationServicesEnabled();
        if (state.isLocationPermissionGranted != isGranted && isGranted) {
          hidOpenAppSettingsDialog();
        }
        emit(state.copyWith(
          isLocationPermissionGranted: isGranted,
          isLocationServicesEnabled: isServicesEnabled,
        ));
      }
    });
  }

  void _internetCheckerListener(InternetConnectionStatus status) {
    switch (status) {
      case InternetConnectionStatus.connected:
        if (state.internetConnectionStatus != true) {
          LocalDBHandler.loopForSendingEvents();
          emit(state.copyWith(internetConnectionStatus: true));
          log('+++Data connection is available.');
        }
        break;
      case InternetConnectionStatus.disconnected:
        emit(state.copyWith(internetConnectionStatus: false));
        log('+++You are disconnected from the internet.');
        break;
    }
  }

  void requestLocationPermission() async {
    final status = await _permissionService.requestLocationPermission();
    final bool displayOpenAppSettingsDialog =
        status == LocationPermissionStatus.deniedForever;
    final bool isGranted = status == LocationPermissionStatus.granted;
    emit(state.copyWith(
        isLocationPermissionGranted: isGranted,
        displayOpenAppSettingsDialog: displayOpenAppSettingsDialog));
  }

  Future<void> openAppSettings() async {
    await _permissionService.openAppSettings();
  }

  Future<void> openLocationSettings() async {
    await _permissionService.openLocationSettings();
  }

  @override
  Future<void> close() async {
    await internetCheckerSubscription.cancel();
    await _appLifeCycleSubscription?.cancel();
    await _locationStatusSubscription?.cancel();
    super.close();
  }

  void hidOpenAppSettingsDialog() {
    emit(state.copyWith(displayOpenAppSettingsDialog: false));
  }
}
