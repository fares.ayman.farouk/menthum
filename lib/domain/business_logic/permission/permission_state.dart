part of 'permission_cubit.dart';

class PermissionState {
  bool internetConnectionStatus;
  bool isLocationPermissionGranted;
  bool isLocationServicesEnabled;
  bool displayOpenAppSettingsDialog;
  bool displayPermissionDialog;

  PermissionState({
    this.internetConnectionStatus = true,
    this.isLocationPermissionGranted = false,
    this.isLocationServicesEnabled = false,
    this.displayOpenAppSettingsDialog = false,
    this.displayPermissionDialog = false,
  });

  bool get isLocationPermissionGrantedAndServiceEnabled =>
      isLocationPermissionGranted && isLocationServicesEnabled;

  PermissionState copyWith({
    bool? internetConnectionStatus,
    bool? isLocationPermissionGranted,
    bool? isLocationServicesEnabled,
    bool? displayOpenAppSettingsDialog,
    bool? displayPermissionDialog,
  }) {
    return PermissionState(
      internetConnectionStatus:
          internetConnectionStatus ?? this.internetConnectionStatus,
      isLocationPermissionGranted:
          isLocationPermissionGranted ?? this.isLocationPermissionGranted,
      isLocationServicesEnabled:
          isLocationServicesEnabled ?? this.isLocationServicesEnabled,
      displayOpenAppSettingsDialog:
          displayOpenAppSettingsDialog ?? this.displayOpenAppSettingsDialog,
      displayPermissionDialog:
          displayPermissionDialog ?? this.displayPermissionDialog,
    );
  }
}
