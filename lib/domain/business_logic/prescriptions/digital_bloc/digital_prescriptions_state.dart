part of 'digital_prescriptions_bloc.dart';

abstract class DigitalPrescriptionsState {}

class DigitalPrescriptionsInitial extends DigitalPrescriptionsState {}

///Digitalized Prescriptions.

class LoadingDigitalPrescriptions extends DigitalPrescriptionsState {}
class EmptyDigitalPrescriptions extends DigitalPrescriptionsState {}

class SuccessDigitalizedPrescriptionsState extends DigitalPrescriptionsState {
  final List<DigitalPrescriptionsMapper> digitalPrescriptionsList;

  SuccessDigitalizedPrescriptionsState(this.digitalPrescriptionsList);
}

class FailedDigitalizedPrescriptionsState extends DigitalPrescriptionsState {
  final Failure failure;

  FailedDigitalizedPrescriptionsState(this.failure);
}

