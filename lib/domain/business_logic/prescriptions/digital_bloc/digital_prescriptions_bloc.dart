import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/application/extensions.dart';
import 'package:patientapp/domain/models/digital_prescriptions_mapper.dart';

import '../../../../data/models/apiResponse/prescriptions_response/digital_prescriptions_response.dart';
import '../../../../data/network/failure.dart';
import '../../../repositories/consultations_repositories.dart';

part 'digital_prescriptions_event.dart';

part 'digital_prescriptions_state.dart';

class DigitalPrescriptionsBloc
    extends Bloc<DigitalPrescriptionsEvent, DigitalPrescriptionsState> {
  final ConsultationsRepositories repository;

  DigitalPrescriptionsBloc(this.repository)
      : super(DigitalPrescriptionsInitial()) {
    /// get digital prescriptions
    on<GetDigitalPrescriptionsList>((event, emit) async {
      emit(LoadingDigitalPrescriptions());

      Either<Failure, DigitalPrescriptionResponse> response =
          await repository.getDigitalPrescriptions();

      response.fold((failure) {
        emit(FailedDigitalizedPrescriptionsState(failure));
      }, (digitalPrescriptionRes) async {
        if (digitalPrescriptionRes.data!.isNotEmpty) {
          /// mapping model to entity.
          List<DigitalPrescriptionsMapper> digitalPrescriptions =
              mapDigitalPrescriptions(digitalPrescriptionRes.data ?? []);

          emit(SuccessDigitalizedPrescriptionsState(digitalPrescriptions));
        } else {
          emit(EmptyDigitalPrescriptions());
        }
      });
    });
  }

  List<DigitalPrescriptionsMapper> mapDigitalPrescriptions(
      List<DigitalPrescriptions> data) {
    List<DigitalPrescriptionsMapper> digitalPrescriptions = [];
    for (int i = 0; i < data.length; i++) {
      digitalPrescriptions.add(DigitalPrescriptionsMapper(
        id: data[i].id ?? "",
        name: data[i].name ?? "",
        filePath: data[i].filePath ?? "",
        digitalPrescriptions: mapDrugs(data[i].digitalPrescriptions ?? []),
        createdAt: data[i].createdAt ?? 0,
      ));
    }
    return digitalPrescriptions;
  }

  List<DrugMapper> mapDrugs(List<Drug> data) {
    List<DrugMapper> drugs = [];
    for (int i = 0; i < data.length; i++) {
      drugs.add(DrugMapper(
        name: data[i].name ?? "",
        description: data[i].description ?? "",
        logo: data[i].logo ?? "",
        comment: data[i].comment ?? "",
        route: RouteMedicineEnum.values[data[i].route! - 1].routeName(),
        numberOfTimes:
            "${data[i].intervalCount} ${ForIntervalEnum.values[data[i].interval! - 1].forIntervalName()}",
        forIntervals:
            "${data[i].duration} ${FrequencyEnum.values[data[i].frequency! - 1].freqName()}",
      ));
    }
    return drugs;
  }
}
