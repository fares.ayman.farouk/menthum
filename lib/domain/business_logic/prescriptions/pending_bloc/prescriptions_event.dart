part of 'prescriptions_bloc.dart';

abstract class PrescriptionsEvent {}

class GetPrescriptionsList extends PrescriptionsEvent {}

class DeletePendingPrescriptions extends PrescriptionsEvent {
  String id;

  DeletePendingPrescriptions(this.id);
}

class UploadPresFiles extends PrescriptionsEvent {}

class UpdatePresFiles extends PrescriptionsEvent {
  String id;

  UpdatePresFiles(this.id);
}

class OrderOnlineEvent extends PrescriptionsEvent {
  String id;

  OrderOnlineEvent(this.id);
}
