import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/repositories/consultations_repositories.dart';
import '../../../../application/utils.dart';
import '../../../../data/models/apiResponse/base_response.dart';
import '../../../../data/models/apiResponse/prescriptions_response/pending_prescriptions_response.dart';

part 'prescriptions_event.dart';

part 'prescriptions_state.dart';

class PendingPrescriptionsBloc
    extends Bloc<PrescriptionsEvent, PendingPrescriptionsState> {
  final ConsultationsRepositories repository;
  late List<PendingPrescriptions> prescriptionsList;

  PendingPrescriptionsBloc(this.repository)
      : super(PendingPrescriptionsInitial()) {
    /// get prescriptions
    on<GetPrescriptionsList>((event, emit) async {
      emit(LoadingPrescriptionsState());
      try {
        Either<Failure, PendingPrescriptionsApiResponse> response =
            await repository.getPrescriptions();

        response.fold((failure) {
          emit(FailedPrescriptionsState(failure));
        }, (upcomingResponse) async {
          prescriptionsList = upcomingResponse.data ?? [];

          if (prescriptionsList.isNotEmpty) {
            emit(SuccessPrescriptionsState(prescriptionsList));
          } else {
            emit(EmptyPrescriptionsState());
          }
        });
      } catch (e) {}
    });

    /// Upload prescriptions
    on<UploadPresFiles>((event, emit) async {
      var result = await getLocalFile(['jpg', 'png', 'jpeg']);
      if (result != null) {
        File file = File(result.files.single.path!);

        // emit(LoadingPendingPrescriptionsState());
        Either<Failure, BaseResponse> response =
            await repository.uploadPresFiles(file);

        response.fold((failure) {
          emit(FailedPrescriptionsState(failure));
          add(GetPrescriptionsList());
        }, (radsResult) {
          add(GetPrescriptionsList());
        });
      } else {}
    });

    /// delete prescription
    on<DeletePendingPrescriptions>((event, emit) async {
      emit(LoadingPrescriptionsState());
      Either<Failure, BaseResponse> response =
          await repository.deletePendingPrescription(event.id);

      response.fold((failure) {
        emit(FailedPrescriptionsState(failure));
        add(GetPrescriptionsList());
      }, (upcomingResponse) async {
        add(GetPrescriptionsList());
      });
    });

    /// order online
    on<OrderOnlineEvent>((event, emit) async {
      emit(LoadingPrescriptionsState());

      Either<Failure, BaseResponse> response =
          await repository.orderOnline(event.id);

      response.fold((failure) {
        emit(FailedPrescriptionsState(failure));
        add(GetPrescriptionsList());
      }, (upcomingResponse) async {
        add(GetPrescriptionsList());
      });
    });

    /// Update prescriptions
    on<UpdatePresFiles>((event, emit) async {
      var result = await getLocalFile(['jpg', 'png', 'jpeg']);
      if (result != null) {
        File file = File(result.files.single.path!);

        emit(LoadingPrescriptionsState());
        Either<Failure, BaseResponse> response =
            await repository.updatePresFiles(file, event.id);

        response.fold((failure) {
          emit(FailedPrescriptionsState(failure));
          add(GetPrescriptionsList());
        }, (radsResult) {
          /// todo
          add(GetPrescriptionsList());
        });
      } else {}
    });
  }
}
