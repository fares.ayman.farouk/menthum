part of 'prescriptions_bloc.dart';

abstract class PendingPrescriptionsState {}

class PendingPrescriptionsInitial extends PendingPrescriptionsState {}

class LoadingPrescriptionsState extends PendingPrescriptionsState {}

class EmptyPrescriptionsState extends PendingPrescriptionsState {}

///Pending Prescriptions.
class SuccessPrescriptionsState extends PendingPrescriptionsState {
  final List<PendingPrescriptions> prescriptionsList;

  SuccessPrescriptionsState(this.prescriptionsList);
}

class FailedPrescriptionsState extends PendingPrescriptionsState {
  final Failure failure;

  FailedPrescriptionsState(this.failure);
}
