part of 'terms_page_bloc.dart';

@immutable
abstract class TermsPageEvent {}

class GetTermsPageEvent extends TermsPageEvent {
  String url;

  GetTermsPageEvent(this.url);
}
