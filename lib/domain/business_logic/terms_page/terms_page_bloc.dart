import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/register_request.dart';
import 'package:patientapp/data/models/apiResponse/TermsPageApiResponse.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/register_response.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/repositories/auth_repositories.dart';

part 'terms_page_event.dart';

part 'terms_page_state.dart';

class TermsPageBloc extends Bloc<TermsPageEvent, TermsPageState> {
  final AuthRepository repository;


  TermsPageBloc(this.repository) : super(InitialState()) {
    on<GetTermsPageEvent>((event, emit) async {
      emit(LoadingState());

      Either<Failure, TermsPageApiResponse> response =
      await repository.termsPage(event.url);

      response.fold((failure) {
        emit(FailState(failure));
      }, (registerResponse) {
        emit(SuccessState(registerResponse.data!));
      });
    });
  }
}
