part of 'terms_page_bloc.dart';

@immutable
abstract class TermsPageState {}

class InitialState extends TermsPageState {}

class LoadingState extends TermsPageState {}

class SuccessState extends TermsPageState {
  final Data terms;

  SuccessState(this.terms);
}

class FailState extends TermsPageState {
  final Failure failure;

  FailState(this.failure);
}
