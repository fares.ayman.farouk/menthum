part of 'rads_profile_bloc.dart';

abstract class RadsProfileEvent {}

class MakeRadAppointmentEvent extends RadsProfileEvent {
  final BookAppointmentReq bookAppointmentReq;

  MakeRadAppointmentEvent(this.bookAppointmentReq);
}

class GetLabProfile extends RadsProfileEvent {
  final String labId;

  GetLabProfile(this.labId);
}

class GetRadProfile extends RadsProfileEvent {
  final String radId;

  GetRadProfile(this.radId);
}
