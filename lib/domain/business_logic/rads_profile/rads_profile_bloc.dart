import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/data/models/apiRequest/clinic/book_clinic_appointment_req.dart';
import 'package:patientapp/data/models/apiResponse/clinic/book_clinic_appointment_response.dart';
import 'package:patientapp/data/models/apiResponse/laboratory/lab_profile_response.dart';
import 'package:patientapp/data/models/apiResponse/laboratory/laboratory_response.dart';
import 'package:patientapp/data/models/apiResponse/rads_response/rad_profile_response.dart';
import 'package:patientapp/data/models/apiResponse/rads_response/rads_response.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/repositories/consultations_repositories.dart';

part 'rads_profile_event.dart';

part 'rads_profile_state.dart';

class RadsProfileBloc extends Bloc<RadsProfileEvent, RadsProfileState> {
  final ConsultationsRepositories repository;

  late LaboratoryItem laboratoryItem;
  late RadiologyBranch radiologyBranch;

  RadsProfileBloc(this.repository) : super(RadsProfileInitial()) {
    on<GetLabProfile>((event, emit) async {
      emit(LoadingProfile());

      print('//////////////////${event.labId}');

      Either<Failure, LabProfileResponse> response =
          await repository.getLabProfile(event.labId);

      response.fold((failure) {
        emit(FailedGetProfileState(failure));
      }, (labResponse) {
        laboratoryItem = labResponse.laboratoryItem;
        emit(SuccessGetLabProfileState(labResponse.laboratoryItem));
      });
    });

    on<GetRadProfile>((event, emit) async {
      emit(LoadingProfile());
      Either<Failure, RadProfileResponse> response =
          await repository.getRadProfile(event.radId);

      response.fold((failure) {
        emit(FailedGetProfileState(failure));
      }, (radResponse) {
        radiologyBranch = radResponse.radioloyBranch;
        emit(SuccessGetRadProfileState(radResponse.radioloyBranch));
      });
    });

    on<MakeRadAppointmentEvent>((event, emit) async {
      emit(LoadingBookAppointment());
      Either<Failure, BookAppointmentResponse> response =
          await repository.makeAppointment(event.bookAppointmentReq);

      response.fold((failure) {
        emit(FailedMakeAppointmentState(failure));
      }, (bookAppointmentResponse) {
        emit(SuccessMakeAppointmentState(
            bookAppointmentResponse.data.toString()));
      });
    });
  }
}
