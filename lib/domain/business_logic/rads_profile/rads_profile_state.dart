part of 'rads_profile_bloc.dart';

abstract class RadsProfileState {}

class RadsProfileInitial extends RadsProfileState {}

class LoadingBookAppointment extends RadsProfileState {}

class LoadingProfile extends RadsProfileState {}

class FailedMakeAppointmentState extends RadsProfileState {
  final Failure failure;

  FailedMakeAppointmentState(this.failure);
}

class SuccessMakeAppointmentState extends RadsProfileState {
  final String bookingResponse;

  SuccessMakeAppointmentState(this.bookingResponse);
}

class FailedGetProfileState extends RadsProfileState {
  final Failure failure;

  FailedGetProfileState(this.failure);
}

class SuccessGetLabProfileState extends RadsProfileState {
  final LaboratoryItem laboratoryItem;

  SuccessGetLabProfileState(this.laboratoryItem);
}

class SuccessGetRadProfileState extends RadsProfileState {
  final RadiologyBranch radiologyBranch;

  SuccessGetRadProfileState(this.radiologyBranch);
}
