part of 'blog_bloc.dart';

class BlogState {
  final RequestState requestState;
  final Failure? failure;
  final List<BlogEntity>? blogs;
  final BlogDetailsEntity? blogDetailsEntity;

  BlogState(
      {this.requestState = RequestState.initiate,
      this.failure,
      this.blogs,
      this.blogDetailsEntity});

  copyWith({RequestState? requestState, Failure? failure}) {
    return BlogState(
        requestState: requestState ?? this.requestState,
        failure: failure ?? this.failure,
        blogs: blogs ?? [],
        blogDetailsEntity: blogDetailsEntity);
  }
}
