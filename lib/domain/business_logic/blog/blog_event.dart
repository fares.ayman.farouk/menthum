part of 'blog_bloc.dart';

abstract class BlogEvent {}

class GetBlogsList extends BlogEvent {}

class GetBlogDetails extends BlogEvent {
  final String slug;

  GetBlogDetails({required this.slug});
}
