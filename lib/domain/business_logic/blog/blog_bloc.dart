import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/domain/models/blog_entity.dart';
import 'package:patientapp/domain/repositories/blog_repositories.dart';

import '../../../data/network/failure.dart';
import '../../../data/repository/blog_repositories_imp.dart';
import '../../models/blog_details_entity.dart';
import '../../models/digital_prescriptions_mapper.dart';

part 'blog_event.dart';

part 'blog_state.dart';

class BlogBloc extends Bloc<BlogEvent, BlogState> {
  BlogRepository blogRepository = instance<BlogsRepositoryImp>();

  BlogBloc() : super(BlogState(requestState: RequestState.initiate)) {
    /// get blog list
    on<GetBlogsList>((event, emit) async {
      emit(BlogState(requestState: RequestState.loading));

      try {
        Either<Failure, List<BlogEntity>> response =
            await blogRepository.getBlogsList();

        response.fold((failure) {
          emit(BlogState(failure: failure, requestState: RequestState.error));
        }, (blogs) {
          emit(BlogState(blogs: blogs, requestState: RequestState.loaded));
        });
      } catch (e) {
        log(e.toString());
      }
    });

    /// get blog details
    on<GetBlogDetails>((event, emit) async {
      emit(BlogState(requestState: RequestState.loading));

      try {
        Either<Failure, BlogDetailsEntity> response =
            await blogRepository.getBlog(event.slug);

        response.fold((failure) {
          emit(BlogState(failure: failure, requestState: RequestState.error));
        }, (blog) {
          emit(BlogState(
              blogDetailsEntity: blog, requestState: RequestState.loaded));
        });
      } catch (e) {
        log(e.toString());
      }
    });
  }
}
