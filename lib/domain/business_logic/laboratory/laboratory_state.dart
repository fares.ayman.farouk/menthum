part of 'laboratory_bloc.dart';

abstract class LaboratoryState {}

class LaboratoryInitial extends LaboratoryState {}

class LoadingLabsState extends LaboratoryState {}

class EmptyLabsState extends LaboratoryState {}

class LoadedLabsState extends LaboratoryState {}

class FailedLabsState extends LaboratoryState {
  Failure failure;

  FailedLabsState(this.failure);
}
