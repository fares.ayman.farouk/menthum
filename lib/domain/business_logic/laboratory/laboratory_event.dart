part of 'laboratory_bloc.dart';

abstract class LaboratoryEvent {}

class GetLabsEvent extends LaboratoryEvent {
  LaboratoryRequest labRequest;

  GetLabsEvent(this.labRequest);
}
