import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/data/models/apiRequest/laboratory/laboratory_request.dart';
import 'package:patientapp/data/models/apiResponse/tests_response.dart';
import 'package:patientapp/domain/repositories/laboratory_repositories.dart';

import '../../../application/app_preference.dart';
import '../../../application/di.dart';
import '../../../data/models/apiResponse/laboratory/laboratory_response.dart';
import '../../../data/models/apiResponse/location_filter/city_response.dart';
import '../../../data/models/apiResponse/location_filter/governorate_response.dart';
import '../../../data/network/failure.dart';
import '../../repositories/consultations_repositories.dart';

part 'laboratory_event.dart';

part 'laboratory_state.dart';

class LaboratoryBloc extends Bloc<LaboratoryEvent, LaboratoryState> {
  final LaboratoriesRepositories labsRepository;
  final ConsultationsRepositories consultationsRepositories;

  //default val
  String countryId = "1";

  final AppPreferences _appPreferences = instance<AppPreferences>();

  List<Governorate> governoratesList = [];

  List<City> citiesList = [];
  List<LaboratoryItem> laboratoryItems = [];
  List<Test> testsList = [];

  LaboratoryBloc(
      {required this.labsRepository, required this.consultationsRepositories})
      : super(LaboratoryInitial()) {
    on<GetLabsEvent>((event, emit) async {
      emit(LoadingLabsState());
      try {
        Either<Failure, LaboratoryResponse> response =
            await labsRepository.getLaboratories(event.labRequest);

        /// check if the shared preference is empty or not
        citiesList = await _appPreferences.getCities() ?? [];
        governoratesList = await _appPreferences.getGovernorates() ?? [];
        testsList =
            await _appPreferences.getTests(key: LIST_OF_LABS_TESTS) ?? [];
        if (citiesList.isEmpty ||
            governoratesList.isEmpty ||
            testsList.isEmpty) {
          print("++++++++++++++++++++++++++");
          await getListOfGovernorates(countryId);
          await getListOfCities(governoratesList[0].id.toString());
          await getListOfTests();
        }
        response.fold((failure) {
          laboratoryItems = [];

          emit(FailedLabsState(failure));
        }, (labsResponse) async {
          if (labsResponse.data?.length == 0) {
            laboratoryItems = [];
            emit(EmptyLabsState());
          } else {
            laboratoryItems = labsResponse.data ?? [];
            emit(LoadedLabsState());
          }
        });
      } catch (e) {
        print(e);
      }
    });
  }

  getListOfCities(String governorateId) async {
    try {
      Either<Failure, CityResponse> response =
          await consultationsRepositories.getCities(governorateId);

      response.fold((failure) {
        citiesList = [];
      }, (cityResponse) async {
        if (cityResponse.data?.isEmpty == true) {
          citiesList = [];
        } else {
          citiesList = cityResponse.data ?? [];
          await _appPreferences.addCities(citiesList);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  getListOfGovernorates(String countryId) async {
    try {
      Either<Failure, GovernorateResponse> response =
          await consultationsRepositories.getGovernorates(countryId);

      response.fold((failure) {
        governoratesList = [];
      }, (governorateResponse) async {
        if (governorateResponse.data?.isEmpty == true) {
          governoratesList = [];
        } else {
          governoratesList = governorateResponse.data ?? [];
          await _appPreferences.addGovernorates(governoratesList);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  getListOfTests() async {
    try {
      Either<Failure, TestsResponse> response =
          await labsRepository.getAllLabsTests();

      response.fold((failure) {
        governoratesList = [];
      }, (testsResponse) async {
        if (testsResponse.data?.isEmpty == true) {
          testsList = [];
        } else {
          testsList = testsResponse.data ?? [];
          await _appPreferences.addTests(
              tests: testsList, key: LIST_OF_LABS_TESTS);
        }
      });
    } catch (e) {
      print(e);
    }
  }
}
