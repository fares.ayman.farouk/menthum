part of 'visit_details_bloc.dart';

abstract class VisitDetailsEvent {}

class GetVisitDetailsEvent extends VisitDetailsEvent {
  final int id;
  GetVisitDetailsEvent(this.id);
}
