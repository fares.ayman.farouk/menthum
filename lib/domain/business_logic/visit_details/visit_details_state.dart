part of 'visit_details_bloc.dart';

abstract class VisitDetailsState {}

class VisitDetailsInitial extends VisitDetailsState {}

class VisitDetailsLoading extends VisitDetailsState {}

class VisitDetailsSuccess extends VisitDetailsState {
  final VisitDetailsData visitDetailsData;

  VisitDetailsSuccess(this.visitDetailsData);
}

class VisitDetailsFailure extends VisitDetailsState {
  final Failure failure;

  VisitDetailsFailure(this.failure);
}
