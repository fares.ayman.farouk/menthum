import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/data/models/apiResponse/visit_detail_response/visitDetailResponse.dart';
import 'package:patientapp/data/network/failure.dart';

import '../../repositories/consultations_repositories.dart';

part 'visit_details_event.dart';

part 'visit_details_state.dart';

class VisitDetailsBloc extends Bloc<VisitDetailsEvent, VisitDetailsState> {
  final ConsultationsRepositories repository;
  late bool hasSpeciality = false, hasRadiology = false, hasLaboratory = false;

  late bool hasSpecialityAppoint = false,
      hasRadiologyAppoint = false,
      hasLaboratoryAppoint = false;

  VisitDetailsBloc(this.repository) : super(VisitDetailsInitial()) {
    on<VisitDetailsEvent>((event, emit) {});

    /// get VisitDetails
    on<GetVisitDetailsEvent>((event, emit) async {
      emit(VisitDetailsLoading());
      Either<Failure, VisitDetailResponse> response =
          await repository.getVisitDetails(event.id);

      response.fold((failure) {
        emit(VisitDetailsFailure(failure));
      }, (visitDetailsResponse) {
        checkData(visitDetailsResponse.visitDetailsData);
        emit(VisitDetailsSuccess(visitDetailsResponse.visitDetailsData!));
      });
    });
  }

  checkData(VisitDetailsData? visitDetailsData) {
    hasSpeciality = visitDetailsData?.callRecommendationsSpecialty != null &&
        visitDetailsData?.appointmentClinic.isEmpty == true;

    hasLaboratory =
        visitDetailsData?.callRecommendationsLaboratory?.isNotEmpty == true &&
            visitDetailsData?.appointmentLaboratory.isEmpty == true;

    hasRadiology =
        visitDetailsData?.callRecommendationsRadiology?.isNotEmpty == true &&
            visitDetailsData?.appointmentRadiologyBranch.isEmpty == true;


    hasSpecialityAppoint =
        visitDetailsData?.appointmentClinic.isNotEmpty == true;

    hasLaboratoryAppoint =
        visitDetailsData?.appointmentLaboratory.isNotEmpty == true;

    hasRadiologyAppoint =
        visitDetailsData?.appointmentRadiologyBranch.isNotEmpty == true;
  }
}
