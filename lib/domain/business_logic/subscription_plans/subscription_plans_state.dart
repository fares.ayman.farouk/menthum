part of 'subscription_plans_cubit.dart';

class SubscriptionPlansState {
  final RequestState getPlansState;
  final RequestState subscribeState;
  final RequestState renewState;
  Failure? failure;
  List<Plan>? plans;
  String? paymentUrl;

  SubscriptionPlansState({
    this.getPlansState = RequestState.initiate,
    this.failure,
    this.plans,
    this.paymentUrl,
    this.subscribeState = RequestState.initiate,
    this.renewState = RequestState.initiate,
  });

  SubscriptionPlansState copyWith(
      {RequestState? getPlansState,
      RequestState? subscribeState,
      RequestState? renewState,
      Failure? failure,
      String? paymentUrl,
      List<Plan>? plans}) {
    return SubscriptionPlansState(
      getPlansState: getPlansState ?? this.getPlansState,
      subscribeState: subscribeState ?? this.subscribeState,
      renewState: renewState ?? this.renewState,
      failure: failure ?? this.failure,
      plans: plans ?? this.plans,
      paymentUrl: paymentUrl ?? this.paymentUrl,
    );
  }
}
