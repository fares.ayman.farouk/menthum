import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/login_api_response.dart';
import 'package:patientapp/data/models/apiResponse/plans_response.dart';
import 'package:patientapp/data/models/apiResponse/profile/paymentApiResponse.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/models/digital_prescriptions_mapper.dart';
import 'package:patientapp/domain/repositories/consultations_repositories.dart';

part 'subscription_plans_state.dart';

class SubscriptionPlansCubit extends Cubit<SubscriptionPlansState> {
  final ConsultationsRepositories repository;

  SubscriptionPlansCubit(this.repository) : super(SubscriptionPlansState());

  getPlans() async {
    emit(state.copyWith(getPlansState: RequestState.loading));
    log("STate: ${state.getPlansState}");
    try {
      Either<Failure, PlansResponse> response = await repository.getPlans();

      response.fold((failure) {
        log("Error${failure.code}");
        emit(state.copyWith(
            failure: failure, getPlansState: RequestState.error));
      }, (response) {
        log("success: ${response.plans?.length}");
        emit(state.copyWith(
            plans: response.plans, getPlansState: RequestState.loaded));
      });
    } catch (e) {
      log("fun=> getPlans | cubit=> SubscriptionPlansCubit | error=> $e");
    }
  }

  subscribe(int planId) async {
    emit(state.copyWith(subscribeState: RequestState.loading));

    try {
      Either<Failure, PaymentApiResponse> response =
          await repository.subscribe(planId);

      response.fold((failure) {
        log("Error in subscription: ${failure.code}");
        emit(state.copyWith(
            failure: failure, subscribeState: RequestState.error));
      }, (response) {
        log("Success Subscription: ${response.data?.url}");
        emit(state.copyWith(
            paymentUrl: response.data?.url,
            subscribeState: RequestState.loaded));
      });
    } catch (e) {
      log("fun=> subscribe | cubit=> SubscriptionPlansCubit | error=> $e");
    }
  }

  renew({required int planId, required int subscriptionId}) async {
    emit(state.copyWith(renewState: RequestState.loading));

    try {
      Either<Failure, PaymentApiResponse> response =
          await repository.renew(planId, subscriptionId);

      response.fold((failure) {
        log("Error in renew subscription: ${failure.code}");
        emit(state.copyWith(
            failure: failure, renewState: RequestState.error));
      }, (response) {
        log("Success renew Subscription: ${response.data?.url}");
        emit(state.copyWith(
            paymentUrl: response.data?.url,
            renewState: RequestState.loaded));
      });
    } catch (e) {
      log("fun=> renew | cubit=> SubscriptionPlansCubit | error=> $e");
    }
  }
}
