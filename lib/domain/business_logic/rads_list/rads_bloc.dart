import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/data/models/apiRequest/rads_request/rads_request.dart';
import 'package:patientapp/data/models/apiResponse/location_filter/city_response.dart';
import 'package:patientapp/data/models/apiResponse/location_filter/governorate_response.dart';
import 'package:patientapp/data/models/apiResponse/rads_response/rads_response.dart';
import 'package:patientapp/data/models/apiResponse/tests_response.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/repositories/consultations_repositories.dart';
import 'package:patientapp/domain/repositories/rads_repositories.dart';

part 'rads_event.dart';

part 'rads_state.dart';

class RadsBloc extends Bloc<RadsEvent, RadsState> {
  final RadsRepositories radsRepository;
  final ConsultationsRepositories consultationsRepositories;

  //default val
  String countryId = "1";

  final AppPreferences _appPreferences = instance<AppPreferences>();

  List<Governorate> governoratesList = [];

  List<City> citiesList = [];
  List<RadiologyBranch> radioloyBranchs = [];
  List<Test> testsList = [];

  /// all the tests for the radiology search.

  RadsBloc(
      {required this.radsRepository, required this.consultationsRepositories})
      : super(RadsInitial()) {
    on<GetRadsEvent>((event, emit) async {
      emit(LoadingRadsState());
      try {
        Either<Failure, RadsResponse> response =
            await radsRepository.getListOfRads(event.radsRequest);

        /// check if the shared preference is empty or not
        citiesList = await _appPreferences.getCities() ?? [];
        governoratesList = await _appPreferences.getGovernorates() ?? [];
        testsList =
            await _appPreferences.getTests(key: LIST_OF_RADS_TESTS) ?? [];
        if (citiesList.isEmpty ||
            governoratesList.isEmpty ||
            testsList.isEmpty) {
          print("++++++++++++++++++++++++++");
          await getListOfGovernorates(countryId);
          await getListOfCities(governoratesList[0].id.toString());
          await getListOfTests();
        }
        response.fold((failure) {
          radioloyBranchs = [];

          emit(FailedRadsState(failure));
        }, (radsResponse) async {
          if (radsResponse.radioloyBranchs?.isEmpty == true) {
            radioloyBranchs = [];
            emit(EmptyRadsState());
          } else {
            radioloyBranchs = radsResponse.radioloyBranchs ?? [];
            emit(LoadedRadsState());
          }
        });
      } catch (e) {
        print(e);
      }
    });
  }

  getListOfCities(String governorateId) async {
    try {
      Either<Failure, CityResponse> response =
          await consultationsRepositories.getCities(governorateId);

      response.fold((failure) {
        citiesList = [];
      }, (cityResponse) async {
        if (cityResponse.data?.isEmpty == true) {
          citiesList = [];
        } else {
          citiesList = cityResponse.data ?? [];
          await _appPreferences.addCities(citiesList);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  getListOfGovernorates(String countryId) async {
    try {
      Either<Failure, GovernorateResponse> response =
          await consultationsRepositories.getGovernorates(countryId);

      response.fold((failure) {
        governoratesList = [];
      }, (governorateResponse) async {
        if (governorateResponse.data?.isEmpty == true) {
          governoratesList = [];
        } else {
          governoratesList = governorateResponse.data ?? [];
          await _appPreferences.addGovernorates(governoratesList);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  getListOfTests() async {
    try {
      Either<Failure, TestsResponse> response =
          await radsRepository.getAllRadsTests();

      response.fold((failure) {
        governoratesList = [];
      }, (testsResponse) async {
        if (testsResponse.data?.isEmpty == true) {
          testsList = [];
        } else {
          testsList = testsResponse.data ?? [];
          await _appPreferences.addTests(
              tests: testsList, key: LIST_OF_RADS_TESTS);
        }
      });
    } catch (e) {
      print(e);
    }
  }
}
