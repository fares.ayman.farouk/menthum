part of 'rads_bloc.dart';

abstract class RadsState {}

class RadsInitial extends RadsState {}

class LoadingRadsState extends RadsState {}

class EmptyRadsState extends RadsState {}

class LoadedRadsState extends RadsState {}

class FailedRadsState extends RadsState {
  Failure failure;

  FailedRadsState(this.failure);
}
