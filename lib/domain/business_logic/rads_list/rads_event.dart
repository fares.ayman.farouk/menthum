part of 'rads_bloc.dart';

abstract class RadsEvent {}

class GetRadsEvent extends RadsEvent {
  RadsRequest radsRequest;

  GetRadsEvent(this.radsRequest);
}
