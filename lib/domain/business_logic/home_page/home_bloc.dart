import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:patientapp/data/models/apiResponse/homePageApiResponse.dart';
import 'package:patientapp/data/models/apiResponse/upcoming_response.dart';
import 'package:patientapp/data/network/failure.dart';
import 'package:patientapp/domain/repositories/consultations_repositories.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc
    extends Bloc<HomeEvents, HomeStates> {
  final ConsultationsRepositories repository;

  HomeBloc(this.repository)
      : super(HomeInitial()) {
    on<HomePageEvent>(
          (event, emit) async {
        emit(LoadingHome());
        try {
          Either<Failure, HomePageApiResponse> response =
          await repository.homePage();

          response.fold((failure) {
            emit(FailedHome(failure));
          }, (upcomingResponse) {
            emit(SuccessHome(upcomingResponse.data ?? []));
          });
        } catch (e) {
          print(e);
        }
      },
    );
  }
}
