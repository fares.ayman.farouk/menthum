part of 'home_bloc.dart';

@immutable
abstract class HomeStates {}

class HomeInitial extends HomeStates {}

class LoadingHome extends HomeStates {}

class EmptyHomeState extends HomeStates {}

class SuccessHome extends HomeStates {
  final List<DataListItem> homeItem;

  SuccessHome(this.homeItem);
}

class FailedHome extends HomeStates {
  final Failure failure;

  FailedHome(this.failure);
}

