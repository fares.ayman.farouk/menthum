import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:patientapp/application/app_preference.dart';
import 'package:patientapp/application/di.dart';
import 'package:patientapp/application/utils.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/EditMedicalProfileApiRequest.dart';
import 'package:patientapp/data/models/apiRequest/auth_requests/EditProfileApiRequest.dart';
import 'package:patientapp/data/models/apiResponse/auth_response/login_api_response.dart';
import 'package:patientapp/data/models/apiResponse/profile/medicalProfileApiResponse.dart';
import 'package:patientapp/data/network/failure.dart';

import '../../../data/repository/auth_repository_imp.dart';
import '../../models/digital_prescriptions_mapper.dart';

part 'update_patient_profile_event.dart';

part 'update_patient_profile_state.dart';

class UpdatePatientProfileBloc
    extends Bloc<UpdatePatientProfileEvent, UpdatePatientProfileState> {
  AuthRepositoryImp repository;

  UpdatePatientProfileBloc(this.repository)
      : super(
            UpdatePatientProfileState(needUpdateState: RequestState.initiate)) {
    on<CheckIfUserNeedUpdateProfile>((event, emit) async {
      log("CheckIfUserNeedUpdateProfile");
      emit(state.copyWith(needUpdateState: RequestState.loading));

      final medical = instance<AppPreferences>().getMedicalProfile();

      final medicalProfileEntity = EditMedicalProfileApiRequest(
          bloodType: medical?.bloodType ?? 1,
          height: medical?.height ?? "",
          weight: medical?.weight ?? "");

      final user = instance<AppPreferences>().getUser();
      final userProfileEntity = EditProfileApiRequest(
          email: user?.email,
          fullname: user?.fullname,
          maritalStatus: user?.maritalStatus,
          gender: user?.gender ?? 1,
          birthOfDate: timeStampToDateFormatYMD(
              int.parse(user?.birthOfDate.toString() ?? "0")));

      bool needUpdateMedicalData = !(medicalProfileEntity.weight!.isNotEmpty &&
          medicalProfileEntity.height!.isNotEmpty);

      bool needUpdateUserData = !(user?.birthOfDate != null &&
          user?.birthOfDate?.toString().isNotEmpty == true && user?.birthOfDate?.toString() != "0");

      log("Check conditions User data:${user?.birthOfDate} --"
          "condition Val:$needUpdateUserData  medical data: ${medicalProfileEntity.weight} "
          "--  ${medicalProfileEntity.height} --  ${medicalProfileEntity.bloodType}  "
          "condition val: $needUpdateMedicalData");

      emit(state.copyWith(
        needUpdateState: RequestState.loaded,
        medicalProfileEntity: medicalProfileEntity,
        userProfileEntity: userProfileEntity,
        needUpdateMedicalData: needUpdateMedicalData,
        needUpdateUserData: needUpdateUserData,
      ));
    });
    on<UpdateMedicalProfile>((event, emit) async {
      try {
        Either<Failure, MedicalProfileApiResponse> response =
            await repository.editMedicalProfile(event.medicalProfileEntity);

        response.fold((failure) {
          log("Fail in Update Medical Profile");
        }, (response) {
          log("Success in Update Medical Profile");
        });
      } catch (e) {
        log("catch error in UpdatePatientProfileBloc >> UpdateMedicalProfile");
      }
    });
    on<UpdateUserProfile>((event, emit) async {
      try {
        Either<Failure, LoginResponse> response =
            await repository.editUserProfile(event.userProfileEntity);

        response.fold((failure) {
          log("Fail in Update User Profile");
        }, (response) {
          log("Success in Update User Profile");
        });
      } catch (e) {
        log("catch error in UpdatePatientProfileBloc >> UpdateUserProfile");
      }
    });
  }
}
