part of 'update_patient_profile_bloc.dart';

class UpdatePatientProfileState {
  RequestState needUpdateState;
  bool needUpdateUserData;
  bool needUpdateMedicalData;
  EditProfileApiRequest? userProfileEntity;
  EditMedicalProfileApiRequest? medicalProfileEntity;

  UpdatePatientProfileState({
    this.needUpdateState = RequestState.initiate,
    this.needUpdateUserData = false,
    this.needUpdateMedicalData = false,
    this.userProfileEntity,
    this.medicalProfileEntity,
  });

  UpdatePatientProfileState copyWith({
    RequestState? needUpdateState,
    bool? needUpdateUserData,
    bool? needUpdateMedicalData,
    EditProfileApiRequest? userProfileEntity,
    EditMedicalProfileApiRequest? medicalProfileEntity,
  }) =>
      UpdatePatientProfileState(
        needUpdateState: needUpdateState??this.needUpdateState,
        needUpdateMedicalData:
            needUpdateMedicalData ?? this.needUpdateMedicalData,
        medicalProfileEntity: medicalProfileEntity ?? this.medicalProfileEntity,
        needUpdateUserData: needUpdateUserData ?? this.needUpdateUserData,
        userProfileEntity: userProfileEntity ?? this.userProfileEntity,
      );
}
