part of 'update_patient_profile_bloc.dart';

abstract class UpdatePatientProfileEvent {}

class UpdateUserProfile extends UpdatePatientProfileEvent {
  final EditProfileApiRequest userProfileEntity;

  UpdateUserProfile({required this.userProfileEntity});
}

class UpdateMedicalProfile extends UpdatePatientProfileEvent {
  final EditMedicalProfileApiRequest medicalProfileEntity;

  UpdateMedicalProfile({required this.medicalProfileEntity});
}

class CheckIfUserNeedUpdateProfile extends UpdatePatientProfileEvent {}
