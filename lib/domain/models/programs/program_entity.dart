class Program {
  String title;
  String icon;
  String route;
  int id;

  Program(
      {required this.id, required this.title, required this.icon,required this.route});
}
