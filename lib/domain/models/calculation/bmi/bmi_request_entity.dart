class BmiRequestEntity {
  final String height;
  final String weight;

  BmiRequestEntity({required this.height, required this.weight});
}
