class BmiResponseEntity {
  String bmi;
  bool idealWeight;
  String name;
  String description;

  BmiResponseEntity(
      {required this.bmi,
      required this.idealWeight,
      required this.name,
      required this.description});
}
