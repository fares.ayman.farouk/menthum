class PregnancyResponseEntity {
  int numWeek;
  String name;
  String description;
  String pregnancyDate;

  PregnancyResponseEntity({
    required this.numWeek,
    required this.name,
    required this.description,
    required this.pregnancyDate
  });
}
