class CaloriesRequestEntity {
  final String height;
  final String weight;
  final String age;

  CaloriesRequestEntity({
    required this.height,
    required this.weight,
    required this.age,
  });
}
