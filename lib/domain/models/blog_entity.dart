class BlogEntity {
  final int id;
  final String title;
  final String image;
  final String description;
  final String slug;
  final int createdAt;

  BlogEntity(
      {required this.id,
      required this.title,
      required this.image,
      required this.description,
      required this.slug,
      required this.createdAt});
}
