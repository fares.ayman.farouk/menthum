import 'diabetes_action.dart';

class DiabetesHistory {
  List<DiabetesAction> diabetesAction;
  final int lastPage;
  DiabetesHistory({
    required this.diabetesAction,
    required this.lastPage,
  });
}
