import 'diabetes_action.dart';
import 'next_visit.dart';

class DiabetesData {
  List<DiabetesAction> diabetesAction;
  NextVisit nextVisit;

  DiabetesData({
    required this.diabetesAction,
    required this.nextVisit,
  });
}
