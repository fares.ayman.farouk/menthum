import '../../../data/models/apiResponse/specialties_response.dart';

class NextVisit {
  int id;
  String name;
  String date;
  Speciality speciality;

  NextVisit({
    required this.id,
    required this.name,
    required this.date,
    required this.speciality,
  });
}
