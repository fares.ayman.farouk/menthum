class Reading {
  int id;
  String name;
  final String unit;
  final int min;
  final int max;

  Reading({
    required this.id,
    required this.name,
    required this.unit,
    required this.min,
    required this.max,
  });
}
