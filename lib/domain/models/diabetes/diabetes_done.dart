class DiabetesDone {
  int eventId;
  int? conditionId;
  String? note;
  String? unit;
  String? value;

  DiabetesDone({
    required this.eventId,
    this.conditionId,
    this.unit,
    this.value,
    this.note,
  });
}
