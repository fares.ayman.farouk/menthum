import 'package:patientapp/domain/models/diabetes/reading.dart';

class DiabetesAction {
  int id;
  String name;
  int done;
  String? date;
  Reading? reading;
  String? value;

  DiabetesAction({
    required this.id,
    required this.name,
    required this.done,
    required this.date,
    this.reading,
    this.value,
  });
}
