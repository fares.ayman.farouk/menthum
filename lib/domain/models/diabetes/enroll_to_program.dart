class EnrollToProgram {
  int enrolled;
  String description;

  EnrollToProgram({
    required this.enrolled,
    required this.description,
  });

  factory EnrollToProgram.fromJson(Map<String, dynamic> json) =>
      EnrollToProgram(
          enrolled: json['enrolled'], description: json['description']);
}
