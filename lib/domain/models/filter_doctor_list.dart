import 'package:flutter/material.dart';
import 'package:patientapp/data/models/apiResponse/location_filter/city_response.dart';
import 'package:patientapp/data/models/apiResponse/location_filter/governorate_response.dart';
import 'package:patientapp/data/models/apiResponse/specialties_response.dart';

class FilterDoctorList {
  bool? isFilter = false;
  bool? filterWithLocation = false;
  Speciality speciality;
  RangeValues rangeValues;
  List<City> cities;
  Governorate governorate;

  FilterDoctorList({
    this.isFilter,
    this.filterWithLocation,
    required this.speciality,
    required this.rangeValues,
    required this.cities,
    required this.governorate,
  });
}
