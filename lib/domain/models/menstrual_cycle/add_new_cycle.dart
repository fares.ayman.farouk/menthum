class AddNewCycle {
  double cycleLength;
  String startPeriodDate;

  AddNewCycle({required this.cycleLength, required this.startPeriodDate});
}
