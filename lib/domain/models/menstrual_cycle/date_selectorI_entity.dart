import 'dart:ui';

class DateSelectorEntity {
  DateTime date;
  Color bgColor;

  DateSelectorEntity({
    required this.date,
    required this.bgColor,
  });
}
