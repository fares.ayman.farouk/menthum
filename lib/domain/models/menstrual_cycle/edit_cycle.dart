class EditCycle {
  int id;
  String bleedingEndAt;
  String note;

  EditCycle({
    required this.id,
    required this.bleedingEndAt,
    required this.note,
  });
}
