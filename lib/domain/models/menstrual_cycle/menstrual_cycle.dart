class MenstrualCycle {
  int id;
  double periodLength;
  double cycleLength;
  String? bleedingNotes;
  DateTime startPeriodDate;
  DateTime endCycleDate;
  DateTime endPeriodDate;
  DateTime startPreOvulationDate;
  DateTime endPreOvulationDate;
  DateTime ovulationDate;
  DateTime startPostOvulationDate;
  DateTime endPostOvulationDate;
  DateTime currentDate;

  MenstrualCycle({
    required this.id,
    required this.periodLength,
    required this.endCycleDate,
    required this.cycleLength,
    required this.endPeriodDate,
    required this.currentDate,
    this.bleedingNotes,
    required this.startPeriodDate,
    required this.startPostOvulationDate,
    required this.endPostOvulationDate,
    required this.ovulationDate,
    required this.startPreOvulationDate,
    required this.endPreOvulationDate,
  });
}
