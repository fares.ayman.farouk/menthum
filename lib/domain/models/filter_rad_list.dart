import 'package:patientapp/data/models/apiResponse/location_filter/city_response.dart';
import 'package:patientapp/data/models/apiResponse/location_filter/governorate_response.dart';
import 'package:patientapp/data/models/apiResponse/tests_response.dart';

class FilterList {
  bool? isFilter = false;
  bool? filterWithLocation = false;
  List<Test>? selectedTests;
  List<City>? cities;
  Governorate? governorate;

  FilterList({
    this.isFilter,
    this.filterWithLocation,
    this.selectedTests,
    this.cities,
    this.governorate,
  });
}
