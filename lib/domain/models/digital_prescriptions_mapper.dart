class DigitalPrescriptionsMapper {
  final String id;
  final String name;
  final String filePath;
  List<DrugMapper>? digitalPrescriptions;
  int? createdAt;

  DigitalPrescriptionsMapper(
      {required this.id,
      required this.name,
      required this.filePath,
      required this.digitalPrescriptions,
      required this.createdAt});
}

class DrugMapper {
  final String logo;
  final String name;
  final String description;
  final String route;
  final String comment;
  final String numberOfTimes; //كام مره

  final String forIntervals; // لمدة اد ايه

  DrugMapper({
    required this.logo,
    required this.name,
    required this.description,
    required this.comment,
    required this.route,
    required this.numberOfTimes,
    required this.forIntervals,
  });
}

enum RouteMedicineEnum {
  CREAM,
  DROPS_ORAL,
  EAR_DROP,
  EYE_DROPS,
  NASAL_DROPS,
  INTRADERMAL,
  INTRAVENOUS,
  INTRAMUSCULAR,
  METERED_DOSE_INHALER,
  INTRANASAL,
  ORAL,
  RECTAL,
  SUBCUTANEAS,
  SUBLINGUAL,
  TOPICAL,
  VAGINEL,
  SPRAY,
  MOUTH_WASH
}

enum FrequencyEnum {
  day,
  week,
  month,
}

enum ForIntervalEnum {
  daily,
  weekly,
  monthly,
}

enum RequestState { loading, loaded, error, initiate }
