

import '../enums/blood_type.dart';

class MedicalProfileEntity {
  BloodTypeEnum bloodType;
  String weight;
  String height;

  MedicalProfileEntity(
      {required this.bloodType, required this.weight, required this.height});
}


