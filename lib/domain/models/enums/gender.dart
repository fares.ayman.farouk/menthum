
import 'package:easy_localization/easy_localization.dart';

import '../../../presentation/resources/strings_manger.dart';

enum GenderEnum { male, female }

/// extensions
extension GenderName on GenderEnum {
  String genderTypeName() {
    if (name == GenderEnum.male.name) {
      return AppStrings.male.tr();
    } else if (name == GenderEnum.female.name) {
      return AppStrings.female.tr();
    } else {
      return " ";
    }
  }


}
