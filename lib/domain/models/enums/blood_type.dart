

import 'package:patientapp/presentation/resources/strings_manger.dart';

enum BloodTypeEnum {
  A1,
  A2,
  B1,
  B2,
  O1,
  O2,
  AB1,
  AB2,
}


/// extensions
extension BloodeTypeName on BloodTypeEnum {
  String bloodTypeName() {
    if (name == BloodTypeEnum.A1.name) {
      return AppStrings.a1;
    }
    else if (name == BloodTypeEnum.A2.name) {
      return AppStrings.a2;
    }
    else if (name == BloodTypeEnum.B1.name) {
      return AppStrings.b1;
    }
    else if (name == BloodTypeEnum.B2.name) {
      return AppStrings.b2;
    }
    else if (name == BloodTypeEnum.O1.name) {
      return AppStrings.o1;
    }
    else if (name == BloodTypeEnum.O2.name) {
      return AppStrings.o2;
    }

    else if (name == BloodTypeEnum.AB1.name) {
      return AppStrings.ab1;
    } else if (name == BloodTypeEnum.AB2.name) {
      return AppStrings.ab2;
    }


    else {
      return " ";
    }
  }
}

