class BlogDetailsEntity {
  final String title;
  final String image;
  final String body;
  final int createdAt;

  BlogDetailsEntity(
      {
        required this.title,
        required this.image,
        required this.body,
        required this.createdAt});
}
