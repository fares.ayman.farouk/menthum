class ReadingRequestEntity{

  String typeId;
  String from;
  String to;

  ReadingRequestEntity({required this.typeId,required this.from,required this.to});
}