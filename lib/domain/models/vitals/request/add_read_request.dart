class AddReadRequestEntity {
  final int id;
  final String value;
  final int readingAt;
  String? readingConditionId;

  AddReadRequestEntity({
    required this.id,
    required this.value,
    required this.readingAt,
    this.readingConditionId,
  });
}
