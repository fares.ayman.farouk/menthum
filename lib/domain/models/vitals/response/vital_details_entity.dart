class VitalDetailsEntity {
  final String name;
  final String value;
  final String humanMinimum;
  final String humanMaximum;
  final String unit;
  final int readingAt;
  final String createdBy;
  final bool hasCall;
  final String? coupon;

  VitalDetailsEntity({
    required this.name,
    required this.value,
    required this.humanMinimum,
    required this.humanMaximum,
    required this.unit,
    required this.readingAt,
    required this.createdBy,
    required this.hasCall,
    this.coupon,
  });
}
