import 'package:patientapp/domain/models/vitals/response/reading_condition_entity.dart';

class VitalEntity {
  final int id;
  final String title;
  String value;
  final String unit;
  final bool canCall;
  final int readingAt;
  final String min;
  final String max;
  final String? coupon;
  List<ReadingConditionEntity> readingConditionEntity;

  VitalEntity({
    required this.id,
    required this.title,
    required this.value,
    required this.canCall,
    required this.unit,
    required this.readingAt,
    required this.min,
    required this.max,
    required this.coupon,
    required this.readingConditionEntity,
  });
}
