class ReadingConditionEntity {
  int id;
  String name;

  ReadingConditionEntity({
    required this.id,
    required this.name,
  });
}
