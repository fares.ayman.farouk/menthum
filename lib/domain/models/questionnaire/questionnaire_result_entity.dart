class QuestionnaireResultEntity {
  int value;
  String message;

  QuestionnaireResultEntity({
    required this.value,
    required this.message,
  });
}
