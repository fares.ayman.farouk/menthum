class QuestionnaireQuestionEntity {
  String id;
  String question;
  List<QuestionnaireOptionEntity> options;
  int type;
  dynamic answer;

  QuestionnaireQuestionEntity({
    required this.id,
    required this.question,
    required this.options,
    required this.type,
    this.answer,
  });
}

class QuestionnaireOptionEntity {
  QuestionnaireOptionEntity({
    required this.id,
    required this.text,
  });

  int id;
  String text;
}
