import 'package:patientapp/domain/models/questionnaire/questionnaire_entity.dart';

class AnswerQuestionnaireEntity {
  List<QuestionnaireQuestionEntity> questions;

  AnswerQuestionnaireEntity({required this.questions});
}
