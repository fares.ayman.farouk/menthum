class QuestionnaireItemEntity {
  final String id;
  final String slug;
  final String name;
  final String description;

  QuestionnaireItemEntity({
    required this.id,
    required this.slug,
    required this.name,
    required this.description,
  });
}
