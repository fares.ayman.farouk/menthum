class PediatricDataEntity {
  String enrollAt;
  String date;
  String actionableType;
  int done;
  int patientMedicalEventId;
  dynamic pediatricCondition;

  PediatricDataEntity({
    required this.enrollAt,
    required this.date,
    required this.actionableType,
    required this.patientMedicalEventId,
    required this.done,
    required this.pediatricCondition,
  });
}

class PediatricCondition {
  int id;

  PediatricCondition({required this.id});
}

class VaccinCondition extends PediatricCondition {
  String name;
  String description;
  int type;

  VaccinCondition(
      {required super.id,
      required this.name,
      required this.description,
      required this.type});
}

class HeadCircumFerence extends PediatricCondition {
  String minCircum;
  String avgCircum;
  String maxCircum;

  HeadCircumFerence(
      {required super.id,
      required this.minCircum,
      required this.avgCircum,
      required this.maxCircum});
}

class WeightConditions extends PediatricCondition {
  String severelyUnderweight;
  String underweightFrom;
  String underweightTo;
  String normalFrom;
  String normalTo;
  String overweight;

  WeightConditions({
    required super.id,
    required this.normalFrom,
    required this.normalTo,
    required this.overweight,
    required this.severelyUnderweight,
    required this.underweightFrom,
    required this.underweightTo,
  });
}
