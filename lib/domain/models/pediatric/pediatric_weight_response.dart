class PediatricWeightResponseEntity {
  final String message;
  final PediatricWeightModelResponse pediatricWeightModelResponse;

  const PediatricWeightResponseEntity(
      {required this.message, required this.pediatricWeightModelResponse});
}

class PediatricWeightModelResponse {
  int id;
  String severely_underweight;
  String underweightFrom;
  String underweightTo;
  String normalFrom;
  String normalTo;
  String overweight;
  int gender;
  int program_action_id;
  String created_at;
  String updated_at;

  PediatricWeightModelResponse(
      {required this.id,
      required this.severely_underweight,
      required this.underweightFrom,
      required this.underweightTo,
      required this.normalFrom,
      required this.normalTo,
      required this.overweight,
      required this.gender,
      required this.program_action_id,
      required this.created_at,
      required this.updated_at});
}
