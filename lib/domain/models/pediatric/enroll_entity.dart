class EnrollRequestEntity {
  String patientId;
  String birthDate;
  int gender;

  EnrollRequestEntity({
    required this.patientId,
    required this.birthDate,
    required this.gender,
  });
}
