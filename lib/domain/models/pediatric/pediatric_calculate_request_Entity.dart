class PediatricCalculateRequestEntity {
  String id;
  int type;
  String value;

  PediatricCalculateRequestEntity({
    required this.id,
    required this.type,
    required this.value,
  });
}
