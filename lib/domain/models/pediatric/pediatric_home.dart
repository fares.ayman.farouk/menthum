class PediatricHomeEntity {
  String name;
  int type;

  PediatricHomeEntity({
    required this.name,
    required this.type,
  });
}
