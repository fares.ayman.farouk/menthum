class PediatricHeadResponseEntity {
  String message;
  PediatricHeadModelEntity pediatricHeadModelResponse;

  PediatricHeadResponseEntity(
      {required this.message, required this.pediatricHeadModelResponse});
}

class PediatricHeadModelEntity {
  int id;
  String minCircum;
  String avgCircum;
  String maxCircum;
  int gender;
  int program_action_id;
  String created_at;
  String updated_at;

  PediatricHeadModelEntity(
      {required this.id,
      required this.minCircum,
      required this.avgCircum,
      required this.maxCircum,
      required this.gender,
      required this.program_action_id,
      required this.created_at,
      required this.updated_at});
}
