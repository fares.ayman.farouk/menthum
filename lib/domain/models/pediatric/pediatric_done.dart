class PediatricDoneEntity {

  String patientId;
  int patientMedicalEventId;
  String? note;
  String? value;

  PediatricDoneEntity(
      {required this.patientId, required this.patientMedicalEventId, this.note, this.value});
}