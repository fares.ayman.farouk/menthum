import 'package:patientapp/domain/models/pediatric/pediatric_data_entity.dart';

class PediatricHistoryEntity {
  List<PediatricDataEntity> pediatricData;
  int latPage;

  PediatricHistoryEntity({
    required this.pediatricData,
    required this.latPage,
  });
}
