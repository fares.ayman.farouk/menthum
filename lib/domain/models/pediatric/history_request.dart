class PediatricHistoryRequest {
  String patientId;
  int? page;

  PediatricHistoryRequest({required this.patientId, required this.page});
}
